'use strict'
const Mongoose = require('mongoose').Mongoose
Mongoose.Promise = require('bluebird')
const config = require('config')
var processExit = false

const options = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 25, // Maintain up to 25 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  promiseLibrary: require('bluebird'),
  useNewUrlParser: true
}

const mongoW2fDomain = new Mongoose()

const db = mongoW2fDomain.connection

db.once('error', function (err) {
  console.error('mongoose connection error' + err)
  mongoW2fDomain.disconnect()
})
db.on('open', function () {
  console.log('successfully connected to W2fDomain DB')
})
db.on('reconnected', function () {
  console.log('MongoDB reconnected!')
})
db.on('disconnected', function () {
  console.log('MongoDB disconnected!')
  if (!processExit) {
    mongoW2fDomain.connect(config.mongodb.W2fDomainUri, options)
      .then(() => console.log('connection successful to W2fDomain DB'))
      .catch((err) => console.error(err))
  }
})

// mongoose.connect(config.mongodb.uri, options);

mongoW2fDomain.connect(config.mongodb.W2fDomainUri, options)
  .then(() => console.log('connection successful to W2fDomain DB'))
  .catch((err) => console.error(err))

exports = module.exports = mongoW2fDomain // expose app
