import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { PagerService } from "../../shared/pagination/pager.service";
@Component({
  selector: "app-view-users",
  templateUrl: "./view-users.component.html",
  styleUrls: ["./view-users.component.css"]
})
export class ViewUsersComponent implements OnInit {
  Name: any;
  EmailId: "";
  CreditDue: "";
  CreditLimit: "";
  editItem: { id: any; name: any; email: any; credit_due: any; credit_limit: any; user_status: any; con_fee: any; sdk_con_fee: any; web_con_fee: any };
  Id = ""
  user_status: ""
  app_con_fee: any
  sdk_con_fee: any
  web_con_fee: any
  users: any;
  status: any;
  showmodal = false
  superAdmin: boolean = false;
  PnIdbutton: boolean = false;
  pnId: any;
  count_0: any;
  count_500: any;
  count_1000: any;
  count_1500: any;
  Totalsmscounts: any;
  count_3000: any;
  count_5000: any;

  Totalsmscountsobj: any = {}
  Detailsobj: any = {}
  // Totalsmscountsobj:  {
  //   monthly_processed_count: any;
  //   transaction_un_useful_sms_counts: any;
  //   transaction_unidentified_sms_counts: any;
  //   unidentified_sms_counts: any;
  //   savings_account_sms_counts: any;
  //   loan_account_sms_counts: any;
  //   credit_card_sms_counts: any;
  //   policy_sms_counts: any;
  //   fixed_deposit_sms_counts: any;
  //   wallet_sms_counts: any;
  //   transaction_sms_counts: any;
  //   total_smsprocessed_count: any;
  // };
  totalsmssync_count: any;
  PredictreResult: any;
  constructor(
    private apiService: RestServicesService,
    private pagerservice: PagerService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    // router.events.subscribe(val => {
    //   if (val instanceof NavigationEnd) {
    //     //                  .log('cpagee : ', this.route.snapshot.params.cpage);
    //     this.currentPage = this.route.snapshot.params.cpage;
    //     if (this.currentPage < 1 || this.currentPage == undefined) {
    //       this.currentPage = 1;
    //     }
    //     if (this.searchStatus) {
    //       this.setSearchPage(this.currentPage);
    //     } else {
    //       this.getCount();
    //     }
    //   }
    // });
  }
  amount_from: any;
  amount_to: any;
  authenticateStatus: any = false;
  authenticateInfo: any;
  countStatus: any = false;
  userDetails: any = [];
  paginationStatus = false;
  searchString: String = "";
  searchMid: String = "";
  searchVariable = "mobile_number";
  searchmid = "mid"
  fromDate: any = "";
  toDate: any = "";
  searchStatus: boolean = false;
  pager: any;
  userLength: any;
  findStatus: any;
  pagedItems: any;
  currentPage: number;
  cpage: any;
  // responseStatus: any = false;
  resetButtonShowStatus: any = false;
  buttonActive: any = false;
  Version: any;
  OS: any = "select"
  android: boolean = false;
  ios: boolean = false;
  showsmscountsModal: boolean = false;
  OSOptions = [
    { name: 'android', value: 'android' },
    { name: 'ios', value: 'ios' }
  ]
  VersionOptionsAndroid = [
    { name: '3.3', value: '3.3' },
    { name: '3.1', value: '3.1' },
    { name: '3.0', value: '3.0' },
    { name: '2.7', value: '2.7' },
    { name: '2.3', value: '2.3' },
    { name: '2.1', value: '2.1' },
    { name: '1.5', value: '1.5' },
    { name: '1.0', value: '1.0' },
  ]

  VersionOptionsIos = [
    { name: '1.0', value: '1.0' },
    { name: '1.1', value: '1.1' },
  ]
  predictedAmountModel: boolean = false
  usersLength: number = 0
  currentPageValue: number = 1
  searchWithMobile: String = ''
  searchWithMid: String = ''
  searchWithFromAmount: String = ''
  searchWithToAmount: String = ''
  searchWithAppVersion: String = ''
  searchFilterObj: Object = {}

  ngOnInit() {
    this.searchWithAppVersion = "ALL"
    this.getCountOfUsers({})
    this.apiService.getAuth("user/authcheck").subscribe(result => {
      if (result.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else if (result.data.user_type == 'super_admin') {
        this.superAdmin = true
        // console.log("response", result)
      }
    })
    this.UsersCount()
    // this.getCount();
  }

  pageChange(filter) {
    // alert('current page :: ' + this.currentPageValue)
    var skip = (this.currentPageValue - 1) * 10
    var limit = 10
    var filterObj = filter
    filterObj['skip'] = skip
    filterObj['limit'] = limit
    this.apiService.postAuth('api/fetchUsers', filterObj).subscribe((response) => {
      if (response.success && response.users.length > 0) {
        this.userDetails = response.users
        this.findStatus = true
        this.router.navigate(['user/view-user/' + this.currentPageValue]);
        // console.log('User details :: ', this.userDetails)
      } else if (response.success && response.users.length === 0 ) {
        // console.log('')
        alert('No records Found')
        this.userDetails = response.users
        this.findStatus = false
        this.router.navigate(['user/view-user/' + this.currentPageValue]);
      } else {
        // console.log('Error while fetching user details')
        this.findStatus = false
        // alert(response.message)
        // this.toastr.error("ERROR FETCH USERS", null, { showCloseButton: true });
      }
    })
  }

  getCountOfUsers(filter) {
    var filterObj = filter
    this.apiService.postAuth('api/getCountOfUsers', filterObj).subscribe((response) => {
      // console.log('Get count of users response :: ', response)
      if (response.success) {
        this.currentPageValue = 1
        this.usersLength = response.users
        this.pageChange(filterObj)
      } else {
        // console.log('something went wrong while finding the users count')
        this.usersLength = response.users
        this.currentPageValue = 1
        alert(response.message)
        this.pageChange(filterObj)
      }
    })
  }
  getCount() {
    // console.log('cpagee : ', this.route.snapshot.params.cpage);
    // this.currentPage = this.route.snapshot.params.cpage;
    if (!this.countStatus) {
      this.apiService.getAuth("user/getCount").subscribe(result => {
        if (result.status) {
          this.userLength = parseInt(result.data);
          this.countStatus = true;
          this.setUserPage(this.currentPage);
        } else {
          this.userLength = 0;
        }
      });
    } else {
      this.setUserPage(this.currentPage);
    }
  }
  getUsers(skip, limit) {
    this.apiService
      .postAuth("user/getUsers", { skip: skip, limit: limit })
      .subscribe(result => {
        // console.log('result of aggregrate :: ', result)
        if (result.status) {
          // console.log(result.data)
          this.pagedItems = result.data;
          /* adjusting paged Items */
          this.pagedItems = this.checkRePredictedDisplay(this.pagedItems)
        } else {
          this.paginationStatus = false;
          this.findStatus = false;
        }
        this.resetButtonShowStatus = false;
      });
  }
  search() {
    // var searchFilterObj = {}
    if (this.searchWithMobile) {
      this.searchFilterObj['mobile_number'] = this.searchWithMobile
    }

    if (this.searchWithMid) {
      this.searchFilterObj['mid'] = this.searchWithMid
    }

    if (this.searchWithFromAmount || Number(this.searchWithFromAmount) == 0) {
      this.searchFilterObj['credit_limit_from'] = this.searchWithFromAmount
    }

    if (this.searchWithToAmount || Number(this.searchWithToAmount) == 0) {
      this.searchFilterObj['credit_limit_to'] = this.searchWithToAmount
    }

    if(this.searchWithAppVersion) {
      this.searchFilterObj['app_version'] = this.searchWithAppVersion
    }

    var from_date, to_date;
    
    if (this.fromDate && this.fromDate != "") {
      from_date = new Date(this.fromDate).setHours(0, 0, 0, 0);
      this.searchFilterObj["type"] = "due_date";
      this.searchFilterObj["fromDate"] = from_date;
    }
    if (this.toDate && this.toDate != "") {
      to_date = new Date(this.toDate);
      to_date.setDate(to_date.getDate() + 1);
      to_date.setHours(0, 0, 0, 0);
      this.searchFilterObj["toDate"] = to_date.getTime();
    }

    this.getCountOfUsers(this.searchFilterObj)

  }
  // search() {
  //   // console.log(this.fromDate, this.toDate, this.amount_from, this.amount_to)
  //   this.paginationStatus = false;
  //   this.searchStatus = false;
  //   // this.responseStatus = true;
  //   var data = {};
  //   //data[this.searchVariable] = this.searchString;
  //   var from_date, to_date;
  //   // console.log("***",this.fromDate, this.toDate, this.amount_from, this.amount_to)
  //   // if ((this.amount_from  && this.amount_from != null) && (this.amount_to && this.amount_to != null)&& (this.amount_from || this.amount_from == 0) && (this.amount_to || this.amount_to == 0)) {
  //   //   console.log('Hello', this.amount_from, this.amount_to)
  //   //   data['amount_from'] = this.amount_from
  //   //   data['amount_to'] = this.amount_to

  //   // }
  //   if (this.amount_from != "" && this.amount_to != "" && (this.amount_from || this.amount_from == 0) && (this.amount_to || this.amount_to == 0)) {
  //     // console.log('Hello :":":"', this.amount_from, this.amount_to)
  //     data['amount_from'] = this.amount_from
  //     data['amount_to'] = this.amount_to

  //   } else {
  //     // alert("Please Enter Correct Values")
  //     // console.log('NOT ')
  //     // data['amount_from'] = this.amount_from
  //     // data['amount_to'] = this.amount_to
  //   }
  //   if (this.fromDate && this.fromDate != "") {
  //     from_date = new Date(this.fromDate).setHours(0, 0, 0, 0);
  //     data["type"] = "due_date";
  //     data["fromDate"] = from_date;
  //   }
  //   if (this.toDate && this.toDate != "") {
  //     to_date = new Date(this.toDate);
  //     to_date.setDate(to_date.getDate() + 1);
  //     to_date.setHours(0, 0, 0, 0);
  //     data["toDate"] = to_date.getTime();
  //   }
  //   // console.log("this.searchString:::",this.searchString)
  //   if (this.searchVariable == "mobile_number") {
  //     if (this.searchString != "")
  //       data["mobile_number"] = this.searchString;
  //   }
  //   if (this.searchmid == "mid") {
  //     if (this.searchMid != "")
  //       data["mid"] = this.searchMid;
  //   }
  //   // if(this.Os != "select"){
  //   //   data["os"] = this.Os
  //   // }else{
  //   //   data["os"] = ""
  //   // }
  //   if (this.Version != "ALL") {
  //     data["app_version"] = this.Version
  //   } else {
  //     data["app_version"] = ""
  //   }
  //   // console.log('USER LENGTH', this.userLength)
  //   this.userLength = 0
  //   console.log('Prepared Data Before send', data)
  //   this.apiService.postAuth("user/search", data).subscribe(result => {
  //     if (result.status) {
  //       this.userDetails = result.data;
  //       this.userLength = this.userDetails.length;
  //       // console.log("user_length",this.userLength)
  //       this.countStatus = false;
  //       this.searchStatus = true;
  //       this.setPage(1);
  //       this.setSearchPage(1);
  //       // this.responseStatus = false;
  //     } else {
  //       this.userLength = 0
  //       this.searchStatus = false;
  //       this.findStatus = false;
  //       this.resetButtonShowStatus = false
  //       // this.responseStatus = false;
  //     }
  //   });
    
  // }
  convertInt(page) {
    return parseInt(page);
  }
  reset() {
    console.log("calling reset")
    this.amount_from = ''
    this.amount_to = ''
    this.resetButtonShowStatus = true;
    // this.getCount();
    this.searchString = "";
    this.searchStatus = false;
    this.fromDate = "";
    this.toDate = "";
    this.Version = "ALL"
    this.searchMid = ""
    this.OS = "select"
    this.android = false
    this.ios = false
    this.usersLength = 0
    this.currentPageValue = 1
    this.searchWithMobile = ''
    this.searchWithMid = ''
    this.searchWithFromAmount = ''
    this.searchWithToAmount = ''
    this.searchWithAppVersion = ''
    this.searchFilterObj = {}
    this.getCountOfUsers({})
    // this.search()
  }
  setPage(page) {
    // console.log("set page :::",page)
    if (page < 1) {
      page = 1;
      this.currentPage = 1;
    } else if (page > this.pager.totalPages) {
      page = this.pager.totalPages;
    }
    this.cpage = "user/view-user/" + page;
    this.router.navigate([this.cpage]);
  }
  setUserPage(page) {
    // console.log('page::',page)
    if (this.userLength) {
      // console.log("Users Length :: ", this.userLength)
      if (this.userLength > 10) {
        this.findStatus = true;
        this.paginationStatus = true;
        this.searchStatus = false;
        this.pager = this.pagerservice.getPager(
          Number(this.userLength),
          Number(page),
          10
        );
        // console.log('user pager:: ',this.pager)

        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getUsers(
          this.pager.startIndex,
          this.pager.endIndex + 1 - this.pager.startIndex
        );
      } else {
        this.currentPage = 1;
        this.getUsers(0, this.userLength);
        this.findStatus = true;
        this.paginationStatus = false;
      }
    } else {
      this.findStatus = false;
    }
  }

  setSearchPage(page) {
    // console.log('search page::',page)
    if (this.userLength) {
      // console.log("user length in search page::",this.userLength)
      if (this.userLength > 10) {
        this.findStatus = true;
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(this.userLength, page, 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.pagedItems = this.userDetails.slice(
          this.pager.startIndex,
          this.pager.endIndex + 1
        );
        // console.log('search pager:: ',this.pager)
      } else {
        this.pagedItems = this.userDetails;
        this.findStatus = true;
        this.currentPage = 1;
        this.paginationStatus = false;
      }
    } else {
      this.findStatus = false;
    }
  }

  getUserInformation(mobile_number) {
    var obj = {};
    obj["mobile_number"] = mobile_number;
    this.apiService.postAuth("user/authentication_info", obj).subscribe(result => {
      if (result.success && result.authenticateInfo != null) {
        this.authenticateStatus = true;
        this.authenticateInfo = result.authenticateInfo;
        // console.log(this.authenticateInfo)
      } else {
        this.authenticateStatus = false;
        this.authenticateInfo = null;
      }
    });
  }

  getColor(loginStatus) {
    alert(loginStatus);
  }

  recalculate(user) {
    console.log("hi")
    if (user == 2) {
    } else {
      if (user == 1) {
        var obj = {};
        // console.log("user--------", obj)
      } else {
        var obj = {};
        obj["_id"] = user._id
      }
      if (user.is_predict_value || user.predicted_date || user == 1) {
        this.buttonActive = true
        this.apiService.postAuth("user/deleteValue", obj).subscribe(result => {
          if (result.message == "you can recalculate now!") {
            alert("you can recalculate now!")
            this.buttonActive = false
            this.reset()
          } else {
            alert(result.message)
          }
        })
      } else {
        this.buttonActive = false
      }

    }
  }

  /* Ramu */
  rePredictCheck(user) {
    var obj = {};
    console.log("user data ", user)
    obj["_id"] = user._id
    obj["mobile"] = user.mobile_number
    obj["mid"] = user.mid
    obj["last_re_predicted_check_date"] = user.last_re_predicted_check_date ? user.last_re_predicted_check_date : -1
    let newObj = {
      "data": obj
    }

    if (!user.is_predict_value && !user.predicted_date) {

      this.apiService.postAuth("api/data/repredict-credit-limit", newObj).subscribe(result => {
        if (result.status == false) {
          alert(result.message)
          user.last_repredicted_check = false
        } else {
          user.last_repredicted_check = false
          alert(result.message)
        }
      })
    } else {
      user.last_repredicted_check = false
    }
  }

  edit(user) {
    this.showmodal = true
    console.log("------------->", user.user_status)
    this.Id = user._id
    this.Name = user.name
    this.EmailId = user.email
    this.CreditDue = user.credit_due
    this.CreditLimit = user.credit_limit
    this.status = user.user_status
    this.app_con_fee = user.con_fee
    if (user.sdk_con_fee != undefined) {
      this.sdk_con_fee = user.sdk_con_fee
    } else {
      this.sdk_con_fee = 0
    }
    if (user.web_con_fee != undefined) {
      this.web_con_fee = user.web_con_fee
    } else {
      this.web_con_fee = 0
    }
  }

  Update() {
    if (this.EmailId != "" && this.EmailId != null && this.EmailId != undefined) {
      this.editItem = {
        id: this.Id,
        name: this.Name,
        email: this.EmailId,
        credit_due: this.CreditDue,
        credit_limit: this.CreditLimit,
        user_status: this.status,
        con_fee: this.app_con_fee,
        sdk_con_fee: this.sdk_con_fee,
        web_con_fee: this.web_con_fee
      }
      // console.log("item::::", this.editItem)
      this.apiService.postAuth('user/edit-user-details', this.editItem).subscribe(result => {
        if (result.status == true) {
          this.EmailId = "";
          this.CreditDue = "";
          this.CreditLimit = "";
          this.showmodal = false
          this.reset()
          // console.log("updated successfully")
          alert("updated successfully");
        } else {
          // console.log("error in updation")
          alert(result.message);
        }
      });

    } else {
      alert("Enter brand")
    }

  }

  PnIdpopup(id) {
    if (id) {
      this.pnId = id
    }
    this.PnIdbutton = true
  }

  getEvents(userId) {

    this.router.navigate(['user/view-events'], { queryParams: { user_id: userId } });
  }
  UsersCount() {
    this.apiService.getAuth('user/usersCount').subscribe(usersresult => {
      if (usersresult) {
        this.count_0 = usersresult.final_users_result[0].users_count_0
        this.count_500 = usersresult.final_users_result[1].users_count_500
        this.count_1000 = usersresult.final_users_result[2].users_count_1000
        this.count_1500 = usersresult.final_users_result[3].users_count_1500
        this.count_3000 = usersresult.final_users_result[4].users_count_3000
        this.count_5000 = usersresult.final_users_result[5].users_count_5000
        console.log("usersresult", usersresult)
      } else {
        // console.log("error")
      }

    })
  }

  SmsCounts(user) {
    if (user && user.mid) {
      let obj = {}
      obj["mid"] = user.mid
      // console.log("userid", user.mid)
      this.apiService.postAuth('user/smscounts', obj).subscribe(UserSmsResult => {
        // console.log("usersmsresult---->",UserSmsResult)
        this.showsmscountsModal = true
        if (UserSmsResult && UserSmsResult.final_sms_counts.length > 0 && UserSmsResult.final_sms_counts[0].final_smsprocessed_counts) {
          this.Totalsmscounts = UserSmsResult.final_sms_counts[0].final_smsprocessed_counts
          // console.log("totalsmscoounts---->",this.Totalsmscounts)
          this.Totalsmscountsobj['total_smsprocessed_count'] = this.Totalsmscounts.total_count
          this.Totalsmscountsobj['unidentified_sms_counts'] = this.Totalsmscounts.unidentified_sms_counts
          this.Totalsmscountsobj['transaction_unidentified_sms_counts'] = this.Totalsmscounts.transaction_unidentified_sms_counts
          this.Totalsmscountsobj['transaction_un_useful_sms_counts'] = this.Totalsmscounts.transaction_un_useful_sms_counts
          this.Totalsmscountsobj['monthly_processed_count'] = this.Totalsmscounts.monthly_processed_count
          this.Totalsmscountsobj['savings_account_sms_counts'] = this.Totalsmscounts.savings_account_sms_counts
          this.Totalsmscountsobj['loan_account_sms_counts'] = this.Totalsmscounts.loan_account_sms_counts
          this.Totalsmscountsobj['credit_card_sms_counts'] = this.Totalsmscounts.credit_card_sms_counts
          this.Totalsmscountsobj['policy_sms_counts'] = this.Totalsmscounts.policy_sms_counts
          this.Totalsmscountsobj['fixed_deposit_sms_counts'] = this.Totalsmscounts.fixed_deposit_sms_counts
          this.Totalsmscountsobj['wallet_sms_counts'] = this.Totalsmscounts.wallet_sms_counts
          this.Totalsmscountsobj['transaction_sms_counts'] = this.Totalsmscounts.transaction_sms_counts
          // this.fin_profile_count = this.Totalsmscounts.fin_profile_count
          // this.device_registration_sms_count = this.Totalsmscounts.device_registration_sms_count
        }
        if (UserSmsResult.final_sms_counts.length > 0 && UserSmsResult.final_sms_counts[1].final_smssync_counts) {
          console.log("total sms sync count----->", UserSmsResult.final_sms_counts[1].final_smssync_counts)
          this.Totalsmscountsobj['totalsmssync_count'] = UserSmsResult.final_sms_counts[1].final_smssync_counts.total_count
        }
        // console.log("finallllll---->",this.Totalsmscountsobj)
      })
    }

  }
  smsCountsModelClose() {
    // // this.Totalsmscounts = {}
    // console.log("close...................................", this.Totalsmscounts)
    this.showsmscountsModal = false
    this.Totalsmscountsobj = {}
  }

  versionType(OS) {
    console.log("os", OS)
    if (OS === 'android') {
      this.android = true
      this.ios = false
    }
    if (OS === 'ios') {
      this.ios = true
      this.android = false
    }
  }

  displyDetails(user_id) {
    console.log("user id-->", user_id)
    this.predictedAmountModel = true
    if (user_id) {
      let obj = {}
      obj["user_id"] = user_id
      this.apiService.postAuth('user/PredictedAmountDetails', obj).subscribe(UserPredictedAmountResult => {
        console.log("UserPredictedAmountResult", UserPredictedAmountResult)
        if (UserPredictedAmountResult && UserPredictedAmountResult.PredictedAmount_details) {
          this.PredictreResult = UserPredictedAmountResult.PredictedAmount_details
          // this.Detailsobj["mid"]= this.PredictreResult.mid
          // this.Detailsobj["old_amount"]= this.PredictreResult.old_amount
          // this.Detailsobj["new_amount"]= this.PredictreResult.new_amount
          // this.Detailsobj["is_amount_change"]= this.PredictreResult.is_amount_change
          // this.Detailsobj["updated_date"]= this.PredictreResult.updated_date
          // this.Detailsobj["status"]= this.PredictreResult.status
          // this.Detailsobj["spendings"]= this.PredictreResult.spendings
          // this.Detailsobj["earnings"]= this.PredictreResult.earnings
          // this.Detailsobj["main_cluster"]= this.PredictreResult.main_cluster
          // this.Detailsobj["sub_cluster"]= this.PredictreResult.sub_cluster
          // this.Detailsobj["salary_account"]= this.PredictreResult.salary_account
        }
        console.log("final result", this.PredictreResult)

      })
    } else {
      console.log("user_id is not available")
    }
  }

  PredictedAmountClose() {
    this.predictedAmountModel = false
    // this.Detailsobj = {}
    this.PredictreResult = null
  }

  /* Ramu */
  /* Checking is re predicted display visible or not */
  checkRePredictedDisplay(data) {
    data.forEach(function (element, i) {
      if (element && !element.is_predict_value && !element.predicted_date) {
        if (element.last_re_predicted_check_date) {
          if ((element.last_re_predicted_check_date + (7 * 24 * 60 * 60 * 1000)) > Date.now()) {
            data[i]['last_repredicted_check'] = false
          } else {
            data[i]['last_repredicted_check'] = true
          }
        } else {
          data[i]['last_repredicted_check'] = true
        }
      } else {
        data[i]['last_repredicted_check'] = true
      }
    });
    return data
  }

}
