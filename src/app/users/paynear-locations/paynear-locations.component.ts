import { Component, OnInit } from '@angular/core';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { PagerService } from '../../shared/pagination/pager.service';

@Component({
  selector: 'app-paynear-locations',
  templateUrl: './paynear-locations.component.html',
  styleUrls: ['./paynear-locations.component.css']
})
export class PaynearLocationsComponent implements OnInit {

  constructor(
    private dataService: RestServicesService,
    private pagerService: PagerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        // this.setSearchPage(this.currentPage);
      }
    });
  }
  paramsData: any = {};
  collectionSize: Number = 5;
  pageSize: number = 10;
  skip: number;
  limit: number;
  maxSize: Number = 5;
  page: number = 1;
  pageStartIndex: number;
  from_date: any;
  to_date: any;
  cpage: any;
  searchStatus: any = false;
  pageLength: any;
  currentPage: number = 1;
  errMsg = false;
  showData = false;
  pagedItems: any[];
  pager: any = {};
  private allItems: any[];
  paginationStatus: any = false

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.page = params.page ? params.page : 1;
      this.searchPaynearLocations(this.page)
      this.pager.startIndex = 0;
      this.pager.endIndex = 9;
    })
  }
  onClickReset() {
    this.from_date = null;
    this.to_date = null;
    this.page = 1;
    this.searchStatus = false
    this.paginationStatus = false
    this.ngOnInit()
  }

  searchPaynearLocations(page) {
    this.page = page;
    var data = {};
    data['skip'] = (this.page - 1) * this.pageSize ? (this.page - 1) * this.pageSize : 0;
    data['limit'] = this.pageSize;
    if (this.from_date) {
      var fromDate = new Date(this.from_date).setHours(0, 0, 0, 0);
      data["from_date"] = fromDate;
    }
    if (this.to_date) {
      var toDate = new Date(this.to_date);
      toDate.setDate(toDate.getDate() + 1);
      toDate.setHours(0, 0, 0, 0);
      data["to_date"] = toDate.getTime();
    }
    this.searchStatus = true;
    console.log('===>>', data)
    this.dataService.postAuth("user/paynear-locations", data).subscribe(result => {
      if (result.status) {
        console.log(result)
        this.pagedItems = result.data.total_records;
        this.collectionSize = result.data.Countresult[0].count
        // this.pageLength = this.allItems.length;
        // if (this.pageLength > 10) {
        //   this.paginationStatus = true
        // } else {
        //   this.paginationStatus = false
        // }
        // this.setPage(1)
        // this.setSearchPage(1);
        // this.pagedItems = this.allItems.slice(
        //   this.pager.startIndex,
        //   this.pager.endIndex + 1
        // );
        this.errMsg = false;
      } else {
        this.errMsg = true;
      }
    });
    this.pageStartIndex = ((this.page - 1) * (this.pageSize)) ? ((this.page - 1) * (this.pageSize)) : 0;
  }

  // setPage(page) {
  //   this.currentPage = page;
  //   this.cpage = this.router.routerState.snapshot.url;
  //   this.cpage = "user/paynear-locations/" + page;
  //   this.router.navigate([this.cpage]);
  // }

  // convertInt(page) {
  //   return parseInt(page);
  // }
  // setSearchPage(page) {
  //   if (this.pageLength) {
  //     if (this.pageLength > 10) {
  //       this.showData = true;
  //       this.pager = this.pagerService.getPager(this.pageLength, page, 10);
  //       if (this.currentPage > this.pager.totalPages) {
  //         this.currentPage = this.pager.totalPages;
  //       }
  //       this.pagedItems = this.allItems.slice(
  //         this.pager.startIndex,
  //         this.pager.endIndex + 1
  //       );
  //     } else {
  //       this.currentPage = 1;
  //       this.pagedItems = this.allItems;
  //       this.showData = true;
  //     }
  //   } else {
  //     this.errMsg = true;
  //     this.showData = false
  //   }
  // }

}
