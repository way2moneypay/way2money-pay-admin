import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaynearLocationsComponent } from './paynear-locations.component';

describe('PaynearLocationsComponent', () => {
  let component: PaynearLocationsComponent;
  let fixture: ComponentFixture<PaynearLocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaynearLocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaynearLocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
