import { Component, OnInit } from "@angular/core";
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { PagerService } from "../../shared/pagination/pager.service";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from "../../../environments/environment";

@Component({
  selector: "app-user-transactions",
  templateUrl: "./user-transactions.component.html",
  styleUrls: ["./user-transactions.component.css"]
})
export class UserTransactionsComponent implements OnInit {
  brandsArray: any;
  perPage = environment.perPage;
  today_date: any;
  yesterday_date: any;
  constructor(
    private apiService: RestServicesService,
    private dataService: RestServicesService,
    private pagerService: PagerService,
    private router: Router, private route: ActivatedRoute,
    private _location: Location
  ) {

    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;

        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        if (this.searchStatus) {
          this.setSearchPage(this.currentPage);
        } else {
          this.isMerchant = localStorage.getItem('isMerchant');
          if (this.isMerchant === "yes") {
            this.merchantId = localStorage.getItem('id');
          }
          this.getCount();
        }
      }
    });
  }
  /* Added by suresh */
  from_date: any;
  to_date: any;
  paramsData: any = {};
  mobile: any;
  status: any;
  b_name: any;
  merchant_id: any;
  collectionSize: any;
  pageSize: number = 10;
  maxSize: Number = 10;
  skip: number;
  page: number = 1;
  limit: number;
  pageStartIndex: any;
  isDataFound = false;
  brands_data: any;
  transaction_data: any = [];
  merchant_id_array: any = [];
  merchant_brand_array: any = [];
  /*---  */
  userStatus: any = false;
  userInfo: any;
  countStatus: any = false;
  transactionDetails: any;
  private allItems: any[];
  pager: any = {};
  cpage: any;
  pagedItems = [];
  currentPage: number = 1;
  errMsg = false;
  pageLength: any;
  searchString = "";
  searchVariable = "mobile";
  showData = false;
  checkRefund: any;
  checkRefundStatus = false;
  refundStatus = 0;
  refundRemarks: any;
  searchString2: any;
  isMerchant: any;
  searchStatus: any = false;
  paginationStatus: any = false;
  merchantId: any;
  userTransactionsApi: any = 0;
  transactionId: any;
  transactionObj: any = {};
  merchantObj: any = {};
  updateTransactionResponse: any = {};
  redirectUrl: String;
  obj: any;
  responseStatus: any = false;
  refundButtonShowStatus: any = false;
  resetButtonShowStatus: any = false;
  refundstatus: any = 0;
  FromDate;
  ToDate;
  InputAutoFill: any;
  transaction: any;
  BrandsAutofillData = [];
  statusOptions = [
    { name: 'Initiated', value: '0' },
    { name: 'Success', value: '1' },
    { name: 'Failure', value: '2' },
    { name: 'Cancel', value: '3' },
    { name: 'Refund', value: '4' },
    { name: 'Paid', value: '5' }
  ];
  showModel: any = true;
  userType: any;
  isSuperAdmin: boolean;
  ngOnInit() {//1537727400000



    this.dataService.getAuth('api/user/userPermissions').subscribe(result => {
      this.userType = result.data.user_type;
      if (this.userType == 'super_admin') {
        this.isSuperAdmin = true
      } else {
        this.isSuperAdmin = false
      }
      // console.log("BHAGAVAN", result.data.user.name)
    });
    this.route.params.subscribe(params => {
      console.log(params)
      this.page = params.cpage ? params.cpage : 1;
      this.today_date = this.route.snapshot.queryParams.today_date;
      // this.today_date = 1537727400000
      this.yesterday_date = this.route.snapshot.queryParams.yesterday_date;
      // this.yesterday_date = 1537641000000
      if (this.today_date && !this.yesterday_date) {
        this.status = 'ALL';
        console.log("coming to today ", this.today_date)
        this.from_date = this.convertToDate(this.today_date);
        this.to_date = this.convertToDate(this.today_date);
        console.log(this.from_date);
        console.log(this.to_date);
        this.onClickSubmit(this.currentPage);
        // } else if (this.yesterday_date && this.today_date) {
      } else if (this.yesterday_date) {
        this.status = 'ALL';
        console.log("coming to yesterday", this.yesterday_date)
        this.from_date = this.convertToDate(this.yesterday_date);
        this.to_date = this.convertToDate(this.yesterday_date);
        // console.log(this.yesterday_date, "and", this.today_date)
        this.onClickSubmit(this.currentPage);
      } else {
        this.status = 'ALL';
        this.transactionId = this.route.snapshot.params.transId;
        this.onClickSubmit(this.currentPage);
        // this.getBrands();
        if (this.transactionId) {
          this.dataService.getAuth('api/getTransaction/' + this.transactionId).subscribe(result => {
            if (result.success) {
              console.log("---------->", result.data)
              this.transactionObj = result.transactionFound;
              this.merchantObj = result.merchantFound;
            }
          });
        } else {
          // this.isMerchant = localStorage.getItem('isMerchant');
          // if (this.isMerchant === "yes") {
          //   this.merchantId = localStorage.getItem('id');
          // }
        }

      }

      // this.today_date = 1535135400000;
      // this.yesterday_date = 1535049000000
      // console.log("--------------->", this.today_date)

      // if(this.today_date){
      //   console.log("going to search")

      //   this.search()
      // }

    })
  }

  convertToDate(dates) {
    let date_milli = dates
    let date = new Date(+date_milli);

    // console.log("dater : ", dates)
    // var date = new Date();
    console.log("dater : ", date)

    console.log(date.getDate())
    console.log(date.getMonth() + 1)
    console.log(date.getFullYear());

    var obj = {};
    obj['year'] = date.getFullYear();
    obj['month'] = date.getMonth() + 1;
    obj['day'] = date.getDate();
    return obj;
    // t_date = this.to_date.year + '-' + this.to_date.month + '-' + this.to_date.day;
    // var d = new Date(date); 
    // var ds = d.toLocaleString(); 
    // // alert(ds); 
    // console.log(ds);
    // var date = new Date();
    // console.log(date)

    // console.log(date.toString()) // "Dec 20"
    // console.log((date.getDate).toString());
    // console.log(date.getMonth);
    // console.log(date.getFullYear);

  }

  getBrandsList() {
    this.dataService.getAuth("merchant/getBrandsList").subscribe(result => {
      if (result.message == 'success') {
        this.BrandsAutofillData = result.data
      }
    });
    this.InputAutoFill = (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        map(term => term.length < 2 ? []
          : this.BrandsAutofillData.filter(v => v.indexOf(term) > -1).slice(0, 10))
      );
    console.log("ON START DATA OF INPUTAUTOFILL", this.BrandsAutofillData);
  }
  getBrands(event, brand_name) {
    console.log(event.keyCode, brand_name);
    if (!brand_name) { this.brands_data = []; }
    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 65 || event.keyCode > 90) &&
      event.keyCode !== 32 && event.keyCode !== 8 && event.keyCode !== 190) {
      return;
    }

    if (event.keyCode === 13 || !brand_name || brand_name.length < 3) { return; }
    const params: any = {
      brand_name: brand_name,
      limit: 6
    };

    // this.companies_loading = true;
    this.dataService.postAuth('user/searchingBrands', params)
      .subscribe((Response) => {
        console.log("SEARCH RESULTS", Response.data);
        // console.log("SEARCHING BRANDING COPYING TO ARRA", this.merchant_brand_array);
        if (Response.data) {
          for (let i = 0; i < Response.data.length; i++) {
            this.merchant_brand_array.push({
              _id: Response.data[i]._id,
              brand_name: Response.data[i]._source.brand_name
            })
            // this.merchant_id_array[i] = Response.data[i]._id;
            // this.merchant_brand_array[i] = Response.data[i]._source.brand_name;
            // this.InputAutoFill[i] = Response.data[i]._source.brand_name;
          }
          // this.InputAutoFill = (text$: Observable<string>) =>
          //   text$.pipe(
          //     debounceTime(200),
          //     distinctUntilChanged(),
          //     map(term => term.length < 2 ? []
          //       : this.merchant_brand_array.filter(v => v.indexOf(term) > -1).slice(0, 10))
          //   );
          // console.log("SEARCHING _IDDSS COPYING TO ARRA", this.merchant_id_array.length);

        } else {
          // console.log('NO DATA FOUND BRANDS');
        }
      }, (error_data) => {
      });
  }
  brandSelect() {
    // console.log("BRAND SELECTED==", this.b_name);
  }
  selectedItem(item) {
    console.log("ITEM", item);
    this.b_name = item.data.brand_name;
    this.merchant_id = item.data._id;
    this.merchant_brand_array.length = 0;
    this.merchant_brand_array = [];
    console.log("merchant id", this.merchant_id);

  }
  backClicked() {
    // this._location.back();
    // console.log(this.route.snapshot.params.cpage)
    this.redirectUrl = '/user/transactions/' + this.route.snapshot.params.cpage;
    this.router.navigate([this.redirectUrl]);
  }

  getCount() {
    // console.log('cpagee : ', this.route.snapshot.params.cpage);
    // this.currentPage = this.route.snapshot.params.cpage;
    if (this.isMerchant === "yes") {
      this.userTransactionsApi = 'user/getTransactionsCount/' + this.merchantId;
    } else {
      this.userTransactionsApi = 'user/getTransactionsCount/' + this.merchantId;
    }
    if (!this.countStatus) {
      this.dataService.getAuth(this.userTransactionsApi).subscribe(result => {
        if (result.status) {
          this.pageLength = parseInt(result.data);
          // console.log("count is ",this.pageLength);
          this.countStatus = true;
          this.setTransactionPage(this.currentPage);
        } else {
          this.pageLength = 0;
        }
      })
    } else {
      this.setTransactionPage(this.currentPage);
    }
  }
  getTransactions(skip, limit) {
    if (this.merchantId) {
      this.obj = {
        skip: skip,
        limit: limit,
        merchant_id: this.merchantId
      }
    } else {
      this.obj = {
        skip: skip,
        limit: limit,
      }
    }
    this.dataService.postAuth("user/transactions", this.obj).subscribe(result => {
      if (result.status) {
        this.pagedItems = result.data.transaction_data;
        // console.log(result.data)
        this.showData = true;
        this.errMsg = false;
      } else {
        this.showData = false;
        this.errMsg = true;
      }
      this.resetButtonShowStatus = false;
    })
  }
  convertInt(page) {
    // console.log("convertInt page------>", this.pager.currentPage);
    // console.log("convertInt page------>", this.pager.totalPages);
    if (page > this.pager.totalPages) {
      return this.pager.totalPages;
    } else {
      return parseInt(page) + 1;
    }
  }
  setPage(page) {
    this.currentPage = page;
    // console.log("currentPage",this.currentPage);
    this.cpage = '/user/transactions/' + page;
    this.router.navigate([this.cpage]);

  }
  setTransactionPage(page) {
    if (this.pageLength) {
      this.errMsg = false;
      if (this.pageLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerService.getPager(Number(this.pageLength), Number(page), 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getTransactions(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      } else {
        this.currentPage = 1;
        this.getTransactions(0, this.pageLength);
        this.paginationStatus = false;
      }
    } else {
      this.errMsg = true;
    }

  }
  setSearchPage(page) {
    // console.log("page------>", page);
    this.currentPage = page;
    console.log("currentPage", this.currentPage);
    this.cpage = '/user/transactions/' + page;
    this.router.navigate([this.cpage]);
    // this.currentPage = page;
    // this.cpage = this.router.routerState.snapshot.url;
    if (this.pageLength) {
      this.errMsg = false;
      if (this.pageLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerService.getPager(this.pageLength, page, 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        // console.log("currentPage",this.currentPage);
        this.pager = this.pagerService.getPager(this.pageLength, this.currentPage, 10);
        this.pagedItems = this.transactionDetails.slice(this.pager.startIndex, this.pager.endIndex + 1);
      } else {
        this.currentPage = 1;
        this.pagedItems = this.transactionDetails;
        this.paginationStatus = false;

      }
    } else {
      this.errMsg = true;
    }
  }
  onClickSubmit(page) {
    this.currentPage = page;
    console.log("currentPage", this.currentPage);
    this.cpage = '/user/transactions/' + page;
    this.router.navigate([this.cpage]);
    let f_date;
    let t_date;
    console.log("from and to date", this.from_date, this.to_date);
    if (this.from_date && this.to_date) {
      f_date = this.from_date.year + '-' + this.from_date.month + '-' + this.from_date.day;
      t_date = this.to_date.year + '-' + this.to_date.month + '-' + this.to_date.day;
      this.paramsData.from_date = f_date;
      this.paramsData.to_date = t_date;
    }
    if (this.mobile) {
      this.paramsData.mobile = this.mobile;
    }
    if (this.status) {
      this.paramsData.status = this.status;
    }
    if (this.merchant_id) {
      this.paramsData.merchant_id = this.merchant_id;
    }
    this.page = page;
    this.paramsData.skip = (this.page - 1) * this.pageSize;
    this.paramsData.limit = this.pageSize;
    console.log("PARAMS DATA------------------->", this.paramsData);

    this.dataService.postAuth("user/transactions", this.paramsData).subscribe((Response) => {
      if (Response.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        // console.log("fetching transactions", Response.data)
        // console.log("fetching transactions", this.transactionId)
        // console.log("fetching transactions", this.isMerchant)
        // console.log("fetching transactions", this.showData)
        if (Response.data && Response.data.total_records > 0) {
          console.log("count is there")
          this.isDataFound = true;
          this.pageStartIndex = ((this.page - 1) * (this.pageSize));
          this.collectionSize = Response.data.total_records;
          this.transaction_data = Response.data.transaction_data;
          console.log("Response received ", this.transaction_data);
        } else {
          // console.log("coming to else")
          if (Response.message === "no data found") {
            this.isDataFound = false;
          }
        }
        this.merchant_brand_array = [];
      }
      // console.log("response:::::::::::::::::", Response)


    });
    this.InputAutoFill = [];
  }
  /* ----------- */
  /* Reset Added by Suresh */
  onClickReset() {
    this.mobile = '';
    this.mobile = null;
    this.paramsData = {};
    this.status = "ALL";
    this.merchant_id = null;
    this.from_date = null;
    this.to_date = null;
    this.b_name = '';
    this.page = 1;
    this.merchant_brand_array = [];
    this.redirectUrl = '/user/transactions/1';
    this.router.navigate([this.redirectUrl]);
    this.onClickSubmit(1);
  }
  /* -- */

  search() {
    // console.log("calling search")
    this.responseStatus = true;
    var data = { skip: 0, limit: 10 };
    data["merchant_id"] = this.merchantId;
    // console.log(this.route.snapshot.params.cpage);
    var cpage = this.route.snapshot.params.cpage;
    var skip = 0;
    var limit = 0;

    if (!this.perPage) {
      this.perPage = 10;
    }
    if (!cpage) {
      cpage = 1;
    }
    limit = this.perPage;
    if (cpage === 1) {
      skip = 0;
    } else {
      cpage = cpage - 1;
      skip = skip * this.perPage * cpage;
    }
    data.skip = skip;
    data.limit = limit;





    /* Date */
    if (this.FromDate || this.ToDate) {
      let toDate, fromDate
      if (this.FromDate) {
        fromDate = new Date(this.FromDate).setHours(0, 0, 0, 0);
        data["fromDate"] = fromDate;
      }
      if (this.ToDate)
        toDate = new Date(this.ToDate).setHours(0, 0, 0, 0);
      else {
        toDate = new Date()
        toDate.setDate(toDate.getDate() + 1);
        toDate.setHours(0, 0, 0, 0);
        toDate = toDate.getTime();
      }
      data["toDate"] = toDate;
    }

    if (this.searchVariable == "mobile") {
      data["mobile_no"] = this.searchString;
    }
    if (this.searchVariable == "status") {
      data["status"] = this.searchString;
    }
    if (this.searchVariable == "m_id") {
      data["merchant_id"] = this.searchString;
    }
    if (this.searchVariable == "r_id") {
      data["reference_id"] = this.searchString;
    }
    if (this.searchVariable == "t_id") {
      data["_id"] = this.searchString;
    }
    if (this.searchVariable == "brand_name") {
      data["brand_name"] = this.searchString;
    }
    // console.log("data--->", data)
    this.dataService
      .postAuth("user/transactions", data)
      .subscribe(result => {
        this.responseStatus = false;
        if (result.status) {

          this.transactionDetails = result.data;
          // console.log("........>", this.transactionDetails)
          this.countStatus = false;
          this.pageLength = this.transactionDetails.length;

          this.showData = true;
          this.errMsg = false;
          this.searchStatus = true;
          this.setSearchPage(1);
        } else {
          this.errMsg = true;
          this.showData = false;
          this.searchStatus = false;
        }

      });
  }
  reset() {
    this.resetButtonShowStatus = true;
    this.searchString = "";
    this.searchString2 = "";
    this.getCount();
    this.errMsg = false;
    this.searchStatus = false;
  }
  reFundProcess(transaction) {
    this.transaction = {};
    this.refundStatus = 0;
    if (transaction.userInfo.length > 0) {
      console.log("in refundProcess", transaction)
      // console.log(transaction._id)
      if (transaction._id != undefined) {
        this.transaction['_id'] = transaction._id;
      }
      if (transaction.user_id != undefined) {
        this.transaction['user_id'] = transaction.user_id;
      }
      if (transaction.con_fee != undefined) {
        this.transaction['con_fee'] = transaction.con_fee;
      }
      if (transaction.merchant_id != undefined) {
        this.transaction['merchant_id'] = transaction.merchant_id;
      }
      if (transaction.amount != undefined) {
        this.transaction['amount'] = transaction.amount;
      }
      if (transaction.transaction_date != undefined) {
        this.transaction['transaction_date'] = transaction.transaction_date;
      }

      if (transaction.userInfo[0].mobile_number != undefined) {
        this.transaction['mobile'] = transaction.userInfo[0].mobile_number;
      }
      if (transaction.userInfo[0].name != undefined) {
        this.transaction['name'] = transaction.userInfo[0].name;
      }

      if (transaction.merchantInfo.length > 0) {
        if (transaction.merchantInfo[0].name != undefined) {
          this.transaction['merchant_name'] = transaction.merchantInfo[0].name;
        }
        if (transaction.merchantInfo[0].mobile != undefined) {
          this.transaction['merchant_mobile'] = transaction.merchantInfo[0].mobile;
        }
      }
      if (transaction.reference_id != undefined) {
        this.transaction['reference_id'] = transaction.reference_id;
      }
      if (transaction.merchant_commission != undefined) {
        this.transaction['merchant_commission'] = transaction.merchant_commission;
      }
      this.transaction['transaction_id'] = transaction._id
      // this.checkRefundStatus = false;
      // this.refundStatus = 0;
      this.dataService.postAuth('user/checkRefund', transaction).subscribe(result => {

        if (result.status == true) {
          this.showModel = true;
          this.checkRefundStatus = true;
          this.checkRefund = result.data
          // this.checkRefund['transaction_id'] = transaction_id
          console.log("checkRefund", this.checkRefund);
          // this.ngOnInit();
        } else if (result.status == false) {
          // this.showModel = false;
          this.checkRefundStatus = false;
          alert("User Data Not Found")
        }
      })

    } else {
      this.showModel = false
      alert("User Data Not Found")
    }
  }
  refund() {
    let page = this.page;
    if (this.showModel) {
      this.refundButtonShowStatus = true;
      this.refundStatus = 0;
      // console.log(this.transaction)
      this.dataService.postAuth('user/refund', this.transaction).subscribe(result => {
        // console.log(result)
        if (result.status == true) {
          this.refundStatus = 1
          this.checkRefundStatus = false;
          this.ngOnInit()
          // this.cpage = '/user/transactions/' + page;
          // this.router.navigate([this.cpage]);
          // this.refundstatus = result.refund_status;
          // this.showModel = false;
          // this.getTransactions()

          // this.cpage = '/user/transactions/' + page;
          // this.router.navigate([this.cpage]);
          // this.onClickSubmit(page);
        } else if (result.status == false) {
          this.checkRefundStatus = false;
          this.refundRemarks = result.message;
          this.refundStatus = 2;
          this.showModel = false;
        }
        this.refundButtonShowStatus = false;
      })
    }
  }

  onUpdateTransaction() {
    this.dataService.postAuth('api/transaction/update', this.transactionObj).subscribe(result => {
      if (result.success) {
        this.redirectUrl = '/user/transactions/' + this.route.snapshot.params.cpage;
        this.router.navigate([this.redirectUrl]);
      } else {
        this.updateTransactionResponse = result;
      }
    });
  }TT
}