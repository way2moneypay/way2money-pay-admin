import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { environment } from '../../environments/environment';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { ViewUsersComponent } from './view-users/view-users.component';
import { UserTransactionsComponent } from './user-transactions/user-transactions.component';
import { UserPaymentsComponent } from './user-payments/user-payments.component';
import { FilterPipe } from '../shared/pipes/filter.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UsersComponent } from './users.component';
import { AgmCoreModule } from '@agm/core';
import { AutoCompleteModule } from '../shared/auto-complete/auto-complete.module';
import { UserEventsComponent } from './user-events/user-events.component';
import { PaynearLocationsComponent } from './paynear-locations/paynear-locations.component';
import { DueUsersComponent } from './due-users/due-users.component';


@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    NgbModule.forRoot(),
    AutoCompleteModule,
    AgmCoreModule.forRoot({
      apiKey: environment.mapsAPIKey,
      libraries: ['places'],
      language: 'en',
    })
  ],
  declarations: [
    ViewUsersComponent,
    UserTransactionsComponent,
    UserPaymentsComponent,
    FilterPipe,
    UsersComponent,
    UserEventsComponent,
    PaynearLocationsComponent,
    DueUsersComponent
  ]
})
export class UsersModule { }
