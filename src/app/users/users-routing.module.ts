import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewUsersComponent } from './view-users/view-users.component';
import { UserTransactionsComponent } from './user-transactions/user-transactions.component';
import { UserPaymentsComponent } from './user-payments/user-payments.component'
import { AuthGuard } from '../shared/guard/auth.guard';
import { UsersComponent } from "./users.component";
import { UserEventsComponent } from "./user-events/user-events.component"
import { PaynearLocationsComponent } from './paynear-locations/paynear-locations.component'
import { DueUsersComponent } from './due-users/due-users.component'
const routes: Routes = [
  {
    path: '', children: [
      { path: '', redirectTo: 'view-user/:cpage', pathMatch: 'full', data: { title: 'View Users' }, canActivate: [AuthGuard] },
      { path: 'view-user/:cpage', component: ViewUsersComponent, data: { title: 'View Users' }, canActivate: [AuthGuard] },
      { path: 'transactions', pathMatch: 'full', component: UserTransactionsComponent, data: { title: 'User Transactions' }, canActivate: [AuthGuard] },
      { path: 'transactions/:cpage', pathMatch: 'full', component: UserTransactionsComponent, data: { title: 'User Transactions' }, canActivate: [AuthGuard] },
      { path: 'transactions/:transId/:cpage', component: UserTransactionsComponent, data: { title: 'User Transactions' }, canActivate: [AuthGuard] },
      { path: 'payments/:cpage', component: UserPaymentsComponent, data: { title: 'User Payments' }, canActivate: [AuthGuard] },
      { path: 'view-events', component: UserEventsComponent, data: { title: 'User Events' }, canActivate: [AuthGuard] },
      { path: 'paynear-locations/:cpage', component: PaynearLocationsComponent, data: { title: 'Paynear-Locations' }, canActivate: [AuthGuard] },
      { path: 'due-users/:cpage', component: DueUsersComponent, data: { title: 'Due Users' }, canActivate: [AuthGuard] }


    ], component: UsersComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
