import { Component, OnInit } from '@angular/core';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-events',
  templateUrl: './user-events.component.html',
  styleUrls: ['./user-events.component.css']
})
export class UserEventsComponent implements OnInit {

  errMsg: boolean;
  paramsData: any = {};
  user_id: any;
  constructor(private dataService: RestServicesService,
    private router: Router,
    private route: ActivatedRoute) { }

  from_date: any;
  to_date: any;
  showData: any = false;
  pagedItems: any[];

  ngOnInit() {
    this.user_id = this.route.snapshot.queryParams.user_id;
    console.log("EVENTS user_id--------------->", this.user_id)
    this.onClickSubmit()
  }
  onClickSubmit() {
    // console.log("Clicked")
    this.paramsData.user_id = this.user_id;
    this.dataService.postAuth("user/view-events", this.paramsData).subscribe((Response) => {
      if (Response.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (Response.data && Response.data.length > 0) {
          this.showData = true;
          this.pagedItems = Response.data;
          this.errMsg = false
          // console.log("Response received ", Response.data);
        } else {
          this.pagedItems = []
          this.showData = false;
          this.errMsg = true
        }

      }
    })
  }
  onClickReset() {
    // this.paramsData = {};
    this.from_date = null;
    this.to_date = null;
    this.onClickSubmit();
  }
  searchEvents() {
    var data = {};
    // console.log('this from date', this.from_date)
    if (this.from_date) {
      var fromDate = new Date(this.from_date).setHours(0, 0, 0, 0);
      data["fromDate"] = (fromDate / 1);
    }
    if (this.to_date) {
      var toDate = new Date(this.to_date);
      toDate.setDate(toDate.getDate());
      toDate.setHours(23, 59, 59, 999);
      data["toDate"] = toDate.getTime();
    }
    // console.log(data)
    data["user_id"] = this.user_id;
    this.dataService.postAuth("user/events-search", data).subscribe(result => {
      if (result.status) {
        this.pagedItems = result.data
        this.showData = true;
        this.errMsg = false;
      } else {
        this.errMsg = true;
        this.showData = false;
      }
    });
  }
}
