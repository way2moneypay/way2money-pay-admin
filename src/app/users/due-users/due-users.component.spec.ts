import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DueUsersComponent } from './due-users.component';

describe('DueUsersComponent', () => {
  let component: DueUsersComponent;
  let fixture: ComponentFixture<DueUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DueUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DueUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
