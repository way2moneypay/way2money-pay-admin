import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { PagerService } from '../../shared/pagination/pager.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-due-users',
  templateUrl: './due-users.component.html',
  styleUrls: ['./due-users.component.css']
})
export class DueUsersComponent implements OnInit {
  previousUserID: any
  searchString: String = "";
  paramsData: any = {};
  collectionSize: Number = 5;
  collection: Number = 5;
  pageSize: number = 10;
  skip: number;
  limit: number;
  maxSize: Number = 5;
  page: number = 1;
  pageStartIndex: number;
  from_date: any;
  to_date: any;
  refresh_fDate: any;
  refresh_tDate: any;
  data_alert_info: string;
  datafound: boolean = true;
  alert_error_message_type: string;
  date_info: any;
  isview: boolean = false;
  searchVariable = "mobile_number";
  dueCount_details: any;
  addcomment: any = {};
  closeResult: string;
  message: any;
  user_data: { "user_id": any; };
  comment_details: any;
  isViewData: boolean = false;
  constructor(private dataService: RestServicesService,
    private pagerservice: PagerService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private _location: Location) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.page = params.page ? params.page : 1;
      // ****************.................For resolve the refresh handling ......****************
      this.refresh_fDate = params.from_date ? params.from_date : this.from_date;
      this.refresh_tDate = params.to_date ? params.to_date : this.to_date;
      if (this.refresh_fDate && this.refresh_tDate) {
        this.from_date = this.refresh_fDate;
        this.to_date = this.refresh_tDate;
        // #######################CONVERTING DARE TO OBJECT FORMAT########################
        let dt = this.from_date.split("-");
        this.from_date = { "year": parseInt(dt[0]), "month": parseInt(dt[1]), "day": parseInt(dt[2]) };
        dt = this.to_date.split("-");
        this.to_date = { "year": parseInt(dt[0]), "month": parseInt(dt[1]), "day": parseInt(dt[2]) };
        // console.log(this.from_date, this.to_date);
      }
      this.onClickSubmit(this.page);
    });
  }

  onClickSubmit(page) {
    this.page = page;
    this.paramsData.skip = (this.page - 1) * this.pageSize;
    this.paramsData.limit = this.pageSize;
    let fDate;
    let tDate;
    if (this.searchVariable == "mobile_number") {
      if (this.searchString != "")
        this.paramsData["mobile_number"] = this.searchString;
    }
    if (this.from_date != null && this.to_date != null) {
      fDate = this.from_date.year + "-" + this.from_date.month + "-" + this.from_date.day;
      tDate = this.to_date.year + "-" + this.to_date.month + "-" + this.to_date.day;
      if (new Date(fDate).getTime() >= new Date(tDate).getTime()) {
        if (new Date(fDate).getTime() == new Date(tDate).getTime()) {
          this.date_info = null;
        }
        else {
          this.datafound = true;
          this.alert_error_message_type = "warning";
          this.date_info = "FromDate should be less than ToDate";
        }
      }
      else {
        this.date_info = null;
      }
    }
    if (this.from_date == null && this.to_date != null) {
      this.alert_error_message_type = "warning";
      this.date_info = "Please Select FromDate";
    }
    if (this.from_date != null && this.to_date == null) {
      this.alert_error_message_type = "warning";
      this.date_info = "Please Select ToDate";
    }
    if (this.refresh_fDate && this.refresh_tDate) {
      fDate = this.refresh_fDate;
      tDate = this.refresh_tDate;
    }
    this.paramsData.from_date = fDate;
    this.paramsData.to_date = tDate;
    this.data_alert_info = "Loading..";
    console.log('paramsData==>>>', this.paramsData)
    this.dataService.getAuthParams("api/dueUsers", this.paramsData).subscribe((Response) => {
      console.log(Response)
      if (Response.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (Response.data.length != 0) {
          this.collectionSize = Response.data.total_records;
          this.dueCount_details = Response.data.dueCount_details;
          this.datafound = true;
          this.alert_error_message_type = "success";
          this.data_alert_info = "Results Found " + this.collectionSize;
          this.isview = true
        }
        else {
          this.isview = false
          this.datafound = false;
          this.alert_error_message_type = "warning";
          this.data_alert_info = "Results Found " + this.collectionSize;
        }
        this.pageStartIndex = ((this.page - 1) * (this.pageSize));
      }
    })
  }
  onClickReset() {
    this.page = 1;
    this.from_date = null;
    this.to_date = null;
    this.date_info = null;
    this.searchString = "";
    // this.searchStatus = false;
    this.paramsData = {};
    this.ngOnInit();
  }
  open(content) {
    this.popupCalls(content);
  }
  cancel() {
  }
  TemplateEdit(content, data) {
    console.log("content", content);
    // console.log("data", data);
    this.addcomment = data;
    // console.log(",",this.userpermissions);
    this.popupCalls(content);
  }

  popupCalls(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  EnterComment() {
    this.dataService.postAuth("api/contactedUser", this.addcomment).subscribe((Response) => {
      if (Response.success == true) {
        this.message = Response.message
        console.log("Successfully inserted")
        this.addcomment = {}
        this.ngOnInit();
        // this.userpermissions = {};
      } else {
        console.log("Error While inserting");
      }
    })
  }
  getUserComments(user_details) {
    this.comment_details = []
    // console.log("user_details:: ", user_details);
    this.user_data = {
      "user_id": user_details._id
    }
    this.getComments(this.user_data, 1);
    // this.setTransactionPage(1);
  }

  getComments(data, page) {
    this.page = page;
    let reqDataObj = {}
    if (data === 'pagination') {
      reqDataObj['user_id'] = this.previousUserID;
    } else {
      if (data && data.user_id) {
        reqDataObj['user_id'] = data.user_id;
        this.previousUserID = data.user_id
      }
    }
    reqDataObj['skip'] = (this.page - 1) * this.pageSize;
    reqDataObj['limit'] = this.pageSize;
    this.dataService.getAuthParams("api/getCommentsData", reqDataObj).subscribe(result => {
      if (result.status) {
        this.collection = result.data.collectionSize;
        this.comment_details = result.data.results;
        this.pageStartIndex = ((this.page - 1) * (this.pageSize));
        if (this.comment_details.length > 0) {
          this.isViewData = true
        }
      }
    })
  }
}
