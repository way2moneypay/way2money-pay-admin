import { Component, OnInit } from "@angular/core";
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { PagerService } from "../../shared/pagination/pager.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";

@Component({
  selector: "app-user-payments",
  templateUrl: "./user-payments.component.html",
  styleUrls: ["./user-payments.component.css"]
})
export class UserPaymentsComponent implements OnInit {
  paymentdetailsobj: any;
  Payment_refId: string;
  userType: any;
  isSuperAdmin: boolean;
  constructor(
    private dataService: RestServicesService,
    private pagerService: PagerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        if (this.searchStatus) {
          this.setSearchPage(this.currentPage);
        } else {
          this.getCount();
        }
      }
    });
  }
  /* Added by suresh */
  from_date: any;
  to_date: any;
  paramsData: any = {};
  mobile: any;
  status: any;
  b_name: any;
  merchant_id: any;
  collectionSize: any;
  pageSize: number = 10;
  maxSize: Number = 10;
  skip: number;
  page: number = 1;
  limit: number;
  pageStartIndex: any;
  isDataFound = false;
  brands_data: any;
  transaction_data: any = [];
  statusOptions = [
    { name: 'Initiated', value: '0' },
    { name: 'Success', value: '1' },
    { name: 'Failure', value: '2' },
    { name: 'Refund', value: '3' },
  ]
  /* ---- */
  countStatus: any = false;
  cpage: any;
  searchStatus: any = false;
  pageLength: any;
  currentPage: number = 1;
  paymentValues: any;
  paymentDetails: any;
  errMsg = false;
  pagedItems: any[];
  pager: any = {};
  private allItems: any[];
  searchString = "";
  searchVariable = "mobile_number";
  showData: any = false;
  responseStatus: any = true;
  searchString2: any;
  resetButtonStatus: any = false;
  showModal = false;
  obj: any = {};
  trans_date :any;
  ngOnInit() {
    this.dataService.getAuth('api/user/userPermissions').subscribe(result => {
      this.userType = result.data.user_type;
      if (this.userType == 'super_admin') {
        this.isSuperAdmin = true
      } else {
        this.isSuperAdmin = false
      }
      // console.log("BHAGAVAN", result.data.user.name)
    });
    this.status = "ALL";
    this.onClickSubmit(1);
    this.pager.startIndex = 0;
    this.pager.endIndex = 9;
    this.getCount();
  }
  /* added by suresh */
  onClickSubmit(page) {
    console.log("Clicked");
    let f_date;
    let t_date;
    if (this.from_date && this.to_date) {
      f_date = this.from_date.year + '-' + this.from_date.month + '-' + this.from_date.day;
      t_date = this.to_date.year + '-' + this.to_date.month + '-' + this.to_date.day;
      this.paramsData.from_date = f_date;
      this.paramsData.to_date = t_date;
    }
    if (this.mobile) {
      this.paramsData.mobile = this.mobile;
    }
    if (this.status) {
      this.paramsData.status = this.status;
    }
    this.page = page;
    this.paramsData.skip = (this.page - 1) * this.pageSize;
    this.paramsData.limit = this.pageSize;
    console.log("PARAMS DATA", this.paramsData);
    this.dataService.postAuth("user/fetchUserPayments", this.paramsData).subscribe((Response) => {
      if (Response.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (Response.data && Response.data.total_records > 0) {
          this.isDataFound = true;
          this.pageStartIndex = ((this.page - 1) * (this.pageSize));
          this.collectionSize = Response.data.total_records;
          this.transaction_data = Response.data.transaction_data;
          // for (var i = 0; i < this.transaction_data.length; i++) {
          //   if(this.transaction_data && this.transaction_data[i].trans_details && (this.transaction_data[i].trans_details[0].status == 0)){
          //     console.log("coming")
          //     this.trans_date = this.transaction_data[i].trans_details[0].date
          //     console.log("date----->",this.trans_date)
  
          //   }else{
          //     console.log("not coming")
          //   }
          // } 
          
          console.log("Response received ", this.transaction_data);
        } else {
          if (Response.message === "no data found") {
            this.isDataFound = false;
          }
        }

      }
    })
  }
  /* --- */
  /* Reset Added by Suresh */
  onClickReset() {
    this.mobile = '';
    this.mobile = null;
    this.paramsData = {};
    this.status = "ALL";
    this.from_date = null;
    this.to_date = null;
    this.page = 1;
    this.onClickSubmit(1);
  }
  /* -- */
  // array of all items to be paged

  // pager object

  // paged items
  getCount() {
    console.log("cpagee : ", this.route.snapshot.params.cpage);
    if (!this.countStatus) {
      this.currentPage = this.route.snapshot.params.cpage;
      this.dataService
        .postAuth("user/getPaymentsCount", {})
        .subscribe(paymentCount => {
          if (paymentCount.status) {
            this.pageLength = parseInt(paymentCount.data);
            console.log(this.pageLength);
            this.countStatus = true;
            this.setPaymentPage(this.currentPage);
          } else {
            this.showData = false;
            this.errMsg = true;
          }
        });
    } else {
      this.setPaymentPage(this.currentPage);
    }
  }
  getUserPayments() {
    this.dataService
      .postAuth("user/payments", {
        skip: this.pager.startIndex,
        limit: this.pager.endIndex + 1 - this.pager.startIndex
      })
      .subscribe(paymentData => {
        if (paymentData.status) {
          this.showData = true;
          this.paymentDetails = paymentData.data;
          this.pagedItems = this.paymentDetails;
          this.errMsg = false;
        } else {
          this.errMsg = true;
          this.showData = false;
        }
        this.resetButtonStatus = false;
      });
  }

  searchPayments() {
    var data = {};
    if (this.from_date) {
      data["type"] = "paid_date";
      var fromDate = new Date(this.from_date).setHours(0, 0, 0, 0);
      data["from_date"] = fromDate;
    }
    if (this.to_date) {
      data["type"] = "paid_date";
      var toDate = new Date(this.to_date);
      toDate.setDate(toDate.getDate() + 1);
      toDate.setHours(0, 0, 0, 0);
      data["to_date"] = toDate.getTime();
    }
    this.searchStatus = true;
    this.responseStatus = false;
    // if (this.searchVariable == "date") {
    //   // var fromDate = new Date(this.searchString).getTime();
    //   // var toDate = new Date(this.searchString2).getTime();


    // }
    if (this.searchVariable == "mobile_number") {
      data["mobile_number"] = this.searchString;
    }
    if (this.searchVariable == "status") {
      data["paid_status"] = this.searchString;
    }
    if (this.searchVariable == "mode") {
      data["payment_mode"] = this.searchString;
    }
    if (data['mobile_number'] == '') {
      delete data['mobile_number'];
    }
    if (data['payment_mode'] == '') {
      delete data['payment_mode'];
    }
    if (data['paid_status'] == '') {
      delete data['paid_status'];
    }
    this.dataService.postAuth("user/payments/search/", data).subscribe(result => {
      if (result.status) {
        this.allItems = result.data;
        this.countStatus = false;
        this.pageLength = this.allItems.length;
        this.setSearchPage(1);
        this.pagedItems = this.allItems.slice(
          this.pager.startIndex,
          this.pager.endIndex + 1
        );
        this.showData = true;
        this.errMsg = false;
      } else {
        this.errMsg = true;
        this.showData = false;
      }
      this.responseStatus = true;
    });
  }
  reset() {
    this.resetButtonStatus = true;
    this.searchString = "";
    this.searchString2 = "";
    this.from_date = "";
    this.to_date = "";
    this.getCount();
    this.searchStatus = false;
    this.errMsg = false;
  }
  setPage(page) {
    this.currentPage = page;
    this.cpage = this.router.routerState.snapshot.url;
    this.cpage = "user/payments/" + page;
    this.router.navigate([this.cpage]);
  }
  setPaymentPage(page) {
    if (this.pageLength) {
      if (this.pageLength > 10) {
        this.showData = true;
        this.pager = this.pagerService.getPager(
          Number(this.pageLength),
          Number(page),
          10
        );
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getUserPayments();
      } else {
        this.currentPage = 1;
        this.getUserPayments();
        this.showData = true;
      }
    } else {
      this.errMsg = true;
    }
  }
  convertInt(page) {
    return parseInt(page);
  }
  setSearchPage(page) {
    if (this.pageLength) {
      if (this.pageLength > 10) {
        this.showData = true;
        this.pager = this.pagerService.getPager(this.pageLength, page, 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.pagedItems = this.allItems.slice(
          this.pager.startIndex,
          this.pager.endIndex + 1
        );
      } else {
        this.currentPage = 1;
        this.pagedItems = this.allItems;
        this.showData = true;
      }
    } else {
      this.errMsg = true;
    }
  }

  getPayment_refId(item) {
    console.log("paymentdetailsobj", this.paymentdetailsobj)
    // console.log("item---->",item)
    this.paymentdetailsobj = item;
    if (item.payment_ref_id != undefined) {
      this.obj = {
        _id: item._id,
        user_id: item.user_id,
        payment_ref_Id: item.payment_ref_id,
        paid_status: item.paid_status
      }
      // console.log("obj------>",this.obj)
      this.dataService.postAuth("user/check_payment_status", this.obj).subscribe(Response => {
        alert(Response.message)
        console.log("Completed :: ", Response);
      });
      this.showModal = false;

    } else {
      console.log("coming to else part------------------>", item.Payment_refId)
      this.showModal = true;
    }



  }

  enteredPayment_refId() {
    if (this.Payment_refId) {
      // this.obj={
      //   payment_ref_id:payment_refid,
      // }
      // console.log("existed :: ", this.paymentdetailsobj);
      this.paymentdetailsobj['payment_ref_id'] = this.Payment_refId;
      this.getPayment_refId(this.paymentdetailsobj);
      this.showModal = false;
    }

  }

  Cancel() {
    console.log("cancel")
    this.Payment_refId = "";
  }
}
