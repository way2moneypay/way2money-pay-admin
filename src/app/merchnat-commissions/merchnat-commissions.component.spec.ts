import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchnatCommissionsComponent } from './merchnat-commissions.component';

describe('MerchnatCommissionsComponent', () => {
  let component: MerchnatCommissionsComponent;
  let fixture: ComponentFixture<MerchnatCommissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchnatCommissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchnatCommissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
