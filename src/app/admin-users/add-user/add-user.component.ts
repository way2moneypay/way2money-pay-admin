import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  updateAdminuser: any = {
    permissions: []
  };

  newpermissions: any = {};
  statusOptions = [
    { value: "inactive" },
    { value: "active" }
  ];
  UserTypeSelected = "-- Select User Type --";
  UserTypeOptions = ["super_admin", "admin"];
  DisplayStatusSelected = "-- Select Display Status --";
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  selected_user_type: any = [];
  isNew: boolean = true;
  updateuserid = {};
  message: any;
  selected_parent: any = [];
  selected_permissions: any = {};
  selected_all_permissions: any = {};
  login_user_details: any = {};
  current_user_details: any = {};
  all_admins: any = [];
  reginalheads_response: any;
  all_admins_dup: any[];
  page_loading: boolean;
  user_permissions: any;
  permissions_toggle: any = {};
  user_details: any = {};
  selected_sub_types: any;
  user_id: string;
  password_type: string = "password";
  constructor(
    private dataService: RestServicesService,
    private route: ActivatedRoute,
    public active_route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private modalService: NgbModal,
    private ngZone: NgZone,
  ) { if (this.active_route.snapshot.params.user_id) this.user_id = this.active_route.snapshot.params.user_id; }
  namePattern = "^[a-zA-Z][a-zA-Z0-9 ]+$";
  emailPattern = "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
  mobilePattern = "^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$";
  clickPasswordShow(type) {
    this.password_type = type;
  }
  user_types: any = [
    // {
    //   id: "admin",
    //   itemName: "Admin"
    // },
    {
      id: "regional_head",
      itemName: "Admin"
    },
    {
      id: "sales_team",
      itemName: "Sales Team"
    },
    {
      id: "accountant",
      itemName: "Accountant"
    }
    // ,
    // {
    //   id: "tele_caller",
    //   itemName: "Tele Caller"
    // }
    // {
    //   id: "accountant_user",
    //   itemName: "Accountant User"
    // }
  ]
  user_type_settings: any = {
    singleSelection: true,
    text: "Select User Type"
  };
  accountant_selection_settings: any = {
    singleSelection: false,
    text: "Select Accountants",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    badgeShowLimit: 5
  };
  parent_select_settings: any = {
    singleSelection: true,
    text: "Select Parent",
    enableSearchFilter: true,
  };
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  changeUserType(notRemovePermissions?, notSelectedParent?) {
    if (!notRemovePermissions) {
      this.selected_permissions = [];
      this.selected_all_permissions = [];
    }

    // if (this.selected_user_type[0]["id"] == "accountant") {
    //   this.accountant_selection_settings.singleSelection = true;
    // } else {
    //   this.accountant_selection_settings.singleSelection = false;
    // }
    if (this.login_user_details.user_type == "super_admin") {
      this.all_admins = [{
        id: this.login_user_details._id,
        itemName: "Super Admin"
      }];

      if (!notSelectedParent) {
        this.selected_parent = [{
          id: this.login_user_details._id,
          itemName: "Super Admin"
        }]
      }
    }


    let name = this.login_user_details.name + " (" + this.login_user_details.user_type + ")";
    if (this.selected_user_type[0]) {
      if (this.selected_user_type[0]["id"] == "sales_team") {
        if (this.login_user_details.user_type == "regional_head") {
          this.all_admins = this.prepareParents([], null, name, this.updateAdminuser.parent_id);
        } else {
          this.all_admins = this.prepareParents(this.reginalheads_response, "regional_head", null, this.updateAdminuser.parent_id);
        }
      }

      if (this.selected_user_type[0]["id"] == "tele_caller") {
        if (this.login_user_details.user_type == "regional_head") {
          this.all_admins = this.prepareParents([], null, name, this.updateAdminuser.parent_id);
        } else {
          this.all_admins = this.prepareParents(this.reginalheads_response, "regional_head", null, this.updateAdminuser.parent_id);
        }
      }


      if (this.selected_user_type[0]["id"] == "accountant_user") {
        if (this.login_user_details.user_type == "accountant") {
          this.all_admins = this.prepareParents([], null, name, this.updateAdminuser.parent_id);
        } else {
          this.all_admins = this.prepareParents([], "accountant", null, this.updateAdminuser.parent_id);
        }
        this.all_admins = this.prepareParents(this.reginalheads_response, "accountant", null, this.updateAdminuser.parent_id);
      }
    }
  }
  getAllReginalHeads(callback) {
    console.log("admin type :: ", this.login_user_details);
    if (this.login_user_details.user_type != "super_admin") {
      let name = this.login_user_details.name + " (" + this.login_user_details.user_type + ")";
      this.all_admins = this.prepareParents([], null, name);
      this.all_admins_dup = this.prepareParents([], null, name);
      if (callback) callback();
      return;
    }

    this.dataService.getAuth("api/get_regional_head")
      .subscribe((parents_response) => {
        console.log("parents_resonse :: ", parents_response);
        if (parents_response.type == "ERROR") {
          console.error("Error while getting the parent details.");
        }
        if (parents_response.success) {
          // this.all_admins = parents_response.data;
          this.reginalheads_response = parents_response.Permissions;
          console.log("this.updateAdminuser :: 111 ", this.updateAdminuser, this.reginalheads_response);

          this.all_admins = this.prepareParents(parents_response.Permissions, null, null, this.updateAdminuser.parent_id);
          this.all_admins_dup = this.prepareParents(parents_response.Permissions, null, null, this.updateAdminuser.parent_id);


          this.changeUserType(true, true);

        }

        if (callback) callback();
        return;
      }, (parents_error) => {
        console.error("Some technical glitches happened while getting the parent details. Please try again...");
      })
  }

  prepareParents(parents, type, name?, parentId?) {
    let return_data = [];

    return_data.push({
      id: this.login_user_details._id,
      itemName: name ? name : "Super Admin"
    });

    console.log("this.updateAdminuser :: ", parentId)

    if (parents && parents.length) {
      for (var index = 0; index < parents.length; ++index) {
        console.log(parents[index], type);
        if (!type || type == parents[index]["user_type"]) {
          return_data.push({
            id: parents[index]["_id"],
            itemName: parents[index]["name"] + " (" + parents[index]["user_type"] + ")"
          });
        }

        if (parentId && parentId === parents[index]["_id"]) {
          this.selected_parent = [
            {
              id: parents[index]["_id"],
              itemName: parents[index]["name"] + " (" + parents[index]["user_type"] + ")"
            }
          ]
        }
      }
    }

    if (!parentId) {
      this.selected_parent = [
        {
          id: this.login_user_details._id,
          itemName: name ? name : "Super Admin"
        }
      ]
    }

    return return_data;
  }
  prepareSelectedUserType(selected_type) {
    console.log("current_user_type :: ", this.current_user_details);
    this.prepareUserTypeData();
    this.selected_user_type = [];
    for (var index = 0; index < this.user_types.length; ++index) {
      if (selected_type && selected_type == this.user_types[index]["id"]) {
        this.selected_user_type.push(this.user_types[index]);
      }
    }

    console.log("this.selected_user_type ::", this.selected_user_type);
  }
  preparePermissions(permissions) {
    for (var index = 0; index < permissions.length; ++index) {
      this.selected_permissions[permissions[index]] = true;
    }
    let user_type = this.selected_user_type[0]['id'];
    let permission_types = [];
    if (this.user_permissions[user_type]) {
      permission_types = Object.keys(this.user_permissions[user_type])
    }
    for (var index = 0; index < permission_types.length; ++index) {
      let permission_type = permission_types[index];
      this.selected_all_permissions[permission_type] = this.checkAllPermissions(permission_type);
    }

  }

  prepareUserData(user_data) {
    console.log("user_data", user_data)
    this.current_user_details = user_data;
    this.updateAdminuser = user_data;
    this.updateAdminuser.temp_password = user_data.temp_password;
    this.prepareSelectedUserType(user_data.user_type);
    this.preparePermissions(user_data.permissions);
    // this.prepareParents()
  }
  prepareUserTypeData() {
    if (this.login_user_details.user_type == "super_admin") {
      if (this.current_user_details.user_type == "accountant") {
        this.user_types = [
          {
            id: "accountant",
            itemName: "Accountant"
          }
        ]
      };
      if (this.current_user_details.user_type == "sales_team") {
        this.user_types = [
          {
            id: "regional_head",
            itemName: "Admin"
          },
          {
            id: "sales_team",
            itemName: "Sales Team"
          }
        ]
      };

      if (this.current_user_details.user_type == "regional_head") {
        this.user_types = [
          {
            id: "regional_head",
            itemName: "Admin"
          },
          {
            id: "sales_team",
            itemName: "Sales Team"
          }
        ]
      };
    }
    if (this.login_user_details.user_type == "regional_head") {
      this.user_types = [
        {
          id: "sales_team",
          itemName: "Sales Team"
        }
      ];
      this.selected_user_type = [{
        id: "sales_team",
        itemName: "Sales Team"
      }]
    };
    if (this.login_user_details.user_type == "accountant") {
      this.user_types = [
        {
          id: "accountant_user",
          itemName: "Accountant User"
        }
      ];
      this.selected_user_type = [{
        id: "accountant_user",
        itemName: "Accountant User"
      }]
    };
  }

  getLoginUser(callback) {
    this.dataService.getAuth("api/LoginUserDetails")
      .subscribe((login_user_response) => {
        if (login_user_response.type == "SUCCESS") {
          this.login_user_details = login_user_response.data;
        } else {
          console.log("Invalid authentication.");
        };
        if (callback) callback();
      }, (user_error) => {
        console.log("Something went wrong while getting login user details.");
        if (callback) callback();
      });
  }
  getUserPermissions(callback) {
    this.dataService.getAuth("api/getAllPermission")
      .subscribe((permissions_response) => {
        // console.log("permissions_response::",permissions_response)
        if (permissions_response.success) {
          this.user_permissions = permissions_response.data;
          console.log("this.user_permissions ::: ", this.user_permissions);
        } else {
          console.error("Error while getting permissions.");
        };
        if (callback) callback();
      }, (user_error) => {
        console.error("Something went wrong while getting login user details.");
        if (callback) callback();
      });
  }
  getValueKeys(obj, value) {
    let keys = Object.keys(obj);
    let return_values = [];
    for (var index = 0; index < keys.length; index++) {
      if (obj[keys[index]] == value) return_values.push(keys[index]);
    };
    return return_values;
  }
  Keys(obj, sort) {
    let keys = Object.keys(obj);
    if (sort) keys = keys.sort();
    return keys;
  }
  ngOnInit() {
    this.getLoginUser(() => {
      this.getUserPermissions(() => {
        this.getPermissions();
        this.prepareUserTypeData();
        this.getUserData(this.updateuserid, () => {
          console.log("^&*())))))))))))00")
          this.getAllReginalHeads(() => {
            this.page_loading = false;
          });
        });
        if (this.updateuserid) {
          //this.EditAdminUser()
          this.isNew = false;
        }
      });
    });
    this.updateuserid = this.route.snapshot.params.userId;
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 7,
      allowSearchFilter: true
    }
  }
  validateData() {
    if (this.user_details.current_password != this.user_details.confirm_password) {
      return false;
    }

    if (!this.selected_user_type.length || this.selected_sub_types.length.length) {
      return false;
    }

    return true;
  }
  permissionsToggle(permission_type, toggle_rest, toggle_value) {
    if (toggle_value == false || toggle_value == true) {
      this.permissions_toggle[permission_type] = toggle_value;
    } else {
      this.permissions_toggle[permission_type] = !this.permissions_toggle[permission_type];
    }
    if (toggle_rest && this.permissions_toggle[permission_type]) {
      this.toggleRest(permission_type);
    }
  }
  toggleRest(permission_type) {
    let keys = Object.keys(this.permissions_toggle);
    for (var index = 0; index < keys.length; ++index) {
      if (keys[index] != permission_type) {
        this.permissions_toggle[keys[index]] = false;
      }
    }
  }
  changeAllPermissions(permission_type, value) {
    let user_type = this.selected_user_type[0]['id'];
    let permissions = this.user_permissions[user_type][permission_type]['permissions'];
    for (var index = 0; index < permissions.length; ++index) {
      this.selected_permissions[permissions[index]["permission_name"]] = value;
    }
  }

  checkAllPermissions(permission_type) {
    let user_type = this.selected_user_type[0]['id'];
    let permissions = this.user_permissions[user_type][permission_type]['permissions'];
    console.log(this.selected_permissions);
    console.log(permissions, permission_type);
    for (var index = 0; index < permissions.length; ++index) {
      if (!this.selected_permissions[permissions[index]["permission_name"]]) return false;
    }
    return true;
  }
  permissionsChecking($event, permission_type, type) {
    let value = $event.target.checked;
    if (type == "all") {
      this.changeAllPermissions(permission_type, value);
    }

    if (type == 'single' && !value) {
      this.selected_all_permissions[permission_type] = value;
    }

    if (type == 'single' && value) {
      this.selected_all_permissions[permission_type] = this.checkAllPermissions(permission_type);
    }
  }
  getPermissions() {
    this.dataService.getAuth('api/getPermissionsList').subscribe((response) => {
      //console.log("getPermissions", response);
      if (response.success == true) {
        this.newpermissions = response.data;
        this.dropdownList = this.preparePermission(this.newpermissions);
        //console.log("prepared Dro pData :: ", this.dropdownList);
      } else {
        console.log("Error While getting permission types")
      }
    });
  }
  preparePermission(permission) {
    //console.log("permission::>>>>>>>>>>>>>", permission)
    let PermissionData = [];
    for (var index = 0; index < permission.length; ++index) {
      console.log("value :: ", permission[index]['permission_name']);
      PermissionData.push({
        item_id: permission[index]["permission_name"],
        item_text: permission[index]["permission_name"]
      });
    }
    return PermissionData;
  }

  preparePermissionData() {
    //console.log(this.selectedItems);
    this.updateAdminuser.permissions = [];
    for (let permission of this.selectedItems) {
      this.updateAdminuser.permissions.push(permission);
    }
  }

  open(content) {
    this.modalService.open(content, { size: 'lg', windowClass: '.my-model' }).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  AddadminUser() {

    //console.log("UserTypeSelected::", this.selected_user_type, this.login_user_details._id)
    this.preparePermissionData();
    this.updateAdminuser['permissions'] = this.getValueKeys(this.selected_permissions, true);
    if (this.selected_user_type[0]) {
      this.updateAdminuser['user_type'] = this.selected_user_type[0]["id"];
    }
    this.updateAdminuser['parent_id'] = this.login_user_details._id;
    if (this.selected_parent[0] && this.selected_parent[0]["id"]) {
      this.updateAdminuser['parent_id'] = this.selected_parent[0]["id"];
    }
    console.log(this.updateAdminuser)
    this.dataService.postAuth('api/addadminuser', this.updateAdminuser).subscribe((response) => {
      console.log("AddUserPermissions", response);
      if (response.success == true) {
        console.log(response);
        this.router.navigate(['admin-users/view-users']);
        // this.updateAdminuser = {};
        // this.updateAdminuser.permissions = [];
        this.message = response.message;
      } else {
        console.log("Error while Adding user permissions ")
      }
    });
  }

  getUserData(user_id, callback) {
    if (!user_id) {
      if (callback) return callback();
      return;
    }
    let params: any = {};
    params.user_id = user_id;
    this.dataService.getAuthParams("api/EditAdminUsersList", params)
      .subscribe((user_response) => {
        if (user_response.success == false) {
          console.error("Error while getting the user details");
        }
        if (user_response.success == true) {
          this.prepareUserData(user_response.data[0]);
        };
        if (callback) return callback();
      }, (user_error) => {
        console.log("user_error :: ", user_error);
        console.error("Something went wrong while getting the user details.");
      })
  }
  // EditAdminUser() {
  //   this.isNew == false;
  //   this.updateuserid = this.route.snapshot.params;
  //   this.dataService.getAuthParams('api/EditAdminUsersList', this.updateuserid).subscribe((response) => {
  //     if (response.success == true) {
  //       //console.log(response);
  //       this.prepareUserData(response.data);
  //       // this.updateAdminuser = response.data[0];
  //       // this.updateAdminuser.password = this.updateAdminuser.temp_password;
  //       // this.updateAdminuser['user_status'] ? this.DisplayStatusSelected = this.updateAdminuser['user_status'] : this.DisplayStatusSelected = '-- Select Display Status --'
  //       // this.updateAdminuser['user_type'] ? this.UserTypeSelected = this.updateAdminuser['user_type'] : this.UserTypeSelected = '-- Select User Type --'
  //       // this.selectedItems = this.updateAdminuser.permissions
  //     } else {
  //       console.log("Error While Getting AdminUsers list");
  //     }
  //   });
  // }
  UpdateUser() {
    this.preparePermissionData()
    this.updateAdminuser['permissions'] = this.getValueKeys(this.selected_permissions, true);
    this.updateAdminuser['password'] = this.updateAdminuser.temp_password
    this.updateAdminuser['temp_password'] = this.updateAdminuser.temp_password
    if (this.selected_user_type[0]) {
      this.updateAdminuser['user_type'] = this.selected_user_type[0]["id"];
    }
    if (this.selected_parent[0] && this.selected_parent[0]["id"]) {
      this.updateAdminuser['parent_id'] = this.selected_parent[0]["id"];
    }
    console.log("UpdateUserPermissions", this.updateAdminuser);
    this.dataService.postAuth('api/updateadminuser', this.updateAdminuser).subscribe((response) => {
      if (response.success == true) {
        console.log(response);
        this.message = response.message;
        //this.updateAdminuser = {};
        //this.updateAdminuser.permissions = [];
        //this.UserTypeSelected = "-- Select User Type --";
        //this.DisplayStatusSelected = "-- Select Display Status --";
      } else {
        console.log("Error while Adding user permissions ")
      }
    });
  }



}
