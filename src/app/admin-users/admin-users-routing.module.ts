import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewUserComponent } from './view-user/view-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AddNewUserComponent } from './add-new-user/add-new-user.component';

const routes: Routes = [
  { path: '', redirectTo: 'add-users', pathMatch: 'full', data: { title: 'Add Users' } },
  { path: 'add-users', component: AddUserComponent, data: { title: 'Add Users' } },
  { path: 'add-users/:userId', component: AddUserComponent, data: { title: 'Add Users' } },
  { path: 'add-new-users', component: AddNewUserComponent, data: { title: 'Add Users' } },
  { path: 'add-new-users/:userId', component: AddNewUserComponent, data: { title: 'Add Users' } },
  { path: 'view-users', component: ViewUserComponent, data: { title: 'View Users' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminUsersRoutingModule { }
