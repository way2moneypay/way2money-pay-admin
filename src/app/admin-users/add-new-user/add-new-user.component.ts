import { Component, OnInit, ViewContainerRef, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-new-user',
  templateUrl: './add-new-user.component.html',
  styleUrls: ['./add-new-user.component.css']
})
export class AddNewUserComponent implements OnInit {
  // user_details: any = {};
  // login_user_details: any = {};
  // page_loading = true;
  // validation_errors: any = {};
  // selected_user_type: any = [];
  // selected_parent: any = [];
  // password_type: string = "password";
  // constructor(
  //   private dataService: RestServicesService,
  //   private route: ActivatedRoute,
  //   public active_route: ActivatedRoute,
  //   private router: Router,
  //   private _location: Location,
  //   private modalService: NgbModal,
  //   private ngZone: NgZone,
  // ) { }
  // namePattern = "^[a-zA-Z][a-zA-Z0-9 ]+$";
  // emailPattern = "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
  // mobilePattern = "^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$";

  ngOnInit() {

  }
  // clickPasswordShow(type) {
  //   this.password_type = type;
  // }
}
