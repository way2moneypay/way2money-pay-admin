import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminUsersRoutingModule } from './admin-users-routing.module';
import { AddUserComponent } from './add-user/add-user.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { AddNewUserComponent } from './add-new-user/add-new-user.component';
@NgModule({
  imports: [
    CommonModule,
    AdminUsersRoutingModule,
    FormsModule,
    NgbModule.forRoot(),
    MultiselectDropdownModule,
    AngularMultiSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
    ReactiveFormsModule
  ],
  declarations: [AddUserComponent, ViewUserComponent, AddNewUserComponent],
})
export class AdminUsersModule { }
