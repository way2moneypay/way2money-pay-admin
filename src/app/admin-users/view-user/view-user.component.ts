import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {
  adminusers: any;
  closeResult: string;
  settarget: any = {};
  from_date: any
  to_date: any;
  date: string;
  previousUserID: any
  obj = {};
  message: any = null;
  tagget_data: any;
  claimRefId: any;
  user_data: any = {};
  admin_user_data: any = {};
  page: number = 1;
  collectionSize: Number = 5;
  pageSize: number = 15;
  skip: number;
  limit: number;
  maxSize: Number = 5;
  pageStartIndex: number;
  target_details: any;
  dataFound: boolean = false;
  constructor(
    private dataService: RestServicesService,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private ngZone: NgZone,
    private modalService: NgbModal,

  ) { }

  ngOnInit() {
    this.dataService.getAuth('api/getAdminUsersList').subscribe((response) => {
      if (response.success == true) {
        console.log(response);
        this.adminusers = response.data;
        // this.tagget_data = this.adminusers.target_details
      } else {
        console.log("Error While Getting AdminUsers list");
      }
    });
  }
  editData(user) {
    // alert(JSON.stringify(user));
    this.router.navigate(['/users/add-new-user/' + user._id]);
  }
  SetTarget() {
    var fromd = this.settarget.from_date.year + "-" + this.settarget.from_date.month + "-" + this.settarget.from_date.day;
    var todate = this.settarget.to_date.year + "-" + this.settarget.to_date.month + "-" + this.settarget.to_date.day;
    var fromDate = new Date(fromd).setHours(0, 0, 0, 0);
    var toDate = new Date(todate).setHours(23, 59, 59, 999);
    this.obj = {
      target_Fromdate: fromDate / 1,
      target_Todate: toDate
    }
    let reqObj = {}
    reqObj['settarget'] = this.settarget
    reqObj['dateobj'] = this.obj
    this.dataService.postAuth('api/setTarget', reqObj).subscribe((response) => {
      if (response.success == true) {
        // console.log(response);
        this.settarget = {}
        this.message = "successfully set target"
      } else {
        console.log("Error While Getting set taret");
        this.message = "Error While Getting set taret"
      }
    });
  }

  open(content) {
    this.popupCalls(content);
  }
  cancel() {
  }
  TemplateEdit(content, data) {
    // console.log("content", content);
    // console.log("data", data);
    this.settarget = data;
    // console.log(",",this.userpermissions);
    this.popupCalls(content);
  }

  popupCalls(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  getTargrtData(user_details) {
    this.target_details = []
    // console.log("user_details:: ", user_details);
    this.admin_user_data = {
      "user_id": user_details._id
    }
    this.onClickSubmit(this.admin_user_data, 1);
    // this.setTransactionPage(1);
  }
  onClickSubmit(data, page) {
    this.page = page;
    let reqDataObj = {}
    if (data === 'pagination') {
      reqDataObj['user_id'] = this.previousUserID;
    } else {
      if (data && data.user_id) {
        reqDataObj['user_id'] = data.user_id;
        this.previousUserID = data.user_id
      }
    }
    reqDataObj['skip'] = (this.page - 1) * this.pageSize;
    reqDataObj['limit'] = this.pageSize;
    this.dataService.getAuthParams("adminuser/getTargetData", reqDataObj).subscribe(result => {
      if (result.status) {
        if (result.data.results.length > 0) {
          this.dataFound = true;
          this.collectionSize = result.data.collectionSize;
          console.log("fetched", this.collectionSize, result);
          this.target_details = result.data.results;
          this.pageStartIndex = ((this.page - 1) * (this.pageSize));
        } else {
          alert('No Data found')
        }

      }
    })
  }
}
