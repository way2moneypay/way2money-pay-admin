import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MerchnatCommissionsComponent } from './merchnat-commissions/merchnat-commissions.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CookieService } from 'ngx-cookie-service';
import { PermissionDeniedComponent } from './permission-denied/permission-denied.component';
// import { DatepickerModule } from 'angular-mat-datepicker';


@NgModule({
  declarations: [
    AppComponent,
    MerchnatCommissionsComponent,
    PermissionDeniedComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    // DatepickerModule
  ],
  providers: [AuthGuard, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
