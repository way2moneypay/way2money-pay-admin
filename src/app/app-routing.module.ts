import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from './shared/guard/auth.guard';
import { PermissionDeniedComponent } from './permission-denied/permission-denied.component';
const routes: Routes = [
    {
        path: '',
        loadChildren: './pages/pages.module#PagesModule',
        canActivate: [AuthGuard]
    },
    { path: 'permission-denied', component: PermissionDeniedComponent },
    {
        path: 'login',
        loadChildren: './login/login.module#LoginModule'
    },
    { path: '**', redirectTo: '' }

];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true }), NgbModule.forRoot()],
    exports: [RouterModule]
})
export class AppRoutingModule { }
