import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { PagerService } from '../../shared/pagination/pager.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pipe, PipeTransform } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-view-merchants',
  templateUrl: './view-merchants.component.html',
  styleUrls: ['./view-merchants.component.css']
})
export class ViewMerchantsComponent implements OnInit {
  display_status_obj: { id: any; display_status: any; };
  searchWithStatus: number;
  setStatusErrMessage: string;
  superAdmin: boolean;
  user_type: any;
  usersList: any;
  users_data_found: boolean;
  show_user_list: boolean;
  paramsData: any = {};

  constructor(private apiService: RestServicesService,
    private pagerservice: PagerService, private router: Router, private route: ActivatedRoute) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        if (this.searchStatus) {
          this.setSearchPage(this.currentPage);
        } else {
          this.getCount();
        }
      }
    });
  }
  merchant_brand_array: any = [];
  qr_download: String = environment.qr_download;
  public model: any;
  BrandsAutofillData = [];
  resetButtonStatus: any = false;
  searchMerchantStatus: any = false;
  countStatus: any = false;
  merchantDetails: any = [];
  merchantsLength: number;
  newMobile = "";
  changeButton = false;
  currentPage: number = 1;
  paginationStatus = false;
  searchString: any = "";
  searchVariable = 'brand_name';
  defaultMStatusMsg: any = "Select Status";
  searchStatus: boolean = false;
  pager: any;
  i: number = 0;
  findStatus: any;
  cpage: any;
  pagedItems: any;
  InputAuotfill: any;
  currentMobileNo: any;
  showModal = false;
  changedStatus = false;
  dismissModal = false;
  changingId: any;
  b_name: any;
  InputAutoFill: any;
  brands_data: any;
  merchant_id: any;
  statusOptions = [
    { value: 'Active' },
    { value: 'Inactive' }
  ]
  merchant_status_obj: any
  ngOnInit() {
    this.getCount();
    this.getBrandsList();
    this.getUsersList();
    this.apiService.getAuth("api/user/authentication_info_call").subscribe(result => {
      console.log(result)
      if (result.status == true) {
        if (result.data.user_type == 'super_admin') {
          this.user_type = result.data.user_type
        } else if (result.data.user_type == 'regional_head') {
          this.user_type = result.data.user_type
        } else if (result.data.user_type == 'sales_team') {
          this.user_type = result.data.user_type
        }
      } else {
        // console.log("response", result)
      }
    })
  }
  MyMethod(data_id, user_id) {
    this.paramsData = {};
    // console.log(data_id, user_id);
    if (user_id && data_id) {
      console.log("SELECTED USER", data_id, user_id);
      this.paramsData.user_id = user_id;
      this.paramsData.data_id = data_id;
      this.apiService.getAuthParams('api/updateMerchants', this.paramsData).subscribe((response) => {
        if (response.status == true) {
          console.log('Successfully assigned to user');
          this.ngOnInit();
        } else {
          console.error('Error while assigning user');
        }
      })
    }
  }
  getUsersList() {
    this.apiService.getAuth("api/getadminusersdata").subscribe(result => {
      if (result.status === true) {
        this.usersList = result.data;
        console.log("result===>", this.usersList)
        this.show_user_list = true;
        if (this.usersList && this.usersList.length > 0) {
          this.users_data_found = true;
        } else {
          this.users_data_found = false;
        }
      } else {
        console.log("No users found")
        this.show_user_list = false;
      }
    })
  }
  /** Getting Brands Auotfill Data */
  getBrandsList() {
    this.apiService.getAuth("merchant/getBrandsList").subscribe(result => {
      if (result.message == 'success') {
        this.BrandsAutofillData = result.data;
        // console.log("brands", result.data);
      }
    });
    this.InputAuotfill = (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        map(term => term.length < 2 ? []
          : this.BrandsAutofillData.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
      );
  }
  getCount() {
    if (!this.countStatus) {
      this.apiService.getAuth("merchant/getCount").subscribe(result => {
        if (result.status) {
          this.merchantsLength = parseInt(result.data);
          this.searchStatus = false;
          // this.showModal =false;
          this.setMerchantPage(this.currentPage);
        } else {
          this.merchantsLength = 0;
        }
      });
    } else {
      this.setMerchantPage(this.currentPage);
    }
  }
  getMerchants(skip, limit) {
    this.apiService.postAuth("merchant/getMerchants", { skip: skip, limit: limit }).subscribe(result => {
      if (result.status) {
        this.pagedItems = result.data;
        console.log(this.pagedItems)
        // this.getStats(0);
      } else {
        this.paginationStatus = false;
      }
      this.resetButtonStatus = false;
    });
  }
  search() {
    this.searchMerchantStatus = true;
    var data = {};
    console.log(this.b_name)
    if (this.searchVariable == 'merchant_status' && this.searchString != 'Inactive' && this.searchString != 'Active') {
      this.searchMerchantStatus = false;
      this.setStatusErrMessage = 'Please select status.';
    } else {
      if (this.searchString == '' && this.b_name == '') {
        data = {};
      }
      if (this.searchVariable == 'merchant_status' && this.searchString === 'Inactive') {
        this.searchWithStatus = 0;
        data[this.searchVariable] = this.searchWithStatus;
      }
      if (this.searchVariable == 'merchant_status' && this.searchString === 'Active') {
        this.searchWithStatus = 1;
        data[this.searchVariable] = this.searchWithStatus;
      }
      if (this.searchVariable != 'merchant_status' && this.searchString) {
        data[this.searchVariable] = this.searchString;
      }
      //if (this.b_name) {
      //data["brand_name"] = this.b_name;
      //}
      if (this.merchant_id) {
        data["_id"] = this.merchant_id;
      }
      this.apiService.postAuth("merchant/search", data).subscribe(result => {
        if (result.status) {
          this.merchantDetails = result.data;
          this.merchantsLength = this.merchantDetails.length;
          this.searchStatus = true;
          this.setStatusErrMessage = '';
          this.setSearchPage(1);
        } else {
          this.searchStatus = false;
          this.findStatus = false;
          this.paginationStatus = false;
        }
        this.searchMerchantStatus = false;
      });
    }
  }
  // getStats(length) {
  //   if (length >= this.pagedItems.length) {
  //     return 1;
  //   } else {
  //     this.apiService.post("merchant/merchantStats", { "id": this.pagedItems[length]._id }).subscribe(result => {
  //       if (result.status) {
  //         if (result.data.length) {
  //           this.pagedItems[length]['transactions'] = result.data[0].total_transactions;
  //           this.pagedItems[length]["amount"] = result.data[0].total_amount;
  //         } else {
  //           this.pagedItems[length]["transactions"] = 0;
  //           this.pagedItems[length]["amount"] = 0;
  //         }
  //         this.getStats(length + 1);
  //       }
  //     });
  //   }
  // }
  reset() {
    this.resetButtonStatus = true;
    this.getCount();
    this.searchString = "";
    this.setStatusErrMessage = '';
    this.searchStatus = false;
    this.b_name = '';

  }
  selectedItem(item) {
    console.log("ITEM", item);
    this.b_name = item.data.brand_name;
    this.merchant_id = item.data._id;
    this.merchant_brand_array.length = 0;
    this.merchant_brand_array = [];
    console.log("HELLO MERCHANT", this.merchant_id);

  }
  setPage(page) {
    this.currentPage = page;
    this.cpage = 'merchant/view-merchant/' + page;
    this.router.navigate([this.cpage]);

  }
  convertInt(page) {
    return parseInt(page);
  }
  setMerchantPage(page) {
    if (this.merchantsLength) {
      if (this.merchantsLength > 10) {
        this.findStatus = true;
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(Number(this.merchantsLength), Number(page), 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getMerchants(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      } else {
        this.getMerchants(0, this.merchantsLength);
        this.findStatus = true;
        this.currentPage = 1;
        this.paginationStatus = false;
      }
    } else {
      this.findStatus = false;
    }
  }
  setSearchPage(page) {
    if (this.merchantsLength) {
      if (this.merchantsLength > 10) {
        this.findStatus = true;
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(this.merchantsLength, page, 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.pagedItems = this.merchantDetails.slice(this.pager.startIndex, this.pager.endIndex + 1);
        // this.getStats(0);
      } else {
        this.currentPage = 1;
        this.pagedItems = this.merchantDetails;
        // this.getStats(0);
        this.findStatus = true;
        this.paginationStatus = false;
      }
    } else {
      this.findStatus = false;
    }
  }
  changeMobileNo(id, mobile) {
    this.showModal = true;
    this.currentMobileNo = mobile;
    this.changingId = id;

  }
  change(close) {
    this.changeButton = true;
    this.apiService.postAuth("merchant/changeMobile", { id: this.changingId, old_number: this.currentMobileNo, new_number: this.newMobile }).subscribe(result => {
      if (result.status) {
        // this.showModal=false;
        alert("updated successfully");
        this.changeButton = false;
        this.getCount();
        this.newMobile = null;
        close.click();
      } else {
        alert(result.message);
        this.changeButton = false;
      }
    });
  }
  cancel() {
    this.newMobile = null;
  }
  addSubmerchant(close) {
    console.log("Entered")
    this.changeButton = true;
    this.apiService.postAuth("merchant/add-submerchant", { id: this.changingId, old_number: this.currentMobileNo, new_number: this.newMobile }).subscribe(result => {
      if (result.status) {
        alert("updated successfully");
        this.changeButton = false;
        this.getCount();
        this.newMobile = null;
        close.click();
      } else {
        alert(result.message);
        this.changeButton = false;
      }
    })
  }
  checkMobileNumber() {
    if (this.newMobile != "" && this.newMobile != null && this.newMobile != undefined) {
      if (this.newMobile.length != 10) {
        this.newMobile = null;
        alert("mobile number length should be 10");
      } else {
        this.apiService.postAuth("merchant/submerchant", { new_number: this.newMobile }).subscribe(result => {
          if (result.status) {
            this.newMobile = null;
            alert(result.message);
          }
        })
      }
    } else {
      alert("Enter mobile number");
    }

  }
  merchantStatusVerify(sid, status) {
    // alert(status)
    this.merchant_status_obj = {
      id: sid,
      merchant_status: status
    }
    console.log("this.merchant_status_obj", this.merchant_status_obj)
    this.apiService.postAuth("merchant/changeMerchantStatus", this.merchant_status_obj).subscribe(result => {
      if (result.status) {
        // this.showModal=false;
        alert("updated successfully");

      } else {
        alert(result.message);

      }
    });
  }

  merchantDisplayStatusVerify(merchant_id, status) {
    this.display_status_obj = {
      id: merchant_id,
      display_status: status
    }
    console.log(this.display_status_obj);
    this.apiService.postAuth("merchant/changeMerchantDisplayStatus", this.display_status_obj).subscribe(result => {
      if (result.success) {
        // this.showModal=false;
        // this.ngOnInit();
        alert("updated successfully");
      } else {
        alert(result.message);
      }
    });
  }
  // clearSearchString() {
  //   this.searchString = '';
  // }
  getAmount(amount) {
    return "10K";
  }

  getBrands(event, brand_name) {
    console.log(event.keyCode, brand_name);
    if (!brand_name) { this.brands_data = []; }
    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 65 || event.keyCode > 90) &&
      event.keyCode !== 32 && event.keyCode !== 8 && event.keyCode !== 190) {
      return;
    }


    if (event.keyCode === 13 || !brand_name || brand_name.length < 3) {
      this.merchant_brand_array = [];
      return;
    }
    const params: any = {
      brand_name: brand_name,
      limit: 6
    };

    // this.companies_loading = true;
    this.apiService.postAuth('user/searchingBrands', params)
      .subscribe((Response) => {
        console.log("SEARCH RESULTS", Response.data);
        // console.log("SEARCHING BRANDING COPYING TO ARRA", this.merchant_brand_array);
        if (Response.data) {
          this.merchant_brand_array = [];
          for (let i = 0; i < Response.data.length; i++) {
            this.merchant_brand_array.push({
              _id: Response.data[i]._id,
              brand_name: Response.data[i]._source.brand_name
            })
            // this.merchant_id_array[i] = Response.data[i]._id;
            // this.merchant_brand_array[i] = Response.data[i]._source.brand_name;
            // this.InputAutoFill[i] = Response.data[i]._source.brand_name;
          }
          // this.InputAutoFill = (text$: Observable<string>) =>
          //   text$.pipe(
          //     debounceTime(200),
          //     distinctUntilChanged(),
          //     map(term => term.length < 2 ? []
          //       : this.merchant_brand_array.filter(v => v.indexOf(term) > -1).slice(0, 10))
          //   );
          // console.log("SEARCHING _IDDSS COPYING TO ARRA", this.merchant_id_array.length);

        } else {
          // console.log('NO DATA FOUND BRANDS');
        }
      }, (error_data) => {
      });
  }
  redirect(merchantId) {
    // console.log("merchantid-------->",merchantId)

    // this.router.navigate(["view-claims/1?merchant_id="+merchantId])
    this.router.navigate(['/view-claims/1'], { queryParams: { merchant_id: merchantId } });
  }
}

