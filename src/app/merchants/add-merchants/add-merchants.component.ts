import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { AgmCoreModule, MapsAPILoader, MouseEvent } from '@agm/core';
import { Location } from '@angular/common';
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { environment } from '../../../environments/environment';
import { AbstractControl, ValidatorFn, FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { toDate } from '@angular/common/src/i18n/format_date';

@Component({
  selector: 'app-add-merchants',
  templateUrl: './add-merchants.component.html',
  styleUrls: ['./add-merchants.component.css']
})
export class AddMerchantsComponent implements OnInit {
  coordinates: ArrayBuffer;

  @ViewChild('companySearch') public companySearchRef: ElementRef;
  OnbrardDate: any = ""
  searchCompany: FormControl;

  latitude: number;
  longitude: number;
  zoom: number;
  company_address_map: any = {};
  maps_details: any = {};

  changeStatus: boolean = false;
  branch: any;
  bankId: any;
  cpageUpdate: any;
  displayImagesObj: any = {};
  verificationErr: string = null;
  check_leaf_file: FileList;
  pan_card_file: FileList;
  logo_url_file: FileList;
  min: any;
  max: any;
  commission: any;
  regMerchantForm = {};
  // commissions = {};
  regMerchantResponse: any = {};
  regCommissions = [];
  length = 0;
  numbers = [];
  singleCommission = {
    min_amount: 0,
    max_amount: 0,
    commission_rate: 0
  };
  merchantId;
  routeData;
  getMerchantApi;
  updateCommissionsArray = [];
  updateCommissionsArrayUpdate = [];
  updateMerchant: any = {};
  updateCommissionSubmit = false;
  onSubmitUpdateCommissions = false;
  cpage: any;
  cpageNum: any;
  admin_email;
  logoPath = environment.images_cdn;
  notValidImageType: boolean = false;
  addButtonOnceOnEdit: boolean = false;
  admin_verified: boolean = false;
  panPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  tinPattern = "[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}[Z][A-Z0-9]{1}";
  PaymentLifeCycleSelected = "-- Select Payment Life Cycle Mode --";
  PaymentLifeCycleOptions = ["Weekly", "No. of days"];
  payment_cycle: any;
  payment_mode_mon = false;
  payment_mode_tue = false;
  payment_mode_wed = false;
  payment_mode_thu = false;
  payment_mode_fri = false;
  payment_mode_sat = false;
  MerchantCategorySelected = "-- Select Merchant Category --";
  MerchantOptions = [
    "Food & Beverages",
    "Groceries",
    "Apparel",
    "Sports & Wellness",
    "Spa & Saloon",
    "Pharmacy",
    "Entertainment",
    "Clinics & Labs",
    "Home Appliances",
    "Electrical Appliances",
    "Other"
  ];
  BankCategorySelected = "-- Select Bank --";
  DisplayStatusSelected = "-- Select Display Status --";
  BankOptions: any;
  bank_details: any;
  statusOptions = [
    { value: "Inactive" },
    { value: "Active" }
  ];

  is_admin_verified: any
  bank_verify: any
  pan_verify: any
  admin_verify: any


  bank_details_show = false
  bank_details_hide = false
  pan_details_show = false
  pan_details_hide = false

  InputAuotfill: any;
  BranchesAutofillData: any;
  bank_ifsc_data: any = [];
  // uploadImage: FileList;
  cropModelShow: boolean = false;
  uploadImageTypeBeforeCrop: any;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private dataService: RestServicesService,
    private route: ActivatedRoute,
    private router: Router, private _location: Location) { }
  namePattern = "^[a-zA-Z][a-zA-Z0-9 ]+$";
  emailPattern = "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
  mobilePattern = "^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$";
  // companyPattern="^[A-Z]([a-zA-Z0-9]|[- @\.#&!])*$";
  // urlPattern="^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$";
  // passwordPattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
  ngOnInit() {
    this.searchCompany = new FormControl();
    setTimeout(() => {
      this.setLocation();
    }, 100);
    this.merchantId = this.route.snapshot.params.merchantId;
    this.cpageUpdate = this.route.snapshot.params.cpage;
    this.admin_email = localStorage.getItem("email");
    this.getPaymentSettings();
    // this.getBankDetails();
    // this.selectBankDetails("5b127b4b7b1b7657a6a665bc");
    if (this.merchantId) {
      this.addButtonOnceOnEdit = true;
      this.getMerchantApi = 'api/getMerchant/' + this.merchantId;
      this.dataService.getAuth(this.getMerchantApi).subscribe(merchantDetails => {
        console.log(merchantDetails)
        if (merchantDetails.success) {
          this.updateCommissionsArray = merchantDetails.responseObj.commissions;
          if (this.updateCommissionsArray.length == 0) {
            this.addCommissionField();
          }
          var fromdate = new Date(merchantDetails.responseObj.profile.onboard_date)
          var from_day = {}
          from_day = {
            year: fromdate.getFullYear(),
            month: fromdate.getMonth() + 1,
            day: fromdate.getDate()
          }
          this.OnbrardDate = from_day
          this.updateMerchant = merchantDetails.responseObj.profile;
          // console.log(this.updateMerchant);
          // if(this.updateMerchant['logo_url'])
          //   this.displayImagesObj['logo_url'] = this.updateMerchant['logo_url']+ '?q=' + Math.random();
          // if(this.updateMerchant['pan_card'])
          //   this.displayImagesObj['pan_card'] = this.updateMerchant['pan_card']+ '?q=' + Math.random();
          // if(this.updateMerchant['check_leaf'])
          //   this.displayImagesObj['check_leaf'] = this.updateMerchant['check_leaf']+ '?q=' + Math.random();

          /** Verify variables*/
          this.updateMerchant.bank_details == 2 ? this.bank_verify = true : this.bank_verify = false
          this.updateMerchant.bank_details == 0 ? this.bank_details_hide = true : this.bank_details_show = true

          this.updateMerchant.pan_details == 3 ? this.pan_verify = true : this.pan_verify = false
          this.updateMerchant.pan_details == 0 || this.updateMerchant.pan_details == 1 ? this.pan_details_hide = true : this.pan_details_show = true

          this.updateMerchant.is_admin_verified ? this.is_admin_verified = true : this.is_admin_verified = false

          /** Select box variables */
          this.updateMerchant.merchant_category ? this.MerchantCategorySelected = this.updateMerchant.merchant_category : this.MerchantCategorySelected = "-- Select Merchant Category --";

          /** Checking whether bank is in BankOptions */
          // if ($.inArray(this.updateMerchant.bank_name, this.BankOptions) != -1) {
          //   this.updateMerchant.bank_name ? this.BankCategorySelected = this.updateMerchant.bank_name : this.BankCategorySelected = "-- Select Bank --";
          // }
          // else this.BankCategorySelected = this.updateMerchant.bank_name != undefined ? this.updateMerchant.bank_name : '-- Select Bank --';

          if (this.updateMerchant.pay_cycle_mode)
            if (Array.isArray(this.updateMerchant.pay_cycle_mode['payment_cycle'])) {
              this.PaymentLifeCycleSelected = "Weekly";
              this.payment_mode_mon = this.updateMerchant.pay_cycle_mode['payment_cycle'][1]
              this.payment_mode_tue = this.updateMerchant.pay_cycle_mode['payment_cycle'][2]
              this.payment_mode_wed = this.updateMerchant.pay_cycle_mode['payment_cycle'][3]
              this.payment_mode_thu = this.updateMerchant.pay_cycle_mode['payment_cycle'][4]
              this.payment_mode_fri = this.updateMerchant.pay_cycle_mode['payment_cycle'][5]
              this.payment_mode_sat = this.updateMerchant.pay_cycle_mode['payment_cycle'][6]
            } else {
              this.PaymentLifeCycleSelected = "No. of days";
              this.payment_cycle = this.updateMerchant.pay_cycle_mode['payment_cycle'];
            }

          this.updateMerchant['merchant_status'] = this.updateMerchant.merchant_status == 0 ? "Inactive" : "Active";
          this.updateMerchant['display_status'] ? this.DisplayStatusSelected = this.updateMerchant['display_status'] : this.DisplayStatusSelected = '-- Select Display Status --'
          this.updateMerchant['admin_email'] = this.admin_email;
          this.updateMerchant['is_admin_verified'] = merchantDetails.responseObj.profile.is_admin_verified;
          this.admin_verified = merchantDetails.responseObj.profile.is_admin_verified;
        }
      });
    } else {

      this.addCommissionField();
      this.updateMerchant = {};
      // this.updateMerchant['merchant_category'] = "-- Select Merchant Category --";
      // this.updateMerchant['display_status'] = "-- Select Display Status --";
      this.updateMerchant['admin_email'] = this.admin_email;
      this.updateMerchant['is_admin_verified'] = false;
      this.updateCommissionsArray = [];
      // this.updateCommissionSubmit = true;

    }
  }
  getKeys(data) {
    return Object.keys(data);
  }
  backClicked() {
    this.cpage = this.router.routerState.snapshot.url;
    this.cpageNum = this.route.snapshot.params.cpage;
    this.cpage = '/merchant/view-merchant/' + this.cpageNum;
    this.router.navigate([this.cpage]);
    // this._location.back();
  }

  onSubmitCommissions(commissionsValues) {
    // console.log("mer :: ", commissionsValues);
    this.regMerchantResponse = {};
    this.updateCommissionSubmit = true;
    if (commissionsValues.commission_rate0 == '') {
      commissionsValues.commission_rate0 = 1;
    }
    if (commissionsValues.min_amount0 == '') {
      commissionsValues.min_amount0 = 1;
    }
    if (commissionsValues.max_amount0 == '') {
      commissionsValues.max_amount0 = 100000;
    }
    if (commissionsValues.commission_rate0 > 0) {
      // console.log("commission values ", commissionsValues);
      for (let i = 0; i < this.length; i++) {
        this.min = 'min_amount' + i;
        this.max = 'max_amount' + i;
        this.commission = 'commission_rate' + i;
        this.singleCommission['min_amount'] = commissionsValues[String(this.min)] != null ? commissionsValues[String(this.min)] : 0;
        this.singleCommission['max_amount'] = commissionsValues[String(this.max)] != null ? commissionsValues[String(this.max)] : 10000000;
        this.singleCommission['commission_rate'] = commissionsValues[String(this.commission)] ? commissionsValues[String(this.commission)] : '';
        this.updateCommissionsArray[this.updateCommissionsArray.length] = this.singleCommission;
        this.singleCommission = { min_amount: 0, max_amount: 0, commission_rate: 0 };
      }
    }
    /** Adding two way binding variables to form  */
    this.updateMerchant['regCommissions'] = this.updateCommissionsArray;
    if (this.MerchantCategorySelected != 'Other') {
      this.updateMerchant['merchant_category'] = this.MerchantCategorySelected;
    }
    // this.updateMerchant['bank_name'] = this.BankCategorySelected;

    /** Initiall stored defaul keys to display -- select options --
     *  Deleting if defaul keys exists beforing inserting */
    if (this.updateMerchant['merchant_category'] == "-- Select Merchant Category --")
      delete this.updateMerchant['merchant_category']

    if (this.DisplayStatusSelected != "-- Select Display Status --") {
      this.updateMerchant['display_status'] = this.DisplayStatusSelected;
    }
    // if (this.updateMerchant['bank_name'] == "-- Select Bank --")
    // delete this.updateMerchant['bank_name']

    // if (this.updateMerchant['bank_name'] == "-- Select Bank --")
    // delete this.updateMerchant['bank_name']

    if (this.PaymentLifeCycleSelected == 'Weekly') {
      let payment_cycle = [false, this.payment_mode_mon, this.payment_mode_tue, this.payment_mode_wed, this.payment_mode_thu, this.payment_mode_fri]
      this.updateMerchant['pay_cycle_mode'] = { payment_cycle: payment_cycle }
    } else if (this.PaymentLifeCycleSelected == 'No. of days') this.updateMerchant['pay_cycle_mode'] = { payment_cycle: this.payment_cycle }

    this.dataService.postAuth('api/merchant/register', this.updateMerchant).subscribe(response => {
      if (response && response.success) {
        this.regMerchantResponse = response;
        for (let i = 0; i < 3; i++) {
          if (i == 0 && this.logo_url_file != undefined) {
            this.uploadImageToDB(this.logo_url_file, 'logo_url');
          } else if (i == 1 && this.pan_card_file != undefined) {
            this.uploadImageToDb(this.pan_card_file, 'pan_card');
          } else if (i == 2 && this.check_leaf_file != undefined) {
            this.uploadImageToDb(this.check_leaf_file, 'check_leaf');
          }
        }
        this.router.navigate(['/merchant/view-merchant/1']);
      } else {
        this.regMerchantResponse = response;
      }
    });
  }
  onSubmitCommissionsUpdate(commissionsValues) {
    // this.regMerchantResponse = {};
    // this.updateCommissionSubmit = true;
    if (commissionsValues.commission_rate0 > 0) {
      for (let i = 0; i < this.length; i++) {
        this.min = 'min_amount' + i;
        this.max = 'max_amount' + i;
        this.commission = 'commission_rate' + i;
        // console.log('Min values : ', regMerchantFormValues[String(this.min)]);
        this.singleCommission['min_amount'] = commissionsValues[String(this.min)] != null ? commissionsValues[String(this.min)] : 0;
        this.singleCommission['max_amount'] = commissionsValues[String(this.max)] != null ? commissionsValues[String(this.max)] : 10000000;
        this.singleCommission['commission_rate'] = commissionsValues[String(this.commission)] ? commissionsValues[String(this.commission)] : '';
        this.updateCommissionsArrayUpdate[this.updateCommissionsArrayUpdate.length] = this.singleCommission;
        this.singleCommission = { min_amount: 0, max_amount: 0, commission_rate: 0 };
      }
      // console.log('regCommissions complete', this.regCommissions);
      // console.log('finalll', this.updateCommissionsArray);
    }
    this.updateMerchant['regCommissionsUpdate'] = this.updateCommissionsArrayUpdate;
    this.updateMerchant['regCommissions'] = this.updateCommissionsArray;
    this.updateMerchant['merchant_status'] = this.updateMerchant.merchant_status == "Inactive" ? 0 : 2;
    if (this.MerchantCategorySelected != 'Other') {
      this.updateMerchant['merchant_category'] = this.MerchantCategorySelected;
    }
    // this.updateMerchant['bank_name'] = this.BankCategorySelected;

    /** Initiall stored defaul keys to display -- select options --
     *  Deleting if defaul keys exists beforing inserting */
    if (this.updateMerchant['merchant_category'] == "-- Select Merchant Category --")
      delete this.updateMerchant['merchant_category'];


    if (this.DisplayStatusSelected != "-- Select Display Status --") {
      this.updateMerchant['display_status'] = this.DisplayStatusSelected;
    }
    // if (this.updateMerchant['bank_name'] == "-- Select Bank --")
    //   delete this.updateMerchant['bank_name']

    /** Intiliazing pay_cycle_mode */
    if (this.PaymentLifeCycleSelected == 'Weekly') {
      let payment_cycle = [false, this.payment_mode_mon, this.payment_mode_tue, this.payment_mode_wed, this.payment_mode_thu, this.payment_mode_fri, this.payment_mode_sat]
      this.updateMerchant['pay_cycle_mode'] = { payment_cycle: payment_cycle }
    } else this.updateMerchant['pay_cycle_mode'] = { payment_cycle: this.payment_cycle }
    if (this.OnbrardDate && this.OnbrardDate != "") {
      let newOnboarddate = new Date(this.OnbrardDate.year, this.OnbrardDate.month - 1, this.OnbrardDate.day).setHours(6, 30, 0, 0);
      this.updateMerchant["onboard_date"] = newOnboarddate / 1;
    }
    console.log('Onboarddate==>>', this.updateMerchant.onboard_date)
    this.dataService.postAuth('api/merchant/update', this.updateMerchant).subscribe(response => {
      if (response && response.success) {
        this.regMerchantResponse = response;
        for (let i = 0; i < 3; i++) {
          console.log("Images uploading :: ");
          if (i == 0 && this.logo_url_file != undefined) {
            console.log("logo uploading start")
            this.uploadImageToDB(this.logo_url_file, 'logo_url');
          } else if (i == 1 && this.pan_card_file != undefined) {
            this.uploadImageToDb(this.pan_card_file, 'pan_card');
          } else if (i == 2 && this.check_leaf_file != undefined) {
            this.uploadImageToDb(this.check_leaf_file, 'check_leaf');
          }
        }
        alert("Merchat Details Successfully Updated");
        this.router.navigate(['/merchant/view-merchant/' + this.cpageUpdate]);
      } else {
        this.regMerchantResponse = response;
      }
    });
  }
  onVerifyMerchant() {
    this.updateMerchant['merchant_status'] = this.updateMerchant.merchant_status == "Inactive" ? 0 : 2;
    this.dataService.postAuth('api/merchant/update', this.updateMerchant).subscribe(response => {
      if (response && response.success) {
        this.regMerchantResponse = response;
        this.length = this.length - 1;
        this.ngOnInit();
      } else {
        this.regMerchantResponse = response;
      }
    });
  }
  addCommissionField() {
    this.addButtonOnceOnEdit = false;
    this.updateCommissionSubmit = false;
    this.length++;
    this.numbers = Array.from(Array(this.length)).map((x, i) => i);
  }
  removeCommissionField() {
    this.length--;
    if (this.length == 0) {
      this.addButtonOnceOnEdit = true;
    }
    this.numbers = Array.from(Array(this.length)).map((x, i) => i);
  }

  // Cropiamge(files: FileList, image_type) {
  //   if (image_type == 'logo_url') {
  //     this.logo_url_file = files;
  //   } else if (image_type == "pan_card") {
  //     this.pan_card_file = files;
  //   } else if (image_type == "check_leaf") {
  //     this.check_leaf_file = files;
  //   }
  //   this.cropModelShow = true;
  // }


  uploadLogo(files: FileList, image_type) {
    if (image_type == "pan_card") {
      this.pan_card_file = files;
    } else if (image_type == "check_leaf") {
      this.check_leaf_file = files;
    }
  }

  uploadImage() {
    if (this.uploadImageTypeBeforeCrop == 'logo_url') {
      this.logo_url_file = this.croppedImage;
      this.cropModelShow = false;
    }
    // this.file = files.item(0);
  }
  uploadImageToDB(files, image_type) {
    const valid_types = ['application/pdf', 'application/vnd.ms-excel', 'image/png', 'image/x-png', 'image/jpeg', 'image/jpg'];
    const url = 'api/uploadDoc';
    const file = files;
    if (valid_types.indexOf(file.type) === -1) {
      this.notValidImageType = true;
    } else {
      this.notValidImageType = false;
      const file_key = 'file';
      const params: any = {};
      params.file_name = this.regMerchantResponse.merchant._id;
      params.image_type = image_type;
      params.file_extension = "." + file.type.split('/')[1];
      // console.log("params.file_extension : ", params.file_extension);
      // params.file_extension = file.name.substr(file.name.lastIndexOf('.'), file.name.length);
      // params.old_file = this.updateMerchant['logo'];
      const progress = false;
      if (file != null && file !== undefined) {
        this.dataService.upload(url, file, file_key, params, progress).subscribe(result => {
          if (result) {
            // console.log("result", result);
            // this.updateMerchant['logo'] = result.file_name;
            // 'file_name_logo' + params.file_extension + '?q=' + Math.random();
            // this.updateMerchant[image_type] = this.logoPath + this.regMerchantResponse.merchant.mobile + '_' + image_type + params.file_extension + '?q=' + Math.random();
            // this.ngOnInit();
            // console.log("this------>",this.updateMerchant[image_type]);
          }
        });
      }
    }
  }

  uploadImageToDb(files: FileList, image_type) {
    console.log("uploading pan card ::");
    const valid_types = ['application/pdf', 'application/vnd.ms-excel', 'image/png', 'image/x-png', 'image/jpeg', 'image/jpg'];
    const url = 'api/uploadDoc';
    const file = files[0];
    if (valid_types.indexOf(file.type) === -1) {
      this.notValidImageType = true;
    } else {
      this.notValidImageType = false;
      const file_key = 'file';
      const params: any = {};
      params.file_name = this.regMerchantResponse.merchant._id;
      params.image_type = image_type;
      params.file_extension = file.name.substr(file.name.lastIndexOf('.'), file.name.length);
      // params.file_extension = "." + file.type.split('/')[1];
      // console.log("params.file_extension : ", params.file_extension);
      // params.old_file = this.updateMerchant['logo'];
      const progress = false;
      if (file != null && file !== undefined) {
        console.log("file exists");
        this.dataService.upload(url, file, file_key, params, progress).subscribe(result => {
          if (result) {
            console.log("pan or check uploaded successfully");
            // this.updateMerchant['logo'] = result.file_name;
            // 'file_name_logo' + params.file_extension + '?q=' + Math.random();
            // this.updateMerchant[image_type] = this.logoPath + this.regMerchantResponse.merchant.mobile + '_' + image_type + params.file_extension + '?q=' + Math.random();
            // this.ngOnInit();
            // console.log("this------>",this.updateMerchant[image_type]);
          }
        });
      }
    }
  }

  getRandomNumber() {
    return Math.random();
  }
  getPaymentSettings() {
    this.dataService.getAuth('view-settingss').subscribe(result => {
      if (result) {
        console.log(result, "%%%%%%%%%%%%%%%5555")
        this.updateMerchant['token_validity'] = result.data.token_validity;
      }
    });
  }
  bankVerify() {
    if (!this.bank_verify) {
      if (this.updateMerchant.bank_details === 1) {
        this.bank_verify = !this.bank_verify
        this.updateMerchant['bank_details'] = 2;
      }
    } else {
      this.updateMerchant['bank_details'] = 1;
      this.bank_verify = !this.bank_verify
    }
    this.onVerifyMerchant();
  }
  panVerify(value) {
    if (!this.pan_verify) {
      if (this.updateMerchant.pan_details === 2) {
        this.pan_verify = !this.pan_verify
        this.updateMerchant['pan_details'] = 3;
      }
    } else {
      this.updateMerchant['pan_details'] = 2;
      this.pan_verify = !this.pan_verify
    }
    this.onVerifyMerchant();
  }
  verifyMerchant() {
    if (!this.is_admin_verified) {
      this.admin_verified = true
      this.is_admin_verified = !this.is_admin_verified
      this.updateMerchant['is_admin_verified'] = true;
    } else {
      this.admin_verified = false
      this.is_admin_verified = !this.is_admin_verified
      this.updateMerchant['is_admin_verified'] = false;
    }
    this.onVerifyMerchant();
  }
  alertverify(value) {
    if (value == 1)
      this.verificationErr = 'Please enter Bank details to verify';
    else if (value == 2) {
      if (this.updateMerchant.pan_details == 1) this.verificationErr = 'Please upload Pan card to verify';
      else this.verificationErr = 'Please enter Pan card details to verify';
    }
  }

  changeValue() {
    this.changeStatus = true;
  }

  ifscSelect(IFSC) {
    // console.log("IFSC :: ", IFSC);
    this.updateMerchant['ifsc_code'] = IFSC.data.ifsc_code;
    this.updateMerchant['bank_name'] = IFSC.data.bank_name;
    this.updateMerchant['branch'] = IFSC.data.branch_name;
    this.updateMerchant['bank_id'] = IFSC.data.bank_id;
    this.bank_ifsc_data.length = 0;
  }

  getBankIfsc(event, ifsc_code, callback) {
    // console.log(event.keyCode);
    if (!ifsc_code) {
      this.bank_ifsc_data.length = 0;
    }
    if ((event.keyCode < 48 || event.keyCode > 57) &&
      (event.keyCode < 65 || event.keyCode > 90) &&
      event.keyCode !== 32 && event.keyCode !== 8 &&
      event.keyCode !== 190) {
      return;
    }
    if (event.keyCode === 13 || !ifsc_code || ifsc_code.length < 3) {
      return;
    }
    // console.log("ifsc_code br :: ", typeof(ifsc_code));
    const params: any = {
      ifsc_code: ifsc_code + "",
      limit: 6
    };
    this.dataService.getAuthParams('api/bank_ifsccodes', params)
      .subscribe(bank_response => {
        if (bank_response.success) {
          this.bank_ifsc_data = bank_response.banks_ifsc_data;
          console.log("IFSC", this.bank_ifsc_data);
          if (callback) {
            callback();
          }
          return;
        }
        // this.toaster.error('Error while getting bank IFSC.');
        if (callback) {
          callback();
        }
      }, (error_data) => {
        // this.toaster.error('Something went wrong while getting bank IFSC . Please try again.');
        if (callback) {
          callback();
        }
      });
  }


  private setLocation() {
    this.setCurrentPosition();

    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {


      const autocomplete = new google.maps.places.Autocomplete(this.companySearchRef.nativeElement, {
        types: []
      });



      autocomplete.addListener('place_changed', () => {

        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude and zoom
          // console.log('place_address_components', place.address_components);
          // console.log('formatted_address', place.formatted_address);
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          // this.company_address_map.formatted_address = place.formatted_address.trim();


          this.setCompanyAddress(place);
          // if(this.company_address_map)
          this.AddressSaved();
        });
      });

    });
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }
  setCompanyAddress(place) {
    this.company_address_map = {};
    this.company_address_map.formatted_address = place.formatted_address.trim();
    this.company_address_map.latitude = place.geometry.location.lat();
    this.company_address_map.longitude = place.geometry.location.lng();

    place.address_components.forEach(addr => {
      addr.types.forEach(type => {
        this.company_address_map.formatted_address = place.formatted_address;
        if (type === 'locality') {
          this.company_address_map.city = addr.long_name;
        } else if (type === 'administrative_area_level_2') {
          this.company_address_map.district = addr.long_name;
        } else if (type === 'administrative_area_level_1') {
          this.company_address_map.state = addr.long_name;
        } else if (type === 'country') {
          this.company_address_map.country = addr.long_name;
        } else if (type === 'postal_code') {
          this.company_address_map.pin_code = addr.long_name;
          this.company_address_map.search_name = addr.long_name;
          this.company_address_map.search_name_dup = addr.long_name;
        } else if (type === 'sublocality_level_1') {
          this.company_address_map.locality = addr.long_name;
        } else if (type === 'sublocality_level_2') {
          this.company_address_map.street_1 = addr.long_name;
        } else if (type === 'sublocality_level_3') {
          this.company_address_map.street_location = addr.long_name;
        } else if (type === 'premise') {
          this.company_address_map.exist_location = addr.long_name;
        } else if (type === 'street_number') {
          this.company_address_map.street_number = addr.long_name;
        }
      });
    });

    this.company_address_map.street = '';

    if (this.company_address_map.street_number) {
      this.company_address_map.street += this.company_address_map.street_number + ', ';
    }
    if (this.company_address_map.exist_location) {
      this.company_address_map.street += this.company_address_map.exist_location + ', ';
    }
    if (this.company_address_map.street_location) {
      this.company_address_map.street += this.company_address_map.street_location + ', ';
    }
    if (this.company_address_map.street_1) {
      this.company_address_map.street += this.company_address_map.street_1;
    }

    // console.log(this.company_address_map);
    // this.company_address_map.name = place.name;
    // let street = place.formatted_address.split(",");
    // if (street.length >= 3) {
    //   this.company_address_map.street = this.getStreet(street, this.company_address_map.city, this.company_address_map.state, this.company_address_map.country, this.company_address_map.locality, this.company_address_map.district);
    // } else {
    //   this.company_address_map.street = "";
    // }
  }
  public AddressSaved() {

    if (this.company_address_map && this.company_address_map.country && this.company_address_map.country.toLowerCase() == "india") {
      // this.loan_details.company_address = JSON.parse(JSON.stringify(this.company_address_map));
      this.updateMerchant.formatted_address = this.company_address_map.formatted_address;
      // console.log('maps selected duploadImgaeata',this.updateMerchant.formatted_address);
      this.updateMerchant['company_address'] = this.company_address_map;
      this.updateMerchant['locations'] = {};
      // this.updateMerchant.locations['type'] = "Point";
      // this.coordinates = this.company_address_map.longitude != undefined ? this.company_address_map.longitude : '';
      // this.coordinates = this.company_address_map.latitude != undefined ? this.company_address_map.latitude : '' ;
      // this.updateMerchant.locations['coordinates'] = this.coordinates;
      console.log(this.updateMerchant);

    } else {
      this.company_address_map = {};

      // this.toast.warning("Please select address in India.");
    }

  }

  imageChangedEvent: any = '';
  croppedImage: any = '';

  fileChangeEvent(event: any, image_type) {
    if (image_type == 'logo_url') {
      this.imageChangedEvent = event;
      this.uploadImageTypeBeforeCrop = image_type;
      // console.log(this.imageChangedEvent)
      this.cropModelShow = true;
    }
    if (image_type == 'pan_card') {
      this.imageChangedEvent = event;
      this.uploadImageTypeBeforeCrop = image_type;
      // console.log(this.imageChangedEvent)
      this.cropModelShow = true;
    }
    if (image_type == 'check_leaf') {
      this.imageChangedEvent = event;
      this.uploadImageTypeBeforeCrop = image_type;
      // console.log(this.imageChangedEvent)
      this.cropModelShow = true;
    }

    // this.imageChangedEvent = files;

  }
  imageCropped(image: String) {
    this.croppedImage = image;
    // console.log(image)

  }
  imageLoaded() {
    // console.log("ok")
  }
  loadImageFailed() {
    // show message
  }



}
