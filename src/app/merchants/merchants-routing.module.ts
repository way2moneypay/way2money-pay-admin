import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddMerchantsComponent } from './add-merchants/add-merchants.component';
import { ViewMerchantsComponent } from './view-merchants/view-merchants.component';
import { AuthGuard } from '../shared/guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'add-merchant', pathMatch: 'full', data: { title: 'Add Merchant' }, canActivate:[AuthGuard] },
  { path: 'add-merchant', component: AddMerchantsComponent, data: { title: 'Add Merchant' },canActivate:[AuthGuard]  },
  { path: 'add-merchant/:merchantId/:cpage', component: AddMerchantsComponent, data: { title: 'Add Merchant' },canActivate:[AuthGuard]  },
  { path: 'view-merchant/:cpage', component: ViewMerchantsComponent, data: { title: 'View Merchant' } ,canActivate:[AuthGuard] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchantsRoutingModule { }
