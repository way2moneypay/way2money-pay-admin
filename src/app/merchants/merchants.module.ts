import { environment } from '../../environments/environment';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchantsRoutingModule } from './merchants-routing.module';
import { AddMerchantsComponent } from './add-merchants/add-merchants.component';
import { ViewMerchantsComponent } from './view-merchants/view-merchants.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ThousandSuffixesPipe } from '../shared/pipes/convert.pipe';
import { AutoCompleteModule } from '../shared/auto-complete/auto-complete.module';
import { AgmCoreModule } from '@agm/core';
import { ImageCropperModule } from 'ngx-image-cropper';
@NgModule({
  imports: [
    CommonModule,
    MerchantsRoutingModule,
    FormsModule,
    AutoCompleteModule,
    ImageCropperModule,
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: environment.mapsAPIKey,
      libraries: ['places'],
      language: 'en',
    })
  ],
  declarations: [AddMerchantsComponent, ViewMerchantsComponent, ThousandSuffixesPipe]
})
export class MerchantsModule { }
