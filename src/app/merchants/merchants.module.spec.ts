import { MerchantsModule } from './merchants.module';

describe('MerchantsModule', () => {
  let merchantsModule: MerchantsModule;

  beforeEach(() => {
    merchantsModule = new MerchantsModule();
  });

  it('should create an instance', () => {
    expect(merchantsModule).toBeTruthy();
  });
});
