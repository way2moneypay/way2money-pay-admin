import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClearUsersComponent } from './clear-users.component';

describe('ClearUsersComponent', () => {
  let component: ClearUsersComponent;
  let fixture: ComponentFixture<ClearUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClearUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClearUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
