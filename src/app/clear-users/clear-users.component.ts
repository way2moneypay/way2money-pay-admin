import { Component, OnInit } from '@angular/core';
import { RestServicesService } from "../shared/services/rest-services/rest-services.service";
@Component({
  selector: 'app-clear-users',
  templateUrl: './clear-users.component.html',
  styleUrls: ['./clear-users.component.css']
})
export class ClearUsersComponent implements OnInit {

  constructor(private restService: RestServicesService) { }

  ngOnInit() {
  }
  user_mobile: String = "";
  merchant_mobile: String = "";
  clearUser() {
    if (this.user_mobile != undefined && this.user_mobile != "") {
      var obj =
        {
          mobile: this.user_mobile
        }
      this.restService.postAuth('clear-user', obj).subscribe(result => {
          if(result.status=="success"){
            alert(result.status)
          }else{
            alert("user not exist with given mobile number");
          }
      })
    }

  }
  clearMerchant() {
    if (this.merchant_mobile != undefined && this.merchant_mobile != "") {
      var obj =
        {
          mobile: this.merchant_mobile
        }
      this.restService.postAuth('clear-merchant', obj).subscribe(result => {
        if(result.status=="success"){
          alert(result.status)
        }else{
          alert("mechant not exist with given mobile number");
        }

      })
    }
  }
}
