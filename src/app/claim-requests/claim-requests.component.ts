import { Component, OnInit } from "@angular/core";
import { RestServicesService } from "../shared/services/rest-services/rest-services.service";
import { PagerService } from "../shared/pagination/pager.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
@Component({
  selector: "app-claim-requests",
  templateUrl: "./claim-requests.component.html",
  styleUrls: ["./claim-requests.component.css"]
})
export class ClaimRequestsComponent implements OnInit {
  constructor(
    private dataService: RestServicesService,
    private pagerService: PagerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        if (this.searchStatus) {
          this.setSearchPage(this.currentPage);
        } else {
          this.emailId = localStorage.getItem("email");
          this.getCount();
        }
      }
    });
  }
  countStatus: any = false;
  claimRequestsDetails: any;
  pager: any = {};
  cpage: any;
  pagedItems: any[];
  currentPage: number = 1;
  errMsg = false;
  pageLength: any;
  searchString = "";
  claimButton = false;
  searchVariable = "mobile";
  showData = false;
  checkClaimStatus = false;
  totalPage = true;
  searchString2: any;
  searchStatus: any = false;
  paginationStatus: any = false;
  emailId: any;
  merchantClaimRequestsApi: any = 0;
  claimRequestsId: any;
  claimRequestsObj: any = {};
  merchantObj: any = {};
  claimId: any;
  claimAmount: any;
  claimRefId: any = "";
  claimedStatus = false;
  redirectUrl: String;
  claimButtonStatus=true;
  obj: any;
  responseStatus: any = false;
  brandName:any;
  resetButtonShowStatus: any = false;
  ngOnInit() {}

  getCount() {
    this.merchantClaimRequestsApi = "merchant/getClaimRequestsCount/";

    if (!this.countStatus) {
      this.dataService.getAuth(this.merchantClaimRequestsApi).subscribe(result => {
        if (result.status) {
          this.pageLength = parseInt(result.data);
          this.countStatus = true;
          this.setClaimRequestsPage(this.currentPage);
        } else {
          this.pageLength = 0;
        }
      });
    } else {
      this.setClaimRequestsPage(this.currentPage);
    }
  }
  getClaimRequests(skip, limit) {
    this.obj = {
      skip: skip,
      limit: limit
    };

    this.dataService
      .postAuth("merchant/claimRequests", this.obj)
      .subscribe(result => {
        if (result.status) {
          this.pagedItems = result.data;
          this.showData = true;
        } else {
          this.showData = false;
          this.errMsg = true;
        }
        this.resetButtonShowStatus = false;
      });
  }
  convertInt(page) {
    if (page > this.pager.totalPages) {
      return this.pager.totalPages;
    } else {
      return parseInt(page) + 1;
    }
  }
  setPage(page) {
    this.currentPage = page;
    this.cpage = "/claim-requests/" + page;
    this.router.navigate([this.cpage]);
  }
  setClaimRequestsPage(page) {
    if (this.pageLength) {
      if (this.pageLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerService.getPager(this.pageLength, page, 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getClaimRequests(
          this.pager.startIndex,
          this.pager.endIndex + 1 - this.pager.startIndex
        );
      } else {
        this.currentPage = 1;
        this.getClaimRequests(0, this.pageLength);
        this.paginationStatus = false;
      }
    } else {
      this.errMsg = true;
    }
  }
  setSearchPage(page) {
    this.currentPage = page;
    this.cpage = this.router.routerState.snapshot.url;
    if (this.pageLength) {
      if (this.pageLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerService.getPager(
          Number(this.pageLength),
          Number(page),
          10
        );
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.pager = this.pagerService.getPager(
          this.pageLength,
          this.currentPage,
          10
        );
        this.pagedItems = this.claimRequestsDetails.slice(
          this.pager.startIndex,
          this.pager.endIndex + 1
        );
      } else {
        this.currentPage = 1;
        this.pagedItems = this.claimRequestsDetails;
        this.paginationStatus = false;
      }
    } else {
      this.errMsg = true;
    }
  }

  search() {
    this.responseStatus = true;
    var data = {};
    // if (this.searchVariable == "date1" || this.searchVariable == "date2") {
    //   var fromDate = new Date(this.searchString).setHours(0, 0, 0, 0);
    //   var toDate = new Date(this.searchString2);
    //   toDate.setDate(toDate.getDate() + 1);
    //   toDate.setHours(0, 0, 0, 0);
    //   if (this.searchVariable == "date1") {
    //     data["type"] = "request_date";
    //   } else {
    //     data["type"] = "accepted_date";
    //   }
    //   data["fromDate"] = fromDate;
    //   data["toDate"] = toDate.getTime();
    // }
    if (this.searchVariable == "mobile") {
      data["mobile"] = this.searchString;
    }
    if (this.searchVariable == "brand") {
      data["brand_name"] = this.searchString;
    }
    this.dataService
      .postAuth("merchant/claimRequests/search", data)
      .subscribe(result => {
        this.responseStatus = false;
        if (result.status) {
          this.claimRequestsDetails = result.data;
          this.countStatus = false;
          this.pageLength = this.claimRequestsDetails.length;
          this.showData = true;
          this.errMsg = false;
          this.searchStatus = true;
          this.setSearchPage(1);
        } else {
          this.errMsg = true;
          this.showData = false;
          this.searchStatus = false;
        }
      });
  }
  reset() {
    this.resetButtonShowStatus = true;
    this.searchString = "";
    this.searchString2 = "";
    this.getCount();
    this.errMsg = false;
    this.searchStatus = false;
  }
  claimProcess(id, amount,brand_name) {
    this.claimedStatus = false;
    this.claimId = id;
    this.claimAmount = amount;
    this.brandName = brand_name;
    this.checkClaimStatus = true;
  }
  claim() {
    var findData = {};
    findData["claimRefId"] = this.claimRefId;
    this.claimRefId='';
    this.checkClaimStatus = false;
    var date = new Date().getTime();
    console.log("date is", date);
    findData["accepted_date"] = date;
    this.dataService
      .getAuth("merchant/getMerchantId/" + this.emailId)
      .subscribe(result => {
        console.log("id for email is ", result.data);
        if (result.status) {
          findData["accepted_by"] = result.data;
          findData["approved_amt"] = this.claimAmount;
          findData["merchant_id"] = this.claimId;
          findData["brand_name"]=this.brandName;
          console.log("find data is ",findData);
          this.dataService
            .postAuth("merchant/merchantClaims", findData)
            .subscribe(result => {
              if (!result.status) {
                alert(result.message);
              } else {
                this.claimedStatus = true;
                // this.ngOnInit();
                this.getCount();
              }
            });
        }
      });
  }
  cancel() {
    this.claimRefId = null;
  }
}
