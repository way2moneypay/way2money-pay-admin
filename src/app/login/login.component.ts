import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestServicesService } from '../shared/services/rest-services/rest-services.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

    isMerchantUser = false;
    invalid_user = false;
    message = '';
    constructor(public router: Router, private api: RestServicesService, 
        private cookieService: CookieService) { }

    ngOnInit() { }

    ngAfterViewInit() {
        $(function () {
            $('.preloader').fadeOut();
        });
        $('#to-recover').on('click', function () {
            $('#loginform').slideUp();
            $('#recoverform').fadeIn();
        });
    }

    onLoggedin(form) {
        this.invalid_user = false;
        this.api.post('login', { email: form.value.email, password: form.value.password }).subscribe(result => {
            if (result.data === false) {
                this.invalid_user = true;
                this.message = result.message;
            } else if (result.data === true) {
                this.cookieService.set("admin_token", result.admin_token, new Date(Date.now() + (86400000 * 7)), '/');
                if (form.value.email !== 'admin@way2pay.com') {
                    localStorage.setItem('isMerchant', 'yes');
                    localStorage.setItem('id', result.id);
                }
                localStorage.setItem('email', form.value.email);
                this.router.navigate(['']);
            } else {
                this.invalid_user = true;
                this.message = result.message;
            }
        });
    }

}
