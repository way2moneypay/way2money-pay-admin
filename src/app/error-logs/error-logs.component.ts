import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { RestServicesService } from '../shared/services/rest-services/rest-services.service';
import { PagerService } from '../shared/pagination/pager.service';
@Component({
  selector: 'app-error-logs',
  templateUrl: './error-logs.component.html',
  styleUrls: ['./error-logs.component.css']
})
export class ErrorLogsComponent implements OnInit {

  constructor(private apiService: RestServicesService, private pagerservice: PagerService, private router: Router, private route: ActivatedRoute) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
          this.getCount();
      }
    });
  }
  countStatus: any = false;
  userDetails: any = [];
  paginationStatus = false;
  searchString: String = "";
  pager: any;
  logsLength: any;
  findStatus: any;
  errMsg=false;
  pagedItems: any;
  currentPage: number;
  cpage: any;
  ngOnInit() {
    this.apiService.getAuth("logs/authcheck").subscribe(result => {
      if (result.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
     }


    })
    // this.getCount();

  }
  getCount() {
    if (!this.countStatus) {
      this.apiService.getAuth("logs/getCount").subscribe(result => {
        if (result.status) {
          this.logsLength = parseInt(result.data);
          this.countStatus = true;
          this.setLogsPage(this.currentPage);
        } else {
          this.logsLength = 0;
        }
      })
    } else {
      this.setLogsPage(this.currentPage);
    }
  }
  getLogs(skip, limit) {
    this.apiService.postAuth("logs/getLogs", { skip: skip, limit: limit }).subscribe(result => {
      if (result.status) {
        this.pagedItems = result.data;
      } else {
        this.paginationStatus = false;
        this.findStatus = false;
      }
    })
  }
  
  convertInt(page) {
    return parseInt(page);
  }
  reset() {
    this.getCount();
  }
  setPage(page) {
    // console.log("set page of ",page);
    this.cpage = 'error-logs/' + page;
    // console.log(this.currentPage);
    this.router.navigate([this.cpage]);
  }
  setLogsPage(page) {
    if (this.logsLength) {
      this.errMsg=false;
      if (this.logsLength > 10) {
        this.currentPage=page;
        this.findStatus = true;
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(Number(this.logsLength), Number(page), 10);
        // console.log("pager",this.pager);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getLogs(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      }else{
        this.currentPage=1;
        this.getLogs(0, this.logsLength);
        this.findStatus = true;
        this.paginationStatus = false;
      }
    } else {
      this.errMsg=true;
      
    }
  }

 
}