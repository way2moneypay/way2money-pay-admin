import { Component, OnInit } from '@angular/core';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { PagerService } from '../../shared/pagination/pager.service';
@Component({
  selector: 'app-push-notifications',
  templateUrl: './push-notifications.component.html',
  styleUrls: ['./push-notifications.component.css']
})
export class PushNotificationsComponent implements OnInit {
  sendPushAlerts: any = {};
  SendEmail: any = {};
  sendsmsAlerts: any = {};
  mobilePattern = "^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$";
  namePattern = "^[a-zA-Z][a-zA-Z0-9 ]+$";
  emailPattern = "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
  message: any;
  sms_message: any;
  searchString: any = "";
  searchVariable = 'sms';
  resetButtonStatus: any = false;
  email_message: any;
  constructor(
    private dataService: RestServicesService,
    private pagerservice: PagerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }
  // options = [
  //   { name: "pay", value: 'pay' },
  //   { name: "loans", value: 'loans' }
  // ]
  // type_options = [
  //   { name: "PERSONAL", value: 'PERSONAL' },
  //   { name: "BUSINESS", value: 'BUSINESS' }
  // ]
  ngOnInit() {
  }
  SendPushNotification() {
    console.log(this.sendPushAlerts)
    this.dataService.getAuthParams('api/sendPushAlerts', this.sendPushAlerts).subscribe((response) => {
      console.log("PushNotification response", response);
      if (response.success == true) {
        this.message = response.message;
        this.sendPushAlerts = {};
      } else {
        this.message = response.message;
      }
    })
  }

  SendPaymentlinkBySms() {
    // console.log(this.sendsmsAlerts)
    this.dataService.getAuthParams('api/sendSmsAlerts', this.sendsmsAlerts).subscribe((response) => {
      console.log("AddUserPermissions", response);
      if (response.success == true) {
        this.sms_message = response.message;
        this.sendsmsAlerts = {};
      } else {
        this.sms_message = response.message;
      }
    })
  }
  SendEmailAlert() {
    console.log(this.SendEmail)
    this.dataService.getAuthParams('api/SendEmail', this.SendEmail).subscribe((response) => {
      console.log("AddUserPermissions", response);
      if (response.success == true) {
        this.email_message = response.message;
        this.SendEmail = {};
      } else {
        this.email_message = response.message;
      }
    })
  }
  reset() {
    this.resetButtonStatus = true;
    this.sendPushAlerts.mobile_number = "";
    this.sendsmsAlerts.mobile_number = "";
    this.SendEmail.email = "";
  }
}
