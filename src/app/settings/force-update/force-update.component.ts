import { Component, OnInit } from '@angular/core';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'

@Component({
  selector: 'app-force-update',
  templateUrl: './force-update.component.html',
  styleUrls: ['./force-update.component.css']
})
export class ForceUpdateComponent implements OnInit {

  constructor(private dataService: RestServicesService, private router: Router, ) { }

  ngOnInit() {
    this.onClick();
  }
  modalStatus = false;
  data: any;
  isDataFound = false;
  addButton = false;
  item: any;
  newVersion = "";
  force_update = false;
  isChecked = false;
  obj: any;
  mobile = "";


  onClick() {
    this.dataService.getAuth('api/get-versions').subscribe(result => {
      if (result.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (result.status == true) {
          this.isDataFound = true;
          this.data = result.data;
          // console.log(this.data);
        } else {
          this.isDataFound = false;
        }
      }
    })
  }


  add(close) {
    this.addButton = true;
    if (this.newVersion != "" && this.newVersion != null && this.newVersion != undefined) {
      if (this.mobile == "") {
        this.item = {
          version: this.newVersion,
          force_update: this.force_update,
          mobile_type: "android"
        }
      } else {
        if (this.mobile != "") {
          this.item = {
            version: this.newVersion,
            force_update: this.force_update,
            mobile_type: this.mobile
          }
        }
      }
      this.dataService.postAuth('api/add-version', this.item).subscribe(result => {
        if (result.status) {
          // this.showModal=false;
          alert("updated successfully");
          this.addButton = false;
          this.newVersion = null;
          this.isChecked = false;
          this.mobile = "";
          close.click();
          this.ngOnInit();
        } else {
          alert(result.message);
          this.addButton = false;
          this.mobile = "";
          this.ngOnInit();
        }
      });

    } else {
      alert("Enter Version")
    }

  }
  cancel() {
    this.newVersion = null;
    this.isChecked = false;
    this.mobile = "";
  }

  addVersion() {
    this.addButton = true;
    this.force_update = false;
  }
  checkValue(event: any) {
    if (event) {
      this.force_update = event;
    }
    else {
      this.force_update = false;
    }
  }
  updateVersion(item) {
    if (item.force_update == true) {
      this.obj = {
        version: item.version,
        force_update: false,
        mobile_type: item.mobile_type
      }
    } else {
      this.obj = {
        version: item.version,
        force_update: true,
        mobile_type: item.mobile_type
      }
    }
    this.dataService.postAuth('api/add-version', this.obj).subscribe(result => {
      if (result.status) {
        // this.showModal=false;
        alert("updated successfully");
        this.ngOnInit();
      } else {
        alert("Error while updating");
        this.ngOnInit();
      }
    });
  }
}
