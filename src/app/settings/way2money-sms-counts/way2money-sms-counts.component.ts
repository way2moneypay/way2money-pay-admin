import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { PagerService } from '../../shared/pagination/pager.service';
@Component({
  selector: 'app-way2money-sms-counts',
  templateUrl: './way2money-sms-counts.component.html',
  styleUrls: ['./way2money-sms-counts.component.css']
})
export class Way2MoneysmsStagsComponent implements OnInit {
  paramsData: any = {};
  collectionSize: Number = 5;
  pageSize: number = 10;
  skip: number;
  limit: number;
  maxSize: Number = 5;
  page: number = 1;
  pageStartIndex: number;
  from_date: any;
  to_date: any;
  refresh_fDate: any;
  refresh_tDate: any;
  data_alert_info: string;
  datafound: boolean = true;
  getsmscount_details: any;
  alert_error_message_type: string;
  date_info: any;
  total_smscounts: any;
  isview: boolean = false;
  constructor(private dataService: RestServicesService, private pagerservice: PagerService, private router: Router, private route: ActivatedRoute, private _location: Location) {
  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.page = params.page ? params.page : 1;
      // ****************.................For resolve the refresh handling ......****************
      this.refresh_fDate = params.from_date ? params.from_date : this.from_date;
      this.refresh_tDate = params.to_date ? params.to_date : this.to_date;
      if (this.refresh_fDate && this.refresh_tDate) {
        this.from_date = this.refresh_fDate;
        this.to_date = this.refresh_tDate;
        // #######################CONVERTING DARE TO OBJECT FORMAT########################
        let dt = this.from_date.split("-");
        this.from_date = { "year": parseInt(dt[0]), "month": parseInt(dt[1]), "day": parseInt(dt[2]) };
        dt = this.to_date.split("-");
        this.to_date = { "year": parseInt(dt[0]), "month": parseInt(dt[1]), "day": parseInt(dt[2]) };
        // console.log(this.from_date, this.to_date);
      }
      this.onClickSubmit(this.page);
    });
  }
  onClickSubmit(page) {
    this.page = page;
    this.paramsData.skip = (this.page - 1) * this.pageSize;
    this.paramsData.limit = this.pageSize;
    let fDate;
    let tDate;
    if (this.from_date != null && this.to_date != null) {
      fDate = this.from_date.year + "-" + this.from_date.month + "-" + this.from_date.day;
      tDate = this.to_date.year + "-" + this.to_date.month + "-" + this.to_date.day;
      if (new Date(fDate).getTime() >= new Date(tDate).getTime()) {
        if (new Date(fDate).getTime() == new Date(tDate).getTime()) {
          this.date_info = null;
        }
        else {
          this.datafound = true;
          this.alert_error_message_type = "warning";
          this.date_info = "FromDate should be less than ToDate";
        }
      }
      else {
        this.date_info = null;
      }
    }
    if (this.from_date == null && this.to_date != null) {
      this.alert_error_message_type = "warning";
      this.date_info = "Please Select FromDate";
    }
    if (this.from_date != null && this.to_date == null) {
      this.alert_error_message_type = "warning";
      this.date_info = "Please Select ToDate";
    }
    if (this.refresh_fDate && this.refresh_tDate) {
      fDate = this.refresh_fDate;
      tDate = this.refresh_tDate;
    }
    this.paramsData.from_date = fDate;
    this.paramsData.to_date = tDate;
    this.data_alert_info = "Loading..";
    // alert(JSON.stringify(this.paramsData));
    //console.log(this.paramsData)
    this.dataService.getAuthParams("api/getSmsCounts", this.paramsData).subscribe((Response) => {
      // console.log("UPdated REESPONSE");
      // console.log(Response);
      if (Response.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (Response.data.length != 0) {
          this.collectionSize = Response.data.total_records;
          this.getsmscount_details = Response.data.smscounts_details;
          this.datafound = true;
          this.alert_error_message_type = "success";
          this.data_alert_info = "Results Found " + this.collectionSize;
          if (Response.data.result_total.length != 0) {
            this.isview = true
            this.total_smscounts = "TOTAL SENDING SMS " + Response.data.result_total[0].total_counts;
          } else {
            this.isview = false
            this.total_smscounts = "TOTAL SENDING SMS 0";
          }
        }
        else {
          this.datafound = false;
          this.alert_error_message_type = "warning";
          this.total_smscounts = "TOTAL SENDING SMS " + Response.data.result_total[0].total_counts;;
          this.data_alert_info = "Results Found " + this.collectionSize;
        }
        this.pageStartIndex = ((this.page - 1) * (this.pageSize));
      }
    })
  }
  onClickReset() {
    this.page = 1;
    this.from_date = null;
    this.to_date = null;
    this.date_info = null;
    this.paramsData = {};
    this.router.navigate(["/settings/way2money-sms-counts/"]);
    this.ngOnInit();
  }
}
