import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Way2MoneysmsStagsComponent } from './way2money-sms-counts.component';

describe('Way2MoneysmsStagsComponent', () => {
  let component: Way2MoneysmsStagsComponent;
  let fixture: ComponentFixture<Way2MoneysmsStagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Way2MoneysmsStagsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Way2MoneysmsStagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
