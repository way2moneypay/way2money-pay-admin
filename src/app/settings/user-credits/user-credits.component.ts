import { Component, OnInit } from '@angular/core';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'


@Component({
  selector: 'app-user-credits',
  templateUrl: './user-credits.component.html',
  styleUrls: ['./user-credits.component.css']
})
export class UserCreditsComponent implements OnInit {
  data: any;
  editItem: { id: string; brand: string; creditLimit: string; osName: string; status: string; };

  constructor(private dataService: RestServicesService, private router: Router, ) { }

  ngOnInit() {
    this.start()
  }
  addButton = false;
  mobileBrand = "";
  Os = "";
  status = ""
  creditLimit = ""
  item: any;
  isDataFound = false
  editButton = false
  Id = ""

  start() {
    this.dataService.getAuth('user/get-user-credits').subscribe(result => {
      if (result.status == true) {
        // console.log("getting info",result)
        this.isDataFound = true
        this.data = result.data

      } else {
        this.isDataFound = false
        console.log("error while getting info")

      }
    })

  }

  addCredits() {
    this.addButton = true
  }
  cancel() {
    this.mobileBrand = null
    this.Os = ""
    this.status = ""
    this.creditLimit = ""
    this.editButton = false

  }
  add(close) {
    console.log("adding the data")
    this.editButton = false
    this.addButton = true;
    if (this.mobileBrand != "" && this.mobileBrand != null && this.mobileBrand != undefined) {
      if (this.creditLimit == "") {
        this.item = {
        }
      } else {
        if (this.creditLimit != "") {
          this.item = {
            brand: this.mobileBrand,
            creditLimit: this.creditLimit,
            osName: this.Os,
            status: this.status

          }
          console.log("item::::", this.item)
        }
      }
      this.dataService.postAuth('user/user-credits', this.item).subscribe(result => {
        if (result.status == true) {
          this.isDataFound == true
          // this.showModal=false;
          // console.log("updated successfully")

          alert("updated successfully");
          this.addButton = false;
          this.mobileBrand = null;
          this.creditLimit = "";
          this.Os = "";
          this.status = ""
          this.start()
        } else {
          // console.log("error in updation")
          alert(result.message);
          this.addButton = false;
          this.creditLimit = "";
          this.Os = "";
          this.status = ""
        }
      });

    } else {
      alert("Enter brand")
    }

  }
  edit(item) {
    this.Id = item._id
    this.editButton = true
    this.addButton = false
    this.mobileBrand = item.brand
    this.creditLimit = item.creditLimit
    this.Os = item.osName
    this.status = item.status
  }
  Update() {
    if (this.mobileBrand != "" && this.mobileBrand != null && this.mobileBrand != undefined) {
      if (this.creditLimit == "") {
        // this.item = {
        // }
      } else {
        // if (this.creditLimit != "") {}
        this.editItem = {
          id: this.Id,
          brand: this.mobileBrand,
          creditLimit: this.creditLimit,
          osName: this.Os,
          status: this.status

        }
        // console.log("item::::",this.editItem)

      }
      this.dataService.postAuth('user/edit-user-credits', this.editItem).subscribe(result => {
        if (result.status == true) {
          this.isDataFound == true
          // console.log("updated successfully")
          alert("updated successfully");
          this.addButton = false;
          this.mobileBrand = null;
          this.creditLimit = "";
          this.Os = "";
          this.status = ""
          this.start()
        } else {
          // console.log("error in updation")
          alert(result.message);
          this.addButton = false;
          this.creditLimit = "";
          this.Os = "";
          this.status = ""
        }
      });

    } else {
      alert("Enter brand")
    }

  }

}
