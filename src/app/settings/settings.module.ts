import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { AddSettingsComponent } from './add-settings/add-settings.component';
import { FormsModule } from '@angular/forms';
import { SmsAlertsComponent } from './sms-alerts/sms-alerts.component';
import { EmailAlertsComponent } from './email-alerts/email-alerts.component';
import { PushAlertsComponent } from './push-alerts/push-alerts.component';
import { ForceUpdateComponent } from './force-update/force-update.component';
import { UserCreditsComponent } from './user-credits/user-credits.component';
import { PaymentBySmsComponent } from './payment-by-sms/payment-by-sms.component';
import { PushNotificationsComponent } from './push-notifications/push-notifications.component';
import { Way2MoneysmsStagsComponent } from './way2money-sms-counts/way2money-sms-counts.component';
import { NgbModule, NgbPagination } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    FormsModule,
    NgbModule.forRoot()

  ],
  declarations: [AddSettingsComponent, SmsAlertsComponent, EmailAlertsComponent, PushAlertsComponent, ForceUpdateComponent, UserCreditsComponent, PaymentBySmsComponent, PushNotificationsComponent, Way2MoneysmsStagsComponent]
})
export class SettingsModule { }
