import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { PagerService } from '../../shared/pagination/pager.service';

@Component({
  selector: 'app-email-alerts',
  templateUrl: './email-alerts.component.html',
  styleUrls: ['./email-alerts.component.css']
})
export class EmailAlertsComponent implements OnInit {
  emailAlerts: any;
  cpage: any;
  countStatus: any = false;
  paginationStatus = false;
  noAlerts: any;
  emailLength: any;
  currentPage: number;
  isAddAlert = false;
  pager: any;
  closeResult: string;
  addEmailAlert: any = {
    status: 'active'
  };
  errAddEmailAlert = false;
  errAddEmailAlertMessage: any;
  editAlertId: any;
  alertButtonStatus: any = false;
  options = [
    { name: "active", value: 'active' },
    { name: "inactive", value: 'inactive' }
  ]
  api_details: any = {};
  smtp_details: any = {};
  constructor(
    private dataService: RestServicesService,
    private modalService: NgbModal,
    private pagerservice: PagerService,
    private router: Router,
    private route: ActivatedRoute,
    public vcr: ViewContainerRef,
    private _location: Location,

  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        this.getCount();
      }
    });

  }

  ngOnInit() {
    this.editAlertId = this.route.snapshot.params.alertId;
    if (this.editAlertId && this.editAlertId != 0) {
      this.editAlert();
    } else if (this.editAlertId && this.editAlertId == 0) {
      this.isAddAlert = true;
    } else {
      this.getEmailAlerts(0, 10);
    }
  }
  convertInt(page) {
    return parseInt(page);
  }
  getCount() {
    if (!this.countStatus) {
      // console.log("getCount")
      this.dataService.getAuth("api/email-alerts/getCount").subscribe(result => {
        if (result.status) {
          this.emailLength = parseInt(result.data);
          this.countStatus = true;
          this.setEmailPage(this.currentPage);
        } else {
          this.emailLength = 0;
        }
      })
    } else {
      this.setEmailPage(this.currentPage);
    }
  }
  setPage(page) {
    if (page < 1) {
      page = 1;
    } else if (page > this.pager.totalPages) {
      page = this.pager.totalPages;
    }
    this.cpage = 'settings/email-alerts/' + page;
    this.router.navigate([this.cpage]);
  }
  setEmailPage(page) {
    if (this.emailLength) {
      if (this.emailLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(Number(this.emailLength), Number(page), 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getEmailAlerts(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      }
      else {
        this.currentPage = 1;
        // console.log("current:: ", this.currentPage);
        this.getEmailAlerts(0, this.emailLength);
        this.paginationStatus = false;
      }
    } else {
      this.noAlerts = true;
    }
  }
  editAlert() {
    this.dataService.getAuth('api/email-alerts/edit/' + this.editAlertId).subscribe(result => {
      if (result.success) {
        //console.log(result)
        this.isAddAlert = true;
        this.paginationStatus = false;
        let emial_data = result.EmailAlerts[0];

        // console.log("this.addEmailAlert:::::::::",this.addEmailAlert);
        if (emial_data.alert_route === "api") {
          this.api_details.api_url = emial_data.api_url;
        } else {
          this.smtp_details.smtp_host = emial_data.smtp_host;
          this.smtp_details.smtp_username = emial_data.smtp_username;
          this.smtp_details.smtp_password = emial_data.smtp_password;
          this.smtp_details.smtp_port = emial_data.smtp_port
        }
        this.addEmailAlert.alert_name = emial_data.alert_name;
        this.addEmailAlert.alert_subject = emial_data.alert_subject;
        this.addEmailAlert.alert_content = emial_data.alert_content;
        this.addEmailAlert.alert_route = emial_data.alert_route;
        this.addEmailAlert.status = emial_data.status;
        this.addEmailAlert._id = emial_data._id;
      }
    });
  }
  getEmailAlerts(skip, limit) {
    var data = {};
    data["skip"] = skip;
    data["limit"] = limit;
    if (!this.route.snapshot.params.alertId) {
      this.isAddAlert = false;
      // this.noAlerts=false;
    }

    this.dataService.postAuth('api/email-alerts', data).subscribe(result => {
      // console.log(result);
      if (result.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (result.success) {
          this.emailAlerts = result.EmailAlerts;
          // console.log("this.emailAlerts::", this.emailAlerts)
        } else {
          this.noAlerts = true;
        }
      }
    });
  }

  redirectAddEmailAlert() {
    this.isAddAlert = true;
    this.paginationStatus = false;
    this.addEmailAlert = {};
    this.router.navigate(['settings/email-alerts/add/0/' + this.currentPage]);
  }

  onSubmitAddAlert() {
    this.alertButtonStatus = true;
    //console.log(this.addEmailAlert);
    if (this.addEmailAlert.alert_route === "api") {
      this.addEmailAlert.api_url = this.api_details.api_url;
    } else {
      this.addEmailAlert.smtp_host = this.smtp_details.smtp_host;
      this.addEmailAlert.smtp_username = this.smtp_details.smtp_username;
      this.addEmailAlert.smtp_password = this.smtp_details.smtp_password;
      this.addEmailAlert.smtp_port = this.smtp_details.smtp_port;
    }
    this.dataService.postAuth('api/email-alerts/register', this.addEmailAlert).subscribe(result => {
      if (result.success) {
        this.isAddAlert = false;
        this.currentPage = this.route.snapshot.params.cpage;
        this.router.navigate(['settings/email-alerts/' + this.pager.totalPages]);
      } else {
        this.errAddEmailAlert = true;
        this.errAddEmailAlertMessage = result.message;
      }
      this.alertButtonStatus = false;
    });
  }

  onSubmitUpdateAlert() {
    if (this.addEmailAlert.alert_route === "api") {
      this.addEmailAlert.api_url = this.api_details.api_url;
    } else {
      this.addEmailAlert.smtp_host = this.smtp_details.smtp_host;
      this.addEmailAlert.smtp_username = this.smtp_details.smtp_username;
      this.addEmailAlert.smtp_password = this.smtp_details.smtp_password;
      this.addEmailAlert.smtp_port = this.smtp_details.smtp_port;
    }
    // console.log(this.addEmailAlert)
    this.dataService.postAuth('api/email-alerts/update', this.addEmailAlert).subscribe(result => {
      if (result.success) {
        this.isAddAlert = false;
        this.currentPage = this.route.snapshot.params.cpage;
        this.router.navigate(['settings/email-alerts/' + this.currentPage]);
        // this._location.back();
      } else {
        this.errAddEmailAlert = true;
        this.errAddEmailAlertMessage = result.message;
      }
    });
  }

  backClicked() {
    this._location.back();
  }

  open(content) {
    this.popupCalls(content);
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  popupCalls(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
}
