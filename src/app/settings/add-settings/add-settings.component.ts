import { Component, OnInit } from '@angular/core';
import {RestServicesService} from '../../shared/services/rest-services/rest-services.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
@Component({
  selector: 'app-add-settings',
  templateUrl: './add-settings.component.html',
  styleUrls: ['./add-settings.component.css']
})
export class AddSettingsComponent implements OnInit {
  paySettingsObj : any = {};
  constructor(private api:RestServicesService,private router: Router) { }
  settingsStatus:any=false;
  ngOnInit() {
    this.api.getAuth('view-settings').subscribe(viewSetResponce => {
      if (viewSetResponce.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else{
        if (viewSetResponce.status) {
          this.paySettingsObj = viewSetResponce.data ? viewSetResponce.data : {};
          console.log("viewSetResponce", this.paySettingsObj);
        } else {
          alert("something went wrong");
        }
      }
    });
  }
  addSettings() {
    this.settingsStatus=true;
    // console.log("paySettingsObj",this.paySettingsObj);
    // if(settingsForm.init_amount !=undefined && settingsForm.init_amount !=null){
    //   this.settingsObj.init_amount = settingsForm.init_amount;
    // }
    // if(settingsForm.user_commission !=undefined && settingsForm.user_commission !=null){
    //   this.settingsObj.user_commission = settingsForm.user_commission;
    // }
    // if(settingsForm.refund_period !=undefined && settingsForm.refund_period !=null){
    //   this.settingsObj.refund_period = settingsForm.refund_period;
    // }
    // if(settingsForm.due_date !=undefined && settingsForm.due_date !=null){
    //   this.settingsObj.due_date = settingsForm.due_date;
    // }
    // if(settingsForm.due_amount !=undefined && settingsForm.due_amount !=null){
    //   this.settingsObj.due_amount = settingsForm.due_amount;
    // }
    // console.log("setings",this.settingsObj);
    this.api.postAuth('add-settings', this.paySettingsObj).subscribe(settingsResponce => {
      if (settingsResponce.status) {
        alert("payment settings updated successfully");
        this.settingsStatus=false;
      } else {
        alert("something went wrong");
        this.settingsStatus=false;

      }
    });
  }

}
