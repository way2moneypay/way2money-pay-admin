import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsAlertsComponent } from './sms-alerts.component';

describe('SmsAlertsComponent', () => {
  let component: SmsAlertsComponent;
  let fixture: ComponentFixture<SmsAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
