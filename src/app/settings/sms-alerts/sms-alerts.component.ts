import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { PagerService } from '../../shared/pagination/pager.service';

@Component({
  selector: 'app-sms-alerts',
  templateUrl: './sms-alerts.component.html',
  styleUrls: ['./sms-alerts.component.css']
})
export class SmsAlertsComponent implements OnInit {

  smsAlerts: any;
  cpage: any;
  countStatus: any = false;
  paginationStatus = false;
  noAlerts: any;
  smsLength: any;
  currentPage: number;
  isAddAlert = false;
  pager: any;

  addSMSAlert: any = {
    status: 'active'
  };
  errAddSMSAlert = false;
  errAddSMSAlertMessage: any;
  editAlertId: any;
  alertButtonStatus: any = false;
  options = [
    { name: "active", value: 'active' },
    { name: "inactive", value: 'inactive' }
  ]
  constructor(private dataService: RestServicesService, private pagerservice: PagerService, private router: Router, private route: ActivatedRoute, private _location: Location) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        this.getCount();
      }
    });

  }

  ngOnInit() {
    this.editAlertId = this.route.snapshot.params.alertId;
    if (this.editAlertId && this.editAlertId != 0) {
      this.editAlert();
    } else if (this.editAlertId && this.editAlertId == 0) {
      this.isAddAlert = true;
    } else {
      this.getSMSAlerts(0, 10);
    }
  }
  convertInt(page) {
    return parseInt(page);
  }
  getCount() {
    if (!this.countStatus) {
      // console.log("getCount")
      this.dataService.getAuth("api/sms-alerts/getCount").subscribe(result => {
        if (result.status) {
          console.log(result)
          this.smsLength = parseInt(result.data);
          this.countStatus = true;
          this.setSmsPage(this.currentPage);
        } else {
          this.smsLength = 0;
        }
      })
    } else {
      this.setSmsPage(this.currentPage);
    }
  }
  setPage(page) {
    if (page < 1) {
      page = 1;
    } else if (page > this.pager.totalPages) {
      page = this.pager.totalPages;
    }
    this.cpage = 'settings/sms-alerts/' + page;
    this.router.navigate([this.cpage]);
  }
  setSmsPage(page) {
    if (this.smsLength) {
      if (this.smsLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(Number(this.smsLength), Number(page), 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getSMSAlerts(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      }
      else {
        this.currentPage = 1;
        // console.log("current:: ", this.currentPage);
        this.getSMSAlerts(0, this.smsLength);
        this.paginationStatus = false;
      }
    } else {
      this.noAlerts = true;
    }
  }
  editAlert() {
    this.dataService.getAuth('api/sms-alerts/edit/' + this.editAlertId).subscribe(result => {
      if (result.success) {
        this.isAddAlert = true;
        this.paginationStatus = false;
        this.addSMSAlert = result.smsAlerts[0];
      }
    });
  }
  getSMSAlerts(skip, limit) {
    var data = {};
    data["skip"] = skip;
    data["limit"] = limit;
    if (!this.route.snapshot.params.alertId) {
      this.isAddAlert = false;
      // this.noAlerts=false;
    }

    this.dataService.postAuth('api/sms-alerts', data).subscribe(result => {
      if (result.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (result.success) {
          this.smsAlerts = result.smsAlerts;
        } else {
          this.noAlerts = true;
        }
      }
    });
  }

  redirectAddSMSAlert() {
    this.isAddAlert = true;
    this.paginationStatus = false;
    this.addSMSAlert = {};
    this.router.navigate(['settings/sms-alerts/add/0/' + this.currentPage]);
  }

  onSubmitAddAlert() {
    this.alertButtonStatus = true;
    this.dataService.postAuth('api/sms-alerts/register', this.addSMSAlert).subscribe(result => {
      if (result.success) {
        this.isAddAlert = false;
        this.currentPage = this.route.snapshot.params.cpage;
        this.router.navigate(['settings/sms-alerts/' + this.pager.totalPages]);
      } else {
        this.errAddSMSAlert = true;
        this.errAddSMSAlertMessage = result.message;
      }
      this.alertButtonStatus = false;
    });
  }

  onSubmitUpdateAlert() {
    this.dataService.postAuth('api/sms-alerts/update', this.addSMSAlert).subscribe(result => {
      if (result.success) {
        this.isAddAlert = false;
        this.currentPage = this.route.snapshot.params.cpage;
        this.router.navigate(['settings/sms-alerts/' + this.currentPage]);
        // this._location.back();
      } else {
        this.errAddSMSAlert = true;
        this.errAddSMSAlertMessage = result.message;
      }
    });
  }

  backClicked() {
    this._location.back();
  }

}
