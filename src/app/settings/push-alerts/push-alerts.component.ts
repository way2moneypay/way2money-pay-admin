import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { PagerService } from '../../shared/pagination/pager.service';
@Component({
  selector: 'app-push-alerts',
  templateUrl: './push-alerts.component.html',
  styleUrls: ['./push-alerts.component.css']
})
export class PushAlertsComponent implements OnInit {
  pushAlerts: any;
  cpage: any;
  pushLength: any;
  countStatus: any = false;
  paginationStatus = false;
  noAlerts: any;
  currentPage: number;
  isAddAlert = false;
  pager: any;
  addPushAlert: any = {
    status: 'active'
  };
  errAddPUSHAlert = false;
  errAddPUSHAlertMessage: any;
  editAlertId: any;
  alertButtonStatus: any = false;
  options = [
    { name: "active", value: 'active' },
    { name: "inactive", value: 'inactive' }
  ]
  constructor(
    private dataService: RestServicesService,
    private pagerservice: PagerService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        this.getCount();
      }
    });

  }
  ngOnInit() {
    this.editAlertId = this.route.snapshot.params.alertId;
    if (this.editAlertId && this.editAlertId != 0) {
      this.editAlert();
    } else if (this.editAlertId && this.editAlertId == 0) {
      this.isAddAlert = true;
    } else {
      this.getPushAlerts(0, 10);
    }
  }
  convertInt(page) {
    return parseInt(page);
  }
  getCount() {
    if (!this.countStatus) {
      console.log("getCount")
      this.dataService.getAuth("api/push-alerts/getCount").subscribe(result => {
        if (result.status) {
          console.log(result)
          this.pushLength = parseInt(result.data);
          this.countStatus = true;
          this.setPushPage(this.currentPage);
        } else {
          this.pushLength = 0;
        }
      })
    } else {
      this.setPushPage(this.currentPage);
    }
  }
  setPage(page) {
    if (page < 1) {
      page = 1;
    } else if (page > this.pager.totalPages) {
      page = this.pager.totalPages;
    }
    this.cpage = 'settings/push-alerts/' + page;
    this.router.navigate([this.cpage]);
  }
  setPushPage(page) {
    if (this.pushLength) {
      if (this.pushLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(Number(this.pushLength), Number(page), 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getPushAlerts(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      }
      else {
        this.currentPage = 1;
        console.log("current:: ", this.currentPage);
        this.getPushAlerts(0, this.pushLength);
        this.paginationStatus = false;
      }
    } else {
      this.noAlerts = true;
    }
  }
  editAlert() {
    this.dataService.getAuth('api/push-alerts/edit/' + this.editAlertId).subscribe(result => {
      if (result.success) {
        // console.log("Edit Data===>>>>>",result)
        this.isAddAlert = true;
        this.paginationStatus = false;
        this.addPushAlert = result.pushAlerts[0];
      }
    });
  }
  getPushAlerts(skip, limit) {
    var data = {};
    data["skip"] = skip;
    data["limit"] = limit;
    if (!this.route.snapshot.params.alertId) {
      this.isAddAlert = false;
      // this.noAlerts=false;
    }

    this.dataService.postAuth('api/push-alerts', data).subscribe(result => {
      // console.log(result);
      if (result.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (result.success) {
          this.pushAlerts = result.pushAlerts;
          // console.log("this.emailAlerts::", this.pushAlerts)
        } else {
          this.noAlerts = true;
        }
      }
    });
  }



  // PushAlerts Starts
  redirectAddPushAlert() {
    this.isAddAlert = true;
    this.paginationStatus = false;
    this.addPushAlert = {};
    console.log("!@#$%^&*(")
    this.router.navigate(['settings/push-alerts/add/0/' + this.currentPage]);
    this.router.navigate(['settings/push-alerts/add/0/' + this.currentPage]);
  }
  onSubmitAddPushAlert() {
    this.alertButtonStatus = true;
    console.log("****************************************")
    console.log(this.addPushAlert);
    console.log("****************************************")
    this.dataService.postAuth('api/push-alerts/register', this.addPushAlert).subscribe(result => {
      if (result.success) {
        this.isAddAlert = false;
        this.currentPage = this.route.snapshot.params.cpage;
        this.router.navigate(['settings/push-alerts/' + this.pager.totalPages]);
      } else {
        this.errAddPUSHAlert = true;
        this.errAddPUSHAlertMessage = result.message;
      }
      this.alertButtonStatus = false;
    });
  }

  onSubmitUpdatePushAlert() {
    this.dataService.postAuth('api/push-alerts/update', this.addPushAlert).subscribe(result => {
      if (result.success) {
        this.isAddAlert = false;
        this.currentPage = this.route.snapshot.params.cpage;
        this.router.navigate(['settings/push-alerts/' + this.currentPage]);
        this._location.back();
      } else {
        this.errAddPUSHAlert = true;
        this.errAddPUSHAlertMessage = result.message;
      }
    });
  }
  backPushClicked() {
    this._location.back();
  }
}
