import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PushAlertsComponent } from './push-alerts.component';

describe('PushAlertsComponent', () => {
  let component: PushAlertsComponent;
  let fixture: ComponentFixture<PushAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PushAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PushAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
