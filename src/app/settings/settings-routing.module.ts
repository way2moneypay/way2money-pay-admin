import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddSettingsComponent } from './add-settings/add-settings.component';
import { SmsAlertsComponent } from './sms-alerts/sms-alerts.component';
import { EmailAlertsComponent } from './email-alerts/email-alerts.component';
import { PushAlertsComponent } from './push-alerts/push-alerts.component';
import { ForceUpdateComponent } from './force-update/force-update.component';
import { PaymentBySmsComponent } from './payment-by-sms/payment-by-sms.component';
import { UserCreditsComponent } from './user-credits/user-credits.component';
import { PushNotificationsComponent } from './push-notifications/push-notifications.component';
import { Way2MoneysmsStagsComponent } from './way2money-sms-counts/way2money-sms-counts.component';
const routes: Routes = [
  { path: '', redirectTo: 'add-settings', pathMatch: 'full' },
  { path: 'add-settings', component: AddSettingsComponent, data: { title: 'Settings' } },
  { path: 'sms-alerts/:cpage', component: SmsAlertsComponent, data: { title: 'SMS-Alerts' } },
  { path: 'sms-alerts/add/:alertId/:cpage', component: SmsAlertsComponent, data: { title: 'Add SMS-Alerts' } },
  { path: 'email-alerts/:cpage', component: EmailAlertsComponent, data: { title: 'Email-Alerts' } },
  { path: 'email-alerts/add/:alertId/:cpage', component: EmailAlertsComponent, data: { title: 'Add Email-Alerts' } },
  { path: 'push-alerts/:cpage', component: PushAlertsComponent, data: { title: 'Push-Alerts' } },
  // { path: 'push-alerts/:cpage', component: PushAlertsComponent, data: { title: 'Push-Alerts' } },
  { path: 'push-alerts/add/:alertId/:cpage', component: PushAlertsComponent, data: { title: 'Add Push-Alerts' } },
  { path: 'user-credits', component: UserCreditsComponent, data: { title: 'User-credits' } },
  { path: 'force-update', component: ForceUpdateComponent, data: { title: 'Force-Update' } },
  { path: 'payment-sms/:cpage', component: PaymentBySmsComponent, data: { title: 'Payment-by-SMS' } },
  { path: 'push-notifications', component: PushNotificationsComponent, data: { title: 'Send-Alerts' } },
  { path: 'way2money-sms-counts', component: Way2MoneysmsStagsComponent, data: { title: 'Way2money-sms-counts' } },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
