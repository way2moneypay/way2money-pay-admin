import { Component, OnInit, NgZone } from '@angular/core';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { PagerService } from '../../shared/pagination/pager.service';
@Component({
  selector: 'app-payment-by-sms',
  templateUrl: './payment-by-sms.component.html',
  styleUrls: ['./payment-by-sms.component.css']
})
export class PaymentBySmsComponent implements OnInit {
  sendsmsAlerts: any = {};
  message: any;
  currentPage: number;
  pager: any;
  logsLength: any;
  findStatus: any;
  errMsg = false;
  pagedItems: any;
  cpage: any;
  countStatus: any = false;
  paginationStatus = false;
  fromDate: any = "";
  toDate: any = "";
  searchStatus: boolean = false;
  responseStatus: any = false;
  searchVariable = "mobile_number";
  searchString: String = "";
  PaymentLinksLength: any;
  PaymentlinksDetails: any = [];
  resetButtonShowStatus: any = false;
  sendPushAlerts: any = {};
  mobilePattern = "^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$";
  constructor(
    private dataService: RestServicesService,
    private pagerservice: PagerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        if (this.searchStatus) {
          this.setSearchPage(this.currentPage);
        } else {
          this.getCount();
        }
      }
    });
  }

  ngOnInit() {
  }
  //SendPaymentlinkBySms() {
  // this.dataService.getAuthParams('api/sendSmsAlerts', this.sendsmsAlerts).subscribe((response) => {
  //console.log("AddUserPermissions", response);
  // if (response.success == true) {
  //   this.message = response.message;
  //   this.sendsmsAlerts = {};
  // } else {
  //   this.message = response.message;
  //  }
  //})
  //}

  getCount() {
    // console.log('cpagee : ', this.route.snapshot.params.cpage);
    // this.currentPage = this.route.snapshot.params.cpage;
    if (!this.countStatus) {
      this.dataService.getAuth("api/getPaymentLinksCount").subscribe(result => {
        console.log("result count::::::::", result);
        if (result.status) {
          this.logsLength = parseInt(result.data);
          this.countStatus = true;
          this.setLogsPage(this.currentPage);
        } else {
          this.logsLength = 0;
        }
      })
    } else {
      this.setLogsPage(this.currentPage);
    }
  }
  getLogs(skip, limit) {
    this.dataService.postAuth("api/getPaymentLinks", { skip: skip, limit: limit }).subscribe(result => {
      console.log("result data::::::::", result);
      if (result.status) {
        this.pagedItems = result.data;
        console.log('Result :: ', result.data);
      } else {
        this.paginationStatus = false;
        this.findStatus = false;
      }
      this.resetButtonShowStatus = false;
    })
  }
  convertInt(page) {
    return parseInt(page);
  }
  setPage(page) {
    // console.log("set page of ",page);
    this.cpage = 'settings/payment-sms/' + page;
    // console.log(this.currentPage);
    this.router.navigate([this.cpage]);
  }
  setLogsPage(page) {
    if (this.logsLength) {
      this.errMsg = false;
      if (this.logsLength > 10) {
        this.currentPage = page;
        this.findStatus = true;
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(Number(this.logsLength), Number(page), 10);
        // console.log("pager",this.pager);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getLogs(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      } else {
        this.currentPage = 1;
        this.getLogs(0, this.logsLength);
        this.findStatus = true;
        this.paginationStatus = false;
      }
    } else {
      this.errMsg = true;

    }
  }
  setSearchPage(page) {
    if (this.PaymentLinksLength) {
      if (this.PaymentLinksLength > 10) {
        this.findStatus = true;
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(this.PaymentLinksLength, page, 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.pagedItems = this.PaymentlinksDetails.slice(
          this.pager.startIndex,
          this.pager.endIndex + 1
        );
      } else {
        this.pagedItems = this.PaymentlinksDetails;
        this.findStatus = true;
        this.currentPage = 1;
        this.paginationStatus = false;
      }
    } else {
      this.findStatus = false;
    }
  }
  search() {
    console.log(this.fromDate, this.toDate)
    this.paginationStatus = false;
    this.searchStatus = false;
    this.responseStatus = true;
    var data = {};
    var from_date, to_date;
    console.log(this.fromDate, this.toDate)
    if (this.fromDate && this.fromDate != "") {
      from_date = new Date(this.fromDate).setHours(0, 0, 0, 0);
      data["type"] = "last_sent";
      data["fromDate"] = from_date;
    }
    if (this.toDate && this.toDate != "") {
      to_date = new Date(this.toDate);
      to_date.setDate(to_date.getDate() + 1);
      to_date.setHours(0, 0, 0, 0);
      data["toDate"] = to_date.getTime();

    }
    if (this.searchVariable == "mobile_number") {
      if (this.searchString != "")
        data["mobile_number"] = this.searchString;
    }
    console.log("search data is ", data);
    // console.log("got result from back end", result);
    this.dataService.postAuth("api/paymentlinksbysms", data).subscribe(result => {
      console.log(result)
      if (result.status) {
        this.PaymentlinksDetails = result.data;
        this.PaymentLinksLength = this.PaymentlinksDetails.length;
        this.countStatus = false;
        this.searchStatus = true;
        this.setPage(1);
        this.setSearchPage(1);
      } else {
        this.searchStatus = false;
        this.findStatus = false;
      }
      this.responseStatus = false;
    });

  }
  reset() {
    this.resetButtonShowStatus = true;
    this.getCount();
    this.searchString = "";
    this.searchStatus = false;
    this.fromDate = "";
    this.toDate = "";
  }
}
