import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentBySmsComponent } from './payment-by-sms.component';

describe('PaymentBySmsComponent', () => {
  let component: PaymentBySmsComponent;
  let fixture: ComponentFixture<PaymentBySmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentBySmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentBySmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
