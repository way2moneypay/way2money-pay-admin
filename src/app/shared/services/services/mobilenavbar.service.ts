
import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
@Injectable()
export class Mobilenavbar {
  mobile: any = false;
  constructor(
    @Inject(DOCUMENT) public document: any
  ) { }

  mobileToggle(value) {
    console.log("Hello", value);
    this.mobile = value;
    if (value) {
      this.document.body.classList.add('mobile-nav-active');
    } else {
      this.document.body.classList.remove('mobile-nav-active');
    }
  }

}