import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestServicesService } from '../services/rest-services/rest-services.service'
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'ap-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit, AfterViewInit {
    //this is for the open close
    isActive: boolean = true;
    showMenu: string = '';
    showSubMenu: string = '';
    //isMerchant;
    merchantId: any = 0;;
    getDetailsApi: any;
    userName: String;
    userPermissions: any = [];
    userType: String;
    constructor(public router: Router, private dataService: RestServicesService, private cookieService: CookieService

    ) { }

    ngOnInit() {
        //this.isMerchant = localStorage.getItem('isMerchant');
        this.merchantId = localStorage.getItem('id');
        if (this.merchantId) {
            this.getDetailsApi = 'api/user/details/' + this.merchantId;
        } else {
            this.getDetailsApi = 'api/user/details/' + this.merchantId;
        }
        this.dataService.getAuth(this.getDetailsApi).subscribe(result => {
            if (result.success) {
                // this.userName = result.merchantFound.name;
            } else {
                // this.userName = 'Admin';
            }
        });
        this.dataService.getAuth('api/user/userPermissions').subscribe(result => {
            this.userPermissions = result.data.permissions;
            this.userType = result.data.user_type;
            this.userName = result.data.user.name;
            console.log("BHAGAVAN", result.data.user.name)
        });
        //alert(this.isMerchant);
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
    addActiveClass(element: any) {
        if (element === this.showSubMenu) {
            this.showSubMenu = '0';
        } else {
            this.showSubMenu = element;
        }
    }
    eventCalled() {
        this.isActive = !this.isActive;

    }

    ngAfterViewInit() {
        $(function () {
            $(".sidebartoggler").on('click', function () {
                if ($("body").hasClass("mini-sidebar")) {
                    $("body").trigger("resize");
                    $(".scroll-sidebar, .slimScrollDiv").css("overflow", "hidden").parent().css("overflow", "visible");
                    $("body").removeClass("mini-sidebar");
                    $('.navbar-brand span').show();
                    //$(".sidebartoggler i").addClass("ti-menu");
                }
                else {
                    $("body").trigger("resize");
                    $(".scroll-sidebar, .slimScrollDiv").css("overflow-x", "visible").parent().css("overflow", "visible");
                    $("body").addClass("mini-sidebar");
                    $('.navbar-brand span').hide();
                    //$(".sidebartoggler i").removeClass("ti-menu");
                }
            });

        });
    }
    logout() {
        localStorage.removeItem('email');
        //localStorage.removeItem('isMerchant');
        localStorage.removeItem('id');
        this.cookieService.delete("admin_token");
        this.router.navigate(['/login']);
    }
}
