import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { CookieService } from "ngx-cookie-service";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private cookieService: CookieService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if(!this.cookieService.get("admin_token")){
            this.router.navigate(['/login']);
            return false;
        }else{
            return true;
        }
        // const urls = state.url.split('/')
        // if (localStorage.getItem('email')) {
        //     if (localStorage.getItem('email') != 'admin@way2pay.com') {
        //         if (localStorage.getItem('isMerchant') == 'yes') {
        //             if (urls[0] == "" && urls[1] == "") {
        //                 return true;
        //             }
        //             if (urls[1] == "user" && urls[2] == "transactions") {
        //                 return true
        //             }
        //             else {
        //                 return false;
        //             }
        //         } else {
        //             this.router.navigate(['/login']);
        //             return false;
        //         }
        //     } else {
        //         return true;
        //     }
        // } else {
        //     this.router.navigate(['/login']);
        //     return false;
        // }
    }
}
