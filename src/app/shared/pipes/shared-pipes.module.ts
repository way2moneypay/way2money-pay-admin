import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';


// @NgModule({
//     imports: [


//     ],
//     declarations: []
// })
@Pipe({
  name: 'numberFormat'
})

export class SharedPipesModule implements PipeTransform {

  constructor() { }

  transform(number: any, sub: boolean) {
    let hasMinus = String(number).charAt(0) === '-' ? true : false;
    number = String(number).charAt(0) === '-' ?
      + String(number).substring(1, number.length) : number;
    // hundreds
    if (number <= 999) {
      number = sub ? number.toFixed(2) : number;
    }
    // thousands
    else if (number >= 1000 && number <= 999999) {
      number = this.getNumber(number / 1000) + 'K';
    }
    // millions
    else if (number >= 1000000 && number <= 999999999) {
      number = this.getNumber(number / 1000000) + 'M';
    }
    // billions
    else if (number >= 1000000000 && number <= 999999999999) {
      number = this.getNumber(number / 1000000000) + 'B';
    }
    if (hasMinus) {
      return '-' + number;
    } else {
      return number;
    }
  }

  getNumber = function (num) {
    let numbefore = num.toString().split(".")[0];
    let numberafter = num.toString().split(".")[1];
    numbefore = numbefore ? numbefore : '0';
    /* if(numberafter.length) */
    if (numberafter && numberafter.length > 2) {
      numberafter = numberafter.substring(0, 2);
    }
    numberafter = numberafter ? numberafter : '00';
    return numbefore + '.' + numberafter;
  }



}
