import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedPipesModule } from './shared-pipes.module';
import { TruncatePipe } from './exponential-strength.pipe';

@NgModule({
    imports: [CommonModule],
    declarations: [SharedPipesModule, TruncatePipe],
    exports: [SharedPipesModule, TruncatePipe]
})
export class PipesModule {}
