import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
    name: 'thousandSuff'
})

@Injectable()
export class ThousandSuffixesPipe implements PipeTransform {

    transform(number: any, args?: any): any {
        if(number == 0) {
            return 0;
        }
        else
        {        
          // hundreds
          if(number <= 999){
            return number ;
          }
          // thousands
          else if(number >= 1000 && number <= 999999){
            return (number / 1000).toFixed(1) + 'K';
          }
          // millions
          else if(number >= 1000000 && number <= 999999999){
            return (number / 1000000).toFixed(1) + 'M';
          }
          // billions
          else if(number >= 1000000000 && number <= 999999999999){
            return (number / 1000000000).toFixed(1) + 'B';
          }
          else
            return number ;
          }
        }
    }

        // var exp, rounded,
        //     suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];

        // if (Number.isNaN(input)) {
        //     return null;
        // }

        // if (input < 1000) {
        //     return input;
        // }

        // exp = Math.floor(Math.log(input) / Math.log(1000));

        // // console.log("op at pipes module :: ", (input / Math.pow(1000, exp)).toFixed(args) + suffixes[exp - 1]);
        // return (input / Math.pow(1000, exp)).toFixed(args) + suffixes[exp - 1];


    



