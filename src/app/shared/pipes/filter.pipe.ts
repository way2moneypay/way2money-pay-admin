import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
    name: 'filter'
})

@Injectable()
export class FilterPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {
        // console.log("items:: ", items, "value:: ", field, "val :", value);
        if (!items) {
            return [];
        }
        if (!field || !value) {
            return items;
        }

        items = items.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()));
        // console.log("data ::",items);
        return items;
    }
}


