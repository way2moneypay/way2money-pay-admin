import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutoCompleteComponent } from './auto-complete.component';
import { PipesModule } from '../pipes/pipes-module';
import { RouterModule } from '@angular/router';
import { ClickOutsideModule } from 'ng4-click-outside';

@NgModule({
    imports: [CommonModule, PipesModule, RouterModule, ClickOutsideModule],
    declarations: [AutoCompleteComponent],
    exports: [AutoCompleteComponent]
})
export class AutoCompleteModule { }
