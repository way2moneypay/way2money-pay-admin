import { Component, OnInit, Input, Output, EventEmitter, HostListener, PLATFORM_ID, Inject } from '@angular/core';
// import { isPlatformBrowser, isPlatformServer } from '@angular/common';
// import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'auto-complete',
  templateUrl: './auto-complete.component.html',
  styleUrls: ['./auto-complete.component.css']
})
export class AutoCompleteComponent implements OnInit {
  @Input() dataSets: any = [];
  @Input() dataName: string;
  @Input() dataSplit: string;
  @Input() type: string;
  @Input() name: string;
  @Input() loading: boolean;
  @Input() loadingMessage: string;
  @Output() select: EventEmitter<any> = new EventEmitter<any>();
  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  selectItems: any = {};

  constructor() {

  }

  onSelect(index) {
    console.log(index);
    let return_data: any = {};
    return_data.index = index;
    return_data.data = this.dataSets[index];
    this.select.emit(return_data);
  }

  onClose() {
    this.close.emit();
  }

  ngOnChanges(changes) {
    if (changes.dataSets) {
      this.selectItems[this.name] = 0;
    }
  }

  ngOnInit() {
    this.selectItems[name] = 0;
    console.log(this.selectItems);
  }

  changeSelect(i) {
    // console.log("mouse over :: ", i)
    this.selectItems[this.name] = i;
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    let key = event.keyCode;

    if ([38, 40, 13].indexOf(key) == -1) return;

    if (this.dataSets.length == 0 && !this.selectItems[name]) {
      return;
    }



    switch (key) {
      case 38:
        console.log(this.name);
        this.selectItems[this.name] = this.selectItems[this.name] - 1 < 0 ? this.dataSets.length - 1 : this.selectItems[this.name] - 1;
        break;
      case 40:
        this.selectItems[this.name] = this.selectItems[this.name] + 1 == this.dataSets.length ? 0 : this.selectItems[this.name] + 1;
        break;
      case 13:
        console.log(this.selectItems[this.name]);
        this.onSelect(this.selectItems[this.name]);
        break;
      default:
        break;
    }
  }
  getName(data, dataName, dataSplit) {
    if (!dataSplit) {
      return data[dataName];
    }

    const dataFields = dataName.split(dataSplit);
    let return_data = '';

    dataFields.forEach((element, index) => {
      // console.log(element);
      if (element.indexOf('@') === 0) {
        // console.log('substr ', element.substring(1));
        return_data += data[element.substring(1)] + ((index === dataFields.length - 1) ? '' : dataSplit);
      } else {
        return_data += data[element] + ((index === dataFields.length - 1) ? '' : dataSplit);
      }
    });
    // console.log('autocomplete return data', return_data);

    return return_data;
  }
}
