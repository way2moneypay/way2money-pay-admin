import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { PagerService } from '../../shared/pagination/pager.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pipe, PipeTransform } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { toDate } from '@angular/common/src/i18n/format_date';

@Component({
  selector: 'app-add-offer',
  templateUrl: './add-offer.component.html',
  styleUrls: ['./add-offer.component.css']
})
export class AddOfferComponent implements OnInit {


  constructor(private apiService: RestServicesService,
    private pagerservice: PagerService, private router: Router, private route: ActivatedRoute) {
      router.events.subscribe((val) => {
        if (val instanceof NavigationEnd) {
          // console.log('cpagee : ', this.route.snapshot.params.cpage);
          // this.offerId = this.route.snapshot.params.offerId;
          // if(this.offerId){
          //   this.reqObj['_id'] = this.offerId
          //   this.apiService.postAuth('offer/edit-offer', this.reqObj)
          //   .subscribe((Response) => {
          //     console.log("SEARCH RESULTS", Response.data);
          //     // console.log("SEARCHING BRANDING COPYING TO ARRA", this.merchant_brand_array);
          //     if (Response.status) {
          //      alert('Added successfully')
          //       } else{
          //         alert('Error while adding offer')
          //       }
          //   }, (error_data) => {
          //   });
          // }
        }
      });
  }
  reqObj:any = {}
  offerId :any;
  BrandsAutofillData = [];
  merchant_brand_array: any = [];
  b_name: any;
  brands_data: any;
  merchant_id: any;
  merchant_logo: any;
  address: any= ""
  fromDate:any = ""
  toDate: any = ""
  status: any= "active"
  offer_percentage:any = ""
  brandName:any =""
  // qr_download: String = environment.qr_download;
  // public model: any;
  // resetButtonStatus: any = false;
  // searchMerchantStatus: any = false;
  // countStatus: any = false;
  // merchantDetails: any = [];
  // merchantsLength: number;
  // newMobile = "";
  // changeButton = false;
  // currentPage: number = 1;
  // paginationStatus = false;
  // searchString: any = "";
  // searchVariable = 'brand_name';
  // defaultMStatusMsg: any = "Select Status";
  // searchStatus: boolean = false;
  // pager: any;
  // i: number = 0;
  // findStatus: any;
  // cpage: any;
  // pagedItems: any;
  // InputAuotfill: any;
  // currentMobileNo: any;
  // showModal = false;
  // changedStatus = false;
  // dismissModal = false;
  // changingId: any;
  // InputAutoFill: any;
  // statusOptions = [
  //   { value: 'Active' },
  //   { value: 'Inactive' }
  // ]

  ngOnInit() {
  }
  getBrands(event, brand_name) {
    console.log(event.keyCode, brand_name);
    if (!brand_name) { this.brands_data = []; }
    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 65 || event.keyCode > 90) &&
      event.keyCode !== 32 && event.keyCode !== 8 && event.keyCode !== 190) {
      return;
    }


    if (event.keyCode === 13 || !brand_name || brand_name.length < 3) {
      this.merchant_brand_array = [];
      return;
    }
    const params: any = {
      brand_name: brand_name,
      limit: 6
    };

    // this.companies_loading = true;
    this.apiService.postAuth('user/searchingBrands', params)
      .subscribe((Response) => {
        console.log("SEARCH RESULTS", Response.data);
        // console.log("SEARCHING BRANDING COPYING TO ARRA", this.merchant_brand_array);
        if (Response.data) {
          this.merchant_brand_array = [];
          for (let i = 0; i < Response.data.length; i++) {
            this.merchant_brand_array.push({
              _id: Response.data[i]._id,
              brand_name: Response.data[i]._source.brand_name
            })
            // this.merchant_id_array[i] = Response.data[i]._id;
            // this.merchant_brand_array[i] = Response.data[i]._source.brand_name;
            // this.InputAutoFill[i] = Response.data[i]._source.brand_name;
          }
          // this.InputAutoFill = (text$: Observable<string>) =>
          //   text$.pipe(
          //     debounceTime(200),
          //     distinctUntilChanged(),
          //     map(term => term.length < 2 ? []
          //       : this.merchant_brand_array.filter(v => v.indexOf(term) > -1).slice(0, 10))
          //   );
          // console.log("SEARCHING _IDDSS COPYING TO ARRA", this.merchant_id_array.length);

        } else {
          // console.log('NO DATA FOUND BRANDS');
        }
      }, (error_data) => {
      });
  }
  selectedItem(item) {
    console.log("ITEM", item);
    this.b_name = item.data.brand_name;
    this.merchant_id = item.data._id;
    this.merchant_logo = item.data.logo_url;
    this.merchant_brand_array.length = 0;
    this.merchant_brand_array = [];
    console.log("HELLO MERCHANT", this.merchant_id);

  }
  addOffer(){
   var data ={}
    var from_date, to_date;
    if (this.fromDate && this.fromDate != "") {
      from_date = new Date(this.fromDate).setHours(0, 0, 0, 0);
      data["fromDate"] = from_date;
    }
    if (this.toDate && this.toDate != "") {
      to_date = new Date(this.toDate);
      to_date.setDate(to_date.getDate() + 1);
      to_date.setHours(0, 0, 0, 0);
      data['toDate'] = to_date.getTime();
      data['merchant_id']= this.merchant_id
      data['merchant_logo'] = this.merchant_logo
      data['brand'] = this.b_name
      data['brand_name'] = this.brandName
      data['status'] = this.status
      data['offer_percentage'] = this.offer_percentage
      data['address'] = this.address
    }
    // console.log('data', data,this.b_name,this.status,this.merchant_logo, this.address)
    this.apiService.postAuth('offer/add-offer', data)
    .subscribe((Response) => {
      console.log("SEARCH RESULTS", Response.data);
      // console.log("SEARCHING BRANDING COPYING TO ARRA", this.merchant_brand_array);
      if (Response.status) {
       alert('Added successfully')
        } else{
          alert('Error while adding offer')
        }
    }, (error_data) => {
    });
  }
}
