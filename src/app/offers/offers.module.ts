import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OffersRoutingModule } from './offers-routing.module';
import { AddOfferComponent } from './add-offer/add-offer.component';
import { ViewOffersComponent } from './view-offers/view-offers.component';


import { environment } from '../../environments/environment';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutoCompleteModule } from '../shared/auto-complete/auto-complete.module';

@NgModule({
  imports: [
    CommonModule,
    OffersRoutingModule,
    CommonModule,
    FormsModule,
    AutoCompleteModule,
    NgbModule.forRoot(),
  ],
  declarations: [AddOfferComponent, ViewOffersComponent]
})
export class OffersModule { }
