import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { PagerService } from '../../shared/pagination/pager.service';
@Component({
  selector: 'app-view-offers',
  templateUrl: './view-offers.component.html',
  styleUrls: ['./view-offers.component.css']
})
export class ViewOffersComponent implements OnInit {

  fromDate: any = ""
  toDate: any = ""
  status: any = "active"
  offer_percentage: any = ""
  brandName: any = ""
  BrandsAutofillData = [];
  merchant_brand_array: any = [];
  b_name: any;
  brands_data: any;
  merchant_id: any;
  merchant_logo: any;
  offersList: any;
  cpage: any;
  countStatus: any = false;
  paginationStatus = false;
  noAlerts: any;
  smsLength: any;
  currentPage: number;
  isOfferData = false;
  pager: any;
  addOffersReq: any = {};
  addOffers: any = {
    status: 'active'
  };
  errAddOfeer = false;
  errAddOfeerMessage: any;
  editOfferId: any;
  alertButtonStatus: any = false;
  options = [
    { name: "active", value: 'active' },
    { name: "inactive", value: 'inactive' }
  ]
  offers_status: string;
  changeOfferStatus: any = {};
  constructor(private dataService: RestServicesService, private pagerservice: PagerService, private router: Router, private route: ActivatedRoute, private _location: Location) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        // console.log('cpagee : ', this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;
        this.editOfferId =this.route.snapshot.params.offerId
        if(this.editOfferId){
          this.editOffer(this.editOfferId)
        } else {
          if (this.currentPage < 1 || this.currentPage == undefined) {
            this.currentPage = 1;
          }
          this.getCount();
        }
       
      }
    });

  }

  ngOnInit() {
    this.editOfferId = this.route.snapshot.params.offerId;
    if (this.editOfferId && this.editOfferId != 0) {
      this.editOffer(this.editOfferId);
    } else if (this.editOfferId && this.editOfferId == 0) {
      this.isOfferData = true;
    } else {
      this.getOffetsList(0, 10);
    }
  }
  convertInt(page) {
    return parseInt(page);
  }
  getCount() {
    // if (!this.countStatus) {
      this.dataService.getAuth("api/get-Offers-list/getCount").subscribe(result => {
        if (result.status) {
          this.smsLength = parseInt(result.data);
          this.countStatus = true;
          this.setOffersPage(this.currentPage);
        } else {
          this.smsLength = 0;
        }
      })
    // } else {
    //   this.setOffersPage(this.currentPage);
    // }
  }
  setPage(page) {
    if (page < 1) {
      page = 1;
    } else if (page > this.pager.totalPages) {
      page = this.pager.totalPages;
    }
    this.cpage = 'offer/view-offers/' + page;
    this.router.navigate([this.cpage]);
  }
  setOffersPage(page) {
    if (this.smsLength) {
      if (this.smsLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerservice.getPager(Number(this.smsLength), Number(page), 10);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.getOffetsList(this.pager.startIndex, this.pager.endIndex + 1 - this.pager.startIndex);
      }
      else {
        this.currentPage = 1;
        // console.log("current:: ", this.currentPage);
        this.getOffetsList(0, this.smsLength);
        this.paginationStatus = false;
      }
    } else {
      this.noAlerts = true;
    }
  }
  editOffer(ID) {
    this.editOfferId =this.route.snapshot.params.offerId
    var obj={}
    obj["_id"] = ID
    this.dataService.postAuth('offers/edit-offer', obj).subscribe(result => {
      if (result.success) {
        this.isOfferData = true;
        this.paginationStatus = false;
        var fromdate = new Date(result.data.expiry_date_from)
        var from_day ={}
         from_day= {
           year:fromdate.getFullYear(),
           month:fromdate.getMonth() +1,
           day:fromdate.getDate()
          }

        var todate = new Date(+result.data.expiry_date_to)
        var to_day ={}
        to_day= {
           year:todate.getFullYear(),
           month:todate.getMonth() +1,
           day:todate.getDate()
          }
        this.fromDate = from_day
        this.toDate = to_day
        this.status = result.data.offers_status
        this.merchant_id= result.data.merchant_id
        this.b_name = result.data.brand
        this.brandName = result.data.brand_name
        this.offer_percentage = result.data.offer_details
        // console.log('in edit offer', this.offer_percentage, this.brandName)
        // this.addOffers = result.smsAlerts[0];
      }
    });
  }
  getOffetsList(skip, limit) {
    var data = {};
    data["skip"] = skip;
    data["limit"] = limit;
    if (!this.route.snapshot.params.alertId) {
      this.isOfferData = false;
      // this.noAlerts=false;
    }
    this.dataService.postAuth('api/get-Offers-list', data).subscribe(result => {
      if (result.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (result.success) {
          // console.log(result)
          this.offersList = result.offersList;
        } else {
          this.noAlerts = true;
        }
      }
    });
  }

  redirectAddOffers() {
    this.isOfferData = true;
    this.paginationStatus = false;
    this.addOffers = {};
    this.router.navigate(['offer/view-offers/add/0/' + this.currentPage]);
  }

  onSubmitAddOffer() {
    var data = {}
    this.editOfferId =this.route.snapshot.params.offerId
    if(this.editOfferId){
      data['offer_id'] = this.editOfferId
    }
    var from_date, to_date;
    if (this.fromDate && this.fromDate != "") {
      from_date = new Date(this.fromDate.year, this.fromDate.month -1, this.fromDate.day).setHours(0, 0, 0, 0);
      data["fromDate"] = from_date/1;
    }
    if (this.toDate && this.toDate != "") {
      to_date = new Date(this.toDate.year, this.toDate.month, this.toDate.day);
      to_date.setDate(to_date.getDate() + 1);
      to_date.setHours(0, 0, 0, 0);
      data['toDate'] = to_date.getTime();
      data['merchant_id'] = this.merchant_id
      data['brand'] = this.b_name
      data['brand_name'] = this.brandName
      data['status'] = this.status
      data['offer_percentage'] = this.offer_percentage
    }
    this.alertButtonStatus = true;

    this.dataService.postAuth('offer/add-offer', data).subscribe(result => {
      console.log(result)
      if (result.status) {
        this.isOfferData = false;
        this.currentPage = this.route.snapshot.params.cpage;
        this.router.navigate(['offer/view-offers/'+this.currentPage]);
      } else {
        this.errAddOfeer = true;
        this.errAddOfeerMessage = result.message;
      }
      this.alertButtonStatus = false;
    });
  }

  // onSubmitUpdateAlert() {
  //      var data = {}
  //   this.editOfferId =this.route.snapshot.params.offerId
  //   if(this.editOfferId){
  //     data['offer_id'] = this.editOfferId
  //   }
  //   var from_date, to_date;
  //   if (this.fromDate && this.fromDate != "") {
  //     from_date = new Date(this.fromDate.year, this.fromDate.month -1, this.fromDate.day).setHours(0, 0, 0, 0);
  //     data["fromDate"] = from_date/1;
  //   }
  //   if (this.toDate && this.toDate != "") {
  //     to_date = new Date(this.toDate.year, this.toDate.month, this.toDate.day);
  //     to_date.setDate(to_date.getDate() + 1);
  //     to_date.setHours(0, 0, 0, 0);
  //     data['toDate'] = to_date.getTime();
  //     data['merchant_id'] = this.merchant_id
  //     data['brand'] = this.b_name
  //     data['brand_name'] = this.brandName
  //     data['status'] = this.status
  //     data['offer_percentage'] = this.offer_percentage
  //   }
  //   this.alertButtonStatus = true;

  //   this.dataService.postAuth('offer/add-offer', data).subscribe(result => {
  //     console.log(result)
  //     if (result.status) {
  //       this.isOfferData = false;
  //       this.currentPage = this.route.snapshot.params.cpage;
  //       this.router.navigate(['offer/view-offers/'+this.currentPage]);
  //     } else {
  //       this.errAddOfeer = true;
  //       this.errAddOfeerMessage = result.message;
  //     }
  //     this.alertButtonStatus = false;
  //   });
  // }

  backClicked() {
    this._location.back();
  }
  getBrands(event, brand_name) {
    console.log(event.keyCode, brand_name);
    if (!brand_name) { this.brands_data = []; }
    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 65 || event.keyCode > 90) &&
      event.keyCode !== 32 && event.keyCode !== 8 && event.keyCode !== 190) {
      return;
    }


    if (event.keyCode === 13 || !brand_name || brand_name.length < 3) {
      this.merchant_brand_array = [];
      return;
    }
    const params: any = {
      brand_name: brand_name,
      limit: 6
    };

    // this.companies_loading = true;
    this.dataService.postAuth('user/searchingBrands', params)
      .subscribe((Response) => {
        console.log("SEARCH RESULTS", Response.data);
        // console.log("SEARCHING BRANDING COPYING TO ARRA", this.merchant_brand_array);
        if (Response.data) {
          this.merchant_brand_array = [];
          for (let i = 0; i < Response.data.length; i++) {
            this.merchant_brand_array.push({
              _id: Response.data[i]._id,
              brand_name: Response.data[i]._source.brand_name
            })
            // this.merchant_id_array[i] = Response.data[i]._id;
            // this.merchant_brand_array[i] = Response.data[i]._source.brand_name;
            // this.InputAutoFill[i] = Response.data[i]._source.brand_name;
          }
          // this.InputAutoFill = (text$: Observable<string>) =>
          //   text$.pipe(
          //     debounceTime(200),
          //     distinctUntilChanged(),
          //     map(term => term.length < 2 ? []
          //       : this.merchant_brand_array.filter(v => v.indexOf(term) > -1).slice(0, 10))
          //   );
          // console.log("SEARCHING _IDDSS COPYING TO ARRA", this.merchant_id_array.length);

        } else {
          // console.log('NO DATA FOUND BRANDS');
        }
      }, (error_data) => {
      });
  }
  selectedItem(item) {
    console.log("ITEM", item);
    this.b_name = item.data.brand_name;
    this.merchant_id = item.data._id;
    this.merchant_logo = item.data.logo_url;
    this.merchant_brand_array.length = 0;
    this.merchant_brand_array = [];
    console.log("HELLO MERCHANT", this.merchant_id);

  }


  changeStatus(check, offer_id) {
    this.offers_status = 'inactive';
    if (check) {
      this.offers_status = 'active';
    }
    if (confirm('Do You Want Sure Change This Status ?')) {
      this.changeOfferStatus.premission_id = offer_id
      this.changeOfferStatus.offers_status = this.offers_status;
      console.log("--->>>>", offer_id, this.offers_status)
      this.dataService.getAuthParams('api/changeOffersStatus', this.changeOfferStatus).subscribe((response) => {
        if (response.success == true) {
          console.log("Successfully updated offers status")
          this.ngOnInit();
        } else {
          console.log("Error While updataing  offers status");
        }
      });
    }
  }

}
