import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddOfferComponent } from './add-offer/add-offer.component'
import { ViewOffersComponent } from './view-offers/view-offers.component'
import { AuthGuard } from '../shared/guard/auth.guard';

const routes: Routes = [
  // { path: '', redirectTo: 'add-offer', pathMatch: 'full', data: { title: 'Add Offer' }, canActivate: [AuthGuard] },
  // { path: 'add-offer', component: AddOfferComponent, data: { title: 'Add Offer' }, canActivate: [AuthGuard] },
  // { path: 'add-offer/:offerId/:cpage', component: AddOfferComponent, data: { title: 'Add Offer' }, canActivate: [AuthGuard] },
  { path: 'view-offers/:cpage', component: ViewOffersComponent, data: { title: 'View Offers' }, canActivate: [AuthGuard] },
  { path: 'view-offers/add/:offerId/:cpage', component: ViewOffersComponent, data: { title: 'Add Offer' }, canActivate: [AuthGuard] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersRoutingModule { }
