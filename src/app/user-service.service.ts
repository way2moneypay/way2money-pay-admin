import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/map';
import { HttpRequest, HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(public http:Http,private httpClient:HttpClient) { }
  apiurl: String = environment.apiurl;
  get(url) {
    return this.http.get(this.apiurl + url)
      .map(res =>
        res.json()
      );
  }
}
