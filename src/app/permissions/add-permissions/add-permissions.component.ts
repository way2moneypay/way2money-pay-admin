import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-add-permissions',
  templateUrl: './add-permissions.component.html',
  styleUrls: ['./add-permissions.component.css']
})
export class AddPermissionsComponent implements OnInit {
  permission: any = {};
  regAdminUserForm = {};
  permissions: any;
  userpermission: any = {};
  userpermissions: any = {};
  permission_details_id = "";
  selected_user_type: any = [];
  permissionsforusers: any = {};
  message: any;
  constructor(
    private dataService: RestServicesService,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private ngZone: NgZone,
    private modalService: NgbModal,
  ) { }
  user_types: any = [
    // {
    //   id: "admin",
    //   itemName: "Admin"
    // },
    {
      id: "regional_head",
      itemName: "Admin"
    },
    {
      id: "sales_team",
      itemName: "Sales Team"
    },
    {
      id: "accountant",
      itemName: "Accountant"
    }
  ]
  prepareSelectedItemsToStore(data) {
    let return_data = [];
    for (var index = 0; index < data.length; ++index) {
      return_data.push(data[index]['id']);
    }
    return return_data;
  }
  ngOnInit() {
    //this.dataService.getAuth('api/gettemplateAuth').subscribe((templateresponse) => {
    // console.log("templateresponse", templateresponse);
    //  if (templateresponse.message == "User authentication failed.") {
    //    let path = "/permission-denied";
    //   this.router.navigate([path]);
    //   }
    // });
  }
  AddPermissions() {
    if (confirm('Do You Want Sure Add This AddPermission ?')) {
      //console.log(this.permission)
      this.dataService.postAuth('api/addpermissions', this.permission).subscribe((clientresponse) => {
        if (clientresponse.success == true) {
          //this.ngOnInit()
          console.log(clientresponse)
          this.message = clientresponse.message;
          this.permission = {}
        } else {
          console.log("Error While Adding Permissions")
        }
      });
    }
  }

  getPermissions() {
    this.dataService.getAuth('api/getpermissions').subscribe((response) => {
      console.log("getPermissions", response.Permissions);
      if (response.success == true) {
        this.permissions = response.Permissions;
      } else {
        console.log("Error While getting permission types")
      }
    });
  }
  AddUserPermissions() {
    if (confirm('Do You Want Sure Add This UserPermission ?')) {
      this.permissionsforusers.user_types = this.prepareSelectedItemsToStore(this.selected_user_type);
      this.permissionsforusers.permission_name = this.userpermission.permission_name;
      this.permissionsforusers.permission_details_id = this.userpermission.permission_details_id;
      this.permissionsforusers.permission_description = this.userpermission.permission_description;
      console.log(this.permissionsforusers);
      this.dataService.postAuth('api/adduserpermissions', this.permissionsforusers).subscribe((response) => {
        console.log("AddUserPermissions", response);
        if (response.success == true) {
          //console.log(response);
          this.userpermission = '';
          this.message = response.message;
        } else {
          console.log("Error while Adding user permissions ")
        }
      });
    }
  }
}
