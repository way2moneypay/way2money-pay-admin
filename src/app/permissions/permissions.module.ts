import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PermissionsRoutingModule } from './permissions-routing.module';
import { AddPermissionsComponent } from './add-permissions/add-permissions.component';
import { ViewPermissionsComponent } from './view-permissions/view-permissions.component';
//import { ThousandSuffixesPipe } from './../shared/pipes/convert.pipe';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
// import { FormFieldsModule } from './../../shared/modules/form-fields/form-fields.module';
@NgModule({
  imports: [
    CommonModule,
    PermissionsRoutingModule,
    CommonModule,
    FormsModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule,
    NgbModule.forRoot(),
    // FormFieldsModule,
  ],
  declarations: [AddPermissionsComponent, ViewPermissionsComponent]
})
export class PermissionsModule { }
