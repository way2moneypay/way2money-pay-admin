import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { RestServicesService } from "../../shared/services/rest-services/rest-services.service";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-permissions',
  templateUrl: './view-permissions.component.html',
  styleUrls: ['./view-permissions.component.css']
})
export class ViewPermissionsComponent implements OnInit {
  permissions_list: any;
  closeResult: string;
  hidden_status: number;
  userpermissions: any = {};
  permissionsforusers_update: any = {};
  permissions: any;
  deletepremission: any = {};
  selected_user_type: any = [];
  permissionsforusers: any = {};
  constructor(
    private dataService: RestServicesService,
    private route: ActivatedRoute,
    private router: Router,
    private _location: Location,
    private ngZone: NgZone,
    private modalService: NgbModal
  ) { }
  user_types: any = [
    // {
    //   id: "admin",
    //   itemName: "Admin"
    // },
    {
      id: "regional_head",
      itemName: "Admin"
    },
    {
      id: "sales_team",
      itemName: "Sales Team"
    },
    {
      id: "accountant",
      itemName: "Accountant"
    }
  ]
  prepareSelectedUserTypes(selected_types) {
    this.selected_user_type = [];
    for (var index = 0; index < this.user_types.length; ++index) {
      if (selected_types && selected_types.indexOf && selected_types.indexOf(this.user_types[index]["id"]) != -1) {
        this.selected_user_type.push(this.user_types[index]);
      }
    }
  }

  prepareSelectedItemsToStore(data) {
    let return_data = [];
    for (var index = 0; index < data.length; ++index) {
      return_data.push(data[index]['id']);
    }
    return return_data;
  }
  getPermissions() {
    this.dataService.getAuth('api/getpermissions').subscribe((response) => {
      console.log("getPermissions", response.Permissions);
      if (response.success == true) {
        this.permissions = response.Permissions;
      } else {
        console.log("Error While getting permission types")
      }
    });
  }
  ngOnInit() {
    this.dataService.getAuth('api/getPermissionsList').subscribe((response) => {
      if (response.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (response.success == true) {
          //console.log(response);
          this.permissions_list = response.data;
          this.getPermissions()
          console.log(this.permissions);
        } else {
          console.log("Error While Getting Permissions");
        }
      }
    });
  }
  open(content) {
    this.popupCalls(content);
  }

  TemplateEdit(content, data) {
    console.log("content", content);
    console.log("data", data);
    this.userpermissions = data;
    this.prepareSelectedUserTypes(data.user_types);
    // console.log(",",this.userpermissions);
    this.popupCalls(content);
  }

  popupCalls(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  EditUserPermissions() {
    if (confirm('Do You Want Sure Update This UserPermission ?')) {
      //console.log("selected_user_type", this.userpermissions)
      this.permissionsforusers_update.user_types = this.prepareSelectedItemsToStore(this.selected_user_type);
      this.permissionsforusers_update.permission_name = this.userpermissions.permission_name;
      this.permissionsforusers_update.permission_details_id = this.userpermissions.permission_details_id;
      this.permissionsforusers_update.permission_description = this.userpermissions.permission_description;
      this.permissionsforusers_update.permission_id = this.userpermissions._id;
      // console.log(this.permissionsforusers_update);
      this.dataService.postAuth('api/edituserpermissions', this.permissionsforusers_update).subscribe((response) => {
        // console.log('EditUserPermissions', response)
        if (response.success == true) {
          console.log("Successfully updated user permission")
          this.router.navigate(["/permissions/view-permissions/1"]).then(() => {
            this.ngOnInit();
          })
        } else {
          console.log("Error While updataing  user Permissions");
        }
      });
    }
  }

  DeletePermission(check, permission_id) {
    this.hidden_status = 0;
    if (check) {
      this.hidden_status = 1;
    }
    if (confirm('Do You Want Sure Change This Status ?')) {
      this.deletepremission.premission_id = permission_id
      this.deletepremission.hidden_status = this.hidden_status;
      this.dataService.getAuthParams('api/deleteuserpermissions', this.deletepremission).subscribe((response) => {
        if (response.success == true) {
          console.log("Successfully updated user permission status")
          this.router.navigate(["/permissions/view-permissions/1"]).then(() => {
            this.ngOnInit();
            // this.userpermissions = {};
          })
        } else {
          console.log("Error While updataing  user Permissions status");
        }
      });
    }
  }

}
