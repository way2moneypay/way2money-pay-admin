import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPermissionsComponent } from './add-permissions/add-permissions.component';
import { ViewPermissionsComponent } from './view-permissions/view-permissions.component';
const routes: Routes = [
  { path: '', redirectTo: 'add-permissions', pathMatch: 'full', data: { title: 'Add Permissions' } },
  { path: 'add-permissions', component: AddPermissionsComponent, data: { title: 'Add Permissions' } },
  { path: 'view-permissions/:cpage', component: ViewPermissionsComponent, data: { title: 'View Permissions' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermissionsRoutingModule { }
