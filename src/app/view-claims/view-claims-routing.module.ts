import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewClaimsComponent } from './view-claims.component'
const routes: Routes = [
  { path: '', component: ViewClaimsComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewClaimsRoutingModule { }
