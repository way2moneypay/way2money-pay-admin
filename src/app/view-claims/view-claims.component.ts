import { Component, OnInit } from "@angular/core";
import { RestServicesService } from "../shared/services/rest-services/rest-services.service";
import { PagerService } from "../shared/pagination/pager.service";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { Observable } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FormsModule } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: "app-view-claims",
  templateUrl: "./view-claims.component.html",
  styleUrls: ["./view-claims.component.css"]
})
export class ViewClaimsComponent implements OnInit {
  b_name: any;
  status: string;
  userType: any;
  isAccountant: boolean;
  constructor(
    private modalService: NgbModal,
    private dataService: RestServicesService,
    private pagerService: PagerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        // console.log("cpagee : ", this.route.snapshot.params.cpage);
        this.currentPage = this.route.snapshot.params.cpage;

        if (this.currentPage < 1 || this.currentPage == undefined) {
          this.currentPage = 1;
        }
        if (this.searchStatus) {
          this.setSearchPage(this.currentPage);
        } else {
          this.emailId = localStorage.getItem("email");
          this.getCount();
        }
      }
    });
  }
  merchant_id: any;
  brands_data: any;
  merchant_brand_array: any = [];
  merchant_data: any;
  countStatus: any = false;
  viewClaimsDetails: any;
  private allItems: any[];
  pager: any = {};
  transPager: any = {};
  cpage: any;
  brandName: any;
  pagedItems: any[];
  transPagedItems: any[];
  transPaginationStatus: any;
  transErrMsg: any;
  currentPage: number = 1;
  errMsg = false;
  cycle_date: any;
  pageLength: any;
  searchString = "";
  claimButton = false;
  searchVariable = "b_name";
  showData = false;
  checkClaimStatus = false;
  totalPage = true;
  searchStatus: any = false;
  paginationStatus: any = false;
  emailId: any;
  merchantObj: any = {};
  claimId: any;
  fromDate: any = "";
  toDate: any = "";
  claimAmount: any;
  claimRefId: any = "";
  claimedStatus = false;
  redirectUrl: String;
  obj: any;
  responseStatus: any = false;
  resetButtonShowStatus: any = false;
  BrandsAutofillData = [];
  InputAuotfill: any;
  cycle_status: any = "pending";
  pending_cycle_count: any;
  merchant_cycle_transactions: any[];
  user_index = 0;
  transPageLength: any;
  current_brand_name: any;
  currentTransPage: any = 1;
  checkStatus: any = false;
  closeResult: string;
  ngOnInit() {
    this.merchant_id = this.route.snapshot.queryParams.merchant_id;
    // console.log("merchant_id--------------->", this.merchant_id)
    this.getCount();
    this.getBrandsList();
    var data = { "cycle_status": "pending" };
    this.getPendingCount(data);
    this.dataService.getAuth('api/user/userPermissions').subscribe(result => {
      this.userType = result.data.user_type;
      if (this.userType == 'accountant') {
        this.isAccountant = true
      } else {
        this.isAccountant = false
      }
      // console.log("BHAGAVAN", result.data.user.name)
    });
  }
  SendEmail(data) {
    // console.log(data)
    if (confirm('Do You Want send mail to ' + data.merchant_details[0].email + ' ?')) {
      this.transPageLength = data.transactions;
      this.current_brand_name = data.data && data.data[0] && data.data[0].brand_name ? data.data[0].brand_name : "Merchant";
      this.merchant_data = {
        "merchant_id": data.merchant_id,
        "start_date": data.start_date,
        "end_date": data.end_date,
        "email": data.merchant_details[0].email,
      }
      this.getMerchantTransactionsdata(this.merchant_data);
    }
  }
  getMerchantTransactionsdata(data) {
    this.dataService.postAuth("merchant/getCycleTransactionsForEmail", data).subscribe(result => {
      if (result.status) {
        console.log("fetched", this.transPageLength, result.data);
        this.status = "Send Success"
        alert(this.status)
      } else {
        this.status = "Send Faild"
        alert(this.status)
      }
    })
  }
  getCount() {
    console.log("calling get count")
    if (!this.countStatus) {
      console.log("counstatus", this.countStatus)
      if (this.merchant_id) {
        console.log("mercahnt_id found")
        this.obj = {
          merchant_id: this.merchant_id
        }
      } else {

        this.obj = {
          merchant_id: ""
        }
      }
      // console.log("------------------>")
      this.dataService.postAuth("merchant/transCycleCounts/", this.obj).subscribe(result => {
        if (result.status) {
          this.pageLength = parseInt(result.data);
          // console.log("count is ----> ", this.pageLength);
          this.countStatus = true;
          this.setViewClaimsPage(this.currentPage, this.merchant_id);
        } else {
          this.pageLength = 0;
        }
      });

    } else {
      this.setViewClaimsPage(this.currentPage, this.merchant_id);
    }
  }
  viewClaims(skip, limit, merchant_id) {
    if (merchant_id) {
      // console.log("merchant_id found in view claims")
      this.obj = {
        skip: skip,
        limit: limit,
        merchant_id: merchant_id
      };

    } else {
      this.obj = {
        skip: skip,
        limit: limit
      };

    }

    // console.log("Entered")
    this.dataService.postAuth("merchant/transCycles", this.obj).subscribe(result => {
      if (result.status) {
        // console.log("result is ", result);
        this.pagedItems = result.data;
        // console.log(this.pagedItems)
        console.log("trans_cycles count", this.pagedItems)

        // if (this.pagedItems[i].merchant_details[0] && this.pagedItems[i].merchant_details[0].logo_url == undefined) {
        //   this.pagedItems[i].merchant_details[0].logo_url = "";
        // }
        // if (this.pagedItems[i].merchant_details[0] && !this.pagedItems[i].merchant_details[0].brand_name) {
        //   this.pagedItems[i].merchant_details[0].brand_name = "";
        // }

        // var data = {};
        // for (var i = 0; i < this.pagedItems.length; i++) {
        //   // console.log("Entered")
        //   data = {};
        //   data = this.pagedItems[i];

        //   // this.dataService. postAuth("merchant/getMerchant", { "_id": this.pagedItems[i].merchant_id }).subscribe(result => {
        //   //   if (result.status) {
        //   //     if (result.data.brand_name)
        //   //       data["brand_name"] = result.data["brand_name"];
        //   //     if (result.data.logo_url)
        //   //       data["logo_url"] = result.data["logo_url"];
        //   //     this.pagedItems[i] = data;
        //   //   }
        //   // })
        // }
        this.showData = true;
      } else {
        this.showData = false;
        this.errMsg = true;
      }
      this.resetButtonShowStatus = false;
    });
  }


  convertInt(page) {
    // console.log("convertInt page------>", this.pager.currentPage);
    // console.log("convertInt page------>", this.pager.totalPages);
    if (page > this.pager.totalPages) {
      return this.pager.totalPages;
    } else {
      return parseInt(page) + 1;
    }
  }
  setPage(page) {
    // console.log("clicking in set page");
    this.currentPage = page;
    // console.log("currentPage",this.currentPage);
    this.cpage = "/view-claims/" + page;
    // console.log("c page", this.cpage);
    this.router.navigate([this.cpage]);
  }
  setViewClaimsPage(page, merchant_id) {
    // console.log("mercahnt_id--->", merchant_id)
    this.checkStatus = false;
    if (this.pageLength) {
      if (this.pageLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerService.getPager(
          Number(this.pageLength),
          Number(page),
          10
        );
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        this.viewClaims(
          this.pager.startIndex,
          this.pager.endIndex + 1 - this.pager.startIndex,
          this.merchant_id
        );
      } else {
        this.currentPage = 1;
        this.viewClaims(0, this.pageLength, this.merchant_id);
        this.paginationStatus = false;
      }
    } else {
      this.errMsg = true;
    }
  }

  setSearchPage(page) {
    this.checkStatus = true;
    // console.log("page------>", page);
    this.currentPage = page;
    this.cpage = this.router.routerState.snapshot.url;
    if (this.pageLength) {
      if (this.pageLength > 10) {
        this.paginationStatus = true;
        this.pager = this.pagerService.getPager(this.pageLength, page, 10);
        // console.log(this.pager);
        if (this.currentPage > this.pager.totalPages) {
          this.currentPage = this.pager.totalPages;
        }
        // console.log("currentPage",this.currentPage);
        this.pager = this.pagerService.getPager(
          this.pageLength,
          this.currentPage,
          10
        );
        this.pagedItems = this.viewClaimsDetails.slice(
          this.pager.startIndex,
          this.pager.endIndex + 1
        );

      } else {
        this.currentPage = 1;
        this.pagedItems = this.viewClaimsDetails;
        this.paginationStatus = false;
      }

    } else {
      this.errMsg = true;
    }
  }

  search() {
    console.log(this.searchString);
    console.log(this.searchVariable);
    console.log(this.fromDate, this.toDate)
    this.responseStatus = true;
    var data = {};
    var from_date, to_date;
    console.log(this.fromDate, this.toDate)
    if (this.fromDate && this.fromDate != "") {
      from_date = new Date(this.fromDate).setHours(0, 0, 0, 0);
      data["type"] = "cycle_date";
      data["fromDate"] = from_date;
    }
    if (this.toDate && this.toDate != "") {
      to_date = new Date(this.toDate);
      to_date.setDate(to_date.getDate() + 1);
      to_date.setHours(0, 0, 0, 0);
      data["toDate"] = to_date.getTime();

    }
    console.log("searchvariable:: ", this.searchVariable)
    if (this.cycle_status != "") {
      data["cycle_status"] = this.cycle_status;
    }
    if (this.searchVariable == "r_id") {
      if (this.searchString != "")
        data["ref_id"] = this.searchString;
    }
    if (this.searchVariable == "m_id") {
      if (this.searchString != "")
        data["merchant_id"] = this.searchString;
    }
    if (this.searchVariable == "b_name") {
      if (this.searchString != "")
        data["brand_name"] = this.searchString;
    }
    if (this.merchant_id) {
      data["merchant_id"] = this.merchant_id;
    }
    console.log('search string', this.searchString)
    
    console.log("search data is ", data);
    this.getPendingCount(data);
    this.dataService
      .postAuth("merchant/transCycle/search", data)
      .subscribe(result => {
        // console.log("got result from back end", result);
        this.responseStatus = false;
        if (result.status) {
          // console.log("result:: ", result.data)
          this.viewClaimsDetails = result.data;
          this.countStatus = false;
          this.pageLength = result.data.length;
          this.pagedItems = result.data;
          this.showData = true;
          this.errMsg = false;
          this.searchStatus = true;
          this.setSearchPage(this.currentPage);
        } else {
          this.errMsg = true;
          this.showData = false;
          this.searchStatus = false;
        }
      });
  }
  reset() {
    this.merchant_id = this.route.snapshot.queryParams.merchant_id;
    
    this.checkStatus = false;
    this.resetButtonShowStatus = true;
    this.fromDate = "";
    this.toDate = "";
    this.cycle_status = "pending"
    this.errMsg = false;
    this.searchStatus = false;
    this.searchString = "";
    this.b_name = '';
    this.getCount();
    this.getPendingCount({ "cycle_status": "pending" });
  }

  validateProcess(merchant) {
    this.claimedStatus = false;
    this.claimId = merchant.merchant_id;
    this.claimAmount = merchant.amount;
    this.cycle_date = merchant.cycle_date;
    this.brandName = merchant.merchant_details && merchant.merchant_details[0] && merchant.merchant_details[0].brand_name ? merchant.merchant_details[0].brand_name : 'Merchant';
    // console.log(this.brandName, merchant);
    this.checkClaimStatus = true;
  }
  claim() {
    var data = {};
    data["ref_id"] = this.claimRefId;
    data["merchant_id"] = this.claimId;
    data["amount"] = this.claimAmount;
    data["cycle_date"] = this.cycle_date;
    console.log(data);

    this.claimRefId = '';
    this.claimId = '';
    this.checkClaimStatus = false;
    this.dataService.postAuth("merchant/transCycle/update", data).subscribe(result => {
      if (result.status) {
        console.log("updated successfully");
        this.claimedStatus = true;
        if (this.searchStatus) {
          this.search();
        } else {
          this.ngOnInit();
        }
      }
    })
  }
  cancel() {
    this.claimRefId = null;
  }
  getBrandsList() {
    this.dataService.getAuth("merchant/getBrandsList").subscribe(result => {
      if (result.message == "User authentication failed") {
        let path = "/permission-denied";
        this.router.navigate([path]);
      } else {
        if (result.message == 'success') {
          this.BrandsAutofillData = result.data;
          // console.log("brands", result.data);
        }
      }
    });
    this.InputAuotfill = (text$: Observable<string>) =>
      text$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        map(term => term.length < 2 ? []
          : this.BrandsAutofillData.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
      );
  }
  getPendingCount(data) {
    if (this.merchant_id) {
      data['merchant_id'] = this.merchant_id
    }
    this.dataService.postAuth("merchant/pendingCycleCount", data).subscribe(result => {
      console.log("pending", result.data);
      if (result.status) {
        this.pending_cycle_count = result.data;
      }
    })
  }


  merchantTransactions(merchant_details) {
    console.log("merchant_details:: ", merchant_details);
    this.transPageLength = merchant_details.transactions;
    this.current_brand_name = merchant_details.merchant_details && merchant_details.merchant_details[0] && merchant_details.merchant_details[0].brand_name ? merchant_details.merchant_details[0].brand_name : "Merchant";
    this.merchant_data = {
      "merchant_id": merchant_details.merchant_id,
      "start_date": merchant_details.start_date,
      "end_date": merchant_details.end_date,
      // "skip": 0,
      // "limit": 10
    }
    this.getMerchantTransactions(this.merchant_data, 0, 10);
    this.setTransactionPage(1);
  }
  getMerchantTransactions(data, skip, limit) {
    data["skip"] = skip;
    data["limit"] = limit;
    this.dataService.postAuth("merchant/getCycleTransactions", data).subscribe(result => {
      if (result.status) {
        console.log("fetched", this.transPageLength, result.data);
        this.transPagedItems = result.data;

      }
    })
  }
  open(content) {
    this.popupCalls(content);
  }
  popupCalls(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  // GetBankDetails(merchant_details) {
  //   console.log("merchant_details:: ", merchant_details);
  //   // this.transPageLength = merchant_details.transactions;
  //   // this.current_brand_name = merchant_details.merchant_details && merchant_details.merchant_details[0] && merchant_details.merchant_details[0].brand_name ? merchant_details.merchant_details[0].brand_name : "Merchant";
  //   // this.merchant_data = {
  //   //   "merchant_id": merchant_details.merchant_id,
  //   //   "start_date": merchant_details.start_date,
  //   //   "end_date": merchant_details.end_date,
  //   //   // "skip": 0,
  //   //   // "limit": 10
  //   // }
  // }

  setTransactionPage(page) {
    this.checkStatus = false;
    // console.log("Entered")
    this.currentTransPage = page;
    if (this.transPageLength) {
      // console.log("transpageLength:: ", this.transPageLength)
      if (this.transPageLength > 10) {
        this.transPaginationStatus = true;
        this.transPager = this.pagerService.getPager(this.transPageLength, page, 10);
        // console.log(this.pager);
        if (this.currentTransPage > this.transPager.totalPages) {
          this.currentTransPage = this.transPager.totalPages;
        }
        // console.log("currentPage",this.currentPage);
        this.transPager = this.pagerService.getPager(
          this.transPageLength,
          this.currentTransPage,
          10
        );
        this.getMerchantTransactions(this.merchant_data, this.transPager.startIndex, 10);

      } else {
        this.currentTransPage = 1;
        this.transPagedItems = this.merchant_cycle_transactions;
        this.transPaginationStatus = false;
      }
    } else {
      this.transErrMsg = true;
    }
  }

  getBrands(event, brand_name) {
    console.log(event.keyCode, brand_name);
    if (!brand_name) { this.brands_data = []; }
    if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 65 || event.keyCode > 90) &&
      event.keyCode !== 32 && event.keyCode !== 8 && event.keyCode !== 190) {
      return;
    }

    if (event.keyCode === 13 || !brand_name || brand_name.length < 3) {
      this.merchant_brand_array = [];
      return;
    }
    const params: any = {
      brand_name: brand_name,
      limit: 6
    };

    // this.companies_loading = true;
    this.dataService.postAuth('user/searchingBrands', params)
      .subscribe((Response) => {
        console.log("SEARCH RESULTS", Response.data);
        // console.log("SEARCHING BRANDING COPYING TO ARRA", this.merchant_brand_array);
        if (Response.data) {
          this.merchant_brand_array = [];
          for (let i = 0; i < Response.data.length; i++) {
            this.merchant_brand_array.push({
              _id: Response.data[i]._id,
              brand_name: Response.data[i]._source.brand_name
            })
            // this.merchant_id_array[i] = Response.data[i]._id;
            // this.merchant_brand_array[i] = Response.data[i]._source.brand_name;
            // this.InputAutoFill[i] = Response.data[i]._source.brand_name;
          }
          // this.InputAutoFill = (text$: Observable<string>) =>
          //   text$.pipe(
          //     debounceTime(200),
          //     distinctUntilChanged(),
          //     map(term => term.length < 2 ? []
          //       : this.merchant_brand_array.filter(v => v.indexOf(term) > -1).slice(0, 10))
          //   );
          // console.log("SEARCHING _IDDSS COPYING TO ARRA", this.merchant_id_array.length);

        } else {
          // console.log('NO DATA FOUND BRANDS');
        }
      }, (error_data) => {
      });
  }
  selectedItem(item) {
    console.log("ITEM", item);
    this.b_name = item.data.brand_name;
    this.merchant_id = item.data._id;
    this.merchant_brand_array.length = 0;
    this.merchant_brand_array = [];
    console.log("HELLO MERCHANT", this.merchant_id);

  }

}