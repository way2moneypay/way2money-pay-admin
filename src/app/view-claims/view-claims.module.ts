import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewClaimsComponent } from './view-claims.component'
import { ViewClaimsRoutingModule } from './view-claims-routing.module';
import { FormsModule } from '@angular/forms';
import { AutoCompleteModule } from '../shared/auto-complete/auto-complete.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ViewClaimsRoutingModule,
    FormsModule,
    AutoCompleteModule,
    NgbModule
  ],
  declarations: [ViewClaimsComponent]
})
export class ViewClaimsModule { }
