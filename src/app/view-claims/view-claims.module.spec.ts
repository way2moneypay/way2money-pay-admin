import { ViewClaimsModule } from './view-claims.module';

describe('ViewClaimsModule', () => {
  let viewClaimsModule: ViewClaimsModule;

  beforeEach(() => {
    viewClaimsModule = new ViewClaimsModule();
  });

  it('should create an instance', () => {
    expect(viewClaimsModule).toBeTruthy();
  });
});
