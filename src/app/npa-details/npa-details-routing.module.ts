import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NpaDetailsComponent } from './npa-details/npa-details.component'
import { AuthGuard } from '../shared/guard/auth.guard';
const routes: Routes = [
  { path: '', component: NpaDetailsComponent, data: { title: 'NPA Details' },canActivate:[AuthGuard]  },
  { path: ':from_date/&/:to_date', component: NpaDetailsComponent, data: { title: 'NPA Details' },canActivate:[AuthGuard]  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NpaDetailsRoutingModule { }
