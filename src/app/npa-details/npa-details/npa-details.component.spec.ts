import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NpaDetailsComponent } from './npa-details.component';

describe('NpaDetailsComponent', () => {
  let component: NpaDetailsComponent;
  let fixture: ComponentFixture<NpaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NpaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NpaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
