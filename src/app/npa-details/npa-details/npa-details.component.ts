import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { PagerService } from '../../shared/pagination/pager.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-npa-details',
  templateUrl: './npa-details.component.html',
  styleUrls: ['./npa-details.component.css']
})
export class NpaDetailsComponent implements OnInit {


  constructor(private apiService: RestServicesService,
    private pagerservice: PagerService, private router: Router, private route: ActivatedRoute,private _location: Location) {
    router.events.subscribe((val) => {

    });
  }
  pageStartIndex: number;
  paramsData: any = {};
  collectionSize: Number = 5;
  newcollectionSize: Number = 5;
  pageSize: number = 10;
  skip: number;
  limit: number;
  maxSize: Number = 5;
  page: number = 1;
  data: any = [];
  dateObj: any = {}
  transData: any = []
  date: any;
  cpage: any;
  pagedItems: any = [];
  pager: any = {};
  from_date:any
  to_date:any
  // oldDate: any
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.page = params.page ? params.page : 1;
      this.from_date = this.route.snapshot.params.from_date;
      this.to_date = this.route.snapshot.params.to_date;
      // this.page = this.route.snapshot.params.cpage
      if (this.from_date) {
        // let oldDate = this.date
        this.viewPendingTransactions(1)
      } else {
        this.npaDetails(1)
      }
    })
  }
  npaDetails(page) {
    this.page = page;
    this.paramsData.skip = (this.page - 1) * this.pageSize;
    this.paramsData.limit = this.pageSize;
    this.apiService.getAuthParams('npa-details', this.paramsData).subscribe((Response) => {
      console.log("SEARCH RESULTS", Response.data);
      if (Response.status) {
        // this.data = Response.data
        this.collectionSize = Response.data.smscounts_details;
        this.data = Response.data.total_records;
      } else {
        alert('Error while fetching npa details')
      }
      this.pageStartIndex = ((this.page - 1) * (this.pageSize));
    }, (error_data) => {
    });
  }
  viewPendingTransactions(page) {
    // console.log('view pending transactions', this.from_date,this.to_date)
    this.dateObj['from_date'] = this.route.snapshot.params.from_date;
    this.dateObj['to_date'] = this.route.snapshot.params.to_date;
    this.page = 1;
    this.dateObj['skip'] = (this.page - 1) * this.pageSize;
    this.dateObj['limit'] = this.pageSize;
    // console.log('Paramss before sending', this.dateObj)
    this.apiService.postAuth('pendingTransactions', this.dateObj)
      .subscribe((Response) => {
        // console.log("SEARCH RESULTS", Response.data);
        if (Response.status) {
          this.newcollectionSize = Response.data.total_counts;
          this.transData = Response.data.total_transactions;
          // this.pagedItems = this.transData.slice(
          //   this.pager.startIndex,
          //   this.pager.endIndex + 1
          // );
        } else {
          alert('Error while fetching Transactions details')
        }
        this.pageStartIndex = ((this.page - 1) * (this.pageSize));
      }, (error_data) => {
      });
  }
  backClicked() {
    this._location.back();
  }
  // setPage(pagee) {
  //   this.page = pagee
  //   this.date = this.route.snapshot.params.date;
  //   this.cpage = "npa-details/" + this.date + "/"+pagee;
  //   this.router.navigate([this.cpage]);
  // }
}
