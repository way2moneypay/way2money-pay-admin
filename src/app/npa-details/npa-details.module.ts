import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule, NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { NpaDetailsRoutingModule } from './npa-details-routing.module';
import { NpaDetailsComponent } from './npa-details/npa-details.component';

@NgModule({
  imports: [
    CommonModule,
    NpaDetailsRoutingModule,
    NgbModule.forRoot()
  ],
  declarations: [NpaDetailsComponent]
})
export class NpaDetailsModule { }
