import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveSmsDataComponent } from './remove-sms-data.component';

describe('RemoveSmsDataComponent', () => {
  let component: RemoveSmsDataComponent;
  let fixture: ComponentFixture<RemoveSmsDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveSmsDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveSmsDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
