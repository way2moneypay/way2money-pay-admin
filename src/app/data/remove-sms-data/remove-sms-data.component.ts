import { Component, OnInit } from '@angular/core';

import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-remove-sms-data',
  templateUrl: './remove-sms-data.component.html',
  styleUrls: ['./remove-sms-data.component.css']
})
export class RemoveSmsDataComponent implements OnInit {

  removeSmsDataForm = {}
  mid = "";
  message: String;

  constructor(private dataService: RestServicesService) { }

  ngOnInit() {
  }

  removeSmsData() {
    this.message = "Loading..."
    console.log("called remove sms data ", this.mid)
    if (!this.mid) {
      this.message = "Please enter mid"
      return
    }

    if(!confirm('Are you sure to delete')){
      return
    }

    let data = {
      'data': {
        'mid': this.mid
      }
    }

    /* calling remove data */
    this.dataService.postAuth('api/data/remove-sms-data', data).subscribe(response => {
      
      if(response.status){
        this.message = "Successfully deleted sms details"
      }else{
        this.message = response.message
      }
    })
  }
}
