import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DataRoutingModule } from './data-routing.module';
import { RemoveSmsDataComponent } from './remove-sms-data/remove-sms-data.component';
import { PredictCreditLimitComponent } from './predict-credit-limit/predict-credit-limit.component';

@NgModule({
  imports: [
    CommonModule,
    DataRoutingModule,
    FormsModule
  ],
  declarations: [RemoveSmsDataComponent, PredictCreditLimitComponent]
})
export class DataModule { }
