import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RemoveSmsDataComponent } from './remove-sms-data/remove-sms-data.component'
import { PredictCreditLimitComponent } from './predict-credit-limit/predict-credit-limit.component'

const routes: Routes = [
  { path: '', redirectTo: 'remove-sms-data', pathMatch: 'full' },
  { path: 'remove-sms-data', component: RemoveSmsDataComponent, data: { title: 'Remove Sms Data' } },
  { path: 'predict-credit-limit', component: PredictCreditLimitComponent, data: { title: 'Predict credit limit' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataRoutingModule { }
