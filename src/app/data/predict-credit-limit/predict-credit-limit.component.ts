import { Component, OnInit } from '@angular/core';
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service';

@Component({
  selector: 'app-predict-credit-limit',
  templateUrl: './predict-credit-limit.component.html',
  styleUrls: ['./predict-credit-limit.component.css']
})
export class PredictCreditLimitComponent implements OnInit {

  predictCreditForm = {}
  mid = "";
  message: String;
  predictData = {}
  gotPredictedData : boolean = false

  constructor(private dataService: RestServicesService) { }

  ngOnInit() {
  }

  predictLimit() {
    this.message = "Loading..."
    this.predictData = {}
    this.gotPredictedData = false

    console.log("called predict credit limit ", this.mid)
    if (!this.mid) {
      this.message = "Please enter mid"
      return
    }

    let data = {
      'data': {
        'mid': this.mid
      }
    }

    /* calling remove data */
    this.dataService.postAuth('api/data/predict-credit-limit', data).subscribe(response => {
      
      if(response.status){
        this.gotPredictedData = true
        this.message = "Successfully predict data"
        this.predictData = response.data
      }else{
        this.gotPredictedData = false
        this.message = response.message
      }
      
    })
  }

  getKeys(obj){
    return Object.keys(obj)
  }
}
