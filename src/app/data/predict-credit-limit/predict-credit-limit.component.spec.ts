import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictCreditLimitComponent } from './predict-credit-limit.component';

describe('PredictCreditLimitComponent', () => {
  let component: PredictCreditLimitComponent;
  let fixture: ComponentFixture<PredictCreditLimitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictCreditLimitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictCreditLimitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
