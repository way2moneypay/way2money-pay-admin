import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import * as Chart from "chart.js";
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service'
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.css']
})
export class DashboardAdminComponent implements OnInit {
  auth_user: any;
	domain = environment.domain
	Barchart: any;
	merchants_count = 0;
	showButtonStatus: any = false;
	total_users = 0; yesterday_users = 0; today_users = 0;
	total_transactions = 0; today_transactions = 0; yesterday_transactions = 0;
	total_transactions_amt = 0; today_transactions_amt = 0; yesterday_transactions_amt = 0;
	user_payments = 0;
	default_days = 30;
	merchants_amount = 0;
	commission = 0;
	pending_payments = 0;
	above_due_payments = 0;
	pending_amount = 0;
	paid_amount = 0;
	convenience = 0;
	amount_given = 0;
	days_select = "Last 30 days";
	isMerchart: any;
	staging_payment_status: any;
	live_payment_status: any;
	live_paymentStatus: any;
	staging_paymentStatus: any;
	active_merchant_count: any;
	above_due_details: any;
	yesterday: any;
	constructor(private api: RestServicesService, private router: Router,
	) {
		this.api.getAuth('api/user/authentication_info_call').subscribe(result => {
			console.log('------response data ---- auth ', result.data);
			this.auth_user = result.data;
			// alert(this.auth_user);
			switch (this.auth_user.user_type) {
				case 'Super Admin':
					return this.router.navigate(['']);
				case 'admin':
					return this.router.navigate(['/admin']);
			}
		});
	}

  ngOnInit() {
    console.log("kkkkkkkkkkkkkkkkkkkkkkkkkk")
    this.api.getAuth('api/user/authentication_info_call').subscribe(result => {
      this.auth_user = result.data;
      if (this.auth_user.user_type === 'admin') {
            this.api.getAuth('users_count').subscribe(result => {
              if (result) {
                this.total_users = result.data[0].total_users;
                this.today_users = result.data[2].today_users;
                this.yesterday_users = result.data[1].yesterday_users;
              }
            })
            this.api.getAuth('transactions_count').subscribe(result => {
              if (result) {
                this.total_transactions = result.data[0].total_transactions + result.data[1].today_transactions;
                this.today_transactions = result.data[1].today_transactions;
                this.yesterday_transactions = result.data[2].yesterday_transactions;
                this.total_transactions_amt = result.data[0].total_transactions_amt + result.data[1].today_transactions_amt;
                this.today_transactions_amt = result.data[1].today_transactions_amt;
                this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
              }
            })
            this.api.getAuth('month_report/' + this.default_days).subscribe(result => {
              this.chart(result)
            })
          this.getPaymentStatus();
          this.api.getAuth('merchants_count').subscribe(result => {
            if (result) {
              // console.log("mercahntsssssssssss", result)
              this.merchants_count = result.final_merchants_result[0].total_merchant_count
              this.active_merchant_count = result.final_merchants_result[1].active_merchant_count
      
            }
            // this.merchants_count = result.data
          })
          this.api.getAuth('users_count').subscribe(result => {
            if (result) {
              this.total_users = result.data[0].total_users;
              this.today_users = result.data[2].today_users;
              this.yesterday_users = result.data[1].yesterday_users;
            }
          })
          this.api.getAuth('transactions_count').subscribe(result => {
            if (result) {
              this.total_transactions = result.data[0].total_transactions + result.data[1].today_transactions;
              this.today_transactions = result.data[1].today_transactions;
              this.yesterday_transactions = result.data[2].yesterday_transactions;
              this.total_transactions_amt = result.data[0].total_transactions_amt + result.data[1].today_transactions_amt;
              this.today_transactions_amt = result.data[1].today_transactions_amt;
              this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
            }
          })
          // this.api.get('transactions_amount').subscribe(result => {
          // 	if (result) {
          // 		this.total_transactions_amt = result.data[0].total_transactions_amt;
          // 		this.today_transactions_amt = result.data[1].today_transactions_amt;
          // 		this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
          // 	}
          // });
          this.api.getAuth('month_report/' + this.default_days).subscribe(result => {
            this.chart(result)
          })
      
          this.api.getAuth('user_payment_counts').subscribe(result => {
            // console.log("-------------------------------->", result)
            if (result) {
              // console.log("result--------------->", result)
              this.pending_payments = result.final_payment_result[0].payment_details[0].users_due_count
              // console.log("this===",this.pending_payments)
              this.pending_amount = result.final_payment_result[0].payment_details[0].users_due_amount
              // // this.above_due_payments =
              this.above_due_details = result.final_payment_result[2].above_due_details
              this.paid_amount = result.final_payment_result[1].payment_paid_details[0].total_amount_paid
            }
      
          });
        
      }
      else {
        let path = "/permission-denied";
        this.router.navigate([path]);
      }
    });
  }
	chart(result) {
		if (result) {
			this.Barchart = new Chart('barChart', {
				type: 'bar',
				data: {
					labels: result.data.labels,
					datasets: [{
						label: "Users",
						data: result.data.user_counts,
						backgroundColor: 'rgb(255, 127, 127,0.5)',
						borderColor: 'rgb(255, 127, 127,1)'
					}, {
						label: 'Transactions',
						data: result.data.transaction_counts,
						backgroundColor: 'rgb(121, 252, 237,0.5)',
						borderColor: 'rgb(121, 252, 237,1)'
					}]
				},
				options: {
					title: {
						text: "User's Registration",
						display: true
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
			this.showButtonStatus = false;
		}
	}
	filter(form) {
		this.showButtonStatus = true;
		if (this.isMerchart == 'yes') {
			var merchartId = localStorage.getItem('id')
			this.api.getAuth('merchant_month_report/' + this.default_days + '/' + merchartId).subscribe(result => {
				this.chart(result)
			})
		} else {
			console.log("month")
			this.api.getAuth('month_report/' + form.value.days).subscribe(result => {
				this.chart(result)
			})
		}
	}
	getPaymentStatus() {
		this.api.getAuth('paymentStatus').subscribe(result => {
			if (result.data) {
				console.log(result.data)
				this.staging_payment_status = result.data.staging_payment_status;
				this.live_payment_status = result.data.live_payment_status;
				if (this.live_payment_status == 'active') {
					this.live_paymentStatus = true;
				} else {
					this.live_paymentStatus = false;
				}
				if (this.staging_payment_status == 'active') {
					this.staging_paymentStatus = true;

				} else {
					this.staging_paymentStatus = false;
				}
				// console.log(this.live_payment_status)
			}
		})
	}
	updatePaymentStatus(key) {
		// console.log("status :: ", this.live_paymentStatus);
		var updateObject = {};
		updateObject["key"] = key;
		if (key == "live_payment_status")
			updateObject[key] = this.live_paymentStatus ? "active" : "inactive";
		else
			updateObject[key] = this.staging_paymentStatus ? "active" : "inactive";

		this.api.postAuth('updatePaymentStatus', updateObject).subscribe(result => {
			if (result.status) {
				console.log("updated  payment status");
			} else {
				console.log("pyament status not updated");
			}
		})
	}
	today() {
		let date = Date.now();
		let today_date = new Date(date).setHours(0, 0, 0, 0);
		today_date = today_date
		this.router.navigate(['user/transactions/1'], { queryParams: { today_date: today_date } });
	}
	yesterdy() {
		// let date = Date.now();
		// let today_date = new Date(date).setHours(0, 0, 0, 0);
		// today_date = today_date
		this.yesterday = new Date();
		// console.log(this.yesterday);
		this.yesterday.setDate(this.yesterday.getDate() - 1);
		this.yesterday.setHours(0, 0, 0, 0);
		this.yesterday = (this.yesterday / 1)
		// console.log("ints file dates-------------------->",today_date,this.yesterday)
		this.router.navigate(['user/transactions/1'], { queryParams: { yesterday_date: this.yesterday } });

	}
}
