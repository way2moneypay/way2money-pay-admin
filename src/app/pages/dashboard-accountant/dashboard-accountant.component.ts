import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import * as Chart from "chart.js";
import { Component, OnInit } from '@angular/core';
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service'
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-dashboard-accountant',
  templateUrl: './dashboard-accountant.component.html',
  styleUrls: ['./dashboard-accountant.component.css']
})
export class DashboardAccountantComponent implements OnInit {
  total_transactions = 0; today_transactions = 0; yesterday_transactions = 0;
  total_transactions_amt = 0; today_transactions_amt = 0; yesterday_transactions_amt = 0;
  pending_amount = 0; paid_amount = 0; pending_payments = 0; above_due_payments = 0;
  above_due_details: any;
  yesterday_payments: any;
  yesterday_amount_paid: any;
  from_date: any
  to_date: any;
  date: string;
  obj: {}
  constructor(private api: RestServicesService, private router: Router) { }

  ngOnInit() {
    this.api.getAuth('transactions_count').subscribe(result => {
      if (result) {
        this.total_transactions = result.data[0].total_transactions + result.data[1].today_transactions;
        this.today_transactions = result.data[1].today_transactions;
        this.yesterday_transactions = result.data[2].yesterday_transactions;
        this.total_transactions_amt = result.data[0].total_transactions_amt + result.data[1].today_transactions_amt;
        this.today_transactions_amt = result.data[1].today_transactions_amt;
        this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
      }
    })
    this.api.getAuth('user_payment_counts').subscribe(result => {
      // console.log("-------------------------------->", result)
      if (result) {
        // console.log("result--------------->", result)
        if (result.final_payment_result[0] && result.final_payment_result[0].payment_details[0]) {
          this.pending_payments = result.final_payment_result[0].payment_details[0].users_due_count != undefined ? result.final_payment_result[0].payment_details[0].users_due_count : 0

          this.pending_amount = result.final_payment_result[0].payment_details[0].users_due_amount != undefined ? result.final_payment_result[0].payment_details[0].users_due_amount : 0
        }
        if (result.final_payment_result[2] && result.final_payment_result[2].above_due_details) {
          this.above_due_details = result.final_payment_result[2].above_due_details != undefined ? result.final_payment_result[2].above_due_details : 0
        }

        if (result.final_payment_result[1] && result.final_payment_result[1].payment_paid_details[0]) {
          this.paid_amount = result.final_payment_result[1].payment_paid_details[0].total_amount_paid != undefined ? result.final_payment_result[1].payment_paid_details[0].total_amount_paid : 0
        }

      }

    });
    this.api.getAuth('user_payment_daywise').subscribe(result => {
      // console.log("--------------------->", result)
      if (result) {
        this.yesterday_payments = result.data[0].payments
        this.yesterday_amount_paid = result.data[0].amount_paid
      } else {
        console.error("ERROR WHILE GETTING YESTERDAY COUNTS")
      }
    })
    this.onClickSubmit(1)
  }
  onClickSubmit(page) {
    if (page == 1) {
      let temp = new Date();
      let today = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate() - 1);
      let fromDate = today.setHours(0, 0, 0, 0);
      let EndDays = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate() - 1);
      let toDate = EndDays.setHours(23, 59, 59, 999);
      this.obj = {
        paid_Fromdate: fromDate,
        paid_Todate: toDate
      }
    } else {
      var fromd = this.from_date.year + "-" + this.from_date.month + "-" + this.from_date.day;
      var todate = this.to_date.year + "-" + this.to_date.month + "-" + this.to_date.day;
      var fromDate = new Date(fromd).setHours(0, 0, 0, 0);
      var toDate = new Date(todate).setHours(23, 59, 59, 999);
      this.obj = {
        paid_Fromdate: fromDate / 1,
        paid_Todate: toDate
      }
    }
    console.log("entered day", this.obj)
    this.api.getAuthParams('user_payment_daywise', this.obj).subscribe(result => {
      if (result) {
        // console.log("output", result)
        if (result.data[0] && result.data[0].payments && result.data[0].amount) {
          this.yesterday_payments = result.data[0].payments
          this.yesterday_amount_paid = result.data[0].amount
        } else {
          this.yesterday_payments = 0
          this.yesterday_amount_paid = 0
        }
      } else {
        console.log("oops got an error")
      }
    })
  }
  onClickReset() {
    this.from_date = null
    this.to_date = null
    this.onClickSubmit(1)
  }
}
