import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardAccountantComponent } from './dashboard-accountant.component';

describe('DashboardAccountantComponent', () => {
  let component: DashboardAccountantComponent;
  let fixture: ComponentFixture<DashboardAccountantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardAccountantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardAccountantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
