import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import * as Chart from "chart.js";
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service'
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-dashboard-sales-executive',
  templateUrl: './dashboard-sales-executive.component.html',
  styleUrls: ['./dashboard-sales-executive.component.css']
})
export class DashboardSalesExecutiveComponent implements OnInit {
  pending_payments_amt: any;
  total_payments_amt: any;
  pending_payment: any;
  total_payments: any;
  total_transactions = 0; today_transactions = 0; yesterday_transactions = 0;
  total_transactions_amt = 0; today_transactions_amt = 0; yesterday_transactions_amt = 0;
  pending_amount = 0; paid_amount = 0; pending_payments = 0; above_due_payments = 0;
  above_due_details: any;
  yesterday_payments: any;
  yesterday_amount_paid: any;
  from_date: any
  to_date: any;
  date: string;
  obj: {}
  merchants_count = 0;
  active_merchant_count: any;
  total_tagets: any;
  paid_amt: any;
  constructor(private api: RestServicesService, private router: Router) { }

  ngOnInit() {
    this.api.getAuth('merchants_count').subscribe(result => {
      if (result) {
        console.log("mercahntsssssssssss", result)
        this.merchants_count = result.final_merchants_result[0].total_merchant_count
        this.active_merchant_count = result.final_merchants_result[1].active_merchant_count
      }
      // this.merchants_count = result.data
    })
    this.getTransactionsCount()
    // this.getPaymentsCount()
    // this.api.getAuth('sales_payments_count').subscribe(result => {
    //   if (result) {
    //     console.log("*******************************")
    //     console.log(result)
    //     console.log("*******************************")
    //   }
    // })

    this.onClickSubmit(1)
    // this.api.getAuth('target_count').subscribe(result => {
    //   if (result) {
    //     // console.log("targettttttttttttt", result.data[3])
    //   }
    //   // this.merchants_count = result.data
    // })
  }

  getTransactionsCount() {
    this.api.getAuth('sales_transactions_count').subscribe(result => {
      console.log('in sales ===>>>>>>', result.data)
      if (result.status) {
        this.total_transactions = result.data[0].total_transactions + result.data[1].today_transactions;
        this.total_payments_amt = result.data[0].total_paid_amt
        this.total_payments = result.data[0].total_payments
        this.pending_payment = result.data[0].total_transactions - result.data[0].total_payments
        this.pending_payments_amt = result.data[0].total_transactions_amt - result.data[0].total_paid_amt
        this.today_transactions = result.data[1].today_transactions;
        this.yesterday_transactions = result.data[2].yesterday_transactions;
        this.total_transactions_amt = result.data[0].total_transactions_amt + result.data[1].today_transactions_amt;
        this.today_transactions_amt = result.data[1].today_transactions_amt;
        this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
        if (result.data[3]) {
          this.total_tagets = result.data[3].total_tagets;
        }
      }
    })
  }
  // getPaymentsCount() {
  //   this.api.getAuth('sales_payments_count').subscribe(result => {
  //     // console.log('in sales ', result.data)
  //     if (result.status) {
  //       // this.total_payments = result.data[0].total_payments
  //       // this.yesterday_payments = result.data[2].yesterday_payments;
  //       this.paid_amt = result.data[0].total_payments_amt
  //       this.pending_payment = result.data[1].pending_payments;
  //       this.pending_payments_amt = result.data[1].pending_payments_amt;
  //       // this.yesterday_payments_amt = result.data[2].yesterday_payments_amt;
  //     }
  //   })
  // }
  onClickSubmit(page) {
    if (page == 1) {
      console.log("true")
      this.obj = {}
    } else {
      console.log("false")
      var fromd = this.from_date.year + "-" + this.from_date.month + "-" + this.from_date.day;
      var todate = this.to_date.year + "-" + this.to_date.month + "-" + this.to_date.day;
      var fromDate = new Date(fromd).setHours(0, 0, 0, 0);
      var toDate = new Date(todate).setHours(23, 59, 59, 999);
      this.obj = {
        paid_Fromdate: fromDate / 1,
        paid_Todate: toDate
      }
    }
    console.log("entered day", this.obj)
    // this.api.getAuthParams('sales_user_payment_daywise', this.obj).subscribe(result => {
    //   if (result) {
    //     // console.log("output", result)
    //     if (result.data[0] && result.data[0].payments && result.data[0].amount) {
    //       this.yesterday_payments = result.data[0].payments
    //       this.yesterday_amount_paid = result.data[0].amount
    //     } else {
    //       this.yesterday_payments = 0
    //       this.yesterday_amount_paid = 0
    //     }
    //   } else {
    //     console.log("oops got an error")
    //   }
    // })
  }
}


// db.getCollection('merchants').aggregate([{
//   $match:{
//     created_by: ObjectId("5bed0fb0eab52e1fe04d7e24")
//   }
// },
// { $lookup: { "from": 'user_transactions', "localField": '_id', "foreignField": 'merchant_id', "as": 'merchants_payments' } },
// { $unwind: "$merchants_payments" },
// {
//   $group: {
//     _id: {merchant_id:'$merchants_payments.merchant_id'},
//     total: { $sum: { $cond: [ { $eq: [ '$merchants_payments.status', 5 ] }, 1, 0 ] } },
//     amount: { $sum: { $cond: [ { $eq: [ '$merchants_payments.status', 5 ] }, '$merchants_payments.trans_amount', 0 ] } }
//   }
// }
// ])