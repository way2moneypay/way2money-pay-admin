import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSalesExecutiveComponent } from './dashboard-sales-executive.component';

describe('DashboardSalesExecutiveComponent', () => {
  let component: DashboardSalesExecutiveComponent;
  let fixture: ComponentFixture<DashboardSalesExecutiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSalesExecutiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSalesExecutiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
