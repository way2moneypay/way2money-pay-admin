import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './pages.component';
import { AuthGuard } from '../shared/guard/auth.guard';
import { ErrorLogsComponent } from '../error-logs/error-logs.component';
import { ClaimRequestsComponent } from '../claim-requests/claim-requests.component';
import { ClearUsersComponent } from '../clear-users/clear-users.component';
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { DashboardAccountantComponent } from './dashboard-accountant/dashboard-accountant.component';
import { DashboardSalesExecutiveComponent } from './dashboard-sales-executive/dashboard-sales-executive.component';
import { AdminRegionalHeadComponent } from './admin-regional-head/admin-regional-head.component';
//import { PermissionDeniedComponent }   from '../permission-denied/permission-denied.component';

const routes: Routes = [
    {
        path: '', component: PageComponent,
        children: [
            { path: '', loadChildren: './starter/starter.module#StarterModule', canActivate: [AuthGuard] },
            { path: 'admin', component: DashboardAdminComponent, canActivate: [AuthGuard] },
            { path: 'accountant', component: DashboardAccountantComponent, canActivate: [AuthGuard] },
            { path: 'sales-executive', component: DashboardSalesExecutiveComponent, canActivate: [AuthGuard] },
            { path: 'regional-head', component: AdminRegionalHeadComponent, canActivate: [AuthGuard] },
            { path: 'user', loadChildren: '../users/users.module#UsersModule', canActivate: [AuthGuard] },
            { path: 'merchant', loadChildren: '../merchants/merchants.module#MerchantsModule', canActivate: [AuthGuard] },
            { path: 'permissions', loadChildren: '../permissions/permissions.module#PermissionsModule', canActivate: [AuthGuard] },
            { path: 'admin-users', loadChildren: '../admin-users/admin-users.module#AdminUsersModule', canActivate: [AuthGuard] },
            { path: 'settings', loadChildren: '../settings/settings.module#SettingsModule', canActivate: [AuthGuard] },
            { path: 'error-logs/:cpage', component: ErrorLogsComponent, canActivate: [AuthGuard] },
            { path: 'claim-requests/:cpage', component: ClaimRequestsComponent, canActivate: [AuthGuard] },
            { path: 'view-claims/:cpage', loadChildren: '../view-claims/view-claims.module#ViewClaimsModule', canActivate: [AuthGuard] },
            { path: 'clear-users', component: ClearUsersComponent, canActivate: [AuthGuard] },
            // { path: 'transactions/:cpage', loadChildren: '../users/user-transactions', canActivate: [AuthGuard] },
            { path: 'data', loadChildren: '../data/data.module#DataModule', canActivate: [AuthGuard] },
            { path: 'offer', loadChildren: '../offers/offers.module#OffersModule', canActivate: [AuthGuard] },
            { path: 'npa-details', loadChildren: '../npa-details/npa-details.module#NpaDetailsModule', canActivate: [AuthGuard] }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
