import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import * as Chart from "chart.js";
import { Component, OnInit } from '@angular/core';
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service'
import { environment } from '../../../environments/environment';

@Component({
	selector: 'app-starter',
	templateUrl: './starter.component.html',
	styleUrls: ['./starter.component.css']
})
export class StarterComponent implements OnInit {
	domain = environment.domain
	Barchart: any;
	merchants_count = 0;
	showButtonStatus: any = false;
	total_users = 0; yesterday_users = 0; today_users = 0;
	total_transactions = 0; today_transactions = 0; yesterday_transactions = 0;
	total_transactions_amt = 0; today_transactions_amt = 0; yesterday_transactions_amt = 0;
	user_payments = 0;
	default_days = 30;
	merchants_amount = 0;
	commission = 0;
	pending_payments = 0;
	above_due_payments = 0;
	pending_amount = 0;
	paid_amount = 0;
	convenience = 0;
	amount_given = 0;
	days_select = "Last 30 days";
	isMerchart: any;
	staging_payment_status: any;
	live_payment_status: any;
	live_paymentStatus: any;
	staging_paymentStatus: any;
	active_merchant_count: any;
	above_due_details: any;
	yesterday: any;
	auth_user: any;
	EventsArray: any;
	Android: any = {};
	Ios: any = {};
	from_date: any
	date: string;
	datemilli: Date;
	datemillis: number;
	crrent_date: number;
	dateinMilli: number;
	currentDay: Date;
	obj: {}
	showModel:any = false
	constructor(private api: RestServicesService, private router: Router,
	) {
		this.api.getAuth('api/user/authentication_info_call').subscribe(result => {
			// console.log('------response data ---- auth ', result.data);
			this.auth_user = result.data;
			switch (this.auth_user.user_type) {
				case 'super_admin':
					return this.router.navigate(['']);
				case 'admin':
					return this.router.navigate(['/admin']);
				case 'accountant':
					return this.router.navigate(['/accountant']);
				case 'sales_team':
					return this.router.navigate(['/sales-executive']);
				case 'regional_head':
					return this.router.navigate(['/regional-head']);
			}
		});
	}

	ngOnInit() {

		/*this.isMerchart = localStorage.getItem('isMerchant')
		if (this.isMerchart == 'yes') {
			var merchartId = localStorage.getItem('id')
			this.api.getAuth('users_count_merchant/' + merchartId).subscribe(result => {
				if (result) {
					this.total_users = result.data[0].total_users;
					this.today_users = result.data[2].today_users;
					this.yesterday_users = result.data[1].yesterday_users;
				}
			})
			this.api.getAuth('merchant_transactions_count/' + merchartId).subscribe(result => {
				if (result) {
					this.total_transactions = result.data[0].total_transactions;
					this.today_transactions = result.data[1].today_transactions;
					this.yesterday_transactions = result.data[2].yesterday_transactions;
				}
			})
			this.api.getAuth('merchant_transactions_amt/' + merchartId).subscribe(result => {
				if (result) {
					this.total_transactions_amt = result.data[0].total_transactions_amt;
					this.today_transactions_amt = result.data[1].today_transactions_amt;
					this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
				}
			})
			this.api.getAuth('merchant_month_report/' + this.default_days + '/' + merchartId).subscribe(result => {
				this.chart(result)
			})

		} else if (this.isMerchart != 'yes') {
			
		} */


		this.api.getAuth('users_count').subscribe(result => {
			if (result) {
				this.total_users = result.data[0].total_users;
				this.today_users = result.data[2].today_users;
				this.yesterday_users = result.data[1].yesterday_users;
			}
		})
		// this.api.get('payment_by_users').subscribe(result => {
		// 	if (result) this.user_payments = result.data;
		// })
		// this.api.get('merchants_amount').subscribe(result => {
		// 	if (result) this.merchants_amount = result.data;
		// })
		// this.api.get('earnings').subscribe(result => {
		// 	if (result) {
		// 		this.commission = result.data.commission;
		// 		this.convenience = result.data.convenience;
		// 	}
		// })
		// this.api.get('amount_to_users').subscribe(result => {
		// 	if (result) this.amount_given = result.data;
		// })
		this.api.getAuth('transactions_count').subscribe(result => {
			if (result) {
				this.total_transactions = result.data[0].total_transactions + result.data[1].today_transactions;
				this.today_transactions = result.data[1].today_transactions;
				this.yesterday_transactions = result.data[2].yesterday_transactions;
				this.total_transactions_amt = result.data[0].total_transactions_amt + result.data[1].today_transactions_amt;
				this.today_transactions_amt = result.data[1].today_transactions_amt;
				this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
			}
		})
		// this.api.get('transactions_amount').subscribe(result => {
		// 	if (result) {
		// 		this.total_transactions_amt = result.data[0].total_transactions_amt;
		// 		this.today_transactions_amt = result.data[1].today_transactions_amt;
		// 		this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
		// 	}
		// });
		this.api.getAuth('month_report/' + this.default_days).subscribe(result => {
			this.chart(result)
		})
		this.getPaymentStatus();
		this.api.getAuth('merchants_count').subscribe(result => {
			if (result) {
				// console.log("mercahntsssssssssss", result)
				this.merchants_count = result.final_merchants_result[0].total_merchant_count
				this.active_merchant_count = result.final_merchants_result[1].active_merchant_count

			}
			// this.merchants_count = result.data
		})
		this.api.getAuth('users_count').subscribe(result => {
			if (result) {
				this.total_users = result.data[0].total_users;
				this.today_users = result.data[2].today_users;
				this.yesterday_users = result.data[1].yesterday_users;
			}
		})
		// this.api.get('payment_by_users').subscribe(result => {
		// 	if (result) this.user_payments = result.data;
		// })
		// this.api.get('merchants_amount').subscribe(result => {
		// 	if (result) this.merchants_amount = result.data;
		// })
		// this.api.get('earnings').subscribe(result => {
		// 	if (result) {
		// 		this.commission = result.data.commission;
		// 		this.convenience = result.data.convenience;
		// 	}
		// })
		// this.api.get('amount_to_users').subscribe(result => {
		// 	if (result) this.amount_given = result.data;
		// })
		this.api.getAuth('transactions_count').subscribe(result => {
			if (result) {
				console.log('trns:::::::::::',result)
				this.total_transactions = result.data[0].total_transactions + result.data[1].today_transactions;
				this.today_transactions = result.data[1].today_transactions;
				this.yesterday_transactions = result.data[2].yesterday_transactions;
				this.total_transactions_amt = result.data[0].total_transactions_amt + result.data[1].today_transactions_amt;
				this.today_transactions_amt = result.data[1].today_transactions_amt;
				this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
				this.paid_amount = result.data[0].total_payments_amt
				this.pending_amount = result.data[0].total_transactions_amt - result.data[0].total_payments_amt
			}
		})
		// this.api.get('transactions_amount').subscribe(result => {
		// 	if (result) {
		// 		this.total_transactions_amt = result.data[0].total_transactions_amt;
		// 		this.today_transactions_amt = result.data[1].today_transactions_amt;
		// 		this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
		// 	}
		// });
		this.api.getAuth('month_report/' + this.default_days).subscribe(result => {
			this.chart(result)
		})

		this.api.getAuth('user_payment_counts').subscribe(result => {
			// console.log("-------------------------------->", result)
			if (result) {
				// console.log("result--------------->", result)
				if (result.final_payment_result[0] && result.final_payment_result[0].payment_details) {
					this.pending_payments = result.final_payment_result[0].payment_details != undefined ? result.final_payment_result[0].payment_details : 0

					// this.pending_amount = result.final_payment_result[0].payment_details[0].users_due_amount != undefined ? result.final_payment_result[0].payment_details[0].users_due_amount : 0
				}
				if (result.final_payment_result[1] && result.final_payment_result[1].above_due_details) {
					this.above_due_details = result.final_payment_result[1].above_due_details != undefined ? result.final_payment_result[1].above_due_details : 0
				}

				// if (result.final_payment_result[1] && result.final_payment_result[1].payment_paid_details[0]) {
				// 	this.paid_amount = result.final_payment_result[1].payment_paid_details[0].total_amount_paid != undefined ? result.final_payment_result[1].payment_paid_details[0].total_amount_paid : 0
				// }

			}

		});
		// this.api.getAuth('user_app_usage').subscribe(result => {
		// 	console.log("coming----------------", result)
		// 	console.log(result.Events)
		// 	if (result && result.Events) {
		// 		this.EventsArray = result.Events
		// 		console.log("array", this.EventsArray)
		// 		for (var i = 0; i < this.EventsArray.length; i++) {
		// 			if (this.EventsArray[i].os == 'android') {
		// 				this.Android = this.EventsArray[i]
		// 				console.log("android data", this.Android)
		// 			} else if (this.EventsArray[i].os == 'ios') {
		// 				this.Ios = this.EventsArray[i]
		// 				console.log("ios data", this.Ios)
		// 			} else {
		// 				console.log("no data found")
		// 			}
		// 		}
		// 	} else {
		// 		console.log("oops got an error")
		// 	}
		// })
		this.onSubmit(1)
	}
	chart(result) {
		if (result) {
			this.Barchart = new Chart('barChart', {
				type: 'bar',
				data: {
					labels: result.data.labels,
					datasets: [{
						label: "Users",
						data: result.data.user_counts,
						backgroundColor: 'rgb(255, 127, 127,0.5)',
						borderColor: 'rgb(255, 127, 127,1)'
					}, {
						label: 'Transactions',
						data: result.data.transaction_counts,
						backgroundColor: 'rgb(121, 252, 237,0.5)',
						borderColor: 'rgb(121, 252, 237,1)'
					}]
				},
				options: {
					title: {
						text: "User's Registration",
						display: true
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}
			});
			this.showButtonStatus = false;
		}
	}
	filter(form) {
		this.showButtonStatus = true;
		if (this.isMerchart == 'yes') {
			var merchartId = localStorage.getItem('id')
			this.api.getAuth('merchant_month_report/' + this.default_days + '/' + merchartId).subscribe(result => {
				this.chart(result)
			})
		} else {
			console.log("month")
			this.api.getAuth('month_report/' + form.value.days).subscribe(result => {
				this.chart(result)
			})
		}
	}
	getPaymentStatus() {
		this.api.getAuth('paymentStatus').subscribe(result => {
			if (result.data) {
				console.log(result.data)
				this.staging_payment_status = result.data.staging_payment_status;
				this.live_payment_status = result.data.live_payment_status;
				if (this.live_payment_status == 'active') {
					this.live_paymentStatus = true;
					this.showModel = true
				} else {
					this.showModel = true
					this.live_paymentStatus = false;
				}
				if (this.staging_payment_status == 'active') {
					this.staging_paymentStatus = true;
				} else {
					this.staging_paymentStatus = false;
				}
				// console.log(this.live_payment_status)
			}
		})
	}
	updatePaymentStatus(key) {
		if(key == 'no_update'){
			if(this.live_paymentStatus){
				this.live_payment_status = 'inactive'
			} else {
				this.live_payment_status = 'active'
			}
			this.ngOnInit()
		}else{
			console.log("status :: ", this.live_paymentStatus);
			var updateObject = {};
			updateObject["key"] = key;
			if (key == "live_payment_status") {
				// this.showModel = true
				this.live_payment_status = this.live_paymentStatus ? "active" : "inactive";
				updateObject[key] = this.live_paymentStatus ? "active" : "inactive";
				console.log("live payment status :: ", this.live_payment_status);
			} else{
				this.staging_payment_status = this.staging_paymentStatus ? "active" : "inactive";
				updateObject[key] = this.staging_paymentStatus ? "active" : "inactive";
			}
			this.api.postAuth('updatePaymentStatus', updateObject).subscribe(result => {
				if (result.status) {
					console.log("updated  payment status");
					this.showModel = false
					this.ngOnInit()
				} else {
					console.log("pyament status not updated");
					this.showModel = false
					this.ngOnInit()
				}
			})
		}
	
	}
	today() {
		let date = Date.now();
		let today_date = new Date(date).setHours(0, 0, 0, 0);
		today_date = today_date
		this.router.navigate(['user/transactions/1'], { queryParams: { today_date: today_date } });
	}
	yesterdy() {
		// let date = Date.now();
		// let today_date = new Date(date).setHours(0, 0, 0, 0);
		// today_date = today_date
		this.yesterday = new Date();
		// console.log(this.yesterday);
		this.yesterday.setDate(this.yesterday.getDate() - 1);
		this.yesterday.setHours(0, 0, 0, 0);
		this.yesterday = (this.yesterday / 1)
		// console.log("ints file dates-------------------->",today_date,this.yesterday)
		this.router.navigate(['user/transactions/1'], { queryParams: { yesterday_date: this.yesterday } });

	}
	onReset() {
		this.from_date = null
		this.onSubmit(1)
	}
	onSubmit(from_date) {
		if (from_date == 1) {
			var fromDate = new Date().setHours(0, 0, 0, 0);
			var d = new Date()
			var day = d.getDate()
			var month = d.getMonth() + 1
			var year = d.getFullYear()
			this.from_date = year + "-" + month + "-" + day
			console.log("date", this.from_date)

			// this.currentDay = new Date()
			// console.log("currentDay", this.currentDay.setHours(0,0,0,0))
			// this.currentDay = new Date(this.currentDay.getTime())
			// this.crrent_date = (this.currentDay).getTime()
			this.obj = {
				used_date: fromDate / 1
			}
			// from_date = new Date(fromDate);

			// from_date.toString("YYYY-MM-dd"); // "Dec 20" 
			// console.log("current day", from_date)
		} else {
			// console.log("Entered date", this.obj         )
			var fromd = this.from_date.year + "-" + this.from_date.month + "-" + this.from_date.day;
			var fromDate = new Date(fromd).setHours(0, 0, 0, 0);
			// this.date = this.from_date.year + "-" + this.from_date.month + "-" + this.from_date.day;
			// this.datemilli = new Date(this.date).setHours(0,0,0,0)
			// console.log(this.datemilli)
			// this.datemilli = new Date(this.datemilli.getTime())
			// console.log("modified date-->", this.datemilli)
			// this.datemillis = this.datemilli.getTime()
			// this.dateinMilli = this.datemilli/1
			// console.log("modified date1-->", fromDate / 1)
			this.obj = {
				used_date: fromDate / 1
			}
			// console.log("entered day", this.obj)


		}

		this.api.postAuth('getTotalEvents', this.obj).subscribe(result => {
			// console.log("output", result.Events)
			if (result && result.Events.length > 0) {
				this.EventsArray = result.Events
				// console.log("array", this.EventsArray)
				for (var i = 0; i < this.EventsArray.length; i++) {
					if (this.EventsArray[i]._id == 'android') {
						if (Object.keys(this.EventsArray[i]).length) {
							this.Android = this.EventsArray[i]
						} else {
							this.Android = {}
						}
						// console.log("android data", this.Android)
					} else if (this.EventsArray[i]._id == 'ios') {
						if (Object.keys(this.EventsArray[i]).length) {
							this.Ios = this.EventsArray[i]
						} else {
							this.Ios = {}
						}
						// console.log("ios data", this.Ios)
					} else {
						console.log("no data found")
					}
				}
			} else {
				this.Android = {}
				this.Ios = {}
				console.log("oops got an error")
			}


		})


	}
}