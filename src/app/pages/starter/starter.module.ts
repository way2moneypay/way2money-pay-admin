import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { StarterComponent } from './starter.component';
import { PipesModule } from '../../shared/pipes/pipes-module';
import { NgbModule, NgbPagination } from '@ng-bootstrap/ng-bootstrap';

// import { DatepickerModule } from 'angular-mat-datepicker';


const routes: Routes = [{
	path: '',
	data: {
        title: 'Dashboard',
        urls: [{title: 'Dashboard',url: '/'},{title: 'Dashboard'}]
    },
	component: StarterComponent
}];

@NgModule({
	imports: [
		PipesModule,
    	FormsModule,
    	CommonModule, 
		RouterModule.forChild(routes),
		NgbModule.forRoot()
		// DatepickerModule
    ],
	declarations: [StarterComponent]
})
export class StarterModule { }