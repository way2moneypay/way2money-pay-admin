import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../shared/pipes/pipes-module';
import { FormsModule } from '@angular/forms';
import { PagesRoutingModule } from './pages-routing.module';
import { PageComponent } from './pages.component';
import { SIDEBAR_TOGGLE_DIRECTIVES } from '../shared/sidebar.directive';
import { NavigationComponent } from '../shared/header-navigation/navigation.component';
import { SidebarComponent } from '../shared/sidebar/sidebar.component';
import { BreadcrumbComponent } from '../shared/breadcrumb/breadcrumb.component';
import { ErrorLogsComponent } from '../error-logs/error-logs.component'
import { ClaimRequestsComponent } from '../claim-requests/claim-requests.component';
import { ClearUsersComponent } from '../clear-users/clear-users.component';
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { DashboardAccountantComponent } from './dashboard-accountant/dashboard-accountant.component';
// import { DatepickerModule } from 'angular-mat-datepicker'
//import { PermissionDeniedComponent } from '../permission-denied/permission-denied.component';
import { NgbModule, NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { DashboardSalesExecutiveComponent } from './dashboard-sales-executive/dashboard-sales-executive.component';
import { AdminRegionalHeadComponent } from './admin-regional-head/admin-regional-head.component';
@NgModule({
    imports: [
        PipesModule,
        CommonModule,
        PagesRoutingModule,
        FormsModule,
        NgbModule.forRoot()
        // DatepickerModule
    ],
    declarations: [
        ErrorLogsComponent,
        ClaimRequestsComponent,
        PageComponent,
        NavigationComponent,
        BreadcrumbComponent,
        SidebarComponent,
        ClearUsersComponent,
        SIDEBAR_TOGGLE_DIRECTIVES,
        DashboardAdminComponent,
        DashboardAccountantComponent,
        DashboardSalesExecutiveComponent,
        AdminRegionalHeadComponent,
    ]
})
export class PagesModule { }
