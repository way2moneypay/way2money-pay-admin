import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import * as Chart from "chart.js";
import { RestServicesService } from '../../shared/services/rest-services/rest-services.service'
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-admin-regional-head',
  templateUrl: './admin-regional-head.component.html',
  styleUrls: ['./admin-regional-head.component.css']
})
export class AdminRegionalHeadComponent implements OnInit {
  table_npa_percentage: any;
  total_transactions = 0; today_transactions = 0; yesterday_transactions = 0;
  total_payments = 0; today_payments = 0; yesterday_payments = 0;
  total_transactions_amt = 0; today_transactions_amt = 0; yesterday_transactions_amt = 0;
  total_payments_amt = 0; today_payments_amt = 0; yesterday_payments_amt = 0;
  pending_amount = 0; paid_amount = 0; pending_payments = 0; above_due_payments = 0;
  above_due_details: any;
  yesterday_amount_paid: any;
  from_date: any
  to_date: any;
  date: string;
  obj: any = {};
  merchants_count = 0;
  active_merchant_count: any;
  usersList: any;
  user_id: any = 'ALL';
  users_data_found: boolean;
  date_info: string;
  total_tagets: any
  pending_payment: any
  pending_payments_amt: any
  active_merchants: any;
  table_merchants: any;
  table_total_payments: any;
  table_total_payments_amt: any;
  table_pending_payments: any;
  table_pending_payments_amt: any;
  table_transactions: any;
  table_transactions_amt: any;
  table_total_tagets: any;
  constructor(private api: RestServicesService, private router: Router) { }

  ngOnInit() {
    this.getUsersList()
    this.getTransactionsCount()
    // this.getPaymentsCount()
    this.api.getAuth('merchants_count').subscribe(result => {
      if (result) {
        // console.log("mercahntsssssssssss", result)
        this.merchants_count = result.final_merchants_result[0].total_merchant_count
        this.active_merchant_count = result.final_merchants_result[1].active_merchant_count
        this.active_merchants = result.final_merchants_result[1].active_merchant_count
        this.table_merchants = result.final_merchants_result[0].total_merchant_count
      }
      // this.merchants_count = result.data
    })
  }

  getUsersList() {
    this.api.getAuth("api/getadminusersdata").subscribe(result => {
      if (result.status === true) {
        this.usersList = result.data;
        console.log("result===>", result)
        // this.show_user_list = true;
        if (this.usersList && this.usersList.length > 0) {
          this.users_data_found = true;
        } else {
          this.users_data_found = false;
        }
      } else {
        console.log("No users found")
        // this.show_user_list = false;
      }
    })
  }
  onClickSubmit(page) {
    let Fromdate
    let Todate
    if (page == null) {
      let temp = new Date();
      let today = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate() - 1);
      let fromDate = today.setHours(0, 0, 0, 0);
      let EndDays = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate() - 1);
      let toDate = EndDays.setHours(23, 59, 59, 999);
      Fromdate = fromDate
      Todate = toDate
    } else {
      console.log('1===>', this.from_date, this.to_date, this.user_id)
      if (this.from_date && this.to_date) {
        var fromd = this.from_date.year + "-" + this.from_date.month + "-" + this.from_date.day;
        var todate = this.to_date.year + "-" + this.to_date.month + "-" + this.to_date.day;
        var fromDate = new Date(fromd).setHours(0, 0, 0, 0);
        var toDate = new Date(todate).setHours(23, 59, 59, 999);
        Fromdate = fromDate
        Todate = toDate
      }
      // console.log('22===>', Fromdate, Todate)
    }
    let paramsData = {}
    if (this.user_id && this.user_id !== 'ALL') {
      paramsData["user_id"] = this.user_id
    }
    paramsData["fromDate"] = Fromdate
    paramsData["toDate"] = Todate
    this.api.getAuthParams('api/getPerfamanceData', paramsData).subscribe(result => {
      if (result) {
        // console.log("output regional sales team1111===>>>>", result)
        this.active_merchants = result.final_merchants_result.active_merchants
        this.table_merchants = result.final_merchants_result.merchants
        
      } else {
        console.log("oops got an error")
      }
    })
    this.api.getAuthParams('api/regionalHeadSalesTeamData', paramsData).subscribe(result => {
      if (result.status) {
        console.log("output regional sales team===>>>>", result)
        this.table_total_payments = result.data[0].payments
        this.table_total_payments_amt = result.data[0].paid_amt
        this.table_pending_payments = result.data[0].transactions - result.data[0].payments
        this.table_pending_payments_amt = result.data[0].transactions_amt - result.data[0].paid_amt
        this.table_transactions_amt = result.data[0].transactions_amt
        this.table_transactions = result.data[0].transactions
        this.table_total_tagets = result.data[1].total_tagets
        this.table_npa_percentage = result.data[0].npa_percentage
        console.log(this.table_pending_payments_amt)
      } else {
        console.log("oops got an error")
      }
    })
  }

  getTransactionsCount() {
    this.api.getAuth('sales_transactions_count').subscribe(result => {
      console.log('in sales ', result.data)
      if (result.status) {
        this.total_transactions = result.data[0].total_transactions + result.data[1].today_transactions;
        this.total_payments_amt = result.data[0].total_paid_amt
        this.total_payments = result.data[0].total_payments
        this.pending_payment = result.data[0].total_transactions - result.data[0].total_payments
        this.pending_payments_amt = result.data[0].total_transactions_amt - result.data[0].total_paid_amt
        this.today_transactions = result.data[1].today_transactions;
        this.yesterday_transactions = result.data[2].yesterday_transactions;
        this.total_transactions_amt = result.data[0].total_transactions_amt + result.data[1].today_transactions_amt;
        this.today_transactions_amt = result.data[1].today_transactions_amt;
        this.yesterday_transactions_amt = result.data[2].yesterday_transactions_amt;
        if (result.data[3]) {
          this.total_tagets = result.data[3].total_tagets;
        }
      
        this.table_total_payments = result.data[0].total_payments
        this.table_total_payments_amt = result.data[0].total_paid_amt
        this.table_pending_payments = result.data[0].total_transactions - result.data[0].total_payments
        this.table_pending_payments_amt = result.data[0].total_transactions_amt - result.data[0].total_paid_amt
        this.table_transactions = result.data[0].total_transactions + result.data[1].today_transactions
        this.table_transactions_amt = result.data[0].total_transactions_amt + result.data[1].today_transactions_amt
        this.table_total_tagets = 0
        this.table_npa_percentage = result.data[0].npa_percentage
      }
    })
  }
  // getPaymentsCount() {
  //   this.api.getAuth('sales_payments_count').subscribe(result => {
  //     console.log('in sales ', result.data)
  //     if (result.status) {
  //       this.total_payments = result.data[0].total_payments
  //       this.pending_payment = result.data[1].pending_payments;
  //       // this.yesterday_payments = result.data[2].yesterday_payments;
  //       this.total_payments_amt = result.data[0].total_payments_amt
  //       this.pending_payments_amt = result.data[1].pending_payments_amt;
  //       // this.yesterday_payments_amt = result.data[2].yesterday_payments_amt;
  //     }
  //   })
  // }
  onClickReset() {
    this.from_date = null
    this.to_date = null
    this.user_id = "ALL"
    // this.active_merchants = 0
    // this.table_merchants = 0
    // this.table_total_payments = 0
    // this.table_total_payments_amt = 0
    // this.table_pending_payments = 0
    // this.table_pending_payments_amt = 0
    // this.table_transactions = 0
    // this.table_transactions_amt = 0
    // this.table_total_tagets = 0
    // this.table_npa_percentage = 0
    this.ngOnInit()
  }

}
