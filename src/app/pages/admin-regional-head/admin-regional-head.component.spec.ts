import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminRegionalHeadComponent } from './admin-regional-head.component';

describe('AdminRegionalHeadComponent', () => {
  let component: AdminRegionalHeadComponent;
  let fixture: ComponentFixture<AdminRegionalHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminRegionalHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminRegionalHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
