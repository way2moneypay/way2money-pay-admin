// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiurl: 'http://localhost:3999/',
  images_cdn: 'http://localhost:3999/',
  qr_download: 'http://localhost:3999/',
  mapsAPIKey: 'AIzaSyChsVnS2EVmAkSw-NhvZHbIqWevlJ8cnv0',
  domain: 'http://localhost:3998/',
  perPage: 10,
  DaysForLastRePredictedCheckDate: 7
};
