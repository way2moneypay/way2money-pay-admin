var mongoose = require('mongoose')
var Schema = mongoose.Schema

var way2moneyEvents = require('../way2mnyEventsDB')

var OverallDayStatsSchema = new Schema({
    used_date: {
        type: Number
    },
    unique_opens: {
        type: Number
    },
    os: {
        type: String
    },
    total_opens: {
        type: Number
    },
    loan_opens: {
        type: Number
    },
    credit_line_opens: {
        type: Number
    },
    expense_manager_opens: {
        type: Number
    },
    paylater_opens: {
        type: Number
    },
    qrcode_scan_opens: {
        type: Number
    },
    personal_loan_opens: {
        type: Number
    },
    transaction_success: {
        type: Number
    },
    logout_users: {
        type: Number
    },
    payment_success: {
        type: Number
    }
}, {
  versionKey: false
})

console.log('events: Collection created.')
module.exports = way2moneyEvents.model('overall_day_stats', OverallDayStatsSchema)
