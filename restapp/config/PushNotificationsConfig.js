let Exports = module.exports;

Exports.templates = {
  "payment-success": {
    "alert_content": "Awesome, we have received your repayment of Rs.##paid_amount## against outstanding in your account. We have refilled your account and there is a surprise coming your way soon"
  }
}
