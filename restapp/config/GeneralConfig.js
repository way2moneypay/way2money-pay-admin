const AppConfig = require('config')
var GeneralConfig = module.exports

GeneralConfig.pageLimit = 10
GeneralConfig.paymentStatus = {
  'initiated': 0,
  'success': 1,
  'failure': 2,
  'failure-cancel': 3,
  'refund': 4,
  'paid': 5
}

GeneralConfig.paymentStatusReverse = {
  0: 'initiated',
  1: 'success',
  2: 'failure',
  3: 'failure-cancel',
  4: 'refund',
  5: 'paid',
  6: 'success'
}

GeneralConfig.TransactionStatus = {
  0: 'initiated',
  1: 'done',
  2: 'failed',
  3: 'cancelled',
  4: 'refunded',
  5: 'paid',
  6: 'done'
}

GeneralConfig.paidStatus = {
  'initiated': 0,
  'success': 1,
  'failure': 2,
  'refund': 3
}

GeneralConfig.paidStatusReverse = {
  0: 'initiated',
  1: 'success',
  2: 'failure',
  3: 'refund'
}

GeneralConfig.creditDetails = {
  'credit_limit': 500,
  'credit_available': 500,
  'credit_due': 0,
  'due_date': null
}
