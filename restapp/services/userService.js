'use strict'
var userService = module.exports
const async = require('async')
const request = require('request')
const config = require('config')
// const Insta = require('instamojo-nodejs')
const Moment = require('moment')
const {
  isMongoId
} = require('validator')
var parallel = require('run-parallel')
const ResponseUtils = require('./../utils/ResponseUtils')
// const GeneralUtils = require('./../utils/GeneralUtils')
const userPaymentModel = require('../models/userPaymentsModel')
const userTransactionsModel = require('../models/userTransactionsModel')
const userDetailsModel = require('../models/userDetails')
const userDevicesModel = require('../models/userDevicesModel')
const usersModel = require('../models/usersModel')
const settingsModel = require('../models/SettingsModel')
const merchantsModel = require('../models/merchantModel')
const RefundTransactions = require('../models/refundTransactionsModel')
const authenticateInfoModel = require('../models/AuthenticateInformationModel')
// const PaymentSettingsModel = require('../models/SettingsModel')
const mongoose = require('mongoose')
const GeneralConfig = require('../config/GeneralConfig')
const merchantStatsModel = require('../models/merchantDaywiseDetails')
const merchantTransCycleModel = require('../models/merchantTransCycle')
const daywiseTransactionsModel = require('../models/daywiseTransactions')
const UserPaymentPendingTransactions = require('../models/userPaymentPendingTransactions')
const PermissionTypesModel = require('../models/PermissionTypesModel')
const UserPermissions = require('../models/UserPermissions')
const usersDaywisePayments = require('../models/dayWisePayments')
const UserCreditAmountModel = require('../models/userCreditAmountsModel')
const UserSmsProcessedCountsModel = require('../smsCountsModel/SmsCounts')
const UserSmsSyncCountsModel = require('../smsCountsModel/SmsSyncCount')
const PredictedAmountModel = require('../models/PredictedAmount')
var PaynearLocationsModel = require('../models/paynearClickLocationsModel')
// var AdminUsersModel = require('../models/AdminUsersModel')
var ContactedUsersModel = require('../models/ContactedUsersModel')
var paynearClickLocationsModel = require('../models/paynearClickLocationsModel')
var SalesTeamTarget = require('../models/SalesTeamTarget')
var notificationUtils = require('../utils/NotificationUtils')
// var encDec = require('../utils/EncDec')

userService.getUserPayments = function (apiReq, apiRes) {
  // console.log('we are in user payments');
  userPaymentModel.aggregate([{
    $sort: {
      _id: -1
    }
  }, {
    $skip: apiReq.body.skip
  },
  {
    $limit: apiReq.body.limit
  },
  {
    $lookup: {
      from: 'user_details',
      localField: 'user_id',
      foreignField: '_id',
      as: 'userInfo'
    }
  }
  ], function (paymentErr, paymentResult) {
    if (paymentErr) {
      console.log('there is an error while getting payment details', paymentErr)
      apiRes.send({
        status: false,
        message: 'error while getting payment details'
      })
    } else if (paymentResult.length > 0) {
      paymentResult = JSON.parse(JSON.stringify(paymentResult))
      async.eachSeries(paymentResult, function (i, next) {
        i['paid_status'] = GeneralConfig.paymentStatusReverse[i['paid_status']]
        next()
      }, function () {
        apiRes.send({
          status: true,
          message: 'payment details found',
          'data': paymentResult
        })
      })
    } else {
      apiRes.send({
        status: false,
        message: 'payment details not found'
      })
    }
  })
}
// userService.getuserTransactions = function (apiReq, apiRes) {
//   console.log('getting user transactions getuserTransactions');
//   var currDate = Date.now();
//   var userTransactions = [];
//   var userTransaction = {};
//   var findObj = {};
//   if (apiReq.body.merchant_id) {
//     findObj['merchant_id'] = apiReq.body.merchant_id;
//   } else {
//     findObj = {};
//   }
//   let aggArr = [];
//   if (apiReq.body.fromDate && apiReq.body.toDate) {
//     let fromDate = new Date(apiReq.body.fromDate);
//     let toDate = new Date(apiReq.body.toDate);
//     console.log(fromDate);
//     console.log(toDate);
//     aggArr.push({
//       $match: {
//         $and: [{
//           transaction_date: {
//             $gte: fromDate.getTime()
//           }
//         }, {
//           transaction_date: {
//             $lte: toDate.getTime()
//           }
//         }]
//       }
//     });
//   }
//   aggArr.push({
//     $sort: {
//       _id: -1
//     }
//   }, {
//     $skip: apiReq.body.skip
//   }, {
//     $limit: apiReq.body.limit
//   }, {
//     $lookup: {
//       from: 'user_details',
//       localField: 'user_id',
//       foreignField: '_id',
//       as: 'userInfo'
//     }
//   });
//   userTransactionsModel.aggregate(aggArr,
//     function (transactErr, transactResult) {
//       // userTransactionsModel.find(findObj, {}, {
//       //   skip: apiReq.body.skip,
//       //   limit: apiReq.body.limit
//       // }, function (transactErr, transactResult) {
//       if (transactErr) {
//         console.log('there is an error while getting transaction details', transactErr);
//         apiRes.send({
//           status: false,
//           message: 'error while getting transaction details'
//         });
//       } else if (transactResult.length > 0) {
//         settingsModel.findOne({}, {
//           refund_period: true
//         }, function (settings_err, settings) {
//           // console.log('settings',settings);
//           transactResult = JSON.parse(JSON.stringify(transactResult));
//           async.eachSeries(transactResult, function (i, next) {
//             var refundExpriryDate = new Date(i.transaction_date).setHours(24 * settings.refund_period);
//             if (refundExpriryDate > currDate) {
//               i.refund_status = 0;
//               if (i.status == 1 || i.status == 6)
//                 i.refund_status = 1;
//               next();
//             } else {
//               i.refund_status = 2;
//               next();
//             }
//           }, function (err) {
//             // console.log('transactResult',transactResult);
//             apiRes.send({
//               status: true,
//               message: 'transaction details found',
//               data: transactResult
//             })
//           })
//         })
//       } else {
//         apiRes.send({
//           status: false,
//           message: 'transaction details not found',
//         })
//       }
//     })

// }
userService.getuserTransactions = function (api_request, api_response) {
  // let Response = new ResponseUtils.response(api_response);
  let this_function_name = 'ConsumerService.deleteLeadStatus'
  // let Print = new ResponseUtils.print(this_file_name, this_function_name);
  let Error_Message = 'Error while delete lead status'
  console.log('ENTERING HERE', api_request.body)
  try {
    let match_query = []
    let count_query = []

    if (api_request.body) {
      if (api_request.body.mobile) {
        match_query.push({
          $match: {
            mobile_no: api_request.body.mobile
          }
        })
      }
      if (api_request.body.status && api_request.body.status != 'ALL') {
        match_query.push({
          $match: {
            status: Number(api_request.body.status)
          }
        })
      }
      if (api_request.body.merchant_id) {
        match_query.push({
          $match: {
            merchant_id: mongoose.Types.ObjectId(api_request.body.merchant_id)
          }
        })
      }
      if (api_request.user.user_type === 'sales_team') {
        match_query.push({
          $match: {
            created_by: mongoose.Types.ObjectId(api_request.user._id)
          }
        })
      }
      if (api_request.user.user_type === 'regional_head') {
        match_query.push({
          $match: {
            created_by_parent: mongoose.Types.ObjectId(api_request.user._id)
          }
        })
      }
      /* Date Filters */
      /* DATE FILTERS */
      console.log('---------------->', api_request.body.from_date, api_request.body.to_date)
      if (api_request.body.from_date && api_request.body.to_date) {
        let date_time
        if (api_request.body.from_date === api_request.body.to_date) {
          console.log('Same date select')
          date_time = getTimeStampDateFilter(api_request.body.from_date, api_request.body.to_date, ['YYYY-MM-DD HH:mm'], true)
        } else {
          console.log('Different select')
          date_time = getTimeStampDateFilter(api_request.body.from_date, api_request.body.to_date, ['YYYY-MM-DD HH:mm'], false)
        }
        match_query.push({
          $match: {
            'transaction_date': date_time
          }
        })
      }
      // } else if (api_request.body.from_today) {
      //   let date_time = { $gte: api_request.body.from_today }
      //   match_query.push({
      //     $match: {
      //       'transaction_date': date_time
      //     }
      //   });

      // } else if (api_request.body.today_date && api_request.body.yesterday_date) {
      //   console.log('coming to backend ')
      //   let date_time = { $gte: api_request.body.yesterday_date, $lte: api_request.body.today_date }
      //   match_query.push({
      //     $match: {
      //       'transaction_date': date_time
      //     }
      //   });

      // }
      console.log('MAtche Query AFTER Date check', match_query)

      count_query = match_query.concat()
      match_query.push({
        $sort: {
          _id: -1
        }
      })
      if (api_request.body.skip) {
        match_query.push({
          $skip: Number(api_request.body.skip, 10)
        })
      }
      if (api_request.body.limit) {
        match_query.push({
          $limit: Number(api_request.body.limit, 10)
        })
      }
      match_query.push({
        $lookup: {
          from: 'user_details',
          localField: 'user_id',
          foreignField: '_id',
          as: 'userInfo'
        }
      })
      match_query.push({
        $lookup: {
          from: 'merchants',
          localField: 'merchant_id',
          foreignField: '_id',
          as: 'merchantInfo'
        }
      })

      count_query.push({
        $group: {
          _id: null,
          total_records: {
            $sum: 1
          }
        }
      })
      console.log('MATCH QUER', match_query)
      // console.log('COUNT QUERY', count_query);

      /* -- */
      userTransactionsModel.aggregate(count_query, function (count_error, count_data) {
        if (count_error) {
          console.log('ERROR WHILE GHETTING COUNT DATA OF TRNSACTIONS', count_error)
        } else {
          console.log('DATA FOUNDED', count_data)
          if (count_data.length > 0) {
            console.log('coming to count next match')
            userTransactionsModel.aggregate(match_query, function (transaction_data_error, transaction_details) {
              if (transaction_data_error) {
                console.log('Error while fetching transaction details', transaction_data_error)
              } else {
                // console.log('///////////////////////////////', transaction_details)
                if (transaction_details) {
                  let response = {}
                  response.total_records = count_data[0].total_records
                  response.transaction_data = transaction_details
                  // console.log('SUCCESSFILLY FETCH DETAILS OF USER TRANSACTIONS', response)
                  api_response.send({
                    status: true,
                    message: 'transaction details found',
                    data: response
                  })
                }
              }
            })
          } else {
            api_response.send({
              status: true,
              message: 'no data found',
              data: null
            })
          }
        }
      })
    }
  } catch (exception) {
    // Print.error(exception);
  }
}
/* User payments fetching api by Suresh */
userService.fetchUserPayments = function (api_request, api_response) {
  let this_function_name = 'userService.fetchUserPayments'
  let Error_Message = 'Error while fetching user payment history'
  // console.log('ENTERING HERE', api_request.body);
  try {
    let match_query = []
    let count_query = []

    if (api_request.body) {
      if (api_request.body.mobile) {
        match_query.push({
          $match: {
            mobile_number: api_request.body.mobile
          }
        })
      }
      if (api_request.body.status && api_request.body.status != 'ALL') {
        match_query.push({
          $match: {
            paid_status: Number(api_request.body.status)
          }
        })
      }

      /* Date Filters */
      /* DATE FILTERS */
      if (api_request.body.from_date && api_request.body.to_date) {
        let date_time
        if (api_request.body.from_date === api_request.body.to_date) {
          date_time = getTimeStampDateFilter(api_request.body.from_date, api_request.body.to_date, ['YYYY-MM-DD HH:mm'], true)
        } else {
          date_time = getTimeStampDateFilter(api_request.body.from_date, api_request.body.to_date, ['YYYY-MM-DD HH:mm'], false)
        }
        match_query.push({
          $match: {
            'payment_initiated_date': date_time
          }
        })
      }
      console.log('MAtche Query AFTER Date check', match_query)

      count_query = match_query.concat()
      match_query.push({
        $sort: {
          _id: -1
        }
      })
      if (api_request.body.skip) {
        match_query.push({
          $skip: Number(api_request.body.skip, 10)
        })
      }
      if (api_request.body.limit) {
        match_query.push({
          $limit: Number(api_request.body.limit, 10)
        })
      }
      match_query.push({
        $lookup: {
          from: 'user_details',
          localField: 'user_id',
          foreignField: '_id',
          as: 'userInfo'
        }
      })
      count_query.push({
        $group: {
          _id: null,
          total_records: {
            $sum: 1
          }
        }
      })
      console.log('MATCH QUER', match_query)
      console.log('COUNT QUERY', count_query)
      if (api_request.user.user_type == 'sales_team') {
        match_query.push({
          $match: {
            created_by: mongoose.Types.ObjectId(api_request.user._id)
          }
        })
      }
      if (api_request.user.user_type == 'regional_head') {
        match_query.push({
          $match: {
            created_by: mongoose.Types.ObjectId(api_request.user.parent_id)
          }
        })
      }
      /* -- */
      userPaymentModel.aggregate(count_query, function (count_error, count_data) {
        if (count_error) {
          console.log('ERROR WHILE GHETTING COUNT DATA OF TRNSACTIONS', count_error)
        } else {
          if (count_data.length > 0) {
            console.log('DATA FOUNDED', count_data)
            userPaymentModel.aggregate(match_query, function (transaction_data_error, transaction_details) {
              if (transaction_data_error) {
                console.log('Error while fetching transaction details', transaction_data_error)
              } else {
                if (transaction_details) {
                  // console.log('SUCCESSFILLY FETCH DETAILS OF USER TRANSACTIONS', transaction_details[0].userInfo[1]);
                  let response = {}
                  response.total_records = count_data[0].total_records
                  response.transaction_data = transaction_details
                  api_response.send({
                    status: true,
                    message: 'transaction details found',
                    data: response
                  })
                }
              }
            })
          } else {
            api_response.send({
              status: true,
              message: 'no data found',
              data: null
            })
          }
        }
      })
    }
  } catch (exception) {
    // Print.error(exception);
  }
}
/* Function that returns Time conversions */
function getTimeStampDateFilter(from_date, to_date, date_formats, add_day) {
  if (!from_date || !to_date) {
    return null
  }

  if (!Moment(from_date, date_formats).isValid() || !Moment(to_date, date_formats).isValid()) {
    return null
  }

  // console.log('FROM DATE IN ADD FUNCVTION BEFORE', from_date);
  // console.log('TO_DATE IN ADD FUNCVTION BEFORE', to_date, add_day);
  from_date = (Moment(from_date, date_formats) / 1)
  if (add_day != false) to_date = Moment(to_date, date_formats).add(86399, 'seconds') / 1
  if (add_day == false) to_date = Moment(to_date, date_formats) / 1
  from_date = from_date - 19800000
  to_date = to_date - 19800000
  if (!add_day) to_date = to_date + 86399000
  // console.log('FROM DATE IN ADD FUNCVTION AFTER', from_date);
  // console.log('TO_DATE IN ADD FUNCVTION AFTER', to_date);

  return {
    $gte: from_date,
    $lte: to_date
  }
}

userService.getUserDetails = function (apiReq, apiRes) {
  console.log('apiReq.body', apiReq.body)
  var findFilter = []
  findFilter.push({
    $sort: {
      _id: -1
    }
  })
  findFilter.push({
    $skip: apiReq.body.skip
  })
  findFilter.push({
    $limit: apiReq.body.limit
  })
  findFilter.push({
    $lookup: {
      from: 'predicted_amounts',
      localField: '_id',
      foreignField: 'user_id',
      as: 'PredictedAmounts'
    }
  })
  // findFilter.push({
  //   $unwind: '$PredictedAmounts'
  // })

  userDetailsModel.aggregate(findFilter,
    function (userError, userResult) {
      if (userError) {
        apiRes.send({
          status: false,
          'message': userError
        })
      } else if (userResult.length) {
        // console.log(userResult)
        apiRes.send({
          'status': true,
          'message': 'details fetched successfully',
          'data': userResult
        })
      } else {
        apiRes.send({
          'status': false,
          'message': 'details not found'
        })
      }
    })
}
userService.userSearch = function (apiReq, apiRes) {
  console.log('RECEIVED :: ', apiReq.body)
  let findObj = {}
  if (apiReq.body.mobile_number) {
    // findObj.push({
    //   $match: {
    //     mobile_number: apiReq.body.mobile_number
    //   }
    // })
    findObj['mobile_number'] = apiReq.body.mobile_number
  }
  if (apiReq.body.mid) {
    findObj['mid'] = apiReq.body.mid
  }
  if (apiReq.body && apiReq.body.amount_from != null && apiReq.body.amount_to != null && (apiReq.body.amount_from || apiReq.body.amount_from === 0) && (apiReq.body.amount_to || apiReq.body.amount_to === 0)) {
    console.log('credit limit', apiReq.body.amount_from, apiReq.body.amount_to)
    findObj['credit_limit'] = {
      $gte: Number(apiReq.body.amount_from),
      $lte: Number(apiReq.body.amount_to)
    }
  }
  console.log('after credit limit')
  if (apiReq.body.type !== undefined && apiReq.body.type != null) {
    if (apiReq.body.fromDate && apiReq.body.toDate) {
      findObj[apiReq.body.type] = {
        $gte: Number(apiReq.body.fromDate),
        $lte: Number(apiReq.body.toDate)
      }
    } else if (apiReq.body.fromDate) {
      findObj[apiReq.body.type] = {
        $gte: Number(apiReq.body.fromDate)
      }
    } else if (apiReq.body.toDate) {
      findObj[apiReq.body.type] = {
        $lte: Number(apiReq.body.toDate)
      }
    }
  }
  console.log('after date')
  if (apiReq.body && apiReq.body.app_version && apiReq.body.app_version !== '') {
    findObj['app_version'] = apiReq.body.app_version
  }
  // if (apiReq.body.os !== '') {
  //   findObj['os'] = apiReq.body.os
  // }
  console.log('apireqBody', findObj)

  var aggregateArr = []
  aggregateArr.push({
    $match: findObj
  })

  aggregateArr.push({
    $sort: {
      _id: -1
    }
  })

  if (apiReq.body && apiReq.body.skip) {
    aggregateArr.push({
      $skip: apiReq.body.skip
    })
  } else if (apiReq.body && apiReq.body.skip === 0) {
    aggregateArr.push({
      $skip: 0
    })
  }

  console.log('after skip')

  // if (apiReq.body && apiReq.body.limit) {
  //   aggregateArr.push({
  //     $limit: apiReq.body.limit
  //   })
  // }
  aggregateArr.push({
    $lookup: {
      from: 'predicted_amounts',
      localField: '_id',
      foreignField: 'user_id',
      as: 'PredictedAmounts'
    }
  })
  console.log('array--->', aggregateArr)
  userDetailsModel.aggregate(aggregateArr, function (userError, userResult) {
    console.log('first----------->', userError, userResult[0])
    if (userError) {
      console.log('resultssss', userError)
      apiRes.send({
        'status': false,
        'message': userError
      })
    } else if (userResult.length) {
      // console.log('user search result', userResult)
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': userResult
      })
    } else {
      console.log('no results found')
      apiRes.send({
        'status': false,
        'message': 'details not found'
      })
    }
  })
}
userService.paymentSearch = function (apiReq, apiRes) {
  // console.log(apiReq.body);
  // console.log('************')
  var findObj = {}
  findObj = apiReq.body
  if (apiReq.body.type != undefined && apiReq.body.type != null) {
    findObj.paid_date = {}
    if (apiReq.body.from_date) {
      findObj.paid_date['$gte'] = apiReq.body.from_date
    }
    if (apiReq.body.to_date) {
      findObj.paid_date['$lt'] = apiReq.body.to_date
    }
    delete findObj['type']
    delete findObj['from_date']
    delete findObj['to_date']
  } else {
    findObj = apiReq.body
    // limitObj = apiReq.body[1];
  }
  // console.log('findObj',findObj);
  // console.log('limit object',limitObj);
  userPaymentModel.find(findObj, {}, function (paymentSearchErr, paymentSearchResult) {
    if (paymentSearchErr) {
      apiRes.send({
        'status': false,
        'message': paymentSearchErr
      })
    } else if (paymentSearchResult.length > 0) {
      // console.log(userResult);
      paymentSearchResult = JSON.parse(JSON.stringify(paymentSearchResult))
      async.eachSeries(paymentSearchResult, function (i, next) {
        i['paid_status'] = GeneralConfig.paymentStatusReverse[i['paid_status']]
        next()
      }, function () {
        apiRes.send({
          status: true,
          message: 'payment details found',
          'data': paymentSearchResult
        })
      })
      // apiRes.send({
      //   'status': true,
      //   'message': 'details fetched successfully',
      //   'data': paymentSearchResult
      // });
    } else {
      apiRes.send({
        'status': false,
        'message': 'details not found'
      })
    }
  }).sort({
    paid_date: -1
  })
}
userService.transactionSearch = function (apiReq, apiRes) {
  console.log('apireq', apiReq.body)
  var findObj = {}
  var currDate = Date.now()
  // console.log('body :: ', apiReq.body);
  async.waterfall([
    (done) => {
      if (apiReq.body && apiReq.body.brand_name) {
        // console.log('brand_name:: ',apiReq.body.brand_name)
        findObj = apiReq.body
        if (apiReq.body.fromDate && apiReq.body.toDate) {
          findObj['transaction_date'] = {
            $gte: apiReq.body.fromDate,
            $lt: apiReq.body.toDate
          }
          delete findObj['fromDate']
          delete findObj['toDate']
        }
        merchantsModel.findOne({
          brand_name: apiReq.body.brand_name
        }, (errFindMerchant, merchantFound) => {
          findObj['merchant_id'] = merchantFound._id
          delete findObj['brand_name']
          done(null)
        })
      } else {
        if (apiReq.body.type != undefined && apiReq.body.type != null) {
          // console.log(apiReq.body.type)
          if (apiReq.body.merchant_id) {
            findObj['merchant_id'] = apiReq.body.merchant_id
            done(null)
          }
        } else {
          // console.log(apiReq.body)
          findObj = apiReq.body
          if (apiReq.body.fromDate && apiReq.body.toDate) {
            findObj['transaction_date'] = {
              $gte: apiReq.body.fromDate,
              $lt: apiReq.body.toDate
            }
            delete findObj['fromDate']
            delete findObj['toDate']
          }
          if (!apiReq.body.merchant_id) {
            delete findObj['merchant_id']
            done(null)
          } else {
            findObj['merchant_id'] = String(apiReq.body.merchant_id)
            done(null)
          }
          // limitObj = apiReq.body[1];
        }
      }
      // if (apiReq.user.user_type == 'sales_team') {
      //   findObj.created_by = mongoose.Types.ObjectId(apiReq.user._id)

      // } if (apiReq.user.user_type == 'regional_head') {
      //   findObj.created_by = mongoose.Types.ObjectId(apiReq.user.parent_id)
      // }
    },
    (done) => {
      // console.log('findObj',findObj)
      userTransactionsModel.find(findObj, {}, function (userTransactionError, userTransactionResult) {
        if (userTransactionError) {
          apiRes.send({
            'status': false,
            'message': userTransactionError
          })
        } else if (userTransactionResult.length > 0) {
          settingsModel.findOne({}, {
            refund_period: true
          }, function (settings_err, settings) {
            userTransactionResult = JSON.parse(JSON.stringify(userTransactionResult))
            async.eachSeries(userTransactionResult, function (i, next) {
              var refundExpriryDate = new Date(i.transaction_date).setHours(24 * settings.refund_period)
              if (refundExpriryDate > currDate) {
                i.refund_status = 0
                if (i.status == 1 || i.status == 6) {
                  i.refund_status = 1
                }
                next()
              } else {
                i.refund_status = 2
                next()
              }
            }, function (err) {
              if (err) {
                apiRes.send({
                  status: false,
                  message: 'transaction details not found',
                  data: userTransactionResult
                })
              } else {
                apiRes.send({
                  status: true,
                  message: 'transaction details found',
                  data: userTransactionResult
                })
              }
            })
          })
        } else {
          apiRes.send({
            'status': false,
            'message': 'details not found'
          })
        }
      }).sort({
        transaction_date: -1
      })
    }
  ])
}
userService.checkRefund = function (apiReq, apiRes) {
  let transaction = apiReq.body
  userTransactionsModel.findById({
    _id: transaction._id
  }, function (err, res) {
    if (err) {
      apiRes.send({
        status: false,
        message: 'Error while finding transaction.'
      })
    } else if (res) {
      // var commission = res.amount - ((res.amount * res.merchant_commission) / 100)
      var convenience = res.amount + ((res.amount * res.con_fee) / 100)
      apiRes.send({
        status: true,
        message: 'Refunded',
        data: {
          // commission_amount: commission,
          // commission_percentage_amount: (res.amount * res.merchant_commission) / 100,
          // commission_percentage: res.merchant_commission,
          convenience_amount: convenience,
          convenience_percentage_amount: (res.amount * res.con_fee) / 100,
          convenience_percentage: res.con_fee,
          transaction_amount: res.amount,
          merchant_id: res.merchant_id,
          user_id: res.user_id
        }
      })
    } else {
      apiRes.send({
        status: false,
        message: 'Refund failed'
      })
    }
  })
}

userService.refund = function (apiReq, apiRes) {
  let transaction = apiReq.body
  let queries = []
  let update = 0
  let creditAvailable = 0
  let creditDue = 0
  let RefundTransactionDetails = {}
  console.log('checkrefund::::::::::', transaction)
  if (transaction.transaction_date != undefined) {
    var transaction_date = new Date(transaction.transaction_date)
    transaction_date = transaction_date.setHours(0, 0, 0, 0, 0)
    transaction_date = (new Date(transaction_date).getTime() - 19800000)
  }
  if (transaction.con_fee != undefined) {
    var f_amount = transaction.amount - (transaction.amount * transaction.con_fee) / 100
  } else {
    var f_amount = transaction.amount
  }
  userTransactionsModel.findOneAndUpdate({
    _id: transaction._id
  }, {
    $set: {
      status: 4,
      transaction_status: 'refunded'
    },
    $addToSet: {
      trans_details: {
        status: 4,
        date: Date.now()
      }
    }
  }, {
    upsert: false,
    new: true
  }, function (updateErr, UpdateRes) {
    if (updateErr) {
      console.log('Error while updating userTransactions')
      apiRes.send({
        error_message: 'Error while updating userTransactions',
        error: updateErr
      })
    } else {
      var convenience = UpdateRes.amount + ((UpdateRes.amount * UpdateRes.con_fee) / 100)
      console.log('userTransactions updated successfully')
      userDetailsModel.findOne({
        _id: transaction.user_id
      }, function (err_get_user_details, user_details) {
        if (err_get_user_details) {
          console.log('Error while updating userDetails')
          apiRes.send({
            error_message: 'Error while updating userDetails',
            error: updateErr
          })
        } else {
          user_details.credit_available = Number(user_details.credit_available) + Number(convenience)
          user_details.credit_due = Number(user_details.credit_due) - Number(convenience)

          if (user_details.credit_available > user_details.credit_limit) {
            user_details.credit_available = user_details.credit_limit
          }

          if (user_details.credit_due < 0) {
            user_details.credit_due = 0
            user_details.due_date = 0
          }

          if (user_details.credit_due == 0) {
            user_details.due_date = 0
          }
          creditAvailable = user_details.credit_available
          creditDue = user_details.credit_due
          user_details.save((err_save_user_details, user_detials_saved) => {
            if (err_save_user_details) {
              console.log('Error while saving userDetails')
              apiRes.send({
                error_message: 'Error while saving userDetails',
                error: updateErr
              })
            } else {
              console.log('userDetails updated successfully in refund')
              merchantTransCycleModel.count({
                cycle_date: {
                  $gte: transaction_date
                }
              },
              function (countError, countResult) {
                if (countError) {
                  console.log(countError)
                  throw cb(countError)
                } else {
                  console.log('count:::::::::::', countResult)
                  if (countResult != 0) {
                    if (transaction.amount != undefined) {
                      //  transaction_date = transaction_date/1;
                      queries.push(function (cb) {
                        daywiseTransactionsModel.findOneAndUpdate({
                            trans_date: transaction_date
                          }, {
                            $inc: {
                              transactions: -1,
                              trans_amount: -transaction.amount,
                              amount: -f_amount
                            }
                          }, {
                            upsert: false
                          },
                          function (updateError, updateResult) {
                            // console.log(updateResult)
                            if (updateError) {
                              console.log(updateError)
                              throw cb(updateError)
                            } else {
                              cb(null, updateResult)
                            }
                          })
                      })
                    }
                    if (transaction.transaction_date != undefined && transaction.merchant_id != undefined && transaction.amount != undefined) {
                      queries.push(function (cb) {
                        merchantStatsModel.findOneAndUpdate({
                            merchant_id: transaction.merchant_id,
                            trans_date: transaction_date
                          }, {
                            $inc: {
                              transactions: -1,
                              trans_amount: -transaction.amount,
                              amount: -f_amount
                            }
                          }, {
                            upsert: false
                          },
                          function (updateError, updateResult) {
                            // console.log(updateResult)
                            if (updateError) {
                              console.log(updateError)
                              throw cb(updateError)
                            } else {
                              cb(null, updateResult)
                            }
                          })
                      })
                    }
                    queries.push(function (cb) {
                      // {cycle_status:{$ne:'pending'}}
                      merchantTransCycleModel.find({
                          merchant_id: transaction.merchant_id
                        }).sort({
                          _id: -1
                        }).limit(1)

                        .exec(function (error, result) {
                          if (error) {
                            console.log(error)
                            throw cb(error)
                          } else {
                            if (Object.getOwnPropertyNames(result).length != 0) {
                              console.log('result', typeof result)
                              if (result.cycle_status != 'pending') {
                                merchantsModel.findOneAndUpdate({
                                    _id: transaction.merchant_id
                                  }, {
                                    $inc: {
                                      previous_cycle_paid_refund_amount: transaction.amount
                                    }
                                  }, {
                                    upsert: false
                                  },
                                  function (updateError, updateResult) {
                                    // console.log(updateResult)
                                    if (updateError) {
                                      throw cb(updateError)
                                    } else {
                                      cb(null, updateResult)
                                    }
                                  })
                              } else {
                                merchantTransCycleModel.findOneAndUpdate({
                                    merchant_id: transaction.merchant_id
                                  }, {
                                    $inc: {
                                      transactions: -1,
                                      amount: -transaction.amount
                                    }
                                  }, {
                                    upsert: false
                                  }).sort({
                                    _id: -1
                                  }).limit(1)

                                  .exec(function (updateErr, UpdateRes) {
                                    if (updateErr) {
                                      throw cb(updateErr)
                                    } else {
                                      cb(null, UpdateRes)
                                    }
                                  })
                              }
                            } else {
                              cb(null, 0)
                            }
                          }
                        })
                    })
                    async.parallel(queries, function (err, docs) {
                      if (err) {
                        apiRes.send({
                          error_message: 'Error while updating transcycle',
                          error: err
                        })
                      } else {
                        let configuration = {}
                        let merchant_commission_amount = 0
                        if (transaction.con_fee != undefined) {
                          var convenience = transaction.amount + ((transaction.amount * transaction.con_fee) / 100)
                        } else {
                          var convenience = transaction.amount
                        }
                        if (transaction.merchant_commission != undefined) {
                          merchant_commission_amount = (transaction.amount * transaction.merchant_commission) / 100
                        }

                        RefundTransactionDetails = new RefundTransactions({
                          transaction_id: transaction.transatcion_id,
                          merchant_id: transaction.merchant_id,
                          user_id: transaction.user_id,
                          con_fee: ((transaction.amount * transaction.con_fee) / 100),
                          merchant_commission: merchant_commission_amount,
                          amount: transaction.amount,
                          trans_amount: convenience,
                          credit_available: creditAvailable,
                          credit_due: creditDue,
                          refunded_date: Date.now(),
                          refund_by: 'Admin'
                        })
                        RefundTransactionDetails.save(function (RefundTransactionSaveErr, RefundTransactionSaved) {
                          if (RefundTransactionSaveErr) {
                            return Response.error({
                              error_message: 'Refund success. Saving redund transaction failed',
                              error_details: RefundTransactionSaveErr
                            }, 'REFUND-FAILED', 500)
                          } else {
                            let obj = {
                              amount: convenience,
                              name: transaction.name
                            }
                            configuration.mobile = transaction.mobile
                            configuration.replace = obj
                            // console.log(configuration.emails);
                            if (transaction.merchant_mobile !== config.merchant_testing_mobile) {
                              notificationUtils.sms('refund_sms', configuration, function (err, resp) {
                                console.error(err)
                                // console.log(resp);
                              })
                            }
                            if (transaction.merchant_name != undefined && transaction.merchant_mobile) {
                              let configuration = {}

                              let obj = {
                                amount: convenience,
                                name: transaction.merchant_name,
                                transaction: transaction.reference_id
                              }
                              configuration.mobile = transaction.merchant_mobile
                              configuration.replace = obj
                              if (transaction.merchant_mobile !== config.merchant_testing_mobile) {
                                notificationUtils.sms('merchant_refund_sms', configuration, function (err, resp) {
                                  console.error(err)
                                  // console.log('In merchant');
                                })
                              }
                            }
                            console.log('In asyc:::::::::::::::')
                            apiRes.send({
                              status: true,
                              message: 'Refunded'
                            })
                          }
                        })
                      }
                    })
                  } else {
                    let configuration = {}
                    let merchant_commission_amount = 0
                    if (transaction.con_fee != undefined) {
                      var convenience = transaction.amount + ((transaction.amount * transaction.con_fee) / 100)
                    } else {
                      var convenience = transaction.amount
                    }
                    if (transaction.merchant_commission != undefined) {
                      merchant_commission_amount = (transaction.amount * transaction.merchant_commission) / 100
                    }

                    RefundTransactionDetails = new RefundTransactions({
                      transaction_id: transaction.transatcion_id,
                      merchant_id: transaction.merchant_id,
                      user_id: transaction.user_id,
                      con_fee: ((transaction.amount * transaction.con_fee) / 100),
                      merchant_commission: merchant_commission_amount,
                      amount: transaction.amount,
                      trans_amount: convenience,
                      credit_available: creditAvailable,
                      credit_due: creditDue,
                      refunded_date: Date.now(),
                      refund_by: 'Admin'
                    })
                    RefundTransactionDetails.save(function (RefundTransactionSaveErr, RefundTransactionSaved) {
                      if (RefundTransactionSaveErr) {
                        return Response.error({
                          error_message: 'Refund success. Saving redund transaction failed',
                          error_details: RefundTransactionSaveErr
                        }, 'REFUND-FAILED', 500)
                      } else {
                        let obj = {
                          amount: convenience,
                          name: transaction.name
                        }
                        configuration.mobile = transaction.mobile
                        configuration.replace = obj
                        // console.log(configuration.emails);
                        if (transaction.merchant_mobile !== config.merchant_testing_mobile) {
                          notificationUtils.sms('refund_sms', configuration, function (err, resp) {
                            console.error(err)
                            // console.log(resp);
                          })
                        }
                        if (transaction.merchant_name != undefined && transaction.merchant_mobile) {
                          let configuration = {}

                          let obj = {
                            amount: convenience,
                            name: transaction.merchant_name,
                            transaction: transaction.reference_id
                          }
                          configuration.mobile = transaction.merchant_mobile
                          configuration.replace = obj
                          if (transaction.merchant_mobile !== config.merchant_testing_mobile) {
                            notificationUtils.sms('merchant_refund_sms', configuration, function (err, resp) {
                              console.error(err)
                              // console.log('In merchant');
                            })
                          }
                        }
                        // console.log('In asyc:::::::::::::::')
                        apiRes.send({
                          status: true,
                          message: 'Refunded'
                        })
                      }
                    })
                  }
                }
              })
            }
          })
          // if (UpdateRes.credit_due == 0) {
          //   userDetailsModel.findOneAndUpdate({ _id: transaction.user_id }, {
          //     $set: {
          //       due_date: 0
          //     }
          //   }, {
          //       upsert: false,
          //     }, function (updateErr, UpdateRes) {
          //       if (updateErr) {
          //         console.log('Error while updating userDetails')
          //         apiRes.send({
          //           error_message: 'Error while updating userDetails',
          //           error: updateErr,
          //         });
          //       } else {
          //         console.log('updated due_date successfully');
          //       }
          //     });
          // }
        }
      })
    }
  })
}

//   var pending_amt, credit_available, credit_due;
//   var rollback_refund_merchant_amount, init_pending_amt, init_credit_available, init_credit_due;
//   var due_date, transaction_date, init_status

//   apiReq.body = apiReq.body.refund

//   /** Rollback functions incase if refund fails */
//   function MerchantRefundRollback(error, callback) {
//     merchantsModel.findByIdAndUpdate({
//       _id: apiReq.body.merchant_id
//     }, {
//         $set: {
//           pending_amt: rollback_refund_merchant_amount
//         }
//       }, function (MerchantUpdateErr, MerchantUpdated) {
//         if (MerchantUpdateErr) MerchantRefundRollback(error)
//         else {
//           error = 'Refund failed and transactions rollbacked. Reason for rollback ' + error
//           return apiRes.send({
//             status: false,
//             message: error,
//           });
//         }
//       })
//   }
//   /** Rollback function incase if refund fails */
//   function UserMerchantRefundRollback(error, callback) {
//     merchantsModel.findByIdAndUpdate({
//       _id: apiReq.body.merchant_id
//     }, {
//         $set: {
//           pending_amt: init_pending_amt
//         }
//       }, function (MerchantUpdateErr, MerchantUpdated) {
//         if (MerchantUpdateErr) UserMerchantRefundRollback(error)
//         else {
//           UserDetailsModel.findByIdAndUpdate({
//             _id: apiReq.body.user_id
//           }, {
//               $set: {
//                 credit_available: init_credit_available,
//                 credit_due: init_credit_due
//               }
//             }, function (UserUpdateErr, UserUpdateUpdated) {
//               if (UserUpdateErr) return apiRes.send({
//                 error_message: 'Rollback for merchant is successfull, rollback for user is failed reason :',
//                 error: UserUpdateErr
//               });
//               else {
//                 return apiRes.send({
//                   status: false,
//                   message: error,
//                 });
//               }
//             })
//         }
//       })
//   }
//   /** Rollback function incase if refund fails */
//   function UserMerchantStatusRefundRollback(error, callback) {
//     merchantsModel.findByIdAndUpdate({
//       _id: apiReq.body.merchant_id
//     }, {
//         $set: {
//           pending_amt: init_pending_amt
//         }
//       }, function (MerchantUpdateErr, MerchantUpdated) {
//         if (MerchantUpdateErr) UserMerchantStatusRefundRollback(error)
//         else {
//           userDetailsModel.findByIdAndUpdate({
//             _id: apiReq.body.user_id
//           }, {
//               $set: {
//                 credit_available: init_credit_available,
//                 credit_due: init_credit_due
//               }
//             }, function (UserUpdateErr, UserUpdateUpdated) {
//               if (UserUpdateErr) return apiRes.send({
//                 error_message: 'Rollback for merchant is successfull, rollback for user is failed reason :',
//                 error: UserUpdateErr
//               });
//               else {
//                 userTransactionsModel.findByIdAndUpdate({
//                   _id: apiReq.body.transaction_id
//                 }, {
//                     $set: {
//                       status: init_status
//                     }
//                   }, function (err, res) {
//                     if (err) return apiRes.send({
//                       error_message: 'Rollback for merchant and user is successfull, rollback for transaction status is failed reason :',
//                       error: err
//                     });
//                     else return apiRes.send({
//                       error_message: 'Refund failed and transactions rollbacked. Reason for rollback'
//                     });
//                   })
//               }
//             })
//         }
//       })
//   }

//   userTransactionsModel.findOne({
//     _id: apiReq.body.transaction_id
//   }, function (checkStatusErr, checkStatusRes) {
//     if (checkStatusErr) return apiRes.send({
//       error_message: 'Error while checking transaction stats',
//       error: checkStatusErr
//     }, 500)
//     else if (checkStatusRes) {
//       merchant_commission_amount = (checkStatusRes.amount * checkStatusRes.merchant_commission) / 100;
//       merchant_pending_amount = checkStatusRes.amount - merchant_commission_amount;
//       con_fee_amount = (checkStatusRes.amount * checkStatusRes.con_fee) / 100;
//       total_trans_amount = checkStatusRes.amount + con_fee_amount;
//       merchant_id = checkStatusRes.merchant_id;
//       user_id = checkStatusRes.user_id;
//       original_amount = checkStatusRes.amount;

//       if (checkStatusRes.status != 4) {
//         /** Refund of transactions starts */
//         async.series([
//           /** 1. Merchant refund */
//           /** Checking whether merchants exists */
//           function (callback) {
//             merchantsModel.findById({
//               _id: apiReq.body.merchant_id
//             }, function (MerchantErr, MerchantRes) {
//               if (MerchantErr) callback('MongoError : Merchants find error', null)
//               else if (MerchantRes) {
//                 /** If merchant found refunding amount to merchant */
//                 let init_pending_amt = MerchantRes.pending_amt ? MerchantRes.pending_amt : 0;
//                 let init_approved_amt = MerchantRes.approved_amt ? MerchantRes.approved_amt : 0;

//                 /** Calculating refund amount */
//                 pending_amt = init_pending_amt - merchant_pending_amount
//                 approved_amt = init_approved_amt - merchant_pending_amount;

//                 refund_merchant_amount = {
//                   pending_amt: pending_amt
//                 }
//                 // Storing initial amount incase to rollback
//                 rollback_refund_merchant_amount = {
//                   pending_amt: init_pending_amt
//                 }
//                 /** Checking status */
//                 if (checkStatusRes.status == 6 || checkStatusRes.status == 7) {
//                   refund_merchant_amount = {
//                     approved_amt: approved_amt
//                   }
//                   // Storing initial amount incase to rollback
//                   rollback_refund_merchant_amount = {
//                     approved_amt: init_approved_amt
//                   }
//                 }

//                 merchantsModel.findByIdAndUpdate({
//                   _id: apiReq.body.merchant_id
//                 }, {
//                     $set: refund_merchant_amount
//                   }, function (MerchantUpdateErr, MerchantUpdated) {
//                     /** If error occurs while refunding throwing error */
//                     /** It's the first step so rollback isn't necessary */
//                     if (MerchantUpdateErr) callback({
//                       error_name: 'MongoError : Merchants refund update failed',
//                       error: MerchantUpdateErr
//                     }, null)
//                     else if (MerchantUpdated) callback(null, null)
//                   })
//               } else {
//                 callback('404 : Merchant not found', null)
//               }
//             })
//           },
//           /** 2. User's refund */
//           /** Checking whether user exists */
//           function (callback) {
//             userDetailsModel.findById({
//               _id: apiReq.body.user_id
//             }, function (UserErr, UserRes) {
//               /** If error occurs while checking user exists rollback merchants refund*/
//               if (UserErr) {
//                 MerchantRefundRollback({
//                   error_refund: 'Refund failed and transactions rollbacked. Reason for rollback',
//                   error_name: 'MongoError : User's find error',
//                   error: err
//                 })
//               } else if (UserRes) {
//                 /** Storing intial amount if incase to rollback refund */
//                 init_credit_available = UserRes.credit_available
//                 init_credit_due = UserRes.credit_due
//                 /** Storing refund amount */
//                 credit_available = UserRes.credit_available + apiReq.body.convenience_amount
//                 credit_due = UserRes.credit_due - apiReq.body.convenience_amount
//                 userDetailsModel.findByIdAndUpdate({
//                   _id: apiReq.body.user_id
//                 }, {
//                     $set: {
//                       credit_available: credit_available,
//                       credit_due: credit_due
//                     }
//                   }, function (UserUpdateErr, UserUpdateUpdated) {
//                     if (UserUpdateErr) {
//                       MerchantRefundRollback({
//                         error_refund: 'Refund failed and transactions rollbacked. Reason for rollback',
//                         error_name: 'MongoError : User's refund update failed',
//                         error: err
//                       })
//                     } else if (UserUpdateUpdated) {
//                       callback(null, null)
//                     }
//                   })
//               } else {
//                 MerchantRefundRollback('404 : User not found')
//               }
//             })
//           },
//           /** 3. Updating redunded in user's transaction */
//           function (callback) {
//             userTransactionsModel.findByIdAndUpdate({
//               _id: apiReq.body.transaction_id
//             }, {
//                 $set: {
//                   status: 4
//                 },
//                 $addToSet: {
//                   trans_details: {
//                     status: 4,
//                     date: Date.now()
//                   }
//                 }
//               }, function (err, res) {
//                 if (err) {
//                   UserMerchantRefundRollback({
//                     error_refund: 'Refund failed and transactions rollbacked. Reason for rollback',
//                     error_name: 'MongoError: Updating in user's transaction failed',
//                     error: err
//                   })
//                 } else {
//                   callback(null, null)
//                 }
//               })
//           },
//           /** 4. Checking to updating due date */
//           function (callback) {
//             /** Explanation of updating due date (Example) */
//             /** Existing due_date = 15 Jul (Period : 15 days)
//              *  List of transactions
//              *  1 Jul - failed (clicked refund on this transaction )
//              *  5 Jul - success
//              *  8 Jul - success
//              *  After refund
//              *  (1 Jul) transaction is not valid anymore
//              *  So due date should be updated to 20 Jul (5 Jul + 15 days)
//              *  Case 1. Check whether successfull transactions (status:1) exists after refunded transaction
//              *          if exists update due_date = transaction_date+due_period
//              *  Case 2. If transactions not found update due_date = 0
//              */

//             /** Step 1. Check whether successfull transactions (status:1) exists after refunded transaction */
//             userTransactionsModel.findOne({
//               user_id: mongoose.Types.ObjectId(apiReq.body.user_id),
//               status: 1
//             }, function (findErr, findRes) {
//               if (findErr) UserMerchantStatusRefundRollback(findErr)
//               /**
//                * Case 1.
//                * If transactions exits.
//                * Check whether transactions are greater than refunded transaction if found
//                * update due_date = transaction_date (results) + due_period
//                */
//               else if (findRes) {
//                 if (findRes.transaction_date >= transaction_date) {
//                   console.log(' | / Due date update / | Case 1: Updating due_date = transaction_date (results) + due_period')
//                   PaymentSettingsModel.findOne({}, function (findErr, findData) {
//                     if (findErr) UserMerchantStatusRefundRollback(findErr)
//                     else {
//                       updated_due_date = new Date(findRes.transaction_date)
//                       updated_due_date.setDate(updated_due_date.getDate() + findData.due_period)
//                       userDetailsModel.findByIdAndUpdate({
//                         _id: apiReq.body.user_id
//                       }, {
//                           $set: {
//                             due_date: updated_due_date.getTime()
//                           }
//                         }, function (updateErr, UpdateRes) {
//                           if (updateErr) UserMerchantStatusRefundRollback(updateErr)
//                           else callback(null, null)
//                         })
//                     }
//                   })
//                 } else {
//                   console.log(' | / Due date update / | Case 3: Nothing to update')
//                   callback(null, null)
//                 }
//               } else {
//                 /**
//                  * Case 2.
//                  * If transactions not exists update due_date = 0
//                  */
//                 console.log(' | / Due date update / | Case 2: Updating due_date = 0')
//                 userDetailsModel.findByIdAndUpdate({
//                   _id: apiReq.body.user_id
//                 }, {
//                     $set: {
//                       due_date: 0
//                     }
//                   }, function (updateErr, UpdateRes) {
//                     if (updateErr) UserMerchantStatusRefundRollback(updateErr)
//                     else callback(null, null)
//                   })
//               }
//             })
//           },
//           /** 5. Saving refund transaction */
//           /** User's, Merchant's refund is successfull so storing refund transaction */
//           function (callback) {
//             RefundTransactionDetails = new RefundTransactions({
//               transaction_id: apiReq.body.transaction_id,
//               merchant_id: apiReq.body.merchant_id,
//               user_id: apiReq.body.user_id,
//               con_fee: apiReq.body.convenience_percentage,
//               merchant_commission: apiReq.body.commission_percentage,
//               amount: apiReq.body.transaction_amount,
//               trans_amount: apiReq.body.convenience_amount,
//               pending_amt: pending_amt,
//               credit_available: credit_available,
//               credit_due: credit_due,
//               refunded_date: Date.now(),
//               refund_by: 'Merchant'
//             })
//             RefundTransactionDetails.save(function (RefundTransactionSaveErr, RefundTransactionSaved) {
//               if (RefundTransactionSaveErr) callback({
//                 error_name: 'Refund is success. MongoError : Transaction details save failed',
//                 error: RefundTransactionSaveErr
//               })
//               else callback(null, null)
//             })
//           },
//         ], function (err, results) {
//           if (err) {
//             return apiRes.send({
//               status: false,
//               message: err,
//             });
//           } else {
//             //user merchant sms
//             return apiRes.send({
//               status: true,
//               message: 'Refunded',
//             });
//           }
//         })
//       } else {
//         return apiRes.send({
//           error_message: 'Transaction is already refunded',
//           error: err
//         }, null);
//       }
//     } else return apiRes.send({
//       error_message: 'Transaction is already refunded',
//       error: err
//     }, null);

//   })
// }
userService.getCount = function (apiReq, apiRes) {
  userDetailsModel.countDocuments({}, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      // console.log('count::: ', countResult);
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': countResult
      })
    }
  })
}
userService.getPaymentsCount = function (apiReq, apiRes) {
  var findObj = apiReq.body
  userPaymentModel.count(findObj, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      // console.log('count::: ', countResult);
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': countResult
      })
    }
  })
}
userService.getTransactionsCount = function (apiReq, apiRes) {
  var findObj = {}
  var merchantId = apiReq.params.merchantId
  if (isMongoId(merchantId)) {
    findObj = {
      merchant_id: merchantId
    }
  } else {
    findObj = {}
  }
  // console.log('findObject::', merchantId);
  userTransactionsModel.count(findObj, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      // console.log('count::: ', countResult);
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': countResult
      })
    }
  })
}
userService.getMerchantId = function (apiReq, apiRes) {
  var findObj = apiReq.body
  // console.log('findObject::', findObj);
  merchantsModel.findOne(findObj, {
    _id: true
  }, function (findError, merchantObj) {
    if (findError) {
      console.log('Error', findError)
      apiRes.send({
        'status': false,
        'message': findError
      })
    } else {
      // console.log('count::: ', countResult);
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': merchantObj
      })
    }
  })
}
userService.getDetails = function (apiReq, apiRes) {
  var merchantId = apiReq.params.merchantId
  if (isMongoId(merchantId)) {
    merchantsModel.findById({
      _id: merchantId
    }, (errFind, merchantFound) => {
      if (errFind) {
        apiRes.send({
          success: false,
          message: 'Error while fetching merchant details'
        })
      } else {
        apiRes.send({
          success: true,
          message: 'Merchant details fetched successfully',
          merchantFound
        })
      }
    })
  } else {
    apiRes.send({
      success: false,
      message: 'User is Admin'
    })
  }
}
userService.getTransaction = function (apiReq, apiRes) {
  console.log('inside getTransaction')
  var transId = apiReq.params.transactionId != undefined ? apiReq.params.transactionId : null
  if (transId) {
    userTransactionsModel.findById({
      _id: transId
    }, (errFind, transactionFound) => {
      if (errFind) {
        apiRes.send({
          success: false,
          message: 'Error while fetching transaction details'
        })
      } else {
        merchantsModel.findOne({
          _id: transactionFound.merchant_id
        }, (errFind, merchantFound) => {
          if (errFind) {
            apiRes.send({
              success: false,
              message: 'Error while fetching merchant details'
            })
          } else if (!merchantFound) {
            apiRes.send({
              success: false,
              merchantSuccess: false,
              message: 'merchant not found.'
            })
          } else {
            apiRes.send({
              success: true,
              merchantSuccess: true,
              message: 'Details fetched successfully',
              transactionFound,
              merchantFound
            })
          }
        })
      }
    })
  } else {
    apiRes.send({
      success: false,
      message: 'Transaction is required'
    })
  }
}
userService.updateTransaction = function (apiReq, apiRes) {
  var transactionObj = apiReq.body != undefined ? apiReq.body : null
  if (transactionObj) {
    userTransactionsModel.findByIdAndUpdate({
      _id: transactionObj._id
    }, {
      $set: transactionObj
    }, (errUpdate, transactionUpdated) => {
      if (errUpdate) {
        apiRes.send({
          success: false,
          message: 'Error while updating transaction details'
        })
      } else {
        apiRes.send({
          success: true,
          message: 'Transaction details updated Successfully'
        })
      }
    })
  } else {
    apiRes.send({
      success: false,
      message: 'Something went wrong.'
    })
  }
}
userService.getAuthenticateInfo = function (apiReq, apiRes) {
  var mobile = apiReq.body.mobile_number
  authenticateInfoModel.findOne({
    mobile_number: mobile
  }, function (error, Authenticate_info) {
    if (error) {
      apiRes.send({
        success: false,
        message: 'Error while fetching the authenticateInfo'
      })
    } else {
      apiRes.send({
        success: true,
        authenticateInfo: Authenticate_info
      })
    }
  })
}
/* Searching Brand names by suresh */
userService.searchingBrands = function (api_request, api_response) {
  let this_function_name = 'userService.searchingBrands'
  let Error_Message = 'Error while fetching brand names details'
  try {
    console.log('API REQUEST RECEIVED', api_request.body)
    // $regex: new RegExp(api_request.query.full_name, 'ig')
    merchantsModel.search({
      query_string: {
        query: api_request.body.brand_name
      }
    }, function (err, results) {
      if (err) {
        console.log('ERROR', err)
      } else {
        console.log('results at brands search :: ', results)
        console.log('results at brands search :: ', results.hits.hits)
        if (results.hits.hits.length > 0) {
          api_response.send({
            status: true,
            message: 'brand names details found',
            data: results.hits.hits
          })
        } else {
          api_response.send({
            status: true,
            message: 'no data found',
            data: null
          })
        }
      }
    })
  } catch (e) {
    console.log('Exception', e)
  }
}

userService.checkPaymentStatus = function (api_request, api_response) {
  console.log('checking payment status:::::::::::;')
  let this_function_name = 'userService.checkPaymentStatus' // MOJO8917005N49641067
  let Error_Message = 'Error while fetching brand names details'
  try {
    let headers = {
      'X-Api-Key': config.X_Api_Key,
      'X-Auth-Token': config.X_Auth_Token
    }
    let payment_ref_id = api_request.body.payment_ref_Id
    // console.log(payment_ref_id)
    // let payment_ref_id= 'MOJO8917005N49641067';
    /* ----------------------------------getting instamojo details && user details-------------------------------------------------------   */
    request.get(config.InstaMojo_url + payment_ref_id, {
      headers: headers
    }, function (error, response, data) {
      // console.log('checking for instamojo details')
      if (error) {
        console.log('error in getting details from instamojo', error)
        api_response.send({
          success: false,
          message: 'Error while getting details from InstaMOjo.'
        })
      } else if (response.statusCode != 200) {
        // console.log('no data found from instamojo')
        api_response.send({
          success: false,
          message: 'No data found from instaMojo with given payment_reference_id'
        })
      } else if (!error && response.statusCode == 200) {
        let instamojo_details = JSON.parse(data)
        // let instamojo_details = dataobj

        /* --------------------- getting user payment information --------------------------- */
        userPaymentModel.findOne({
          _id: api_request.body._id,
          paid_status: {
            $in: [0, 2]
          }
        }, function (error, user_payment) {
          if (error) {
            console.log('error', error)
            api_response.send({
              success: false,
              message: 'Error while finding user payment with _id and status 0,2'
            })
          } else {
            if (user_payment) {
              let user_payment_details = user_payment
              console.log('user_payment_details----->', user_payment_details)
              console.log('instamojo details-------->', instamojo_details)
              // if(instamojo_details.payment.custom_fields && instamojo_details.payment.custom_fields.transaction_ref_id == user_payment_details.transaction_ref_id){

              // } else if
              let instadate = new Date(instamojo_details.payment.created_at)
              let insta_paid_date = instadate.getTime()
              let paid_status
              let instaMojo_mobile_number
              let mobile_length = instamojo_details.payment.buyer_phone.length
              if (mobile_length > 10) {
                instaMojo_mobile_number = instamojo_details.payment.buyer_phone.substring(3, instamojo_details.payment.buyer_phone.length)
              } else {
                instaMojo_mobile_number = instamojo_details.payment.buyer_phone
              }
              // instamojo_details.payment.payment_id == user_payment_details.payment_ref_id &&
              if (user_payment_details && instamojo_details && instamojo_details.payment && instamojo_details.payment.amount == user_payment_details.amount_paid && instaMojo_mobile_number == user_payment_details.mobile_number) {
                if (instamojo_details.payment && instamojo_details.payment.status == 'Credit') { // instamojo payment succss
                  paid_status = GeneralConfig.paymentStatus.success
                  // console.log('paid_status--->', paid_status)
                } else {
                  paid_status = GeneralConfig.paymentStatus.failure
                }

                let find_filter = {}
                if (instamojo_details.payment && instamojo_details.payment.custom_fields && instamojo_details.payment.custom_fields.transaction_ref_id) {
                  console.log('Adding transaction ref id to from instaMojo find filter ')
                  find_filter.transaction_ref_id = instamojo_details.payment.custom_fields.transaction_ref_id
                } else if (user_payment_details.transaction_ref_id) {
                  console.log('Adding transaction ref id to find filter from user payment')
                  find_filter.transaction_ref_id = user_payment_details.transaction_ref_id
                }
                // find_filter._id = //'5b547e7e87cfff4d8852e55b'
                // find_filter.trasaction_ref_id =user_payment_details.transaction_ref_id
                find_filter.paid_status = {
                  $nin: [1, 5]
                }
                let update_data = {
                  $addToSet: {
                    trans_details: {
                      status: paid_status,
                      date: Date.now()
                    }
                  },
                  $set: {
                    user_email: instamojo_details.payment.buyer_email,
                    mobile_number: instaMojo_mobile_number,
                    payment_ref_id: instamojo_details.payment.payment_id,
                    // transaction_ref_id:instamojo_details.payment.custom_fields.transaction_ref_id,
                    amount_paid: instamojo_details.payment.amount,
                    payment_currency: instamojo_details.payment.currency,
                    paid_status: paid_status,
                    payment_mode: instamojo_details.payment.instrument_type,
                    sub_payment_type: instamojo_details.payment.billing_instrument,
                    payment_remarks: instamojo_details.payment.failure,
                    gateway_response_status: 'success',
                    paid_date: insta_paid_date,
                    // bank_name: instamojo_details.payment,
                    payment_created_at: instamojo_details.payment.created_at
                    // payment_type: instamojo_details.payment
                  }
                }

                // let options = {};
                // options = {
                //   new: true
                // };
                if (paid_status == 2 || paid_status == 3) {
                  console.log('paid status details ' + transaction_ref_id, 'PAID-DETAILS', 'Paid status ' + paid_status)
                }

                userPaymentModel.findOneAndUpdate(find_filter, update_data, {
                  new: true
                }, function (payment_completion_error, payment_completion_data) {
                  console.log('...........', find_filter)
                  if (payment_completion_error) {
                    // Print.error(payment_completion_error + '');
                    // Logs.error(payment_completion_error + '', default_error_code, 'Error while completion of payment.', new Error());

                    console.log('Error while completion of payment', payment_completion_error)

                    api_response.send({
                      success: false,
                      message: 'Error while updating payment'
                    })
                  } else if (!payment_completion_data) {
                    // Print.error('No details found with ' + transaction_ref_id + ' or paid status is 'failed'');
                    // Logs.error('no details found for ' + transaction_ref_id, 'PAID-STATUS-ERROR', 'Error while completion of payment.', new Error());

                    console.log('No details found with or paid status is failed', payment_completion_data)

                    api_response.send({
                      success: false,
                      message: 'No data found in user paymetns with given transa ref ID'
                    })
                  } else {
                    if (paid_status == GeneralConfig.paymentStatus.success) {
                      let return_data = {}
                      return_data = payment_completion_data
                      console.log('updated user payments data!', return_data)
                      // insta_paid_date = instamojo_details.payment.created_at

                      console.log('milliseconds time----->', insta_paid_date)
                      let paid_date_query = {}
                      paid_date_query.paid_date = insta_paid_date
                      let update_Data = {
                        $set: {
                          paid_date: insta_paid_date
                        },
                        $inc: {
                          payments: 1,
                          amount_paid: instamojo_details.payment.amount
                        }
                      }
                      usersDaywisePayments.findOneAndUpdate(paid_date_query, update_Data, function (update_error, updated_data) {
                        if (update_error) {
                          api_response.send({
                            success: false,
                            message: 'error while updating into daywise payments'
                          })
                          console.log('error while updating into daywise payments')
                        } else {
                          console.log('updated successfully', updated_data)
                        }
                      })

                      let pending_transaction_find_query = {}
                      // pending_transaction_find_query.transaction_ref_id = '5b547e7e87cfff4d8852e55b'
                      pending_transaction_find_query.transaction_ref_id = user_payment_details.transaction_ref_id
                      // pending_transaction_find_query.transaction_ref_id = instamojo_details.payment.custom_fields.transaction_ref_id
                      UserPaymentPendingTransactions.findOne(pending_transaction_find_query, function (pending_transaction_error, pending_transaction_data) {
                        if (pending_transaction_error) {
                          // Print.error(pending_transaction_error + '');
                          // Logs.error(pending_transaction_error + '', 'GETTINGTRANSACTIONSERROR', 'Error while getting pending transaction data from payment pending transactions');

                          console.log('Error while getting pending transaction data from payment pending transactions', 'DB-ERROR', 500)

                          api_response.send({
                            success: false,
                            message: 'Error while getting user pending payments.'
                          })
                        }

                        let pending_transaction_ids = (pending_transaction_data && pending_transaction_data.pending_transaction_ids) ? pending_transaction_data.pending_transaction_ids : []
                        console.log('pending transaction ids ------------------------> ', pending_transaction_ids)
                        updateUsertransactions(pending_transaction_ids, function (transaction_status_error, transaction_status_data) {
                          if (transaction_status_error) {
                            console.log('error in updating transactions')

                            api_response.send({
                              success: false,
                              message: 'Error while updating transactions'
                            })
                          } else {
                            console.log('success in updating', transaction_status_data)
                            let user_details_find_query = {}
                            user_details_find_query._id = api_request.body.user_id // need to convert to ObjectId
                            let update_data = {
                              $inc: {
                                credit_available: Number(instamojo_details.payment.amount),
                                credit_due: -instamojo_details.payment.amount
                              }
                            }

                            userDetailsModel.findOne(user_details_find_query, function (user_details_update_error, updated_user_details) {
                              console.log('obj-------------->', update_data)
                              if (user_details_update_error) {
                                console.log('  getting user details is failed!', user_details_update_error)

                                api_response.send({
                                  success: false,
                                  message: 'Error while updating user credit details'
                                })
                              } else if (!updated_user_details) {
                                console.log('no data found!', updated_user_details)
                                api_response.send({
                                  success: false,
                                  message: 'User details not found'
                                })
                              } else {
                                // console.log('user details updated successfully!', updated_user_details,updated_user_details.credit_due)
                                updated_user_details.credit_available = updated_user_details.credit_available + Number(instamojo_details.payment.amount)
                                updated_user_details.credit_due = updated_user_details.credit_due - Number(instamojo_details.payment.amount)
                                if (updated_user_details.credit_due == 0) {
                                  updated_user_details.due_date = 0
                                }
                                console.log('coming to if')
                                updated_user_details.save(function (update_error, user_details) {
                                  if (update_error) {
                                    console.log('error while updating credit detaisl')
                                    api_response.send({
                                      success: false,
                                      message: 'error while updating credit detaisl'
                                    })
                                  } else if (user_details) {
                                    console.log('User credit details successfully updated', user_details)
                                    api_response.send({
                                      success: true,
                                      message: 'Successfully updated payment details'
                                    })
                                  }
                                })
                              }
                            })
                          }
                        })
                      })
                    } else {
                      console.log('not success')

                      api_response.send({
                        success: false,
                        message: 'Your instamojo payment was failed.'
                      })
                    }
                  }
                })
              } else {
                console.log('not matched')

                api_response.send({
                  success: false,
                  message: 'amount or mobile number is not matched'
                })
              }
            } else {
              console.log('no user information not  found!')

              api_response.send({
                success: false,
                message: 'Something went wrong while fetching user payment'
              })
            }
          }
        })
      }
    })
  } catch (e) {
    console.log('error')
    api_response.send({
      success: false,
      message: 'Something went wrong while completing user payment'
    })
  }
}
let updateUsertransactions = function (transaction_ids, callback) {
  let this_function_name = 'instamojoService.updateUsertransactions'

  /* defining logs */
  // let Logs = new ResponseUtils.Logs(this_file_name, this_function_name);
  // let Print = new ResponseUtils.print(this_file_name, this_function_name);

  /* defining default error details */
  let default_error = 'Error while updating transaction status of user'
  let default_error_code = 'TRANSACTIONSTATUSERROR'

  try {
    /* preparing find filter */
    let find_filter = {}
    find_filter = {
      _id: {
        $in: transaction_ids
      }
    }

    /* preparing update data */
    let update_data = {}
    update_data = {
      $set: {
        status: GeneralConfig.paymentStatus.paid
      },
      $addToSet: {
        trans_details: {
          status: GeneralConfig.paymentStatus.paid,
          date: Date.now()
        }
      }
    }

    // /* preparing options */
    // let options = {};
    // options = {
    //   multi: true
    // };

    console.log('update->', update_data)

    userTransactionsModel.updateMany(find_filter, update_data, function (error, data) {
      if (error) {
        // Print.error(error + '');
        // Logs.error(error + '', default_error_code, default_error, new Error());
        console.log('Error while updating transactions', error)
        return callback(error, null)
      }

      return callback(null, data)
    })
  } catch (exception) {
    // Print.exception(exception + '');
    console.log(exception)
    return callback(exception, null)

    // return callback(exception, default_error_code, default_error, null);
  }
}

userService.addpermissions = function (api_request, api_response) {
  // console.log(api_request.body);
  try {
    let new_permission_name = {}
    new_permission_name.permission_type = api_request.body.permission_type
    PermissionTypesModel.find(new_permission_name, function (temps_find_error, temps_data) {
      if (temps_find_error) {
        api_response.send({
          success: false,
          message: 'Something went wrong while adding permission alert.'
        })
      } else {
        // console.log(temps_data)
        if (temps_data.length > 0) {
          console.log('NOT INERTED')
          api_response.send({
            success: false,
            message: 'This permission is already inserted'
          })
        } else {
          let permissions = new PermissionTypesModel()
          permissions.permission_type = api_request.body.permission_type
          permissions.permission_title = api_request.body.permission_type
          permissions.save((error, response) => {
            if (error) {
              api_response.send({
                success: false,
                message: 'Something went wrong while adding Permission added alert.'
              })
            } else {
              console.log('sucessfully inserted data')
              api_response.send({
                success: true,
                message: 'Sucessfully inserted data'
              })
              // Logs.error('Error occurred while saving permissions', err + '', new Error());
            }
          })
        }
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while Add Permissions', 'ERRUSRD', exception + '', new Error())
  }
}
userService.getReginalheads = function (api_request, api_response) {
  try {
    let admin_filter = {
      user_type: {
        $in: ['regional_head', 'accountant', 'super_admin']
      }
    }
    usersModel.find(admin_filter, function (reginal_find_error, reginal_response) {
      if (reginal_find_error) {
        api_response.send({
          success: false,
          message: 'Something went wrong while getting reginal heads deatails.'
        })
        return Response.error(error_message, 'reginalFIND', 500)
      }
      api_response.send({
        success: true,
        message: 'Sucessfully getting  data',
        Permissions: reginal_response
      })
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while getting Permissions', 'ERRUSRD', exception + '', new Error())
  }
}
userService.getpermissions = function (api_request, api_response) {
  // console.log(api_request.body);
  try {
    PermissionTypesModel.find(function (temps_find_error, temps_data) {
      if (temps_find_error) {
        api_response.send({
          success: false,
          message: 'Something went wrong while getting permission alert.'
        })
      } else {
        // console.log(temps_data)
        console.log('sucessfully get data')
        api_response.send({
          success: true,
          message: 'Sucessfully getting  data',
          Permissions: temps_data
        })
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while getting Permissions', 'ERRUSRD', exception + '', new Error())
  }
}

userService.getPermissionsList = function (api_request, api_response) {
  // console.log(api_request.body);
  try {
    UserPermissions.aggregate([{
      $lookup: {
        from: 'permission_types',
        localField: 'permission_details_id',
        foreignField: '_id',
        as: 'permission_types'
      }
    }, {
      $unwind: '$permission_types'
    }], function (temps_find_error, temps_data) {
      if (temps_find_error) {
        api_response.send({
          success: false,
          message: 'Something went wrong while getting User permission list.'
        })
      } else {
        // console.log('temps_data:::>', temps_data)
        console.log('sucessfully get data')
        api_response.send({
          success: true,
          message: 'Sucessfully getting data User permission list',
          data: temps_data
        })
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while getting User Permissions list', 'ERRUSRD', exception + '', new Error())
  }
}

userService.adduserpermissions = function (api_request, api_response) {
  try {
    // console.log(api_request.body);
    let new_permission_name = {}
    new_permission_name.permission_name = api_request.body.permission_name
    UserPermissions.find(new_permission_name, function (temps_find_error, temps_data) {
      if (temps_find_error) {
        api_response.send({
          success: false,
          message: 'Error while adding user permission'
        })
      } else {
        // console.log(temps_data)
        if (temps_data.length > 0) {
          console.log('NOT INERTED')
          api_response.send({
            success: false,
            message: 'This permission is already inserted'
          })
        } else {
          console.log('INERTED')
          let permissions = new UserPermissions()
          permissions.user_types = api_request.body.user_types
          permissions.permission_name = api_request.body.permission_name
          permissions.permission_details_id = mongoose.Types.ObjectId(api_request.body.permission_details_id)
          permissions.permission_description = api_request.body.permission_description
          permissions.save()
            .then((data) => {
              // console.log('sucessfully inserted data');
              api_response.send({
                success: true,
                message: 'Sucessfully inserted data',
                data: data
              })
            })
            .error((err) => {
              console.log(err)
              // Logs.error('Error occurred while saving permissions', err + '', new Error());
              api_response.send({
                success: false,
                message: 'Something went wrong while adding user permission.'
              })
            })
        }
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while Add Permissions', 'ERRUSRD', exception + '', new Error())
  }
}

userService.edituserpermissions = function (api_request, api_response) {
  try {
    let edit_permission_name = {}
    edit_permission_name.permission_name = api_request.body.permission_name
    UserPermissions.findOne(edit_permission_name, function (temps_find_error, temps_data) {
      if (temps_find_error) {

      } else {
        if (temps_data && temps_data._id != api_request.body.permission_id) {
          console.log('NOT INERTED')
          api_response.send({
            success: true,
            message: 'This Permission Name is already Exist'
          })
        } else {
          let permission_id = {}
          permission_id._id = mongoose.Types.ObjectId(api_request.body.permission_id)
          let permission_details = {}
          permission_details.user_types = api_request.body.user_types
          permission_details.permission_name = api_request.body.permission_name
          permission_details.permission_details_id = mongoose.Types.ObjectId(api_request.body.permission_details_id)
          permission_details.permission_description = api_request.body.permission_description
          // console.log('******api_request*********', permission_id)
          UserPermissions.findOneAndUpdate(permission_id, {
            $set: permission_details
          }, {
            new: true
          }, function (user_permission_find_error, user_permission_resp) {
            if (user_permission_find_error) {
              console.log(user_permission_find_error)
              api_response.send({
                success: false,
                message: 'Something went wrong while update user permission.'
              })
            } else {
              api_response.send({
                success: true,
                message: 'Sucessfully inserted data',
                data: user_permission_resp
              })
              // console.log(user_permission_resp);
            }
          })
        }
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while getting Updated Permission data', 'PERMISSIONERR', exception + '', new Error())
  }
}
userService.deleteuserpermissions = function (api_request, api_response) {
  try {
    console.log(api_request.query)
    var queryDetails = {
      _id: (api_request.query.premission_id != undefined && api_request.query.premission_id != null) ? api_request.query.premission_id : 0,
      filter: {
        $set: {
          hidden_status: api_request.query.hidden_status
        }
      }
    }
    UserPermissions.findByIdAndUpdate({
      _id: queryDetails._id
    }, queryDetails.filter, function (status_update_error, status_update) {
      if (status_update_error) {
        error_message = ' Error while updating Domain.'
        api_response.send({
          success: false,
          message: 'Error occurred in  while updating  status'
        })
      }
      if (status_update) {
        console.log('Successfully updated the data')
        api_response.send({
          success: true,
          message: 'Status Update successfully',
          data: status_update
        })
      } else {
        api_response.send({
          success: false,
          message: 'Error occurred in  while updating  hidden_status'
        })
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while getting Updated Permission data', 'PERMISSIONERR', exception + '', new Error())
  }
}
// userService.addadminuser = function (api_request, api_response) {
//   try {
//     let user_id = api_request.body.user_id
//     let parent_id = api_request.body.parent_id
//     let user_data = api_request.body.user_details
//     let email_filter = {
//       email: user_data.email
//     }
//   } catch (exception) {
//     Print.exception(exception)
//     Logs.exception('Error while saving', 'USERSAVE', exception + '', new Error())
//     return Response.error(error_message, 'ISERR', 400)
//   }
// }
userService.addadminuser = function (api_request, api_response) {
  // console.log(' api_request.body::::::::::::', api_request.body)
  try {
    let new_admin_user = {}
    new_admin_user.email = api_request.body.email
    usersModel.find(new_admin_user, function (temps_find_error, temps_data) {
      if (temps_find_error) {
        api_response.send({
          success: false,
          message: 'Something went wrong while adding user.'
        })
      } else {
        // console.log('temps_data::', temps_data)
        if (temps_data.length > 0) {
          console.log('NOT INERTED')
          api_response.send({
            success: false,
            message: 'This user is already inserted'
          })
        } else {
          console.log('READY TO INERT')
          let addadminuser = new usersModel()
          addadminuser.name = api_request.body.name
          addadminuser.email = api_request.body.email
          addadminuser.temp_password = api_request.body.temp_password
          addadminuser.password = new usersModel().generateHash(api_request.body.temp_password)
          addadminuser.mobile = api_request.body.mobile
          addadminuser.user_status = api_request.body.user_status
          addadminuser.user_type = api_request.body.user_type
          addadminuser.parent_id = api_request.body.parent_id
          if (api_request.body.user_type == 'super_admin') {
            addadminuser.permissions = ['all']
            addadminuser.permission_value = 1
          } else {
            addadminuser.permissions = api_request.body.permissions
            addadminuser.permission_value = 0
          }
          console.log('CAME HEAR', addadminuser)
          addadminuser.save((error, response) => {
            if (error) {
              console.log(error)
              api_response.send({
                success: false,
                message: 'Something went wrong while adding admin_user.'
              })
            } else {
              console.log('sucessfully inserted data', response)
              api_response.send({
                success: true,
                message: 'Sucessfully inserted data'
              })
              // Logs.error('Error occurred while saving permissions', err + '', new Error());
            }
          })
        }
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while Add Permissions', 'ERRUSRD', exception + '', new Error())
  }
}

userService.getAdminUsersList = function (api_request, api_response) {
  try {
    // console.log('=====>>>', api_request.user);
    let findObj = {}
    if (api_request.user.user_type == 'regional_head') {
      findObj.parent_id = api_request.user._id
    }
    if (api_request.user.user_type == 'accountant') {
      findObj.parent_id = api_request.user._id
    }
    usersModel.find(findObj, function (temps_find_error, temps_data) {
      if (temps_find_error) {
        api_response.send({
          success: false,
          message: 'Something went wrong while getting User admin users list.'
        })
      } else {
        // console.log(temps_data)
        console.log('sucessfully get data')
        api_response.send({
          success: true,
          message: 'Sucessfully getting data User admin users list',
          data: temps_data
        })
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while getting User admin users list', 'ERRUSRD', exception + '', new Error())
  }
}

userService.EditAdminUsersList = function (api_request, api_response) {
  console.log('#$%^&*()*****************', api_request.query)
  try {
    let update_admin_user = {}
    update_admin_user._id = api_request.query.user_id
    usersModel.find(update_admin_user, function (temps_find_error, temps_data) {
      if (temps_find_error) {
        api_response.send({
          success: false,
          message: 'Something went wrong while getting User admin users list.'
        })
      } else {
        // console.log(temps_data)
        console.log('sucessfully get data')
        api_response.send({
          success: true,
          message: 'Sucessfully getting data User admin users list',
          data: temps_data
        })
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while getting User admin users list', 'ERRUSRD', exception + '', new Error())
  }
}

userService.updateadminuser = function (api_request, api_response) {
  try {
    let update_user_eamil = {}
    update_user_eamil.permission_name = api_request.body.email
    usersModel.findOne(update_user_eamil, function (temps_find_error, temps_data) {
      if (temps_find_error) {
        api_response.send({
          success: false,
          message: 'Something went to worng'
        })
      } else {
        if (temps_data && temps_data._id != api_request.body.permission_id) {
          console.log('NOT INERTED')
          api_response.send({
            success: true,
            message: 'This user email is already Exist'
          })
        } else {
          let user_update_id = {}
          user_update_id._id = mongoose.Types.ObjectId(api_request.body._id)
          let update_details = {}
          update_details.name = api_request.body.name
          update_details.email = api_request.body.email
          update_details.mobile = api_request.body.mobile
          update_details.permission_value = 1
          update_details.temp_password = api_request.body.password
          update_details.password = new usersModel().generateHash(api_request.body.password)
          update_details.user_status = api_request.body.user_status
          update_details.parent_id = api_request.body.parent_id
          update_details.user_type = api_request.body.user_type
          update_details.permissions = api_request.body.permissions
          console.log('******api_request*********', update_details)
          usersModel.findOneAndUpdate(user_update_id, {
            $set: update_details
          }, {
            new: true
          }, function (user_update_error, user_update_resp) {
            if (user_update_error) {
              console.log(user_update_error)
              api_response.send({
                success: false,
                message: 'Something went wrong while update user permission.'
              })
            } else {
              api_response.send({
                success: true,
                message: 'Sucessfully update data',
                data: user_update_resp
              })
              // console.log(user_update_resp);
            }
          })
        }
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while getting Updated Permission data', 'PERMISSIONERR', exception + '', new Error())
  }
}

userService.getuserPermissions = function (api_request, api_response) {
  try {
    let response_data = {}
    response_data.permissions = api_request.user.permissions
    response_data.user_type = api_request.user.user_type
    response_data.user = api_request.user
    api_response.send({
      status: true,
      message: 'Successfully Fetched User Permissions',
      data: response_data
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while Getting Permissions', 'ERRUSRD', exception + '', new Error())
  }
}

userService.insertUserCredits = function (api_request, api_response) {
  try {
    console.log('Body :: ', api_request.body)
    let userCreditAmountModel = new UserCreditAmountModel()

    userCreditAmountModel.brand = api_request.body.brand,
      userCreditAmountModel.creditLimit = api_request.body.creditLimit,
      userCreditAmountModel.osName = api_request.body.osName,
      userCreditAmountModel.status = api_request.body.status
    userCreditAmountModel.save(function (insertion_error, inserted_data) {
      if (insertion_error) {
        api_response.send({
          status: false,
          message: 'error while inserting  in to  usercredits model',
          data: inserted_data
        })
        console.log('error in insertion', insertion_error)
      } else {
        api_response.send({
          status: true,
          message: 'successfully inserted in usercredits model',
          data: inserted_data
        })
        console.log('successfully inserted in model', inserted_data)
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('error while inserting data in to model', 'ERRUSRD', exception + '', new Error())
    console.log('error while inserting data in to model', exception)
  }
}
userService.getUserCredits = function (api_request, api_response) {
  try {
    UserCreditAmountModel.find({}, function (get_error, userCreditInfo) {
      if (get_error) {
        api_response.send({
          status: false,
          message: 'error while fetching details'
        })
        console.log('error while fetching user credit details', get_error)
      } else {
        api_response.send({
          status: true,
          message: 'successfully fetched user credits',
          data: userCreditInfo
        })
        // console.log('user info::',userCreditInfo)
      }
    })
  } catch (err) {
    Print.exception(err)
    Logs.exception('error while getting user credit details', 'ERRUSRD', err + '', new Error())
    console.log('error while getting info', err)
  }
}

userService.EditUserCredits = function (apireq, apiRes) {
  try {
    let find_query = {}
    find_query._id = apireq.body.id
    let UpdateData = {
      $set: {
        brand: apireq.body.brand,
        creditLimit: apireq.body.creditLimit,
        osName: apireq.body.osName,
        status: apireq.body.status
      }
    }
    UserCreditAmountModel.findOneAndUpdate(find_query, UpdateData, function (err_update, update_success) {
      if (err_update) {
        console.log('error while updating in user credits', err_update)
        apiRes.send({
          status: false,
          message: 'error while updating details'
        })
      } else {
        // console.log('success in updation', update_success)
        apiRes.send({
          status: true,
          message: 'successfully updated in  user credits',
          data: update_success
        })
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('error while updating user credit details', 'ERRUSRD', exception + '', new Error())
    // console.log('error while updating user credits', exception)
  }
}

userService.deleteValue = function (api_request, api_response) {
  console.log('coming to delete function')
  try {
    let query = {}
    if (api_request.body._id) {
      query._id = api_request.body._id
    }
    let deleteQuery = {
      $unset: {
        is_predict_value: 1,
        predicted_date: 1
      }
    }
    userDetailsModel.updateMany(query, deleteQuery, function (error, sucess) {
      if (error) {
        api_response.send({
          status: true,
          message: 'error while preparing to recalculate',
          data: sucess
        })
        console.log('error while deleting')
      } else {
        api_response.send({
          status: true,
          message: 'you can recalculate now!',
          data: sucess
        })
        console.log('success', sucess)
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('error while deleting the value', 'ERRUSRD', exception + '', new Error())
  }
}

userService.EditUserDetails = function (api_request, api_response) {
  console.log('request body', api_request.body)
  try {
    let find_query = {}
    find_query._id = api_request.body.id
    let UpdateData = {
      $set: {
        name: api_request.body.name,
        email: api_request.body.email,
        credit_available: (api_request.body.credit_limit - api_request.body.credit_due),
        credit_limit: api_request.body.credit_limit,
        user_status: api_request.body.user_status,
        con_fee: api_request.body.con_fee,
        web_con_fee: api_request.body.web_con_fee,
        sdk_con_fee: api_request.body.sdk_con_fee
      }
    }
    userDetailsModel.findOneAndUpdate(find_query, UpdateData, function (err_update, update_success) {
      if (err_update) {
        console.log('error while updating in user details', err_update)
        api_response.send({
          status: false,
          message: 'error while updating details'
        })
      } else {
        console.log('success in updation', update_success)
        api_response.send({
          status: true,
          message: 'successfully updated in  user details',
          data: update_success
        })
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('error while updating user  details', 'ERRUSRD', exception + '', new Error())
    // console.log('error while updating user details', exception)
  }
}

userService.getUserCounts = function (req, res) {
  async.parallel([
    function (callback) {
      let findobj0 = {}
      findobj0.credit_limit = Number(0)
      userDetailsModel.countDocuments(findobj0, function (mongo_err, mongo_res_0) {
        if (mongo_err) console.log('Error in merchants_count')
        else {
          callback(null, {
            users_count_0: mongo_res_0
          })
        }
      })
    },
    function (callback) {
      let findobj500 = {}
      findobj500.credit_limit = {
        $gt: Number(0),
        $lt: Number(500)

      }
      userDetailsModel.countDocuments(findobj500, function (mongo_err_500, mongo_res_500) {
        if (mongo_err_500) console.log('Error in  getting active merchants_count')
        else {
          callback(null, {
            users_count_500: mongo_res_500
          })
        }
      })
    },
    function (callback) {
      let findobj1000 = {}
      findobj1000.credit_limit = {
        $gte: Number(500),
        $lt: Number(1000)

      }
      userDetailsModel.countDocuments(findobj1000, function (mongo_err_1000, mongo_res_1000) {
        if (mongo_err_1000) console.log('Error in  getting active merchants_count')
        else {
          callback(null, {
            users_count_1000: mongo_res_1000
          })
        }
      })
    },
    function (callback) {
      let findobj1500 = {}
      findobj1500.credit_limit = {
        $gte: Number(1000),
        $lte: Number(1500)

      }
      userDetailsModel.countDocuments(findobj1500, function (mongo_err_1500, mongo_res_1500) {
        if (mongo_err_1500) console.log('Error in  getting active merchants_count')
        else {
          callback(null, {
            users_count_1500: mongo_res_1500
          })
        }
      })
    },
    function (callback) {
      let findobj3000 = {}
      findobj3000.credit_limit = Number(3000)
      userDetailsModel.countDocuments(findobj3000, function (mongo_err_3000, mongo_res_3000) {
        if (mongo_err_3000) console.log('Error in  getting active merchants_count')
        else {
          callback(null, {
            users_count_3000: mongo_res_3000
          })
        }
      })
    },
    function (callback) {
      let findobj5000 = {}
      findobj5000.credit_limit = Number(5000)
      userDetailsModel.countDocuments(findobj5000, function (mongo_err_5000, mongo_res_5000) {
        if (mongo_err_5000) console.log('Error in  getting active merchants_count')
        else {
          callback(null, {
            users_count_5000: mongo_res_5000
          })
        }
      })
    }
  ], function (final_result_error, final_result_counts) {
    if (final_result_error) {
      console.log('error while geeting info', final_result_error)
    } else {
      console.log('final_result_counts--------', final_result_counts)
      res.send({
        final_users_result: final_result_counts
      })
    }
  })
}

userService.getSmsCounts = function (req, res) {
  try {
    async.parallel([
      function (callback) {
        let find_query = {}
        find_query.mid = req.body.mid
        UserSmsProcessedCountsModel.findOne(find_query, function (count_error, total_counts) {
          // console.log('sms processed count --->',count_error,total_counts)
          if (count_error) {
            console.log('eroor while getting smscounts of user!')
            res.send({
              final_smsprocessed_counts: count_error
            })
          } else {
            callback(null, {
              final_smsprocessed_counts: total_counts
            })
            // console.log('total_counts', total_counts)
          }
          // } else {
          //   callback(null, {
          //     final_smsprocessed_counts: []
          //   })
          //   console.log('calling else condition')
          // }
        })
      },
      function (callback) {
        let find_query = {}
        find_query.mid = req.body.mid
        UserSmsSyncCountsModel.findOne(find_query, function (countsync_error, totalsync_counts) {
          // console.log('user smssync count info',countsync_error,totalsync_counts)
          if (countsync_error) {
            console.log('error while getting info', countsync_error)
            res.send({
              final_smssync_counts: countsync_error
            })
          } else {
            // console.log('  getting total sms sync count', totalsync_counts)
            callback(null, {
              final_smssync_counts: totalsync_counts
            })
          }
          // else {
          //   callback(null, {
          //     final_smssync_counts: []
          //   })
          //   console.log('comin to else in total sms sync count')
          // }
        })
      }
    ], function (final_result_error, final_smsresult_counts) {
      // console.log('final result------>',final_result_error,final_smsresult_counts)
      if (final_result_error) {
        console.log('error while geeting info', final_result_error)
        res.send({
          final_sms_counts_error: final_result_error
        })
      } else {
        // console.log('final_sms_counts--------', final_smsresult_counts)
        res.send({
          final_sms_counts: final_smsresult_counts
        })
      }
    })
  } catch (countexception) {
    Print.exception(countexception)
    Logs.exception('error while getting sms counts ', 'ERRUSRD', countexception + '', new Error())
    console.log('exception occured while getting info')
  }
}

userService.PredictedAmountDetails = function (req, res) {
  console.log('body---->', req.body)
  try {
    let findObj = {}
    findObj.user_id = req.body.user_id
    PredictedAmountModel.find(findObj, function (findError, findResult) {
      if (findError) {
        res.send({
          find_error: findError
        })
      }
      console.log('findResult-->', findResult)
      res.send({
        PredictedAmount_details: findResult
      })
    }).sort({
      used_date: -1
    })
  } catch (predictedamountException) {

  }
}

// let find_query = {}
// find_query._id = req.body._id
// UserSmsProcessedCountsModel.find(find_query,function(count_error,total_counts){
//   if(count_error){
//     res.send({
//       message: 'eroor while getting smscounts of user!'
//     })
//     console.log('error while getting sms counts',count_error)
//   }else if (total_counts){
//     res.send({
//       final_sms_counts: total_counts
//     })
//     console.log('sms_counts',total_counts)
//   }else{
//     res.send({
//       message: 'No data found!'
//     })

//   }
// })

userService.getUserPermission = function (api_request, api_response) {
  let this_function_name = 'userService.getUserPermission'
  let this_file_name = 'restapp/services/userService'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_function_name, this_file_name)
  let error_message = 'Error while Getting Permissions'
  try {
    // let user_type = api_request.query;
    UserPermissions.aggregate([{
        $match: {}
      },
      {
        $lookup: {
          from: 'permission_types',
          localField: 'permission_details_id',
          foreignField: '_id',
          as: 'permissions'
        }
      }, {
        $unwind: '$permissions'
      }
    ], function (permission_find_error, permission_details) {
      if (permission_find_error) {
        console.log('error::', permission_find_error)
        Print.error('Error while Getting Permissions')
        // Logs.error('Error while Getting Permissions', 'ERRFNDUS', permission_find_error.message, new Error());
        api_response.send({
          status: false,
          message: error_message
        })
      } else {
        // console.log('##########################################')
        // console.log('permission_details :: ', permission_details);
        let newObj = {}
        // console.log('request user :: ', api_request.user);
        async.forEach(permission_details, function (obj, callback) {
          // console.log('permission_object ::', obj);
          if (api_request.user.permissions.indexOf(obj.permission_name) != -1 || api_request.user.user_type == 'super_admin') {
            if (!obj.user_types) {
              return
            }
            // console.log('object user types :: ', obj.user_types);
            for (var index = 0; index < obj.user_types.length; ++index) {
              let user_type = obj.user_types[index]
              // console.log('user_type :: ', user_type);
              if (!newObj[user_type]) {
                newObj[user_type] = {}
              }
              if (!newObj[user_type][obj.permissions.permission_type]) {
                newObj[user_type][obj.permissions.permission_type] = {}
                newObj[user_type][obj.permissions.permission_type]['permissionDetails'] = obj.permissions
                newObj[user_type][obj.permissions.permission_type]['permissions'] = [] // obj
              }
              newObj[user_type][obj.permissions.permission_type]['permissions'].push(obj)
            }
          }
          // console.log('newObj :: ', newObj);
          callback()
        }, function (done) {
          console.log('done******************************')
          console.log(newObj)
          api_response.send({
            success: true,
            message: 'Sucessfully getting data User permission list',
            data: newObj
          })
        })
        // console.log(newObj)
        // console.log('##########################################')
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while Getting Permissions', 'ERRUSRD', exception + '', new Error())
    api_response.send({
      status: false,
      message: error_message
    })
  }
}

userService.getPaynearLocations = function (apiReq, apiRes) {
  var data = apiReq.body
  let matchObj = {}
  let matchQuery = []
  var count_query = []
  console.log('body::1111111111111111 ', data)

  if (data.from_date && data.to_date) {
    matchQuery.push({
      $match: {
        clicked_on: {
          $gte: data.from_date,
          $lt: data.to_date
        }
      }
    })
    count_query.push({
      $match: {
        clicked_on: {
          $gte: data.from_date,
          $lt: data.to_date
        }
      }
    })
  }
  matchQuery.push({
    $lookup: {
      from: 'user_details',
      localField: 'user_id',
      foreignField: '_id',
      as: 'user_data'
    }
  })
  count_query.push({
    $group: {
      _id: null,
      count: {
        $sum: 1
      }
    }
  })
  matchQuery.push({
    $sort: {
      '_id': -1
    }
  })
  // skip = Number(data.skip, 10)
  // limit = Number(data.limit, 10)
  matchQuery.push({
    $skip: data.skip
  })
  matchQuery.push({
    $limit: data.limit
  })
  // console.log('MATCH OBJ', matchQuery)
  // { $match: matchObj }, { $skip: data.skip }, { $limit: data.limit },
  parallel([
    function (callback) {
      PaynearLocationsModel.aggregate(matchQuery, function (error, result) {
        if (error) {
          //   Logs.error("Error @get partners parallel", "ERRGETPART", error.message, new Error());
          callback(error, null)
        } else {
          console.log('Results  ', result.length);
          callback(null, result)
        }
      })
    },
    function (callback) {
      console.log("===========>>>count_query", count_query)
      PaynearLocationsModel.aggregate(count_query, function (error, Countresult) {
        if (error) {
          //   Logs.error("Error @get partners parallel", "ERRGETPART", error.message, new Error());
          callback(error, null)
        } else {
          console.log('CountResults  ', Countresult);
          callback(null, Countresult)
        }
      })
    }
  ], function (error, results) {
    if (error) {
      apiRes.send({
        status: false,
        message: 'ERROR While Getting Pay Near Locations Details'
      })
    }
    let response_data = {}
    if (results && results.length > 0) {
      response_data.total_records = results[0]
      response_data.Countresult = results[1]
      // console.log('Results count', response_data)
      apiRes.send({
        status: true,
        message: 'Get Pay Near Locations Details successfully',
        data: response_data
      })
    }
  })
}

userService.setTarget = function (api_request, api_response) {
  try {
    let setTarget = new SalesTeamTarget()
    setTarget.targeted_to = mongoose.Types.ObjectId(api_request.body.settarget._id)
    setTarget.targeted_by = mongoose.Types.ObjectId(api_request.user._id)
    setTarget.target_date_from = api_request.body.dateobj.target_Fromdate
    setTarget.target_date_to = api_request.body.dateobj.target_Todate
    setTarget.target_number = api_request.body.settarget.target_no
    setTarget.save()
      .then((data) => {
        api_response.send({
          success: true,
          message: 'Sucessfully inserted data',
          data: data
        })
      })
      .error((err) => {
        console.log(err)
        api_response.send({
          success: false,
          message: 'Something went wrong while adding user permission.'
        })
      })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while Add Permissions', 'ERRUSRD', exception + '', new Error())
  }
}
userService.getTargetData = function (api_request, api_response) {
  let query = []
  var countQuery = []
  let skip, limit
  try {
    console.log('************************', api_request.query)
    query.push({
        $match: {
          'targeted_to': mongoose.Types.ObjectId(api_request.query.user_id)
        }
      }),
      countQuery = query.concat()
    countQuery.push({
      $group: {
        _id: null,
        totalCount: {
          $sum: 1
        }
      }

    })
    if (api_request.query.skip && api_request.query.limit) {
      skip = Number(api_request.query.skip, 10)
      limit = Number(api_request.query.limit, 10)
      query.push({
        $skip: skip
      })
      query.push({
        $limit: limit
      })
    }
    query.push({
      $sort: {
        '_id': -1
      }
    })
    SalesTeamTarget.aggregate(countQuery, function (countError, countData) {
      if (countError) {
        console.log('ERROR WHILE GETTING COUNTS', countError)
      } else {
        if (countData) {
          console.log(countData)
          SalesTeamTarget.aggregate(query, function (err, results) {
            if (err) {
              console.log('ERROR WHILE GETTING DATA', err)
            } else {
              if (results && countData.length != 0) {
                let responseData = {}
                responseData.collectionSize = countData[0].totalCount
                responseData.results = results
                // console.log('DATAAAAAAAAAAAAAAAAAAAAA', responseData)
                api_response.send({
                  status: true,
                  message: 'Get Target Details successfully',
                  data: responseData
                })
              } else {
                api_response.send({
                  status: false,
                  message: 'No Data Found...!'
                })
              }
            }
          })
        }
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while Add Permissions', 'ERRUSRD', exception + '', new Error())
  }
}


userService.addOffer = function (apiReq, apiRes) {
  console.log('version ::::::::::::::', apiReq.body)

  if (apiReq.body.offer_id !== '0') {
    merchantsModel.findOne({
      _id: apiReq.body.merchant_id
    }, function (mongo_err, mongo_res) {
      if (mongo_err) {
        apiRes.send({
          status: false,
          message: 'Error while finding merchant'
        })
      } else {
        let reqObj = {
          merchant_id: apiReq.body.merchant_id,
          offers_status: apiReq.body.status,
          brand_name: apiReq.body.brand_name,
          merchant_logo: mongo_res.logo_url,
          expiry_date_to: apiReq.body.toDate,
          expiry_date_from: apiReq.body.fromDate,
          offer_details: apiReq.body.offer_percentage,
          address: mongo_res.company_address.locality + ',' + mongo_res.company_address.city,
          brand: apiReq.body.brand
        }
        console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!', reqObj)
        // console.log('in else Address', mongo_res.company_address.locality, mongo_res.company_address.city)
        mobileOffersModel.findByIdAndUpdate({
          _id: apiReq.body.offer_id
        }, {
          $set: reqObj
        }, function (updateErr, updateResult) {
          if (updateErr) {
            console.log('Error while fetching allversions data', err)
            apiRes.send({
              status: false,
              error: err
            })
          } else {
            apiRes.send({
              status: true,
              data: updateResult
            })
          }
        })
      }
    })
  } else {
    console.log('add ::::::::::::::', apiReq.body)
    merchantsModel.findOne({
      _id: apiReq.body.merchant_id
    }, function (mongo_err, mongo_res) {
      if (mongo_err) {
        apiRes.send({
          status: false,
          message: 'Error while finding merchant'
        })
      } else {
        let reqObj = {
          merchant_id: apiReq.body.merchant_id,
          offers_status: apiReq.body.status,
          brand_name: apiReq.body.brand_name,
          merchant_logo: mongo_res.logo_url,
          expiry_date_to: apiReq.body.toDate,
          expiry_date_from: apiReq.body.fromDate,
          offer_details: apiReq.body.offer_percentage,
          address: mongo_res.company_address.locality + ',' + mongo_res.company_address.city,
          brand: apiReq.body.brand
        }
        console.log(reqObj)
        // console.log('in else Address', mongo_res.company_address.locality, mongo_res.company_address.city)
        var newOffer = new mobileOffersModel(reqObj)
        newOffer.save(function (updateErr, updateResult) {
          if (updateErr) {
            console.log('Error while fetching allversions data', err)
            apiRes.send({
              status: false,
              error: err
            })
          } else {
            apiRes.send({
              status: true,
              data: updateResult
            })
          }
        })
      }
    })
  }
}

userService.editOffer = function (apiReq, apiRes) {
  console.log(apiReq.body._id)
  mobileOffersModel.findOne({
    _id: apiReq.body._id
  }, function (errUpdate, alertUpdated) {
    if (errUpdate) {
      apiRes.send({
        success: false,
        message: 'Something went wrong while updating push alert.'
      })
    } else {
      console.log('edut iff', alertUpdated)
      apiRes.send({
        success: true,
        data: alertUpdated
      })
    }
  })
}


let checkDateLessThanToDate = function (from_date, to_date) {
  console.log('check from -> ', from_date + 'to -> ' + to_date)
  if (from_date <= to_date) {
    return true
  } else {
    return false
  }
}

userService.getDueAmountUsers = function (api_request, api_response) {
  // let skip = apiReq.query.skip;
  // let limit = apiReq.query.limit;
  console.log(api_request.query)
  var count_query = []
  let query = []
  try {
    if (api_request.query.from_date && api_request.query.to_date) {
      let from_date = new Date(api_request.query.from_date).getTime()
      let to_date = new Date(api_request.query.to_date).getTime() + 86399999
      if (checkDateLessThanToDate(from_date, to_date)) {
        query.push({
          $match: {
            'due_date': {
              $gte: from_date,
              $lte: to_date
            }
          }
        })
        count_query.push({
          $match: {
            'due_date': {
              $gte: from_date,
              $lte: to_date
            }
          }
        })
      } else {
        // Print.error("Dates out of range 1");
        console.log('DATE OUT OF RANGE')
        // return Response.error("Dates out of range 1", "BadDate", 400)
      }
    }
    query.push({
      $match: {
        'credit_due': {
          $gte: 1
        }
      }
    })
    count_query.push({
      $match: {
        'credit_due': {
          $gte: 1
        }
      }
    })
    if (api_request.query.mobile_number) {
      query.push({
        $match: {
          'mobile_number': api_request.query.mobile_number
        }
      })
      count_query.push({
        $match: {
          'mobile_number': api_request.query.mobile_number
        }
      })
    }
    count_query.push({
      $group: {
        _id: null,
        count: {
          $sum: 1
        }
      }
    })
    // query.push({
    //   $sort: {
    //     'due_date': -1
    //   }
    // })

    if (api_request.query.skip && api_request.query.limit) {
      let skip = Number(api_request.query.skip, 10)
      let limit = Number(api_request.query.limit, 10)
      query.push({
        $skip: skip
      })
      query.push({
        $limit: limit
      })
    }
    parallel([
        function (callback) {
          userDetailsModel.aggregate(count_query, function (error_finding_count, group) {
            if (error_finding_count) {
              //   Logs.error("Error @get partners count parallel", "ERRGETPART", error_finding_count2.message, new Error());
              callback(error_finding_count, null)
            } else {
              if (group && group[0] && group[0].count) {
                callback(null, group[0].count)
              } else callback(null, 0)
            }
          })
        },
        function (callback) {
          userDetailsModel.aggregate(query, function (error_in_dueCount, result_dueCount) {
            if (error_in_dueCount) {
              //   Logs.error("Error @get partners parallel", "ERRGETPART", error_in_dueCount.message, new Error());
              callback(error_in_dueCount, null)
            } else {
              callback(null, result_dueCount)
              // console.log('Results  ', result_dueCount);
            }
          })
        },
      ],
      function (error, results) {
        if (error) {
          api_response.send({
            status: false,
            message: 'ERROR While Getting DUE-USERS COUNT Details'
          })
        }
        let return_data = {}
        if (results && results.length > 0) {
          return_data.total_records = results[0]
          return_data.dueCount_details = results[1]
          // console.log('Results count', return_data)
          api_response.send({
            status: true,
            message: 'Get DUE-USERS COUNT Details successfully',
            data: return_data
          })
        }
      })
  } catch (exception) {
    Print.exception(exception)
    api_response.send({
      status: false,
      message: 'ERROR While Getting DUE-USERS COUNT Details::>'
    })
  }
}

userService.contactedUser = function (api_request, api_response) {
  try {
    let contacted = new ContactedUsersModel()
    contacted.user_id = api_request.body._id
    contacted.contacted_by_id = api_request.user._id
    contacted.response = api_request.body.comment
    contacted.save((error, response) => {
      if (error) {
        api_response.send({
          success: false,
          message: 'Something went wrong while adding comment .'
        })
      } else {
        let date = Date.now()
        userDetailsModel.findOneAndUpdate({
          _id: api_request.body._id
        }, {
          $set: {
            last_contacted_date: date
          },
        }, {
          upsert: true,
        }, function (updateErr, UpdateRes) {
          if (updateErr) {
            console.log('Error while updating userTransactions')
            apiRes.send({
              error_message: 'Error while updating userTransactions',
              error: updateErr
            })
          } else {
            console.log('<<<<===', UpdateRes)
            api_response.send({
              success: true,
              message: 'Sucessfully inserted data'
            })
          }
        })
      }
    })
  } catch (exception) {
    Print.exception(exception)
    api_response.send({
      status: false,
      message: 'ERROR While Saveing Comment'
    })
  }
}
userService.getCommentsData = function (api_request, api_response) {
  let query = []
  var countQuery = []
  let skip, limit
  try {
    console.log("************************", api_request.query)
    query.push({
        $match: {
          'user_id': mongoose.Types.ObjectId(api_request.query.user_id)
        }
      }),
      countQuery = query.concat()
    query.push({
      $lookup: {
        from: 'admin_users',
        localField: 'contacted_by_id',
        foreignField: '_id',
        as: 'admin_user_data'
      }
    })
    query.push({
      $unwind: "$admin_user_data"
    })
    countQuery.push({
      $group: {
        _id: null,
        totalCount: {
          $sum: 1
        }
      }
    })
    query.push({
      $sort: {
        '_id': -1
      }
    })
    if (api_request.query.skip && api_request.query.limit) {
      let skip = Number(api_request.query.skip, 10)
      let limit = Number(api_request.query.limit, 10)
      query.push({
        $skip: skip
      })
      query.push({
        $limit: limit
      })
    }
    ContactedUsersModel.aggregate(countQuery, function (countError, countData) {
      if (countError) {
        console.log('ERROR WHILE GETTING COUNTS', countError)
      } else {
        if (countData) {
          console.log(countData)
          ContactedUsersModel.aggregate(query, function (err, results) {
            if (err) {
              console.log('ERROR WHILE GETTING DATA', err)
            } else {
              if (results && countData.length != 0) {
                let responseData = {}
                responseData.collectionSize = countData[0].totalCount
                responseData.results = results
                api_response.send({
                  status: true,
                  message: 'Get Target Details successfully',
                  data: responseData
                })
              } else {
                api_response.send({
                  status: false,
                  message: 'No Data Found...!',
                })
              }
            }
          })
        }
      }
    })
  } catch (exception) {
    Print.exception(exception)
    Logs.exception('Error while Add Permissions', 'ERRUSRD', exception + '', new Error())
  }
}

userService.updateUserDetailsInLocations = function(locations, index, callback) {
  if (locations.length === index) {
    callback(null, true)
  } else {
    var locationObj = locations[index]
    userDetailsModel.findOne({ _id: locationObj.user_id }, (errFind, userDetails) => {
      if (errFind) {
        console.error('Error while finding useer details with Id of:: ', locationObj.user_id)
        userService.updateUserDetailsInLocations(locations, index + 1, callback)
      } else {
        if(userDetails && userDetails.name && userDetails.email && userDetails.mobile_number) {
          var update_data = {
            $set: {
              name: userDetails.name ? userDetails.name : '',
              email: userDetails.email ? userDetails.email : '',
              mobile_number: userDetails.mobile_number ? userDetails.mobile_number : ''
            }
          }
          paynearClickLocationsModel.updateOne({ _id: locationObj._id }, update_data, (errUpdate, updateDetails) => {
            if (errUpdate) {
              console.error('Error while updating details in pay near locations:: ', locationObj._id)
              userService.updateUserDetailsInLocations(locations, index + 1, callback)
            } else {
              console.log('Successfully update locations in pay near locations')
              userService.updateUserDetailsInLocations(locations, index + 1, callback)
            }
          })
        } else {
          console.log('No user details found')
          userService.updateUserDetailsInLocations(locations, index + 1, callback)
        }
      }
    })
  }
}

userService.updateUserDetailsInPayNearLocations = function(skip, limit) {
  paynearClickLocationsModel.find({})
  .skip(skip)
  .limit(limit)
  .exec((errFind, locations) => {
    if (errFind) {
      console.error('Erorr while finding pay near locations', errFind)
      userService.updateUserDetailsInPayNearLocations(skip+limit, limit)
    } else if (locations.length <= 0) {
      console.log('&&&&&&&&&&&& All Locations completed &&&&&&&&&&&&')
    } else {
      userService.updateUserDetailsInLocations(locations, 0, (error, completed) => {
        if (error) {
          console.error('Error while updating user details in pay near locations')
          userService.updateUserDetailsInPayNearLocations(skip+limit, limit)
        } else {
          console.log('update user details in pay near locations with :: ', skip, limit)
          userService.updateUserDetailsInPayNearLocations(skip+limit, limit)
        }
      })
    }
  })
}

userService.updateUserDetailsWithDeviceVersion = function(users, index, callback) {
  if (users.length === index) {
    callback(null, true)
  } else {
    var userObj = users[index]
    userDevicesModel.findOne({ mid: userObj.mid }, (errFind, userDevice) => {
      if (errFind) {
        console.error('Error while finding useer devices with Id of:: ', userObj.mid)
        userService.updateUserDetailsWithDeviceVersion(users, index + 1, callback)
      } else {
        if (userDevice && userDevice.os_version) {
          var updateData = {
            $set: {
              os_version: userDevice.os_version ? userDevice.os_version : '',
            }
          }
          userDetailsModel.updateOne({ _id: userObj._id }, updateData, (errUpdate, updateDetails) => {
            if (errUpdate) {
              console.error('Error while updating os version in user details:: ', userObj._id)
              userService.updateUserDetailsWithDeviceVersion(users, index + 1, callback)
            } else {
              console.log('Successfully update locations in pay near locations')
              userService.updateUserDetailsWithDeviceVersion(users, index + 1, callback)
            }
          })
        } else {
          console.log('No user device found')
          userService.updateUserDetailsWithDeviceVersion(locations, index + 1, callback)
        }
      }
    })
  }
}

userService.updateDeviceVersionInUserDetails = function(skip, limit) {
  userDetailsModel.find({})
  .skip(skip)
  .limit(limit)
  .exec((errFind, users) => {
    if (errFind) {
      console.error('Erorr while finding user details', errFind)
      userService.updateDeviceVersionInUserDetails(skip+limit, limit)
    } else if (users.length <= 0) {
      console.log('&&&&&&&&&&&& All Users completed &&&&&&&&&&&&')
    } else {
      userService.updateUserDetailsWithDeviceVersion(users, 0, (error, completed) => {
        if (error) {
          console.error('Error while updating user details with device version')
          userService.updateDeviceVersionInUserDetails(skip+limit, limit)
        } else {
          console.log('update user details with device version with :: ', skip, limit)
          userService.updateDeviceVersionInUserDetails(skip+limit, limit)
        }
      })
    }
  })
}

userService.getCountOfUsers = (apiRequest, apiResponse) => {
  console.log('api Req body at getcount of users :: ', apiRequest.body)

  var findFilter = {}
  var limits = {}
  if (apiRequest.body && apiRequest.body.mobile_number) {
    findFilter['mobile_number'] = apiRequest.body.mobile_number
  }

  if(apiRequest.body && apiRequest.body.mid) {
    findFilter['mid'] = apiRequest.body.mid
  }

  if (apiRequest.body && apiRequest.body.credit_limit_from !== null && apiRequest.body.credit_limit_to !== null && (apiRequest.body.credit_limit_from || apiRequest.body.credit_limit_from === 0) && (apiRequest.body.credit_limit_to || apiRequest.body.credit_limit_to === 0)) {
    findFilter['credit_limit'] = {
      $gte: Number(apiRequest.body.credit_limit_from),
      $lte: Number(apiRequest.body.credit_limit_to)
    }
  }

  if(apiRequest.body && apiRequest.body.app_version && apiRequest.body.app_version !== 'ALL') {
    findFilter['app_version'] = apiRequest.body.app_version
  }

  if (apiRequest.body.type !== undefined && apiRequest.body.type != null) {
    if (apiRequest.body.fromDate && apiRequest.body.toDate) {
      findFilter[apiRequest.body.type] = {
        $gte: Number(apiRequest.body.fromDate),
        $lte: Number(apiRequest.body.toDate)
      }
    } else if (apiRequest.body.fromDate) {
      findFilter[apiRequest.body.type] = {
        $gte: Number(apiRequest.body.fromDate)
      }
    } else if (apiRequest.body.toDate) {
      findFilter[apiRequest.body.type] = {
        $lte: Number(apiRequest.body.toDate)
      }
    }
  }
  
  userDetailsModel.count(findFilter, (errFind, users) => {
    if (errFind) {
      console.log('Error while finding count of users')
      apiResponse.send({ success: false, message: 'Error while finding count of users', users: 0 })
    } else {
      console.log('users count:: ', users)
      apiResponse.send({ success: true, message: 'users count found', users })
    }
  })
}

userService.fetchUsers = (apiRequest, apiResponse) => {
  console.log('api Req body at fetch users :: ', apiRequest.body)
  var findFilter = []
  var matchObj = {}
  if (apiRequest.body && apiRequest.body.mobile_number) {
    matchObj['mobile_number'] = apiRequest.body.mobile_number
  }

  if(apiRequest.body && apiRequest.body.mid) {
    matchObj['mid'] = apiRequest.body.mid
  }

  if (apiRequest.body && apiRequest.body.credit_limit_from !== null && apiRequest.body.credit_limit_to !== null && (apiRequest.body.credit_limit_from || apiRequest.body.credit_limit_from === 0) && (apiRequest.body.credit_limit_to || apiRequest.body.credit_limit_to === 0)) {
    matchObj['credit_limit'] = {
      $gte: Number(apiRequest.body.credit_limit_from),
      $lte: Number(apiRequest.body.credit_limit_to)
    }
  }

  if(apiRequest.body && apiRequest.body.app_version && apiRequest.body.app_version !== 'ALL') {
    matchObj['app_version'] = apiRequest.body.app_version
  }

  if (apiRequest.body.type !== undefined && apiRequest.body.type != null) {
    if (apiRequest.body.fromDate && apiRequest.body.toDate) {
      matchObj[apiRequest.body.type] = {
        $gte: Number(apiRequest.body.fromDate),
        $lte: Number(apiRequest.body.toDate)
      }
    } else if (apiRequest.body.fromDate) {
      matchObj[apiRequest.body.type] = {
        $gte: Number(apiRequest.body.fromDate)
      }
    } else if (apiRequest.body.toDate) {
      matchObj[apiRequest.body.type] = {
        $lte: Number(apiRequest.body.toDate)
      }
    }
  }

  findFilter.push({
    $match: matchObj
  })

  findFilter.push({
    $sort: {
      _id: -1
    }
  })
  findFilter.push({
    $skip: apiRequest.body.skip
  })
  findFilter.push({
    $limit: apiRequest.body.limit
  })
  findFilter.push({
    $lookup: {
      from: 'predicted_amounts',
      localField: '_id',
      foreignField: 'user_id',
      as: 'PredictedAmounts'
    }
  })

  userDetailsModel.aggregate(findFilter, (errFind, users) => {
    if (errFind) {
      console.log('Error while finding count of users')
      apiResponse.send({ success: false, message: 'Error while fetching users', users: [] })
    } else if (users.length > 0){
      console.log('fetch users count:: ', users.length)
      apiResponse.send({ success: true, message: 'successfully fetched the users', users })
    } else {
      console.log('fetch users count:: ', users.length)
      apiResponse.send({ success: true, message: 'No records found', users: [] })
    }
  })
}