var merchantClaimsModel = require('../models/claimRequestsModel')
var async = require('async')
var claimRequestService = module.exports
claimRequestService.getClaims = function (apiReq, apiRes) {
//   console.log("we are in view claims", apiReq.body);
  let findObj = {}
  if (apiReq.body.merchant_id != undefined && apiReq.body.merchant_id != null) {
    findObj = {
      'merchant_id': merchant_id
    }
  }
  merchantClaimsModel.find(findObj, {}, {
    skip: apiReq.body.skip,
    limit: apiReq.body.limit
  }, function (getClaimsErr, getClaimsResult) {
    if (getClaimsErr) {
      console.log('there is an error while getting claims details', getClaimsErr)
      apiRes.send({
        status: false,
        message: 'error while getting claims details'
      })
    } else if (getClaimsResult.length > 0) {
    //   console.log("claims result",getClaimsResult);
      apiRes.send({
        status: true,
        message: 'claims details found',
        'data': getClaimsResult
      })
    } else {
      apiRes.send({
        status: false,
        message: 'claims details not found'
      })
    }
  }).sort({
    request_date: -1
  })
}

claimRequestService.getClaimsCount = function (apiReq, apiRes) {
//   console.log("we are in claim requests count", apiReq.body);
  var findObj = apiReq.body

  merchantClaimsModel.countDocuments(findObj, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      console.log('count::: ', countResult)
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': countResult
      })
    }
  })
}
claimRequestService.viewClaimsSearch = function (apiReq, apiRes) {
  console.log('searching view claims')
  console.log(apiReq.body)
  let findObj = {}
  // var currDate = Date.now();
  if (apiReq.body.merchant_id != undefined && apiReq.body.merchant_id != null) {
    findObj['merchant_id'] = String(apiReq.body.merchant_id)
  }
  // findObj.paid_date=apiReq.body.paid_date;
  if (apiReq.body.type != undefined && apiReq.body.type != null) {
    findObj[apiReq.body.type] = {
      $gte: Number(apiReq.body.fromDate),
      $lt: Number(apiReq.body.toDate)
    }
  } else {
    findObj = apiReq.body
    // if (!apiReq.body.merchant_id) {
    //   delete findObj['merchant_id'];
    // } else {
    //   findObj['merchant_id'] = String(apiReq.body.merchant_id);
    // }
    // limitObj = apiReq.body[1];
  }
  merchantClaimsModel.find(findObj, {}, function (claimRequestErr, getClaimsResult) {
    if (claimRequestErr) {
      apiRes.send({
        'status': false,
        'message': claimRequestErr
      })
    } else if (getClaimsResult.length > 0) {
      // console.log("getClaimsResult",getClaimsResult);
      apiRes.send({
        status: true,
        message: 'transaction details found',
        data: getClaimsResult
      })
    } else {
      // console.log("getClaimsResult",getClaimsResult);

      apiRes.send({
        'status': false,
        'message': 'details not found'
      })
    }
  }).sort({
    request_date: -1
  })
}
