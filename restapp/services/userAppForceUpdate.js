const userAppForceUpdateModel = require('../models/UserAppForceUpdateModel')
userAppForceUpdateService = module.exports

userAppForceUpdateService.getAllVersions = function (apiReq, apiRes) {
  userAppForceUpdateModel.find({}, function (err, result) {
    if (err) {
      console.log('Error while fetching allversions data', err)
      apiRes.send({
        status: false,
        error: err
      })
    } else {
      apiRes.send({
        status: true,
        data: result
      })
    }
  })
}

userAppForceUpdateService.addVersion = function (apiReq, apiRes) {
  console.log('version ::::::::::::::', apiReq.body)
  userAppForceUpdateModel.findOneAndUpdate({ version: apiReq.body.version }, {
    $set: { force_update: apiReq.body.force_update,
      mobile_type: apiReq.body.mobile_type
    } }, { upsert: true }, function (updateErr, updateResult) {
    if (updateErr) {
      console.log('Error while fetching allversions data', err)
      apiRes.send({
        status: false,
        error: err
      })
    } else {
      apiRes.send({
        status: true,
        data: updateResult
      })
    }
  })
}
