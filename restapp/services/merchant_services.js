var ObjectId = require('mongodb').ObjectID
const AppConfig = require('config')
const replaceAll = require('replaceall')
const req = require('request')
const {
  isEmail
} = require('validator')
const ShortId = require('shortid')
const crypto = require('crypto')
const MerchantModel = require('../models/merchantModel')
const AdminUsersModel = require('../models/usersModel')
const BanksModel = require('../models/BanksModel')
const BankDetailsModel = require('../models/BankDetailsModel')
const MerchantCommissionModel = require('../models/merchantComissionModel')
const async = require('async')
const fs = require('fs')
// const AppConfig = require('config');
const FileUploadUtils = require('./../utils/FileUtils')
const passwordHash = require('password-hash')
const merchantServices = module.exports
merchantServices.registerMerchant = function (apiReq, apiRes) {
  console.log("**********************************************************")
  console.log(apiReq.user)
  console.log("**********************************************************")
  var date = new Date()
  var register_date = date / 1
  var regMerchantObj = apiReq.body
  // console.log('regMerchantObj : ', regMerchantObj);

  var merchant = {}
  if (!regMerchantObj.name || !regMerchantObj.name.length > 3) {
    apiRes.send({
      success: false,
      message: 'Name required and it should be greater than 4 characters.',
      merchant
    })
  } else if (!regMerchantObj.email) {
    apiRes.send({
      success: false,
      message: 'Email required and it should be in correct form.',
      merchant
    })
  } else if (!regMerchantObj.mobile) {
    apiRes.send({
      success: false,
      message: 'Mobile number required.',
      merchant
    })
  } else if (!regMerchantObj.brand_name) {
    apiRes.send({
      success: false,
      message: 'Merchant company/brand name is required.',
      merchant
    })
  } else if (!regMerchantObj.token_validity) {
    apiRes.send({
      success: false,
      message: 'Token Validity is required.',
      merchant
    })
  } else if (!regMerchantObj.password) {
    apiRes.send({
      success: false,
      message: 'Password is required.',
      merchant
    })
  } else {
    var hashedPassword = passwordHash.generate(regMerchantObj.password)
    // console.log("Encrypted :: ", hasedPassword);
    var web_access_token = crypto.randomBytes(10).toString('hex')
    var web_secret_key = crypto.randomBytes(10).toString('hex')
    var open_time = ''
    var close_time = ''
    var location = {}
    if (regMerchantObj.open_time) {
      open_time = regMerchantObj.open_time.hour + ':' + regMerchantObj.open_time.minute + ':' + regMerchantObj.open_time.second
    }
    if (regMerchantObj.close_time) {
      close_time = regMerchantObj.close_time.hour + ':' + regMerchantObj.close_time.minute + ':' + regMerchantObj.close_time.second
    }
    if (regMerchantObj && regMerchantObj.web_domains) {
      var web_domains_array = regMerchantObj.web_domains.trim().split(',')
    }

    if (regMerchantObj && regMerchantObj.company_address) {
      location['type'] = 'Point'
      var coordinates = []
      coordinates.push(regMerchantObj.company_address.longitude ? regMerchantObj.company_address.longitude : '')
      coordinates.push(regMerchantObj.company_address.latitude ? regMerchantObj.company_address.latitude : '')
      location['coordinates'] = coordinates
    }
    var bank_details = 0
    var pan_details = 0
    if (regMerchantObj.bank_name && regMerchantObj.account_no && regMerchantObj.name_on_bank && regMerchantObj.ifsc_code) {
      bank_details = 1
    }
    if (regMerchantObj.pan_number && regMerchantObj.pan_name) {
      pan_details = 1
    }
    if (apiReq.user.user_type == 'sales_team') {
      created_by = apiReq.user._id
      created_by_parent = apiReq.user.parent_id
    }
    if (apiReq.user.user_type == 'regional_head') {
      created_by = apiReq.user._id
      created_by_parent = apiReq.user._id
    }
    if (apiReq.user.user_type == 'super_admin') {
      created_by = apiReq.user._id
      created_by_parent = apiReq.user._id
    }
    var newMerchant = new MerchantModel({
      created_by: created_by,
      created_by_parent: created_by_parent,
      name: regMerchantObj.name,
      email: regMerchantObj.email,
      mobile: regMerchantObj.mobile,
      bank_name: regMerchantObj.bank_name,
      branch: regMerchantObj.branch,
      name_on_bank: regMerchantObj.name_on_bank,
      account_no: regMerchantObj.account_no,
      ifsc_code: regMerchantObj.ifsc_code,
      pan_name: regMerchantObj.pan_name,
      pan_number: regMerchantObj.pan_number,
      display_status: regMerchantObj.display_status,
      merchant_category: regMerchantObj.merchant_category,
      brand_name: regMerchantObj.brand_name,
      token_validity: regMerchantObj.token_validity,
      tin: regMerchantObj.tin,
      tid: regMerchantObj.tid,
      bank_details: bank_details,
      pan_details: pan_details,
      password: hashedPassword,
      register_date: register_date,
      merchant_status: 0,
      open_time: open_time,
      close_time: close_time,
      formatted_address: regMerchantObj.formatted_address,
      company_address: regMerchantObj.company_address,
      location: location,
      secret_key: ShortId.generate(),
      web_access_token: web_access_token,
      web_secret_key: web_secret_key,
      web_domains: web_domains_array,
      is_profile_verified: true,
      pay_cycle_mode: regMerchantObj.pay_cycle_mode
    })
    MerchantModel.findOne({
      mobile: regMerchantObj.mobile
    }, (errFind, foundMerchant) => {
      if (errFind) {
        console.log('Error while finding merchant')
        apiRes.send({
          success: false,
          message: 'Error while finding existing merchant.'
        })
      } else if (foundMerchant) {
        apiRes.send({
          success: false,
          message: 'Merchant already exist. Try again with another mobile number.'
        })
      } else {
        AdminUsersModel.findOne({
          email: regMerchantObj.admin_email
        }, (errFindAdmin, admin) => {
          if (errFindAdmin) {
            // console.log("Error while finding admin id with email");
            apiRes.send({
              success: false,
              message: 'Error while finding admin id with email.'
            })
          } else if (!admin) {
            // console.log("No admin existed");
            apiRes.send({
              success: false,
              message: 'Admin not existed.'
            })
          } else {
            newMerchant.save(function (errMerchant, merchant) {
              if (errMerchant) {
                apiRes.send({
                  success: false,
                  message: 'Something went wrong while registering merchant.'
                })
              } else {
                // console.log("Merchant successfully created.");
                if (regMerchantObj && regMerchantObj.mobile) {
                  MerchantModel.findOne({
                    mobile: regMerchantObj.mobile
                  }, (errFindMerchant, merchantFound) => {
                    if (regMerchantObj && regMerchantObj.regCommissions && regMerchantObj.regCommissions.length > 0) {
                      async.eachSeries(regMerchantObj.regCommissions, (commissionObj, callback) => {
                        if (commissionObj) {
                          var merchantCommissionModel = MerchantCommissionModel({
                            merchant_id: merchantFound._id,
                            commission_rate: commissionObj.commission_rate != '' ? commissionObj.commission_rate : 0,
                            rate_by: admin._id,
                            min_amount: commissionObj.min_amount != '' ? commissionObj.min_amount : 1,
                            max_amount: commissionObj.max_amount != '' ? commissionObj.max_amount : 10000000,
                            status: 0
                          })
                          merchantCommissionModel.save((errSave, saved) => {
                            if (errSave) {
                              console.log('Error while saving commission model for merchant : ', merchant.mobile, errSave)
                              callback()
                              // apiRes.send({success: false, message: "Error while saving commission model for merchant.", merchant});
                            } else {
                              callback()
                            }
                          })
                        } else {
                          callback()
                        }
                      }, (success) => {
                        // console.log('merchant successfully inserted.');
                        apiRes.send({
                          success: true,
                          message: 'Merchant successfully created.',
                          merchant
                        })
                      })
                    } else {
                      apiRes.send({
                        success: true,
                        message: 'Merchant successfully created.',
                        merchant
                      })
                    }
                  })
                } else {
                  apiRes.send({
                    success: true,
                    message: 'Merchant successfully created.',
                    merchant
                  })
                }
              }
            })
          }
        })
      }
    })
  }
}

merchantServices.getMerchantDetails = function (apiReq, apiRes) {
  var merchantId = apiReq.params.merchantId
  // console.log(merchantId);
  MerchantModel.findById({
    _id: merchantId
  }, {
    password: false
  }, (errFind, merchantObj) => {
    if (errFind) {
      console.log('Error while fetching merchant details', errFind)
      apiRes.send({
        success: false,
        message: 'Error while fetching merchant details',
        merchantFound: merchantObj
      })
    } else {
      if (merchantObj) {
        var responseObj = {}
        MerchantCommissionModel.find({
          merchant_id: merchantId
        }, (errFindCommissions, merchantCommissions) => {
          if (errFindCommissions) {
            console.log('Error while finding merchant commissions', errFindCommissions)
            apiRes.send({
              success: false,
              message: 'Error while finding merchant commissions',
              merchantFound: merchantObj
            })
          } else {
            // merchantObj.Commissions = merchantCommissions;
            merchantObj = JSON.parse(JSON.stringify(merchantObj))
            let domains = merchantObj.web_domains.toString()
            merchantObj.web_domains = domains // .substr(0, domains.length - 1);
            var open_time = {}
            var close_time = {}
            open_time['hour'] = merchantObj.open_time ? merchantObj.open_time.split(':')[0] : ''
            open_time['minute'] = merchantObj.open_time ? merchantObj.open_time.split(':')[1] : ''
            open_time['second'] = merchantObj.open_time ? merchantObj.open_time.split(':')[2] : ''
            close_time['hour'] = merchantObj.close_time ? merchantObj.close_time.split(':')[0] : ''
            close_time['minute'] = merchantObj.close_time ? merchantObj.close_time.split(':')[1] : ''
            close_time['second'] = merchantObj.close_time ? merchantObj.close_time.split(':')[2] : ''
            merchantObj.open_time = open_time
            merchantObj.close_time = close_time
            responseObj['profile'] = merchantObj
            responseObj['commissions'] = merchantCommissions
            apiRes.send({
              success: true,
              message: 'Merchant details fetched successfully.',
              responseObj
            })
          }
        })
      } else {
        apiRes.send({
          success: false,
          message: 'Error while finding merchant commissions',
          merchantFound: merchantObj
        })
      }
    }
  })
}

merchantServices.updateMerchant = function (apiReq, apiRes) {
  var merchantObj = apiReq.body
  var updateMerchantProfile = {}
  updateMerchantProfile._id = merchantObj._id
  if (merchantObj.onboard_date) {
    updateMerchantProfile.onboard_date = merchantObj.onboard_date
  }
  if (merchantObj.name) {
    updateMerchantProfile.name = merchantObj.name
  }
  if (merchantObj.email) {
    updateMerchantProfile.email = merchantObj.email.toLowerCase()
  }
  if (merchantObj.bank_name) {
    updateMerchantProfile.bank_name = merchantObj.bank_name.toUpperCase()
  } else {
    updateMerchantProfile.bank_name = ''
  }
  if (merchantObj.branch) {
    updateMerchantProfile.branch = merchantObj.branch.toUpperCase()
  } else {
    updateMerchantProfile.bank_name = ''
  }
  if (merchantObj.account_no) {
    updateMerchantProfile.account_no = merchantObj.account_no.toUpperCase()
  } else {
    updateMerchantProfile.account_no = ''
  }
  if (merchantObj.ifsc_code) {
    updateMerchantProfile.ifsc_code = merchantObj.ifsc_code.toUpperCase()
  } else {
    updateMerchantProfile.ifsc_code = ''
  }
  if (merchantObj.pan_number) {
    updateMerchantProfile.pan_number = merchantObj.pan_number.toUpperCase()
  } else {
    updateMerchantProfile.pan_number = ''
  }
  if (merchantObj.pan_name) {
    updateMerchantProfile.pan_name = merchantObj.pan_name.toUpperCase()
  } else {
    updateMerchantProfile.pan_name = ''
  }
  if (merchantObj.name_on_bank) {
    updateMerchantProfile.name_on_bank = merchantObj.name_on_bank.toUpperCase()
  } else {
    updateMerchantProfile.name_on_bank = ''
  }
  if (merchantObj.tin) {
    updateMerchantProfile.tin = merchantObj.tin.toUpperCase()
  } else {
    updateMerchantProfile.tin = ''
  }
  if (merchantObj.tid) {
    updateMerchantProfile.tid = merchantObj.tid
  } else {
    updateMerchantProfile.tid = ''
  }
  if (merchantObj.mobile) {
    updateMerchantProfile.mobile = merchantObj.mobile
  }
  if (merchantObj.brand_name) {
    updateMerchantProfile.brand_name = merchantObj.brand_name.toUpperCase()
  }
  if (merchantObj.token_validity) {
    updateMerchantProfile.token_validity = merchantObj.token_validity
  }
  if (merchantObj.merchant_category) {
    updateMerchantProfile.merchant_category = merchantObj.merchant_category
  }
  if (merchantObj.merchant_status == 0) {
    updateMerchantProfile.merchant_status = merchantObj.merchant_status
  } else {
    updateMerchantProfile.merchant_status = merchantObj.merchant_status
  }
  if (merchantObj.is_admin_verified) {
    updateMerchantProfile.is_admin_verified = merchantObj.is_admin_verified
  }
  if (merchantObj.bank_details) {
    updateMerchantProfile.bank_details = merchantObj.bank_details
  } else {
    updateMerchantProfile.bank_details = 0
  }
  if (merchantObj.pan_details) {
    updateMerchantProfile.pan_details = merchantObj.pan_details
  } else {
    updateMerchantProfile.pan_details = 0
  }
  if (merchantObj.pay_cycle_mode) {
    updateMerchantProfile.pay_cycle_mode = merchantObj.pay_cycle_mode
  }
  if (merchantObj.open_time) {
    updateMerchantProfile.open_time = merchantObj.open_time.hour + ':' + merchantObj.open_time.minute + ':' + merchantObj.open_time.second
  }
  if (merchantObj.close_time) {
    updateMerchantProfile.close_time = merchantObj.close_time.hour + ':' + merchantObj.close_time.minute + ':' + merchantObj.close_time.second
  }
  if (merchantObj.formatted_address) {
    updateMerchantProfile.formatted_address = merchantObj.formatted_address
  }
  if (merchantObj.company_address) {
    var location = {}
    updateMerchantProfile.company_address = merchantObj.company_address
    location['type'] = 'Point'
    var coordinates = []
    coordinates.push(merchantObj.company_address.longitude ? merchantObj.company_address.longitude : '')
    coordinates.push(merchantObj.company_address.latitude ? merchantObj.company_address.latitude : '')
    location['coordinates'] = coordinates
    updateMerchantProfile.location = location
  }

  if (merchantObj.display_status) {
    updateMerchantProfile['display_status'] = merchantObj.display_status
  }
  var web_domains
  if (typeof (merchantObj.web_domains) === 'string') {
    updateMerchantProfile.web_domains = merchantObj.web_domains != null ? merchantObj.web_domains.trim().split(',') : []
  } else {
    updateMerchantProfile.web_domains = merchantObj.web_domains[0].trim().split(',')
  }
  var oldMerchantObj = {}
  async.waterfall([
    (done) => {
      MerchantModel.findOne({
        _id: merchantObj._id
      }, (errFindMerchant, merchantDetails) => {
        if (errFindMerchant) {
          apiRes.send({
            success: false,
            message: 'Error while fetching merchant details.'
          })
        } else {
          oldMerchantObj = merchantDetails
          done(null)
        }
      })
    },
    (done) => {
      if (updateMerchantProfile.bank_name && updateMerchantProfile.account_no && updateMerchantProfile.name_on_bank && updateMerchantProfile.ifsc_code && updateMerchantProfile.bank_details != 2) {
        updateMerchantProfile.bank_details = 1
      }
      if (updateMerchantProfile.bank_name != oldMerchantObj.bank_name || updateMerchantProfile.account_no != oldMerchantObj.account_no || updateMerchantProfile.name_on_bank != oldMerchantObj.name_on_bank || updateMerchantProfile.ifsc_code != oldMerchantObj.ifsc_code) {
        updateMerchantProfile.bank_details = 1
      } else if (updateMerchantProfile.bank_name == oldMerchantObj.bank_name && updateMerchantProfile.account_no == oldMerchantObj.account_no && updateMerchantProfile.name_on_bank == oldMerchantObj.name_on_bank && updateMerchantProfile.ifsc_code == oldMerchantObj.ifsc_code) {
        updateMerchantProfile.bank_details = updateMerchantProfile.bank_details
      }
      if (oldMerchantObj.pan_card && oldMerchantObj.pan_details != 3 && updateMerchantProfile.pan_details != 3) {
        updateMerchantProfile.pan_details = 2
      } else if (updateMerchantProfile.pan_number && updateMerchantProfile.pan_name && updateMerchantProfile.pan_details == 2) {
        updateMerchantProfile.pan_details = 2
      } else if (updateMerchantProfile.pan_number && updateMerchantProfile.pan_name && updateMerchantProfile.pan_details == 3) {
        updateMerchantProfile.pan_details = 3
      } else if (updateMerchantProfile.pan_number && updateMerchantProfile.pan_name) {
        updateMerchantProfile.pan_details = 1
      } else {
        updateMerchantProfile.pan_details = updateMerchantProfile.pan_details
      }

      MerchantModel.findByIdAndUpdate({
        _id: merchantObj._id
      }, {
        $set: updateMerchantProfile
      }, {
        new: true
      }, (errUpdate, merchant) => {
        if (errUpdate) {
          console.log('Error while updating merchant profile.', errUpdate)
        } else {
          console.log('Merchant profile successfully updated', merchant)
          async.eachSeries(merchantObj.regCommissions, (commissionObj, callbackCommissions) => {
            // console.log('commissionObj :: ', commissionObj);
            MerchantCommissionModel.findByIdAndUpdate({
              _id: commissionObj._id
            }, {
              $set: commissionObj
            }, (errUpdateCommission, updatedCommission) => {
              if (errUpdateCommission) {
                console.log('Error while updating existed commission', errUpdateCommission)
                callbackCommissions()
              } else {
                // console.log('Existed commission updated');
                callbackCommissions()
              }
            })
          }, (success) => {
            if (merchantObj && merchantObj.regCommissionsUpdate && merchantObj.regCommissionsUpdate.length > 0) {
              AdminUsersModel.findOne({
                email: merchantObj.admin_email
              }, (errFindAdmin, admin) => {
                if (errFindAdmin) {
                  // console.log("Error while finding Admin id with email");
                  apiRes.send({
                    success: false,
                    message: 'Error while finding Admin id with email.'
                  })
                } else if (!admin) {
                  // console.log("No admin exist");
                  apiRes.send({
                    success: false,
                    message: 'No admin exist.'
                  })
                } else {
                  async.eachSeries(merchantObj.regCommissionsUpdate, (commissionUpdateObj, callbackCUO) => {
                    var merchantCommissionModel = MerchantCommissionModel({
                      merchant_id: merchantObj._id,
                      commission_rate: commissionUpdateObj.commission_rate != '' ? commissionUpdateObj.commission_rate : 0,
                      rate_by: admin._id,
                      min_amount: commissionUpdateObj.min_amount != '' ? commissionUpdateObj.min_amount : 1,
                      max_amount: commissionUpdateObj.max_amount != '' ? commissionUpdateObj.max_amount : 10000000,
                      status: 0
                    })
                    merchantCommissionModel.save((errSave, saved) => {
                      if (errSave) {
                        console.log('Error while saving commission model for merchant : ', merchantObj.mobile, errSave)
                        callbackCUO()
                        // apiRes.send({success: false, message: "Error while saving commission model for merchant."});
                      } else {
                        callbackCUO()
                      }
                    })
                  }, (success) => {
                    apiRes.send({
                      success: true,
                      message: 'Merchant details updated successfully.',
                      merchant
                    })
                  })
                }
              })
            } else {
              apiRes.send({
                success: true,
                message: 'Merchant details updated successfully.',
                merchant
              })
            }
          })
        }
      })
    }
  ])
}

var unlinkLeadFile = function (upload_root_path, name) {
  // console.log('Unlink ::', upload_root_path + '/' + name + '.jpeg');
  let file_name = upload_root_path + '/' + name + '.jpeg'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.jpg'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.png'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
  file_name = upload_root_path + '/' + name + '.pdf'
  if (fs.existsSync(file_name)) fs.unlink(file_name, () => {})
}

merchantServices.uploadDoc = (apiReq, apiRes) => {
  // let Response = new ResponseUtils.response(apiRes);
  var token_data = apiReq.user
  var file_extension = apiReq.query.file_extension
  var image_type = apiReq.query.image_type
  var file_name = apiReq.query.file_name + '_' + image_type + file_extension
  // console.log("*************** file name with ext", file_name);
  var upload_directory = __dirname + AppConfig.Upload_DIR
  // console.log('upload Path from config ::', upload_directory); BUSINESS_WAY2MONEY_COM
  let upload_root_path = upload_directory + '/' + file_name
  //   console.log('Upload directory', upload_root_path);
  unlinkLeadFile(upload_directory, apiReq.query.file_name + '_' + image_type)
  let FileUpload = new FileUploadUtils.upload(apiReq, apiRes, upload_root_path, upload_directory, file_name, 'file', null, true, 'single')
  //   console.log(apiReq.file, apiReq.query, apiReq.body, apiReq.params, apiReq.path);
  FileUpload.exec(function (upload_error, upload_file_name) {
    if (upload_error) {
      // return Response.error("Error while Uploading Image", null);
      // console.log("upload_error :: ", upload_error);
      apiRes.send({
        success: false,
        message: 'Error while uploading Logo.'
      })
    } else {
      var imageUpload = {}
      imageUpload[image_type] = AppConfig.images_cdn + file_name + '?q=' + Math.random()
      // apiRes.send({ success: true, message: "Successfully uploaded", file_name })
      if (image_type == 'pan_card') {
        imageUpload['pan_details'] = 2
      }
      // console.log("Image upload CDN :: ", AppConfig.images_cdn + file_name);
      MerchantModel.findOneAndUpdate({
          _id: ObjectId(apiReq.query.file_name)
        }, {
          $set: imageUpload
        },
        (errUpdateLogoPath, logoPathUpdated) => {
          // console.log(logoPathUpdated)
          if (errUpdateLogoPath) {
            apiRes.send({
              success: false,
              message: 'Error while uploading url in to DB.',
              file_name
            })
          } else {
            // return Response.success("File successfully uploaded.", file_name);
            // console.log("logoPathUpdated", logoPathUpdated);
            apiRes.send({
              success: true,
              message: 'File uploaded successfully.',
              file_name
            })
          }
        })
    }
  })
}
merchantServices.getBrandsList = (apiReq, apiRes) => {
  MerchantModel.find({}, {
    brand_name: true,
    _id: false
  }, function (MongoErr, MongoRes) {
    if (MongoErr) {
      apiRes.status(500).send({
        message: 'Internal server error',
        data: null
      })
    } else {
      let brands = []

      function unique(value, index, self) {
        return self.indexOf(value) === index
      }
      for (let i in MongoRes) {
        if (MongoRes[i].brand_name) {
          brands.push(MongoRes[i].brand_name)
        }
      }
      apiRes.status(200).send({
        message: 'success',
        data: brands.filter(unique)
      })
    }
  })
}

merchantServices.getBanks = (apiReq, apiRes) => {
  BanksModel.find({}, (errFindBanks, banks) => {
    if (errFindBanks) {
      // console.log("Error while finding bank details.");
      apiRes.send({
        success: false,
        message: 'Error while finding banks.'
      })
    } else {
      // console.log("Successfully Found Bank Details.", banks);
      apiRes.send({
        success: true,
        message: 'Successfully Found Banks.',
        banks: banks
      })
      // BankDetailsModel.find({}, (errFindBankDetails, bankDetails) => {
      //   if(errFindBankDetails) {
      //     console.log("Error while finding bank Details");
      //   } else {
      //     console.log("Bank details found.", bankDetails);
      //   }
      // });
    }
  })
}

merchantServices.getBankDetails = (apiReq, apiRes) => {
  console.log('ID :: ', apiReq.params.bankId)
  var bankId = ObjectId(apiReq.params.bankId)
  console.log(bankId)
  BankDetailsModel.find({
    bank_id: bankId
  }, (errFindBankDetails, bankDetails) => {
    if (errFindBankDetails) {
      // console.log("Error while finding bank details.");
      apiRes.send({
        success: false,
        message: 'Error while finding bank details.'
      })
    } else {
      console.log('Successfully Found Bank Details.', bankDetails)
      apiRes.send({
        success: true,
        message: 'Successfully Found Bank Details.',
        bank_details: bankDetails
      })
      // BankDetailsModel.find({}, (errFindBankDetails, bankDetails) => {
      //   if(errFindBankDetails) {
      //     console.log("Error while finding bank Details");
      //   } else {
      //     console.log("Bank details found.", bankDetails);
      //   }
      // });
    }
  })
}

merchantServices.getBranches = (apiReq, apiRes) => {
  var bankId = ObjectId(apiReq.params.bankId)
  BankDetailsModel.find({
    bank_id: bankId
  }, {
    branch: true,
    _id: false
  }, (errFind, branchesFound) => {
    if (errFind) {
      apiRes.send({
        success: false,
        message: 'Error while finding branch details.'
      })
    } else if (branchesFound.length == 0) {
      apiRes.send({
        success: true,
        message: 'Successfully fetched branches',
        branches: []
      })
    } else {
      // console.log(branches);
      var branches = []

      function unique(value, index, self) {
        return self.indexOf(value) === index
      }
      for (let i in branchesFound) {
        if (branchesFound[i].branch) {
          branches.push(branchesFound[i].branch)
        }
      }
      apiRes.send({
        success: true,
        message: 'Successfully fetched branches',
        branches: branches.filter(unique)
      })
    }
  })
}

merchantServices.banks = function (request, response) {
  // let this_function_name = "ConsumerLoanController2.banks";
  // let Response = new ResponseUtils.response(response);
  // let Print = new ResponseUtils.print(this_file_name, this_function_name);
  // let default_error = "Problem while getting banks. Plese try again.";
  // let Logs = new ResponseUtils.Logs(this_file_name, this_function_name);

  try {
    var bank_name = request.query.bank_name
    var limit = request.query.limit

    if (!bank_name) {
      // Print.error("Bank Name not given in request");
      console.log('Bank Name not given in request')
      // return Response.error("Bank Name not given in request.", "BADPARAMS", 400);
      response.send({
        success: false,
        message: 'Bank Name not given in request.'
      })
    } else {
      var searchName = replaceAll('/', '//', bank_name)
      console.log(AppConfig.elastic.url + AppConfig.elastic.index + '/_search')
      req.get({
        url: AppConfig.elastic.url + AppConfig.elastic.banks_index + '/_search',
        qs: {
          'from': 0,
          'size': limit,
          q: 'bank_name:' + '' + searchName
        }
      }, function (error, httpResponse, body) {
        // console.log('elastic response body ', body);
        if (error || (JSON.parse(body).error)) {
          console.error('elastic search error', JSON.parse(body).error)
          // return callback(null, "BANKFIND", error, null);
          response.send({
            success: false,
            message: 'Elastic search error.'
          })
        } else if (body) {
          body = JSON.parse(body)
          let companies = []
          if (body.hits && body.hits.hits) {
            let hits = body.hits.hits
            // console.log('hits ', hits);
            if (hits && hits.length > 0) {
              hits.forEach(function (hit) {
                companies.push({
                  bank_name: hit._source.bank_name,
                  bank_id: hit._source.bank_id
                })
              })
            }
          }
          // return callback(null, "BANKFIND", error, companies);
          response.send({
            success: true,
            message: 'Successfully find bank.',
            banks_data: companies
          })
        } else {
          response.send({
            success: true,
            message: 'Successfully find bank.',
            banks_data: []
          })
          // return callback(null, "BANKFIND", error, []);
        }
      })
    }

    // ConsumerLoanService2.banks(bank_name, limit, function (message, code, error, banks_data) {
    //   if (error) {
    //     Print.error(error + "");
    //     return Response.error(message ? message : default_error, code, 500);
    //   }

    //   return Response.success("Successfully fetched banks data.", banks_data);
    // });
  } catch (exception) {
    // Print.exception(exception + "");
    // Logs.exception("exception while getting banks", "BANKSFINDERROR", exception + "", new Error());
    // Response.error(default_error, "EXCEPTION", 500);
    console.log(exception + '')
    response.send({
      success: false,
      message: 'Error occurred in catch exception'
    })
  }
}

// merchantServices.bankBranches = function (request, response) {
//   let this_function_name = "ConsumerLoanController2.bankBranches";
//   let Response = new ResponseUtils.response(response);
//   let Print = new ResponseUtils.print(this_file_name, this_function_name);
//   let default_error = "Problem while getting bank branches. Plese try again.";
//   let Logs = new ResponseUtils.Logs(this_file_name, this_function_name);

//   try {
//     let bank_id = request.query.bank_id;
//     let branch_name = request.query.branch_name;
//     let limit = request.query.limit;

//     if (!branch_name) {
//       Print.error("Bank Branch Name not given in request");
//       return Response.error("Bank Branch Name not given in request.", "BADPARAMS", 400);
//     }
//     ConsumerLoanService2.bankBranches(bank_id, branch_name, limit, function (message, code, error, banks_branch_data) {
//       if (error) {
//         Print.error(error + "");
//         return Response.error(message ? message : default_error, code, 500);
//       }

//       return Response.success("Successfully fetched banks branches data.", banks_branch_data);
//     });
//   } catch (exception) {
//     Print.exception(exception + "");
//     Logs.exception("exception while getting banks branches", "BANKSBRANCHFINDERROR", exception + "", new Error());
//     Response.error(default_error, "EXCEPTION", 500);
//   }
// }

merchantServices.bankIFSC = function (request, response) {
  // let this_function_name = "ConsumerLoanController2.bankIFSC";
  // let Response = new ResponseUtils.response(response);
  // let Print = new ResponseUtils.print(this_file_name, this_function_name);
  // let default_error = "Problem while getting bank IFSC. Plese try again.";
  // let Logs = new ResponseUtils.Logs(this_file_name, this_function_name);

  try {
    var ifsc_code = request.query.ifsc_code
    var limit = request.query.limit

    if (!ifsc_code) {
      // Print.error("IFSC code not given in request");
      // return Response.error("IFSC code not given in request.", "BADPARAMS", 400);
      response.send({
        success: false,
        message: 'IFSC code not given in request.'
      })
    } else {
      var searchName = replaceAll('/', '//', ifsc_code)
      // console.log("searchName :: ", searchName);
      // console.log(AppConfig.elastic.url + AppConfig.elastic.bank_branches_index + '/_search');
      req.get({
        url: AppConfig.elastic.url + AppConfig.elastic.bank_branches_index + '/_search',
        qs: {
          'from': 0,
          'size': limit,
          q: 'ifsc_code:' + '' + searchName
        }
      }, function (error, httpResponse, body) {
        // console.log('elastic response body ', body);
        if (error || (JSON.parse(body).error)) {
          console.error('elastic search error', JSON.parse(body).error)
          // return callback(null, "BANKIFSCFIND", error, null);
          response.send({
            success: false,
            message: 'Elastic search error'
          })
        } else if (body) {
          body = JSON.parse(body)
          let IFSC_DATA = []
          if (body.hits && body.hits.hits) {
            let hits = body.hits.hits
            // console.log('hits ', hits);
            if (hits && hits.length > 0) {
              hits.forEach(function (hit) {
                IFSC_DATA.push({
                  bank_name: hit._source.bank_name,
                  branch_name: hit._source.branch_name,
                  ifsc_code: hit._source.ifsc_code,
                  bank_id: hit._source.bank_id
                })
              })
            }
          }
          // return callback(null, "BANKIFSCFIND", error, IFSC_DATA);
          response.send({
            success: true,
            message: 'successfully find IFSC',
            banks_ifsc_data: IFSC_DATA
          })
        } else {
          response.send({
            success: true,
            message: 'successfully find IFSC',
            banks_ifsc_data: []
          })
          // return callback(null, "BANKIFSCFIND", error, []);
        }
      })
    }

    // ConsumerLoanService2.bankIFSC(ifsc_code, limit, function (message, code, error, banks_ifsc_data) {
    //   if (error) {
    //     Print.error(error + "");
    //     return Response.error(message ? message : default_error, code, 500);
    //   }

    //   return Response.success("Successfully fetched banks IFSC data.", banks_ifsc_data);
    // });
  } catch (exception) {
    // Print.exception(exception + "");
    // Logs.exception("exception while getting  bankIFSC", "BANKIFSCFINDERROR", exception + "", new Error());
    // Response.error(default_error, "EXCEPTION", 500);
    console.log(exception + '')
    response.send({
      success: false,
      message: 'Error occurred in catch exception'
    })
  }
}