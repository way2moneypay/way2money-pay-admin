var UserDetailsModel = require("./../models/userDetails");
var PushNotificationsModel = require("./../models/PushNotificationsModel");
var config = require("config");
var FCM = require('fcm-node');
const apn = require("apn");
const LoggerModel = require("./../models/LoggerModel");
var logger = require("way2-logger");
logger = new logger(LoggerModel);


var Exports = module.exports;

Exports.prepareConfig = function() {
    var IOSKeyFilePath = config.home_directory + "/product/push_config/"

    var APNOptions = {
        token: {
            key: IOSKeyFilePath + config.apn.keyName,
            keyId: config.apn.keyId,
            teamId: config.apn.teamId
        },
        production: true,
        connectionRetryLimit: 10
    };

    Exports.android = new FCM(config.serverKey);
    Exports.IOS = new apn.Provider(APNOptions);
}

Exports.start = function(callback) {
    PushNotificationsModel.find({}, function(notifications_error, notifications_data) {
        let completed_data = 0;
        if (notifications_error) {
            logger.errorDB("Error while getting notifications .", notifications_error);
            return callback();
        }

        if (!notifications_data || !notifications_data.length) {
            logger.log("No notification details found .", notifications_data);
            return callback();
        }

        for (var index = 0; index < notifications_data.length; ++index) {
            logger.log("index :: ", index);
            if (!notifications_data[index]["pn_id"]) {
                completed_data += 1;
                continue;
            }

            if (notifications_data[index]["os"] == "ios") {
                Exports.sendIOS(notifications_data[index], index, function(complete_index) {
                    completed_data += 1;
                    if (completed_data >= notifications_data.length) {
                        return callback();
                    }
                });
            } else {
                Exports.sendAndroid(notifications_data[index], index, function(complete_index) {
                    completed_data += 1;
                    if (completed_data >= notifications_data.length) {
                        return callback();
                    }
                });
            }
        };
    });
};


Exports.sendIOS = function(notification_data, index, callback) {
    console.log("came to ios");
    let notification = new apn.Notification();
    notification.payload = {};
    notification.badge = 0;
    notification.sound = "default";
    notification.alert = notification_data.content;
    notification.topic = config.iosTopic;
    notification.priority = 10;
    notification.expiry = Math.floor(Date.now() / 1000) + config.iosExpiry;

    Exports.IOS.send(notification, [notification_data.pn_id]).then(result => {

        logger.log("ios result :: ", notification_data, JSON.stringify(result));
        Exports.removeNotification(notification_data._id, function() {
            callback(index);
        });
        let failed = (result && result.failed && result.failed.length) ? true : false;
        let uninstall_status = (result && result.failed && result.failed[0] && result.failed[0].response && result.failed[0].response.reason == "Unregistered") ? "uninstalled" : "installed";

        Exports.updateNotificationStatus(notification_data.user_id, "inactive", null, uninstall_status, !failed, function() {

        });
    });
};


Exports.sendAndroid = function(notification_data, index, callback) {
    let notification = {
        registration_ids: [notification_data.pn_id],
        data: {
            "message": {
                "TITLE": notification_data.content
            }
        },
        "delay_while_idle": true,
        "time_to_live": 86400,
        "priority": "HIGH",
        "dry_run": false
    };

    Exports.android.send(notification, function(notification_error, notification_result) {
        logger.log("android result :: ", notification, notification_error, notification_result);
        Exports.removeNotification(notification_data._id, function() {
            callback(index);

        });
        let uninstall_status = (notification_error && notification_error.results && result.results[0] && result.results[0].error == "NotRegistered") ? "uninstalled" : "installed";
        let pn_id = (notification_error && notification_error.results && result.results[0] && result.results[0].registration_id) ? result.results[0].registration_id : null;


        Exports.updateNotificationStatus(notification_data.user_id, "inactive", pn_id, uninstall_status, !notification_error, function() {

        });

    });
};


Exports.updateNotificationStatus = function(user_id, status, pn_id, uninstall_status, skip_fn, callback) {

    if (skip_fn) {
        return callback(null, null, null, null);
    }

    let user_filter = {};
    user_filter._id = user_id;

    let update = {
        $set: {
            notification_status: status,
            installed_status: uninstall_status
        }
    };

    if (pn_id) {
        update.$set.pn_id = pn_id;
        update.$set.notification_status = "active";
    }

    let options = {
        new: true
    };

    UserDetailsModel.findOneAndUpdate(user_filter, update, options, function(user_update_error, user_data) {
        if (user_update_error) {
            logger.errorDB("Error while updating notification status in user details.", user_update_error);
            callback(true, "DB-ERROR", "Error while updating notification status in user details.", null);
        }

        return callback(null, null, null, null);
    });
};


Exports.removeNotification = function(notification_id, callback) {
    let notification_filter = {};
    notification_filter._id = notification_id;
    logger.log("notification remove filter :: ", notification_filter);
    PushNotificationsModel.findOneAndRemove(notification_filter, function(notification_error, notification_data) {
        if (notification_error) {
            logger.errorDB("Error while getting notification details.", notification_error);
        }
        callback();
    });
};