var dashboard = module.exports
const mongoose = require('mongoose')
const async = require('async')
const users = require('../models/userDetails')
const payments = require('../models/userPaymentsModel')
const transactions = require('../models/userTransactionsModel')
const merchantModel = require('../models/merchantModel')
// const commission = require('../models/merchantComissionModel')
const dayWisePayments = require('../models/dayWisePayments')
const usersModel = require('../models/usersModel')
const merchantOverallDetails = require('../models/merchantOverallDetailsModel')
const merchantsStats = require('../models/merchantDaywiseDetails')
const eventsDayStats = require('../models/EventsDayStats')
const daywiseUsersModel = require('../models/daywiseUsers')
const daywiseTransactionsModel = require('../models/daywiseTransactions')
// const passwordHash = require('password-hash')
const JWT = require('jsonwebtoken')
const AppConfig = require('config')
var dateFormat = require('dateformat')
const SalesTeamTarget = require('../models/SalesTeamTarget')

dashboard.login = function (req, res) {
  var email = req.body.email !== undefined ? req.body.email : null
  var password = req.body.password !== undefined ? req.body.password : null
  if (!email) {
    res.send({
      data: false,
      message: 'Email is required'
    })
  } else if (!password) {
    res.send({
      data: false,
      message: 'Password is required'
    })
  } else {
    usersModel.findOne({
      email: req.body.email
      // password: req.body.password
    }, (findErr, emailFound) => {
      if (findErr) {
        res.send({
          data: false,
          message: 'Something went wrong. Please try again.'
        })
      } else if (emailFound) {
        if (emailFound && emailFound._id) {
          if (req.body.password && emailFound.validPassword(req.body.password)) {
            let admin = {}
            admin._id = emailFound._id
            admin.name = emailFound.name
            admin.email = emailFound.email
            admin.mobile = emailFound.mobile
            admin.user_type = emailFound.user_type
            admin.user_status = emailFound.user_status
            admin.permissions = emailFound.permissions
            admin.parent_id = emailFound.parent_id
            let adminToken = JWT.sign(admin, AppConfig.jwtsecret)
            res.send({
              data: true,
              id: emailFound._id,
              admin_token: adminToken,
              message: 'Admin successfully loggedIn.'
            })
          }
        }
      } else {
        res.send({
          data: false,
          message: 'Please enter valid credentials.'
        })
      }
    })
  }
}
dashboard.get_merchants = function (req, res) {
  async.parallel([
    function (callback) {
      let reqObj = {}
      if (req.user.user_type === 'sales_team') {
        reqObj['created_by'] = mongoose.Types.ObjectId(req.user._id)
      }
      if (req.user.user_type === 'regional_head') {
        reqObj['created_by_parent'] = mongoose.Types.ObjectId(req.user._id)
      }
      merchantModel.count(reqObj, function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in merchants_count')
        else {
          callback(null, {
            total_merchant_count: mongoRes
          })
        }
      })
    },
    function (callback) {
      let query = {}
      query.merchant_status = {
        $in: [1, 2]
      }
      if (req.user.user_type === 'sales_team') {
        query['created_by'] = mongoose.Types.ObjectId(req.user._id)
      }
      if (req.user.user_type === 'regional_head') {
        query['created_by_parent'] = mongoose.Types.ObjectId(req.user._id)
      }
      merchantModel.count(query, function (activeMongoErr, activeMongoRes) {
        if (activeMongoErr) console.log('Error in  getting active merchants_count')
        else {
          callback(null, {
            active_merchant_count: activeMongoRes
          })
        }
      })
    }
  ], function (finalResultError, finalResult) {
    if (finalResultError) {
      console.log('error while geeting info', finalResultError)
    } else {
      res.send({
        final_merchants_result: finalResult
      })
      console.log('finalResult', finalResult)
    }
  })
}
dashboard.users_count = function (req, res) {
  async.parallel([
    function (callback) {
      users.count({}, function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in merchants_count')
        else if (mongoRes) {
          callback(null, {
            total_users: mongoRes
          })
        } else {
          callback(null, {
            total_users: 0
          })
        }
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      var yesterday = new Date()
      yesterday.setDate(yesterday.getDate() - 1)
      yesterday.setHours(0, 0, 0, 0)
      users.count({
        register_date: {
          $gte: (yesterday / 1) - 19800000,
          $lt: today / 1
        }
      }, function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in merchants_count')
        else if (mongoRes) {
          callback(null, {
            yesterday_users: mongoRes
          })
        } else {
          callback(null, {
            yesterday_users: 0
          })
        }
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      users.count({
        register_date: {
          $gte: today / 1
        }
      }, function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in merchants_count')
        else if (mongoRes) {
          callback(null, {
            today_users: mongoRes
          })
        } else {
          callback(null, {
            today_users: 0
          })
        }
      })
    }
  ],
  function (err, results) {
    if (err) console.log(err)
    res.send({
      data: results
    })
  })
}
dashboard.month_report = function (req, res) {
  async.parallel([
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime())
      today.setDate(today.getDate() - req.params.days)
      today.setHours(0, 0, 0, 0)
      var day = dateFormat(Date.now(), 'yyyy-mm-dd')
      day = new Date(day)
      day = new Date(day.getTime() - 19800000)
      daywiseUsersModel.find({
        register_date: {
          $gte: (today / 1) - 19800000
        }
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in users_month_count')
        else {
          var dates = []
          var counts = []
          var inc = 0
          async.eachSeries(mongoRes, function (i, next) {
            dates[inc] = (i.register_date) + 19800000
            counts[inc] = i.users
            inc++
            next()
          }, function () {
            users.count({
              register_date: {
                $gte: day / 1
              }
            }, function (error, usersCount) {
              if (error) {
                console.log(error)
              } else {
                // inc++
                dates[inc] = (day / 1) + 19800000
                counts[inc] = usersCount
                callback(null, {
                  dates: dates,
                  counts: counts
                })
              }
            })
          })
          //   async.eachSeries(mongo_res, function (i, next) {
          //     if (dates.length == 0) {
          //       day = new Date(i.register_date)
          //       day.setHours(0, 0, 0, 0)
          //       dates[inc] = day / 1
          //       counts[inc] = 1
          //       next()
          //     } else {
          //       async.eachOfSeries(dates, function (val, j, next_val) {
          //         day = new Date(i.register_date)
          //         day.setHours(0, 0, 0, 0)
          //         if (val == day / 1) {
          //           counts[j] += 1
          //           next()
          //         } else {
          //           next_val()
          //         }
          //       }, function () {
          //         inc++
          //         day.setHours(0, 0, 0, 0)
          //         dates[inc] = day / 1
          //         counts[inc] = 1
          //         next()
          //       })
          //     }
          //   }, function () {
          //     callback(null, {
          //       dates: dates,
          //       counts: counts
          //     })
          //   })
        }
      }).sort({
        register_date: 1
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime())
      today.setDate(today.getDate() - req.params.days)
      today.setHours(0, 0, 0, 0)
      var day = dateFormat(Date.now(), 'yyyy-mm-dd')
      day = new Date(day)
      day = new Date(day.getTime() - 19800000)
      daywiseTransactionsModel.find({
        trans_date: {
          $gte: (today / 1) - 19800000
        }
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in users_month_count')
        else {
          var dates = []
          var counts = []
          var inc = 0
          async.eachSeries(mongoRes, function (i, next) {
            dates[inc] = (i.trans_date) + 19800000
            counts[inc] = i.transactions
            inc++
            next()
          }, function () {
            transactions.count({
              transaction_date: {
                $gte: day / 1
              }
            }, function (error, transactionsCount) {
              if (error) {
                console.log(error)
              } else {
                console.log(day / 1, transactionsCount)
                dates[inc] = (day / 1) + 19800000
                counts[inc] = transactionsCount
                callback(null, {
                  dates: dates,
                  counts: counts
                })
              }
            })
          })
          // transactions.find({
          //   transaction_date: {
          //     $gte: (today / 1) - 19800000
          //   }
          // },
          // function (mongo_err, mongo_res) {
          //   if (mongo_err) console.log('Error in users_month_count')
          //   else {
          //     var dates = []
          //     var counts = []
          //     var inc = 0
          //     async.eachSeries(mongo_res, function (i, next) {
          //       if (dates.length === 0) {
          //         day = new Date(i.transaction_date)
          //         day.setHours(0, 0, 0, 0)
          //         dates[inc] = day / 1
          //         counts[inc] = 1
          //         next()
          //       } else {
          //         async.eachOfSeries(dates, function (val, j, next_val) {
          //           day = new Date(i.transaction_date)
          //           day.setHours(0, 0, 0, 0)
          //           if (val == day / 1) {
          //             counts[j] += 1
          //             next()
          //           } else {
          //             next_val()
          //           }
          //         }, function () {
          //           inc++
          //           day.setHours(0, 0, 0, 0)
          //           dates[inc] = day / 1
          //           counts[inc] = 1
          //           next()
          //         })
          //       }
          //     }, function () {
          //       callback(null, {
          //         dates: dates,
          //         counts: counts
          //       })
          //     })
          //   }
          // }).sort({
          //   transaction_date: 1
          // })
        }
      })
    }
  ], function (err, results) {
    if (err) {
      console.log(err)
    }
    Array.prototype.unique = function () {
      var a = this.concat()
      for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
          if (a[i] === a[j]) {
            a.splice(j--, 1)
          }
        }
      }
      return a
    }

    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Novr', 'Dec']
    var totalDays = results[0].dates.concat(results[1].dates).unique()
    totalDays = totalDays.sort()
    var userDates = []

    var transactionsDates = []
    var user_counts = []

    var transactions_count = []
    var labels = []

    for (let i = 0; i < totalDays.length; i++) {
      var day = new Date(totalDays[i])
      console.log(day)
      labels.push(months[day.getMonth()] + ' ' + day.getDate())
      if ((results[0].dates.indexOf(totalDays[i]) !== -1)) {
        userDates.push(totalDays[i])
        user_counts.push(results[0].counts[results[0].dates.indexOf(totalDays[i])])
      } else {
        userDates.push(0)
        user_counts.push(0)
      }
      if ((results[1].dates.indexOf(totalDays[i]) !== -1)) {
        transactionsDates.push(totalDays[i])
        transactions_count.push(results[1].counts[results[1].dates.indexOf(totalDays[i])])
      } else {
        transactionsDates.push(0)
        transactions_count.push(0)
      }
    }
    console.log('month report', labels, user_counts, transactions_count)
    res.send({
      data: {
        labels: labels,
        user_counts: user_counts,
        transaction_counts: transactions_count
      }
    })
  })
}
// dashboard.merchants_amount = function (req, res) {
//   transactions.find({
//     $or: [{
//       "status": 5
//     }, {
//       "status": 1
//     }]
//   },
//     function (mongo_err, mongo_res) {
//       if (mongo_err) console.log('Error in merchants_amount')
//       else {
//         var amount = 0
//         async.eachSeries(mongo_res, function (i, next) {
//           amount += i.amount;
//           next()
//         }, function () {
//           res.send({
//             data: amount
//           })
//         })
//       }
//     })
// }
// dashboard.amount_to_users = function (req, res) {
//   users.find({},
//     function (mongo_err, mongo_res) {
//       if (mongo_err) console.log('Error in amount_to_users')
//       else {
//         var amount = 0;
//         async.eachSeries(mongo_res, function (i, next) {
//           if (i.credit_limit != undefined)
//             amount += i.credit_limit
//           next()
//         }, function () {
//           res.send({
//             data: amount.toFixed(2)
//           })
//         })
//       }
//     })
// }
// dashboard.earnings = function (req, res) {
//   transactions.find({
//     $or: [{
//       "status": 5
//     }, {
//       "status": 1
//     }]
//   },
//     function (mongo_err, mongo_res) {
//       if (mongo_err) console.log('Error in earnings_by_commission')
//       else {
//         mongo_res = JSON.parse(JSON.stringify(mongo_res))

//         var commission_amount = 0
//         var convenience_amount = 0
//         async.eachSeries(mongo_res, function (i, next) {
//           commission_amount += (i.amount * i.merchant_commission) / 100;
//           convenience_amount += (i.amount * i.con_fee) / 100;
//           next()
//         }, function () {
//           res.send({
//             data: {
//               commission: commission_amount,
//               convenience: convenience_amount
//             }
//           })
//         })
//       }
//     })
// }
// dashboard.payment_by_users = function (req, res) {
//   payments.find({
//     "payment_type": "PAID"
//   },
//     function (mongo_err, mongo_res) {
//       if (mongo_err) console.log('Error in payment_by_users')
//       else if (Object.keys(mongo_res.length)) {
//         amount = 0
//         async.eachSeries(mongo_res, function (i, next) {
//           if (i.amount_paid) {
//             amount += parseInt(i.amount_paid)
//             next()
//           } else next()
//         }, function () {
//           res.send({
//             data: amount
//           })
//         })
//       } else {
//         res.send({
//           data: 0
//         })
//       }
//     })
// }sales_transactions_count
dashboard.transactions_count = function (req, res) {
  merchantModel.findOne({
    mobile: AppConfig.merchant_testing_mobile
  }, function (err, merchantData) {
    if (err) {
      console.log('Error while fetching merchant data', err)
    } else {
      // console.log('merchantData::::::::::::::', merchantData)
      var merchant = null
      if (merchantData != null && merchantData !== undefined) {
        if (Object.getOwnPropertyNames(merchantData).length !== 0) {
          merchant = merchantData._id
        }
      }

      async.series([
        function (callback) {
          merchantOverallDetails.aggregate([{
            $match: {
              merchant_id: {
                $ne: merchant
              }
            }
          },
          {
            $group: {
              _id: null,
              transactions: {
                $sum: '$transactions'
              },
              trans_amount: {
                $sum: '$trans_amount'
              },
              payments: {
                $sum: '$payments'
              },
              paid_amount: {
                $sum: '$paid_amount'
              }
            }
          }
          ],
          function (mongorrErr, mongoRes) {
            if (mongorrErr) console.log('Error in total_transactions_count')
            else if (mongoRes) {
              console.log()
              callback(null, mongoRes)
            } else {
              callback(null, 0)
            }
          })
        },
        function (callback) {
          var today = dateFormat(Date.now(), 'yyyy-mm-dd')
          today = new Date(today)
          today = new Date(today.getTime() - 19800000)
          transactions.aggregate([{
            $match: {
              transaction_date: {
                $gte: today / 1

              },
              $or: [{
                status: 1
              }, {
                status: 5
              }],
              merchant_id: {
                $ne: merchant
              }
            }

          }, {
            $group: {
              _id: null,
              total: {
                $sum: 1
              },
              amount: {
                $sum: '$amount'
              }
            }
          }],
          function (mongoErr, mongoRes) {
            if (mongoErr) console.log('Error in today_transactions_count')
            else if (mongoRes) {
              callback(null, mongoRes)
            } else {
              callback(null, 0)
            }
          })
        },
        function (callback) {
          var today = dateFormat(Date.now(), 'yyyy-mm-dd')
          today = new Date(today)
          today = new Date(today.getTime() - 19800000)
          var yesterday = new Date()
          yesterday.setDate(yesterday.getDate() - 1)
          yesterday.setHours(0, 0, 0, 0)
          console.log('transactions:::::::::::::::::', today / 1, (yesterday / 1) - 19800000)
          merchantsStats.aggregate([{
            $match: {
              trans_date: {
                $gte: (yesterday / 1) - 19800000,
                $lt: today / 1
              },
              merchant_id: {
                $ne: merchant
              }
            }
          },
          {
            $group: {
              _id: null,
              transactions: {
                $sum: '$transactions'
              },
              trans_amount: {
                $sum: '$trans_amount'
              }
            }
          }
          ],
          function (mongoErr, mongoRes) {
            if (mongoErr) console.log('Error in yesterday_transactions_count')
            else if (mongoRes) {
              callback(null, mongoRes)
            } else {
              callback(null, 0)
            }
          })
        }
      ], function (err, results) {
        if (err) console.log(err)
        var finalArray = []
        var todayTransactions = {}
        var totalTransactions = {}
        var yesterdayTransactions = {}
        if (results[0].length > 0) {
          totalTransactions = {
            total_transactions: results[0][0].transactions,
            total_transactions_amt: results[0][0].trans_amount,
            total_payments: results[0][0].payments,
            total_payments_amt: results[0][0].paid_amount
          }
        } else {
          totalTransactions = {
            total_transactions: 0,
            total_transactions_amt: 0,
            total_payments: 0,
            total_payments_amt: 0
          }
        }
        if (results[1].length > 0) {
          todayTransactions = {
            today_transactions: results[1][0].total,
            today_transactions_amt: results[1][0].amount
          }
        } else {
          todayTransactions = {
            today_transactions: 0,
            today_transactions_amt: 0
          }
        }
        if (results[2].length > 0) {
          yesterdayTransactions = {
            yesterday_transactions: results[2][0].transactions,
            yesterday_transactions_amt: results[2][0].trans_amount
          }
        } else {
          yesterdayTransactions = {
            yesterday_transactions: 0,
            yesterday_transactions_amt: 0
          }
        }
        finalArray[0] = totalTransactions
        finalArray[1] = todayTransactions
        finalArray[2] = yesterdayTransactions
        res.send({
          data: finalArray
        })
      })
    }
  })
}
// dashboard.transactions_amount = function (req, res) {
//   async.series([
//     function (callback) {
//       merchantsStats.merchantsStats.aggregate([{
//         $group: {
//             _id: null,
//             count: { $sum:1 },
//             amount:{$sum :'$trans_amount'}
//         }
//     }],
//         function (mongo_err, mongo_res) {
//           if (mongo_err) console.log('Error in total_transactions_amount')
//           else if (mongo_res.length > 0) {

//               callback(null, {
//                 total_transactions_amt: amount,
//                 total_transactions: count
//               })

//           } else {
//             callback(null, {
//               total_transactions_amt: 0,
//               total_transactions:0
//             })
//           }
//         })
//     },
//     function (callback) {

//       var today = dateFormat(Date.now(), "yyyy-mm-dd");
//         today = new Date(today);
//         today = new Date(today.getTime()-19800000);
//       transactions.find({
//         transaction_date: {
//           $gte: today / 1
//         }
//       },
//         function (mongo_err, mongo_res) {
//           if (mongo_err) console.log('Error in today_transactions_amount')
//           else if (mongo_res.length > 0) {
//             var amount = 0;

//             async.eachSeries(mongo_res, function (i, next) {
//               amount += i.amount
//               next()
//             }, function () {
//               callback(null, {
//                 today_transactions_amt: amount
//               })
//             })
//           } else {
//             callback(null, {
//               today_transactions_amt: 0
//             })
//           }
//         })
//     },
//     function (callback) {
//       var today = dateFormat(Date.now(), "yyyy-mm-dd");
//       today = new Date(today);
//       today = new Date(today.getTime()-19800000);
//       var yesterday = new Date();
//       yesterday.setDate(yesterday.getDate() - 1);
//       yesterday.setHours(0, 0, 0, 0);
//       merchantsStats.aggregate([{
//         trans_date: {
//           $gte: (yesterday / 1)-19800000,
//           $lt: today / 1
//         }
//       }],
//         function (mongo_err, mongo_res) {
//           if (mongo_err) console.log('Error in yesterday_transactions_amount')
//           else if (mongo_res.length > 0) {
//             var amount = 0;
//             async.eachSeries(mongo_res, function (i, next) {
//               amount += i.trans_amount
//               next()
//             }, function () {
//               callback(null, {
//                 yesterday_transactions_amt: amount.toFixed(2)
//               })
//             })
//           } else {
//             callback(null, {
//               yesterday_transactions_amt: 0
//             })
//           }
//         })
//     }
//   ], function (err, results) {
//     console.log(results)
//     res.send({
//       data: results
//     })
//   })
// }

// Merchants Dashboard
dashboard.users_count_merchant = function (req, res) {
  async.parallel([
    function (callback) {
      transactions.distinct('user_id', {
        merchant_id: req.params.id
      }, function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in merchants_count')
        else if (mongoRes) {
          callback(null, {
            total_users: mongoRes.length
          })
        } else {
          callback(null, {
            total_users: 0
          })
        }
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      var yesterday = new Date()
      yesterday.setDate(yesterday.getDate() - 1)
      yesterday.setHours(0, 0, 0, 0)
      transactions.distinct('user_id', {
        merchant_id: req.params.id,
        register_date: {
          $gte: (yesterday / 1) - 19800000,
          $lt: today / 1
        }
      }, function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in merchants_count')
        else if (mongoRes) {
          callback(null, {
            yesterday_users: mongoRes.length
          })
        } else {
          callback(null, {
            yesterday_users: 0
          })
        }
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      transactions.distinct('user_id', {
        merchant_id: req.params.id,
        register_date: {
          $gte: today / 1
        }
      }, function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in merchants_count')
        else if (mongoRes) {
          callback(null, {
            today_users: mongoRes.length
          })
        } else {
          callback(null, {
            today_users: 0
          })
        }
      })
    }
  ],
  function (err, results) {
    if (err) console.log(err)
    res.send({
      data: results
    })
  })
}
dashboard.merchant_transactions_count = function (req, res) {
  async.series([
    function (callback) {
      merchantsStats.count({
        merchant_id: req.params.id
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in total_transactions_count')
        else if (mongoRes) {
          callback(null, {
            total_transactions: mongoRes
          })
        } else {
          callback(null, {
            total_transactions: 0
          })
        }
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      merchantsStats.count({
        merchant_id: req.params.id,
        trans_date: {
          $gte: today / 1
        }
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in today_transactions_count')
        else if (mongoRes) {
          callback(null, {
            today_transactions: mongoRes
          })
        } else {
          callback(null, {
            today_transactions: 0
          })
        }
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      var yesterday = new Date()
      yesterday.setDate(yesterday.getDate() - 1)
      yesterday.setHours(0, 0, 0, 0)
      merchantsStats.count({
        merchant_id: req.params.id,
        trans_date: {
          $gte: (yesterday / 1) - 19800000,
          $lt: today / 1
        }
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in yesterday_transactions_count')
        else if (mongoRes) {
          callback(null, {
            yesterday_transactions: mongoRes
          })
        } else {
          callback(null, {
            yesterday_transactions: 0
          })
        }
      })
    }
  ], function (err, results) {
    if (err) console.log(err)
    res.send({
      data: results
    })
  })
}
dashboard.merchant_transactions_amt = function (req, res) {
  async.series([
    function (callback) {
      merchantsStats.find({
        merchant_id: req.params.id
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in total_transactions_count')
        else if (mongoRes.length > 0) {
          var amount = 0
          async.eachSeries(mongoRes, function (i, next) {
            amount += i.trans_amount
            next()
          }, function () {
            callback(null, {
              total_transactions_amt: amount
            })
          })
        } else {
          callback(null, {
            total_transactions_amt: 0
          })
        }
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      merchantsStats.find({
        merchant_id: req.params.id,
        trans_date: {
          $gte: today / 1
        }
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in today_transactions_count')
        else if (mongoRes.length > 0) {
          var amount = 0
          async.eachSeries(mongoRes, function (i, next) {
            amount += i.trans_amount
            next()
          }, function () {
            callback(null, {
              today_transactions_amt: amount
            })
          })
        } else {
          callback(null, {
            today_transactions_amt: 0
          })
        }
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      var yesterday = new Date()
      yesterday.setDate(yesterday.getDate() - 1)
      yesterday.setHours(0, 0, 0, 0)
      merchantsStats.find({
        merchant_id: req.params.id,
        trans_date: {
          $gte: (yesterday / 1) - 19800000,
          $lt: today / 1
        }
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in yesterday_transactions_count')
        else if (mongoRes.length > 0) {
          var amount = 0
          async.eachSeries(mongoRes, function (i, next) {
            amount += i.trans_amount
            next()
          }, function () {
            callback(null, {
              yesterday_transactions_amt: amount
            })
          })
        } else {
          callback(null, {
            yesterday_transactions_amt: 0
          })
        }
      })
    }
  ], function (err, results) {
    if (err) console.log(err)
    res.send({
      data: results
    })
  })
}
dashboard.merchant_month_report = function (req, res) {
  async.parallel([
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      today.setDate(today.getDate() - req.params.days)
      transactions.find({
        merchant_id: req.params.id,
        transaction_date: {
          $gte: today / 1
        }
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in users_month_count')
        else {
          var dates = []
          var counts = []
          var inc = 0
          var day
          async.eachSeries(mongoRes, function (i, next) {
            if (dates.length === 0) {
              day = new Date(i.transaction_date)
              day.setHours(0, 0, 0, 0)
              dates[inc] = day / 1
              counts[inc] = 1
              next()
            } else {
              async.eachOfSeries(dates, function (val, j, nextVal) {
                day = new Date(i.transaction_date)
                day.setHours(0, 0, 0, 0)
                if (val === day / 1) {
                  counts[j] += 1
                  next()
                } else {
                  nextVal()
                }
              }, function () {
                inc++
                day.setHours(0, 0, 0, 0)
                dates[inc] = day / 1
                counts[inc] = 1
                next()
              })
            }
          }, function () {
            callback(null, {
              dates: dates,
              counts: counts
            })
          })
        }
      }).sort({
        transaction_date: 1
      })
    },
    function (callback) {
      var today = dateFormat(Date.now(), 'yyyy-mm-dd')
      today = new Date(today)
      today = new Date(today.getTime() - 19800000)
      today.setDate(today.getDate() - req.params.days)
      transactions.find({
        merchant_id: req.params.id,
        transaction_date: {
          $gte: today / 1
        }
      },
      function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in users_month_count')
        else {
          var dates = []
          var counts = []
          var inc = 0
          var day
          async.eachSeries(mongoRes, function (i, next) {
            if (dates.length === 0) {
              day = new Date(i.transaction_date)
              day.setHours(0, 0, 0, 0)
              dates[inc] = day / 1
              counts[inc] = 1
              next()
            } else {
              async.eachOfSeries(dates, function (val, j, nextVal) {
                day = new Date(i.transaction_date)
                day.setHours(0, 0, 0, 0)
                if (val === day / 1) {
                  counts[j] += 1
                  next()
                } else {
                  nextVal()
                }
              }, function () {
                inc++
                day.setHours(0, 0, 0, 0)
                dates[inc] = day / 1
                counts[inc] = 1
                next()
              })
            }
          }, function () {
            callback(null, {
              dates: dates,
              counts: counts
            })
          })
        }
      }).sort({
        transaction_date: 1
      })
    }
  ], function (err, results) {
    if (err) {
      console.log(err)
    }
    Array.prototype.unique = function () {
      var a = this.concat()
      for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
          if (a[i] === a[j]) {
            a.splice(j--, 1)
          }
        }
      }
      return a
    }

    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Novr', 'Dec']
    var totalDays = results[0].dates.concat(results[1].dates).unique()
    totalDays = totalDays.sort()
    var userDates = []

    var transactionsDates = []
    var user_counts = []

    var transactions_count = []
    var labels = []

    for (let i = 0; i < totalDays.length; i++) {
      var day = new Date(totalDays[i])
      labels.push(months[day.getMonth()] + ' ' + day.getDate())
      if ((results[0].dates.indexOf(totalDays[i]) !== -1)) {
        userDates.push(totalDays[i])
        user_counts.push(results[0].counts[results[0].dates.indexOf(totalDays[i])])
      } else {
        userDates.push(0)
        user_counts.push(0)
      }
      if ((results[1].dates.indexOf(totalDays[i]) !== -1)) {
        transactionsDates.push(totalDays[i])
        transactions_count.push(results[1].counts[results[1].dates.indexOf(totalDays[i])])
      } else {
        transactionsDates.push(0)
        transactions_count.push(0)
      }
    }
    res.send({
      data: {
        labels: labels,
        user_counts: user_counts,
        transaction_counts: transactions_count
      }
    })
  })
}

dashboard.user_payment_counts = function (request, response) {
  var today = dateFormat(Date.now(), 'yyyy-mm-dd')
  today = new Date(today)
  today = new Date(today.getTime())
  today.setDate(today.getDate() - 15)
  today.setHours(0, 0, 0, 0)
  merchantModel.findOne({
    mobile: AppConfig.merchant_testing_mobile
  }, function (err, merchantData) {
    if (err) {
      console.log('Error while fetching merchant data', err)
    } else {
      // console.log("merchantData::::::::::::::", merchantData)
      // var merchant = null
      // if (merchantData !== null && merchantData !== undefined) {
      //   if (Object.getOwnPropertyNames(merchantData).length !== 0) {
      //     merchant = merchantData._id
      //   }
      // }
      async.parallel([
        function (callback) {
          // console.log("coming user counts")
          users.count({
            $and: [{
              due_date: { $ne: 0 }
            },
            { credit_due: {
              $ne: 0
            }
            }
            ]
          }, function (error, data) {
            if (error) {
              console.log('error while getting user payment datails', error)
            } else {
              // transactions.aggregate([{
              //   $match: {
              //     merchant_id: merchant,
              //     status: 1,
              //     transaction_date: {
              //       $lte: (today / 1) - 19800000
              //     }
              //   }
              // }, {
              //   $group: {
              //     _id: null,
              //     due_count: {
              //       $sum: 1
              //     },
              //     due_amount: {
              //       $sum: '$amount'

              //     }
              //   }
              // }], function (error, transData) {
              //   if (error) {
              //     console.log('error while fetching trans data', error)
              //   } else {
              //     if (transData !== null && transData !== undefined && transData.length !== 0) {
              //       data[0].users_due_count = data[0].users_due_count - transData[0].due_count
              //       data[0].users_due_amount = data[0].users_due_amount - transData[0].due_amount
              //     }
              //     callback(null, {
              //       payment_details: data
              //     })
              //   }
              // })
              callback(null, {
                payment_details: data
              })
              // response.send({
              //   payment_details:data
              // })
              console.log('successfully fetched user payment datails', data)
            }
          })
        },
        // function (callback) {
        //   payments.aggregate([{
        //     $match: {
        //       paid_status: {
        //         $in: [1, 5]
        //       }
        //     }
        //   }, {
        //     $group: {
        //       _id: null,
        //       amount_paid_users: {
        //         $sum: 1
        //       },
        //       total_amount_paid: {
        //         $sum: '$amount_paid'
        //       }
        //     }
        //   }], function (error, amountPaidData) {
        //     if (error) {
        //       console.log('error while getting details', error)
        //     } else {
        //       transactions.aggregate([{
        //         $match: {
        //           merchant_id: merchant,
        //           status: 5
        //         }
        //       }, {
        //         $group: {
        //           _id: null,
        //           paid_users: {
        //             $sum: 1
        //           },
        //           amount_paid: {
        //             $sum: '$amount'

        //           }
        //         }
        //       }], function (error, transData) {
        //         if (error) {
        //           console.log('error while fetching trans data', error)
        //         } else {
        //           if (transData != null && transData !== undefined && transData.length === 0) {
        //             amountPaidData[0].amount_paid_users = amountPaidData[0].amount_paid_users - transData[0].paid_users
        //             amountPaidData[0].total_amount_paid = amountPaidData[0].total_amount_paid - transData[0].amount_paid
        //           }
        //           callback(null, {
        //             payment_paid_details: amountPaidData
        //           })
        //         }
        //       })

        //       console.log('successfully fetched details', amountPaidData)
        //     }
        //   })
        // },
        function (callback) {
          var dateValue5days = new Date()
          dateValue5days.setDate(dateValue5days.getDate() - 5)
          dateValue5days.setHours(0, 0, 0, 0)
          dateValue5days = dateValue5days / 1
          dateValue5days = dateValue5days - 19800000
          users.count({
            $and: [{
              due_date: { $ne: 0 }
            }, {
              due_date: {
                $lt: dateValue5days
              }
            },
            { credit_due: {
              $ne: 0
            }
            }]
          }, function (error, data) {
            if (error) {
              console.log('error while getting user payment datails', error)
            } else {
              callback(null, {
                above_due_details: data
              })
            }
          })
          // let query = {}
          // query.due_date = {
          //   $gt: 0
          // }
          // users.find(query, function (error, result) {
          //   if (error) {
          //     console.log('error in due_dates', error)
          //   } else {
          //     let count = 0
          //     async.forEach(result, function (item, callback) {
          //       let dueDate = item.due_date
          //       let currentDate = Date.now()
          //       if (dueDate < currentDate) {
          //         // console.log("datesssss", due_date)
          //         let timeDifference = Math.abs(currentDate - dueDate)
          //         // console.log(timeDifference);
          //         let differentDays = Math.ceil(timeDifference / (1000 * 3600 * 24))
          //         // console.log(differentDays);
          //         if (differentDays > 5) {
          //           // console.log("datesssss---------------------->", due_date)
          //           count += 1
          //         }
          //         callback()
          //       } else {
          //         callback()
          //       }
          //     }, function (err, res) {
          //       if (err) {
          //         console.log('error', err)
          //       } else {
          //         console.log('count ', count)
          //         let transCount = 0
          //         transactions.find({
          //           merchant_id: merchant,
          //           status: 1
          //         }, function (error, transData) {
          //           if (error) {
          //             console.log('error while fetching trans data', error)
          //           } else {
          //             // let count = 0;
          //             async.forEach(transData, function (item, callback) {
          //               let transactionDate = item.transaction_date
          //               let currentDate = Date.now()
          //               if (transactionDate < currentDate) {
          //                 let timeDifference = Math.abs(currentDate - transactionDate)
          //                 // console.log(timeDifference);
          //                 let differentDays = Math.ceil(timeDifference / (1000 * 3600 * 24))
          //                 // console.log(differentDays);
          //                 if (differentDays > 20) {
          //                   transCount += 1
          //                 }
          //                 callback()
          //               } else {
          //                 callback()
          //               }
          //             }, function (err, res) {
          //               if (err) {
          //                 console.log('error', err)
          //               } else {
          //                 callback(null, {
          //                   above_due_details: count - transCount
          //                 })
          //               }
          //             })
          //           }
          //         })
          //       }
          //     })
          //   }
          // })
        }

      ], function (finalError, finalResults) {
        if (finalError) {
          console.log('error while getting result', finalError)
        } else {
          response.send({
            final_payment_result: finalResults
          })
          // console.log('successfully got result', final_results)
        }
      })
    }
  })

  // response.send({

  //       payment_paid_details:amount_paid_data
  //     })
  // let final_result ={}
  // final_result.user_details_data = data;
  // final_result.user_payments =
}
// dashboard.user_app_usage = function (req, res) {
//   console.log("app usage service called")
//   var today = dateFormat(Date.now(), 'yyyy-mm-dd')
//   today = new Date(today)
//   today = new Date(today.getTime() - 19800000)
//   find_query = {}
//   find_query.used_date = today/1
//   console.log("find query :: ", find_query)
//   eventsDayStats.find(find_query, function (error, response) {
//     if (error) {
//       console.log("error ::::::::::::::", error)
//       res.send({
//         Error: error
//       })
//     } else {
//       console.log("response:::::::::::::", response)
//       res.send({
//         Events: response
//       })
//     }

//   })

// }

dashboard.getTotalEvents = function (req, res) {
  // console.log("calling events functions",req)
  try {
    let reqUsedDate = req.body.used_date
    // console.log("date",req.body.used_date)
    eventsDayStats.aggregate([{
      $match: {
        used_date: reqUsedDate
      }
    },
    {
      $group: {
        _id: '$os',
        total_opens: {
          '$sum': '$total_opens'
        },
        unique_opens: {
          '$sum': '$unique_opens'
        },
        loan_opens: {
          '$sum': '$loan_opens'
        },
        credit_line_opens: {
          '$sum': '$credit_line_opens'
        },
        expense_manager_opens: {
          '$sum': '$expense_manager_opens'
        },
        paylater_opens: {
          '$sum': '$paylater_opens'
        },
        qrcode_scan_opens: {
          '$sum': '$qrcode_scan_opens'
        },
        qrscan_paymentpage: {
          '$sum': '$qrscan_paymentpage'
        },
        nearby_paymentpage: {
          '$sum': '$nearby_paymentpage'
        },
        paynear_opens: {
          '$sum': '$paynear_opens'
        },
        transaction_success: {
          '$sum': '$transaction_success'
        },
        logout_users: {
          '$sum': '$logout_users'
        },
        payment_success: {
          '$sum': '$payment_success'
        },
        payment_failed: {
          '$sum': '$payment_failed'
        },
        payment_cancel: {
          '$sum': '$payment_cancel'
        },
        personal_loan_applied: {
          '$sum': '$personal_loan_applied'
        },
        business_loan_applied: {
          '$sum': '$business_loan_applied'
        },
        all_documents_uploaded: {
          '$sum': '$all_documents_uploaded'
        }
      }
    }
    ], function (eventserror, eventsdata) {
      if (eventserror) {
        console.log('error in events', eventserror)
        res.send({
          Eventserror: eventserror
        })
      } else {
        //  console.log("result-->",eventsdata)
        res.send({
          Events: eventsdata
        })
      }
    })
  } catch (eventsexception) {
    console.log('eventsexception', eventsexception)
  }
}
let checkDateLessThanToDate = function (fromdate, todate) {
  console.log('check from -> ', fromdate + 'to -> ' + todate)
  if (fromdate <= todate) {
    return true
  } else {
    return false
  }
}
dashboard.user_payment_daywise = function (request, response) {
  try {
    if (request.query.paid_Fromdate && request.query.paid_Todate) {
      let fromdate = request.query.paid_Fromdate
      let todate = request.query.paid_Todate
      if (checkDateLessThanToDate(fromdate, todate)) {
        // console.log("********************************", from_date, to_date, typeof to_date)
        let Fromdate = parseInt(fromdate)
        let Todate = parseInt(todate)
        dayWisePayments.aggregate([{
          $match: {
            paid_date: {
              '$gte': Fromdate,
              '$lt': Todate
            }
          }
        },
        {
          $group: {
            _id: null,
            payments: {
              $sum: '$payments'
            },
            amount: {
              $sum: '$amount_paid'
            }
          }
        }
        ], function (mongoErr, mongoRes) {
          if (mongoErr) {
            console.log('Error in yesterday counts')
            response.send({
              status: false,
              message: 'error while getting yesterday payments data'
            })
          } else {
            // console.log("****************************")
            // console.log(mongo_res)
            // console.log("****************************")
            response.send({
              status: true,
              message: 'successfully get yesterday payments data',
              data: mongoRes
            })
          }
        })
      } else {
        // Print.error("Dates out of range 1");
        console.log('DATE OUT OF RANGE')
        response.send({
          status: false,
          message: 'Dates out of range'
        })
      }
    }
    // let temp = new Date();
    // let today = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate() - 1);
    // let startDay = today.setHours(0, 0, 0, 0);
    // console.log("starting timer...", startDay);
    // dayWisePayments.find({ paid_date: startDay }, function (mongo_err, mongo_res) {
    //   if (mongo_err) {
    //     console.log('Error in yesterday counts')
    //     response.send({ status: false, message: "error while getting yesterday payments data", mongo_res })
    //   } else {
    //     // console.log("****************************")
    //     // console.log(mongo_res)
    //     // console.log("****************************")
    //     response.send({ status: true, message: "successfully get yesterday payments data", data: mongo_res })
    //   }
    // })
  } catch (eventsexception) {
    console.log('eventsexception', eventsexception)
  }
}

dashboard.sales_transactions_count = function (req, res) {
  merchantModel.findOne({
    mobile: AppConfig.merchant_testing_mobile
  }, function (err, merchantData) {
    if (err) {
      console.log('Error while fetching merchant data', err)
    } else {
      // console.log('merchantData::::::::::::::', merchantData)
      var merchant = null
      if (merchantData != null && merchantData !== undefined) {
        if (Object.getOwnPropertyNames(merchantData).length !== 0) {
          merchant = merchantData._id
        }
      }
      async.parallel([
        function (callback) {
          let matchOb1 = {}
          matchOb1['_id'] = {
            $ne: mongoose.Types.ObjectId(merchant)
          }
          if (req.user.user_type === 'sales_team') {
            matchOb1['created_by'] = mongoose.Types.ObjectId(req.user._id)
          }
          if (req.user.user_type === 'regional_head') {
            matchOb1['created_by_parent'] = mongoose.Types.ObjectId(req.user._id)
          }
          merchantModel.aggregate([{
            $match: matchOb1
          },
          {
            $lookup: {
              'from': 'merchant_overall_details',
              'localField': '_id',
              'foreignField': 'merchant_id',
              'as': 'merchants_trans'
            }
          },
          {
            $unwind: '$merchants_trans'
          },
          {
            $group: {
              _id: null,
              transactions: {
                $sum: '$merchants_trans.transactions'
              },
              trans_amount: {
                $sum: '$merchants_trans.trans_amount'
              },
              paid_amount: {
                $sum: '$merchants_trans.paid_amount'
              },
              payments: {
                $sum: '$merchants_trans.payments'
              },
              npa_percentage: {
                $avg: '$merchants_trans.npa_percentage'
              }
            }
          }
          ],
          function (mongorrErr, mongoRes) {
            if (mongorrErr) console.log('Error in total_transactions_count')
            else if (mongoRes) {
              console.log('mongoRes************************total', mongoRes)
              callback(null, mongoRes)
            } else {
              callback(null, 0)
            }
          })
        },
        function (callback) {
          var today = dateFormat(Date.now(), 'yyyy-mm-dd')
          today = new Date(today)
          today = new Date(today.getTime() - 19800000)
          let matchOb = {}
          if (req.user.user_type === 'sales_team') {
            matchOb['created_by'] = mongoose.Types.ObjectId(req.user._id)
          }
          if (req.user.user_type === 'regional_head') {
            matchOb['created_by_parent'] = mongoose.Types.ObjectId(req.user._id)
          }
          matchOb['transaction_date'] = {
            $gte: today / 1
          }
          matchOb['status'] = {
            $in: [1, 5]
          }
          matchOb['merchant_id'] = {
            $ne: merchant
          }
          transactions.aggregate([{
            $match: matchOb
          }, {
            $group: {
              _id: null,
              total: {
                $sum: 1
              },
              amount: {
                $sum: '$amount'
              }
            }
          }],
          function (mongoErr, mongoRes) {
            if (mongoErr) console.log('Error in today_transactions_count')
            else if (mongoRes) {
              console.log('Today STATUS======>', mongoRes)
              callback(null, mongoRes)
            } else {
              callback(null, 0)
            }
          })
        },
        function (callback) {
          var today = dateFormat(Date.now(), 'yyyy-mm-dd')
          today = new Date(today)
          today = new Date(today.getTime() - 19800000)
          var yesterday = new Date()
          yesterday.setDate(yesterday.getDate() - 1)
          yesterday.setHours(0, 0, 0, 0)
          let matchOb = {}
          if (req.user.user_type === 'sales_team') {
            matchOb['created_by'] = mongoose.Types.ObjectId(req.user._id)
          }
          if (req.user.user_type === 'regional_head') {
            matchOb['created_by_parent'] = mongoose.Types.ObjectId(req.user._id)
          }
          matchOb['transaction_date'] = {
            $gte: (yesterday / 1) - 19800000,
            $lt: today / 1
          }
          matchOb['status'] = {
            $in: [1, 5]
          }
          matchOb['merchant_id'] = {
            $ne: merchant
          }
          transactions.aggregate([{
            $match: matchOb
          }, {
            $group: {
              _id: null,
              total: {
                $sum: 1
              },
              amount: {
                $sum: '$amount'
              }
            }
          }],
          function (mongoErr, mongoRes) {
            if (mongoErr) console.log('Error in yesterday_transactions_count')
            else if (mongoRes) {
              console.log('mongo_res************', mongoRes)
              callback(null, mongoRes)
            } else {
              callback(null, 0)
            }
          })
        },
        function (callback) {
          let reqObj = {}
          if (req.user.user_type === 'sales_team') {
            reqObj['targeted_to'] = mongoose.Types.ObjectId(req.user._id)
            SalesTeamTarget.find(reqObj, function (mongoErr, mongoRes) {
              if (mongoErr) console.log('Error in merchants_count')
              else if (mongoRes.length > 0) {
                // console.log("=====================mongo_res>>>>>>>", mongo_res)
                callback(null, {
                  total_taget: mongoRes[0].target_number
                })
              } else {
                callback(null, {
                  total_target: 0
                })
              }
            }).sort({
              _id: -1
            }).limit(1)
          } else {
            callback(null, {
              total_target: 0
            })
          }
        }
      ], function (err, results) {
        if (err) console.log(err)
        console.log('=====================mongo_res>>>>>>>', results)
        var transArray = []
        var todayTransactions = {}
        var totalTransactions = {}
        var yesterdayTransactions = {}
        var salesTarget = {}
        if (results[0].length > 0) {
          totalTransactions = {
            total_payments: results[0][0].payments,
            total_paid_amt: results[0][0].paid_amount,
            total_transactions: results[0][0].transactions,
            total_transactions_amt: results[0][0].trans_amount,
            npa_percentage: results[0][0].npa_percentage
          }
        } else {
          totalTransactions = {
            total_transactions: 0,
            total_transactions_amt: 0,
            total_payments: 0,
            total_paid_amt: 0,
            npa_percentage: 0
          }
        }
        if (results[1].length > 0) {
          todayTransactions = {
            today_transactions: results[1][0].total,
            today_transactions_amt: results[1][0].amount
          }
        } else {
          todayTransactions = {
            today_transactions: 0,
            today_transactions_amt: 0
          }
        }
        if (results[2].length > 0) {
          yesterdayTransactions = {
            yesterday_transactions: results[2][0].total,
            yesterday_transactions_amt: results[2][0].amount
          }
        } else {
          yesterdayTransactions = {
            yesterday_transactions: 0,
            yesterday_transactions_amt: 0
          }
        }
        if (results[3]) {
          salesTarget = {
            total_tagets: results[3].total_taget
          }
        } else {
          salesTarget = {
            total_tagets: 0
          }
        }
        transArray[0] = totalTransactions
        transArray[1] = todayTransactions
        transArray[2] = yesterdayTransactions
        if (req.user.user_type === 'sales_team') {
          transArray[3] = salesTarget
        }
        console.log('transarray', transArray)
        res.send({
          status: true,
          data: transArray
        })
      })
    }
  })
}
dashboard.getPerfamanceData = function (req, res) {
  // console.log(">>>>>>>>>>", req.query)
  var reqObj = {}
  if (req.query.user_id && req.query.user_id !== 'ALL') {
    reqObj['created_by'] = mongoose.Types.ObjectId(req.query.user_id)
  }
  if (req.query.fromDate && req.query.toDate) {
    let fromDate = req.query.fromDate
    let toDate = req.query.toDate
    reqObj['register_date'] = {
      $gte: fromDate,
      $lte: toDate
    }
  }
  async.parallel([
    function (callback) {
      // console.log('==>>>', reqObj)
      merchantModel.count(reqObj, function (merchantErr, merchantRes) {
        if (merchantErr) console.log('Error in merchants_count')
        else {
          // console.log("********>>>", merchantRes)
          callback(null, {
            total_merchant_count: merchantRes
          })
        }
      })
    },
    function (callback) {
      var reqObj = {}
      if (req.query.user_id && req.query.user_id !== 'ALL') {
        reqObj['created_by'] = mongoose.Types.ObjectId(req.query.user_id)
      }
      if (req.query.fromDate && req.query.toDate) {
        let fromDate = req.query.fromDate
        let toDate = req.query.toDate
        reqObj['onboard_date'] = {
          $gte: fromDate,
          $lte: toDate
        }
      }
      reqObj['merchant_status'] = {
        $in: [1, 2]
      }
      merchantModel.count(reqObj, function (activeMongoErr, activeMongoRes) {
        if (activeMongoErr) console.log('Error in  getting active merchants_count')
        else if (activeMongoRes) {
          callback(null, {
            active_merchant_count: activeMongoRes
          })
        } else {
          callback(null, {
            active_merchant_count: 0
          })
        }
      })
    }
  ], function (finalResultError, finalResult) {
    var merchants = {}
    if (finalResultError) {
      console.log('error while geeting info', finalResultError)
    } else {
      console.log('final_result==>>', finalResult[0].total_merchant_count, finalResult[1], finalResult)
      if (finalResult.length > 0) {
        merchants['merchants'] = finalResult[0].total_merchant_count
        merchants['active_merchants'] = finalResult[1].active_merchant_count
      } else {
        merchants['merchants'] = 0
        merchants['active_merchants'] = 0
      }
      console.log('finalResulttt==>>', merchants)
      res.send({
        final_merchants_result: merchants
      })
    }
  })
}

dashboard.sales_payments_count = function (req, res) {
  merchantModel.findOne({
    mobile: AppConfig.merchant_testing_mobile
  }, function (err, merchantData) {
    if (err) {
      console.log('Error while fetching merchant data', err)
    } else {
      // console.log('merchantData::::::::::::::', merchantData)
      var merchant = null
      if (merchantData != null && merchantData !== undefined) {
        if (Object.getOwnPropertyNames(merchantData).length !== 0) {
          merchant = merchantData._id
        }
      }
      async.parallel([
        function (callback) {
          let matchOb = {}
          if (req.user.user_type === 'sales_team') {
            matchOb['created_by'] = mongoose.Types.ObjectId(req.user._id)
          }
          if (req.user.user_type === 'regional_head') {
            matchOb['created_by_parent'] = mongoose.Types.ObjectId(req.user._id)
          }
          matchOb['status'] = 5
          matchOb['merchant_id'] = {
            $ne: merchant
          }
          transactions.aggregate([{
            $match: matchOb
          }, {
            $group: {
              _id: null,
              total: {
                $sum: 1
              },
              amount: {
                $sum: '$amount'
              }
            }
          }],
          function (mongoErr, mongoRes) {
            if (mongoErr) console.log('Error in today_transactions_count')
            else if (mongoRes) {
              console.log('Today STATUS======>', mongoRes)
              callback(null, mongoRes)
            } else {
              callback(null, 0)
            }
          })
        },
        function (callback) {
          let matchOb = {}
          if (req.user.user_type === 'sales_team') {
            matchOb['created_by'] = mongoose.Types.ObjectId(req.user._id)
          }
          if (req.user.user_type === 'regional_head') {
            matchOb['created_by_parent'] = mongoose.Types.ObjectId(req.user._id)
          }
          matchOb['status'] = 1
          matchOb['merchant_id'] = {
            $ne: merchant
          }
          transactions.aggregate([{
            $match: matchOb
          }, {
            $group: {
              _id: null,
              total: {
                $sum: 1
              },
              amount: {
                $sum: '$amount'
              }
            }
          }],
          function (mongoErr, mongoRes) {
            if (mongoErr) console.log('Error in today_transactions_count')
            else if (mongoRes) {
              console.log('Today STATUS======>', mongoRes)
              callback(null, mongoRes)
            } else {
              callback(null, 0)
            }
          })
        }
        // function (callback) {
        //   var today = dateFormat(Date.now(), 'yyyy-mm-dd')
        //   today = new Date(today)
        //   today = new Date(today.getTime() - 19800000)
        //   var yesterday = new Date()
        //   yesterday.setDate(yesterday.getDate() - 1)
        //   yesterday.setHours(0, 0, 0, 0)

        //   let matchOb = {}
        //   if (req.user.user_type === 'sales_team') {
        //     matchOb['created_by'] = mongoose.Types.ObjectId(req.user._id)
        //   }
        //   if (req.user.user_type === 'regional_head') {
        //     matchOb['created_by_parent'] = mongoose.Types.ObjectId(req.user._id)
        //   }
        //   matchOb['transaction_date'] = {
        //     $gte: (yesterday / 1) - 19800000,
        //     $lt: today / 1
        //   }
        //   matchOb['status'] = 5
        //   matchOb['merchant_id'] = {
        //     $ne: merchant
        //   }
        //   transactions.aggregate([{
        //     $match: matchOb
        //   }, {
        //     $group: {
        //       _id: null,
        //       total: {
        //         $sum: 1
        //       },
        //       amount: {
        //         $sum: '$amount'
        //       }
        //     }
        //   }],
        //   function (mongoErr, mongoRes) {
        //     if (mongoErr) console.log('Error in yesterday_transactions_count')
        //     else if (mongoRes) {
        //       console.log('mongo_res************', mongoRes)
        //       callback(null, mongoRes)
        //     } else {
        //       callback(null, 0)
        //     }
        //   })
        // }
      ], function (err, results) {
        if (err) console.log(err)
        console.log('=====================mongo_res>>>>>>>', results)
        var paymentsArray = []
        var pendingPayments = {}
        var totalPayments = {}
        // var yesterdayPayments = {}
        if (results[0].length > 0) {
          totalPayments = {
            total_payments: results[0][0].total,
            total_payments_amt: results[0][0].amount
          }
        } else {
          totalPayments = {
            total_payments: 0,
            total_payments_amt: 0
          }
        }
        if (results[1].length > 0) {
          pendingPayments = {
            pending_payments: results[1][0].total,
            pending_payments_amt: results[1][0].amount
          }
        } else {
          pendingPayments = {
            pending_payments: 0,
            pending_payments_amt: 0
          }
        }
        // if (results[2].length > 0) {
        //   yesterdayPayments = {
        //     yesterday_payments: results[2][0].total,
        //     yesterday_payments_amt: results[2][0].amount
        //   }
        // } else {
        //   yesterdayPayments = {
        //     yesterday_payments: 0,
        //     yesterday_payments_amt: 0
        //   }
        // }
        paymentsArray[0] = totalPayments
        paymentsArray[1] = pendingPayments
        // paymentsArray[2] = yesterdayPayments
        console.log('paymentsArray', paymentsArray)
        res.send({
          status: true,
          data: paymentsArray
        })
      })
    }
  })
}
dashboard.regional_head_sales_payments_count = function (req, res) {
  var reqObj = {}
  if (req.query.user_id && req.query.user_id !== 'ALL') {
    reqObj['created_by'] = mongoose.Types.ObjectId(req.query.user_id)
  }

  async.parallel([
    function (callback) {
      let matchOb1 = {}
      if (req.query.user_id && req.query.user_id !== 'ALL') {
        matchOb1['created_by'] = mongoose.Types.ObjectId(req.query.user_id)
      }
      merchantModel.aggregate([{
        $match: matchOb1
      },
      {
        $lookup: {
          'from': 'merchant_overall_details',
          'localField': '_id',
          'foreignField': 'merchant_id',
          'as': 'merchants_trans'
        }
      },
      {
        $unwind: '$merchants_trans'
      },
      {
        $group: {
          _id: null,
          transactions: {
            $sum: '$merchants_trans.transactions'
          },
          trans_amount: {
            $sum: '$merchants_trans.trans_amount'
          },
          paid_amount: {
            $sum: '$merchants_trans.paid_amount'
          },
          payments: {
            $sum: '$merchants_trans.payments'
          },
          npa_percentage: {
            $avg: '$merchants_trans.npa_percentage'
          }
        }
      }
      ],
      function (mongorrErr, mongoRes) {
        if (mongorrErr) console.log('Error in total_transactions_count')
        else if (mongoRes) {
          console.log('mongoRes************************total', mongoRes)
          callback(null, mongoRes)
        } else {
          callback(null, 0)
        }
      })
    },
    function (callback) {
      let reqObj = {}
      reqObj['targeted_to'] = mongoose.Types.ObjectId(req.query.user_id)
      SalesTeamTarget.find(reqObj, function (mongoErr, mongoRes) {
        if (mongoErr) console.log('Error in merchants_count')
        else if (mongoRes.length > 0) {
          // console.log("=====================mongo_res>>>>>>>", mongo_res)
          callback(null, {
            total_taget: mongoRes[0].target_number
          })
        } else {
          callback(null, {
            total_taget: 0
          })
        }
      }).sort({
        _id: -1
      }).limit(1)
    }
  ], function (err, results) {
    if (err) console.log(err)
    console.log('=====================mongo_res>>>>>>>', results)
    var finalArray = []
    var totalTransactions = {}
    var salesTarget = {}
    if (results[0].length > 0) {
      totalTransactions = {
        payments: results[0][0].payments,
        paid_amt: results[0][0].paid_amount,
        transactions: results[0][0].transactions,
        transactions_amt: results[0][0].trans_amount,
        npa_percentage: results[0][0].npa_percentage
      }
    } else {
      totalTransactions = {
        payments: 0,
        paid_amt: 0,
        transactions: 0,
        transactions_amt: 0,
        npa_percentage: 0
      }
    }
    if (results[1]) {
      salesTarget = {
        total_tagets: results[1].total_taget
      }
    } else {
      salesTarget = {
        total_tagets: 0
      }
    }
    finalArray[0] = totalTransactions
    finalArray[1] = salesTarget
    console.log('paymentsArray', finalArray)
    res.send({
      status: true,
      data: finalArray
    })
  })
}
