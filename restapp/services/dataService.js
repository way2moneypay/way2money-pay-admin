var DataService = module.exports
var request = require('request')
var config = require('config')
var smsAlertsModel = require('./../models/smsAlertsModel')
var userDetails = require('./../models/userDetails')

/* Ramu */
/* Remove sms data of user */
DataService.removeSmsData = function (apiReq, apiRes) {
  console.log('called')
  // console.log("we are in claim requests", apiReq.body);
  if (!apiReq.body.data) {
    apiRes.send({
      status: false,
      message: 'no data found for deleting sms data'
    })
  }

  let data = {
    data: apiReq.body.data
  }
  console.log('data ', data)

  request.post({
    url: config.sync_sms_remove_url,
    body: data,
    json: true
  }, function (err, res, body) {
    console.log('removed data 1', err, body)
    if (err) {
      apiRes.send({
        status: false,
        message: 'error while deleting sms data'
      })
    } else {
      request.post({
        url: config.process_sms_remove_url,
        body: data,
        json: true
      }, function (err2, res2, body2) {
        console.log('removed data 2', err2, body2)
        if (err2) {
          apiRes.send({
            status: false,
            message: 'error while deleting sms data.'
          })
        } else {
          apiRes.send({
            status: true,
            message: 'Successfully deleted details'
          })
        }
      })
    }
  })
}

/* Ramu */
/* Re predict credit limit and send sms */
DataService.rePredictCreditLimit = function (apiReq, apiRes) {
  console.log('data ', apiReq.body)
  if (!apiReq.body.data || !apiReq.body.data._id || !apiReq.body.data.mid || !apiReq.body.data.mobile || !apiReq.body.data.last_re_predicted_check_date) {
    return apiRes.send({
      status: false,
      message: 'no data found for predicting credit'
    })
  }

  let data = apiReq.body.data
  console.log('predictAmountURL ', config.predictAmountURL)

  let lastRePredictedCheckDate = data.last_re_predicted_check_date
  console.log('last ', lastRePredictedCheckDate, config.DaysForLastRePredictedCheckDate, (lastRePredictedCheckDate + (config.DaysForLastRePredictedCheckDate * 24 * 60 * 60 * 1000)), Date.now())
  /* check is 7 days over for re prediction check or not */
  if (lastRePredictedCheckDate !== -1 && (lastRePredictedCheckDate + (config.DaysForLastRePredictedCheckDate * 24 * 60 * 60 * 1000)) > Date.now()) {
    return apiRes.send({
      status: false,
      data: {},
      message: '7' + ' days is not completed after re prediction check.'
    })
  }

  request.post({
    url: config.predictAmountURL,
    body: data,
    json: true
  }, function (err, res, predictBody) {
    console.log('predict amount response ', err, predictBody)
    if (err || predictBody.status !== 'SUCCESS') {
      if (!err) {
        /* add into user details model */
        userDetails.update({
          '_id': data._id
        }, {
          $set: {
            last_re_predicted_check_date: Date.now()
          }
        }, {
          new: true
        }, function (updatedError, updatedData) {
          apiRes.send({
            status: false,
            data: {},
            message: 'error while getting credit limit. ' + predictBody.message
          })
        })
      } else {
        apiRes.send({
          status: false,
          data: {},
          message: 'error while getting credit limit. ' + predictBody.message
        })
      }
    } else {
      if (predictBody.data.credit_limit && Number(predictBody.data.credit_limit) > 0) {
        /* sending sms of credit limit */

        /* get sms alert data */
        smsAlertsModel.findOne({
          'alert_type': 'prediction_recheck_alert',
          'status': 'active'
        }, function (smsAlertError, smsAlertData) {
          if (smsAlertError) {
            console.log('in error ', smsAlertError)
          } else {
            if (!smsAlertData) {
              apiRes.send({
                status: false,
                data: predictBody.data,
                message: 'Successfully re predict the details. Sms not send because alert is present or inactive'
              })
            }

            /* preparing aler content */
            let alertContent = smsAlertData.alert_content
            let defaultAmount = config.maxAmountForUser

            /* replacing alert content */
            alertContent = alertContent.replace('##amount##', defaultAmount)

            /* preparing data to be send */
            let sendingBody = {
              'mobile': data.mobile,
              'sender_id': smsAlertData.sender_id,
              'alert_content': alertContent
            }

            /* calling sms api */
            request.post({
              url: config.smsURL,
              headers: {
                'Content-Type': 'application/json'
              },
              body: sendingBody,
              json: true
            }, function (error, smsResponse) {
              console.log('sms response', smsResponse.body)

              if (error) {
                console.log('Error from sms api\n', error)
                apiRes.send({
                  status: false,
                  data: predictBody.data,
                  message: 'Successfully re predict the details. Error response from sms api'
                })
              } else if (smsResponse) {
                /* add into user details model */
                userDetails.update({
                  '_id': data._id
                }, {
                  $set: {
                    last_re_predicted_check_date: Date.now()
                  }
                }, {
                  new: true
                }, function (updatedError, updatedData) {
                  console.log('last_re_predicted_check_date updated')
                })

                let responseBody = smsResponse.body
                if (responseBody && responseBody.status_code && responseBody.status_code === 200) {
                  apiRes.send({
                    status: false,
                    data: predictBody.data,
                    message: 'Successfully re predict the details.'
                  })
                } else {
                  apiRes.send({
                    status: false,
                    data: predictBody.data,
                    message: 'Successfully re predict the details. Error response from sms api.'
                  })
                }
              } else {
                console.log('No response from sms api\n')
                apiRes.send({
                  status: true,
                  data: predictBody.data,
                  message: 'Successfully re predict the details. No response from sms api.'
                })
              }
            })
          }
        })
      } else {
        /* add into user details model */
        userDetails.update({
          '_id': data._id
        }, {
          $set: {
            last_re_predicted_check_date: Date.now()
          }
        }, {
          new: true
        }, function (updatedError, updatedData) {
          console.log('last_re_predicted_check_date updated')
          /* no credit limit */
          apiRes.send({
            status: true,
            data: predictBody,
            message: 'Successfully re predict the details. No credit limit for user.'
          })
        })
      }
    }
  })
}

/* Ramu */
/* Re predict credit limit and send sms */
DataService.predictCreditLimit = function (apiReq, apiRes) {
  console.log('hi ', apiReq.body)
  if (!apiReq.body.data) {
    return apiRes.send({
      status: false,
      message: 'no data found for predicting credit'
    })
  }

  let data = apiReq.body.data
  console.log('predictAmountURL ', config.predictAmountURL)

  request.post({
    url: config.predictAmountURL,
    body: data,
    json: true
  }, function (err, res, predictBody) {
    console.log('predict amount response ', err, predictBody)
    if (err || predictBody.status !== 'SUCCESS') {
      apiRes.send({
        status: false,
        data: {},
        message: 'error while getting credit limit. ' + predictBody.message
      })
    } else {
      apiRes.send({
        status: true,
        data: predictBody.data,
        message: 'Successfully predict the details.'
      })
    }
  })
}