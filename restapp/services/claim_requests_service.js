var claimRequestServiceModel = require('../models/claimRequestsModel')
var merchantModel = require('../models/merchantModel')
var TransactionModel = require('../models/userTransactionsModel')
async = require('async')
claimRequestService = module.exports
const mongoose = require('mongoose')
claimRequestService.getClaimRequests = function (apiReq, apiRes) {
  // console.log("we are in claim requests", apiReq.body);
  let findObj = {}
  let requireKeys = {
    'name': 1,
    'brand_name': 1,
    'pending_amt': 1,
    'approved_amt': 1,
    'claimed_amt': 1,
    'mobile': 1
  }
  if (apiReq.body.merchant_id != undefined && apiReq.body.merchant_id != null) {
    findObj = {
      '_id': merchant_id
    }
  }
  merchantModel.find(findObj, requireKeys, {
    skip: apiReq.body.skip,
    limit: apiReq.body.limit
  }, function (claimRequestErr, claimRequestResult) {
    if (claimRequestErr) {
      console.log('there is an error while getting claim_request details', claimRequestErr)
      apiRes.send({
        status: false,
        message: 'error while getting claim_request details'
      })
    } else if (claimRequestResult.length > 0) {
      // console.log("claim_request result",claimRequestResult);
      apiRes.send({
        status: true,
        message: 'claim_request details found',
        'data': claimRequestResult
      })
    } else {
      apiRes.send({
        status: false,
        message: 'claim_request details not found'
      })
    }
  }).sort({
    approved_amt: -1
  })
}
claimRequestService.getClaimsCount = function (apiReq, apiRes) {
  // console.log("we are in claim requests count", apiReq.body);
  var findObj = apiReq.body
  merchantModel.countDocuments(findObj, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      // console.log("count::: ", countResult);
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': countResult
      })
    }
  })
}
claimRequestService.claimRequestSearch = function (apiReq, apiRes) {
  let findObj = {}
  if (apiReq.body.merchant_id != undefined && apiReq.body.merchant_id != null) {
    findObj['merchant_id'] = String(apiReq.body.merchant_id)
  }
  // if (apiReq.body.type != undefined && apiReq.body.type != null) {
  //   findObj[apiReq.body.type] = {
  //     $gte: Number(apiReq.body.fromDate),
  //     $lt: Number(apiReq.body.toDate)
  //   };
  // } else {
  findObj = apiReq.body
  // }
  let requireKeys = {
    'name': 1,
    'brand_name': 1,
    'pending_amt': 1,
    'approved_amt': 1,
    'claimed_amt': 1,
    'mobile': 1
  }

  merchantModel.find(findObj, requireKeys, function (claimRequestErr, claimRequestResult) {
    if (claimRequestErr) {
      apiRes.send({
        'status': false,
        'message': claimRequestErr
      })
    } else if (claimRequestResult.length > 0) {
      // console.log("claim request search result is ",claimRequestResult);
      apiRes.send({
        status: true,
        message: 'transaction details found',
        data: claimRequestResult
      })
    } else {
      apiRes.send({
        'status': false,
        'message': 'details not found'
      })
    }
  }).sort({
    approved_amt: -1
  })
}
claimRequestService.merchantClaim = function (apiReq, apiRes) {
  let findObj = {}
  // console.log("find obj amount", apiReq.body.approved_amt);
  findObj._id = apiReq.body.merchant_id
  merchantModel.findOne(findObj, function (merchantClaimErr, merchantClaimResult) {
    if (merchantClaimErr) {
      console.log('There is a error in claim merchants', merchantClaimErr)
      apiRes.send({
        'status': false,
        'message': merchantClaimErr
      })
    } else if (merchantClaimResult) {
      if (merchantClaimResult.approved_amt != undefined && merchantClaimResult.approved_amt != null) {
        if (merchantClaimResult.approved_amt < apiReq.body.approved_amt) {
          apiRes.send({
            status: false,
            message: 'insufficient funds for this merchant'
          })
        } else {
          merchantModel.update(findObj, {
            $inc: {
              'approved_amt': -apiReq.body.approved_amt,
              'claimed_amt': apiReq.body.approved_amt
            }
          }, function (amountUpdateErr, amountUpdateResult) {
            if (amountUpdateErr) {
              console.log('there is an error in updating amount', amountUpdateErr)
              apiRes.send({
                status: false,
                message: 'Amounts not updated in merchants'
              })
            } else {
              console.log('amounts updated successfully in merchants')
              // let findObj1 = {},
              let insertObj = {}
              var merchant_id = mongoose.Types.ObjectId(apiReq.body.merchant_id)
              insertObj.merchant_id = merchant_id
              // findObj1.accepted_date = apiReq.body.accepted_date;
              insertObj.accepted_date = apiReq.body.accepted_date
              insertObj.accepted_by = apiReq.body.accepted_by
              insertObj.ref_id = apiReq.body.claimRefId
              insertObj.amount = apiReq.body.approved_amt
              insertObj.brand_name = apiReq.body.brand_name
              insertObj.claim_status = 1
              // console.log("find object",findObj1);
              console.log('insert object', insertObj)
              var claimRequestService = new claimRequestServiceModel(insertObj)
              claimRequestService.save(function (claimDetailsErr, claimDetailsResult) {
                if (claimDetailsErr) {
                  console.log('there is an error in updating claim details', claimDetailsErr)
                  merchantModel.update(findObj, {
                    $inc: {
                      'approved_amt': apiReq.body.approved_amt,
                      'claimed_amt': -apiReq.body.approved_amt
                    }
                  }, function (amountUpdateErr, amountUpdateResult) {
                    if (amountUpdateErr) {
                      console.log('there is an error in Reverting amount', amountUpdateErr)
                      apiRes.send({
                        status: false,
                        message: 'Claim details not updated Amounts not Reverted in merchants'
                      })
                    } else {
                      console.log('amounts Reverted successfully in merchants')
                      apiRes.send({
                        status: false,
                        message: 'Claim details not updated  and Amount Reverted successfully'
                      })
                    }
                  })
                } else {
                  // console.log("saved result",claimDetailsResult);
                  console.log('amount and Claim Details are updated Successfully')
                  var findObj1 = {}
                  findObj1.merchant_id = merchant_id
                  findObj1.m_status = 6
                  let addToSetObj = {
                    trans_details: {
                      m_status: 8,
                      date: Date.now()
                    }
                  }
                  TransactionModel.updateMany(findObj1, {
                    $set: {
                      m_status: 8
                    },
                    $addToSet: addToSetObj
                  },
                  function (transErr, transResult) {
                    if (transErr) {
                      console.log('there is an error while updating transaction status 6', transErr)
                      merchantModel.update(findObj, {
                        $inc: {
                          'approved_amt': apiReq.body.approved_amt,
                          'claimed_amt': -apiReq.body.approved_amt
                        }
                      }, function (amountUpdateErr, amountUpdateResult) {
                        if (amountUpdateErr) {
                          console.log('there is an error in Reverting amount', amountUpdateErr)
                          apiRes.send({
                            status: false,
                            message: 'Transaction status 6 not updated Amounts not Reverted in merchants'
                          })
                        } else {
                          console.log('amounts Reverted successfully in merchants')
                          apiRes.send({
                            status: false,
                            message: 'Transaction status 6 not updated  and Amount Reverted successfully'
                          })
                        }
                      })
                    } else {
                      console.log('first transaction  status change', transResult)
                      let findObj2 = {}
                      findObj2.merchant_id = merchant_id
                      findObj2.m_status = 7
                      let addToSetObj = {
                        trans_details: {
                          m_status: 9,
                          date: Date.now()
                        }
                      }
                      TransactionModel.updateMany(findObj1, {
                        $set: {
                          m_status: 9
                        },
                        $addToSet: addToSetObj
                      }, function (transErr2, transResult2) {
                        if (transErr2) {
                          console.log('there is an error while updating transaction status 7', transErr2)
                          merchantModel.update(findObj, {
                            $inc: {
                              'approved_amt': apiReq.body.approved_amt,
                              'claimed_amt': -apiReq.body.approved_amt
                            }
                          }, function (amountUpdateErr, amountUpdateResult) {
                            if (amountUpdateErr) {
                              console.log('there is an error in Reverting amount', amountUpdateErr)
                              apiRes.send({
                                status: false,
                                message: 'Transaction status 7 not updated Amounts not Reverted in merchants'
                              })
                            } else {
                              console.log('amounts Reverted successfully in merchants')
                              apiRes.send({
                                status: false,
                                message: 'Transaction Status 7 not updated  and Amount Reverted successfully'
                              })
                            }
                          })
                        } else {
                          console.log('second transaction  status change', transResult2)
                          apiRes.send({
                            status: true,
                            message: 'amount,transactions and Claim Details are updated Successfully'
                          })
                        }
                      })
                    }
                  })
                }
              })
            }
          })
        }
      } else {
        apiRes.send({
          status: false,
          message: 'amount details not found for this merchant'
        })
      }
    } else {
      apiRes.send({
        status: false,
        message: 'merchant details not found'
      })
    }
  })
}
claimRequestService.getMerchantId = function (apiReq, apiRes) {
  // console.log("we are at get merchant id");
  let emailId = apiReq.params.id
  merchantModel.findOne({
    email: emailId
  }, function (findErr, findResult) {
    if (findErr) {
      console.log('error found while getting merchant id from email', findErr)
      apiRes.send({
        status: false,
        message: 'merchant details not found'
      })
    } else if (findResult) {
      // console.log("find result", findResult);
      apiRes.send({
        status: true,
        message: ' merchant email details found',
        data: findResult.name
      })
    } else {
      apiRes.send({
        status: false,
        message: 'merchant id not found'
      })
    }
  })
}
