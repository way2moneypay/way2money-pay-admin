var offersService = module.exports
// var request = require('request')
// var parallel = require('run-parallel')
const MobileOffersModel = require('../models/MobileOffersModel')
const merchantsModel = require('../models/merchantModel')

offersService.getoffersList = function (apiReq, apiRes) {
  console.log('**********************getoffersList', apiReq.body)
  MobileOffersModel.find({}, {}, {
    skip: apiReq.body.skip,
    limit: apiReq.body.limit
  }, (errFind, alertsFound) => {
    if (errFind) {
      // console.log(errFind);
      apiRes.send({
        success: false,
        message: 'Error while fetching offers'
      })
    } else {
      // console.log(alertsFound)
      apiRes.send({
        success: true,
        message: 'offers successfully fetched.',
        offersList: alertsFound
      })
    }
  })
}
offersService.getCount = function (apiReq, apiRes) {
  console.log('getCount==>>>')
  MobileOffersModel.countDocuments({}, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      // console.log('count::: ', countResult)
      apiRes.send({
        'status': true,
        'message': 'count fetched successfully',
        'data': countResult
      })
    }
  })
}
offersService.editOfferList = function (apiReq, apiRes) {
  var findObj = {}
  var editOfferId = apiReq.params.editOfferId !== undefined ? apiReq.params.editOfferId : null
  if (editOfferId) {
    findObj['_id'] = editOfferId
    // console.log('==>>>editOfferList', findObj)
    MobileOffersModel.find(findObj, (errFind, alertsFound) => {
      if (errFind) {
        apiRes.send({
          success: false,
          message: 'Error while fetching sms alerts'
        })
      } else {
        apiRes.send({
          success: true,
          message: 'SMS Alerts successfully fetched.',
          offersList: alertsFound
        })
      }
    })
  } else {
    apiRes.send({
      success: false,
      message: 'Error while fetching sms alerts'
    })
  }
}

offersService.changeOffersStatus = function (apiReq, apiRes) {
  // console.log(apiReq.query)
  var queryDetails = {
    _id: (apiReq.query.premission_id !== undefined && apiReq.query.premission_id != null) ? apiReq.query.premission_id : 0,
    filter: {
      $set: {
        offers_status: apiReq.query.offers_status
      }
    }
  }
  MobileOffersModel.findByIdAndUpdate({
    _id: queryDetails._id
  }, queryDetails.filter, function (statusUpdateError, statusUpdate) {
    if (statusUpdateError) {
      apiRes.send({
        success: false,
        message: 'Error occurred in  while updating  status'
      })
    }
    if (statusUpdate) {
      // console.log('Successfully updated the data')
      apiRes.send({
        success: true,
        message: 'Status Update successfully',
        data: statusUpdate
      })
    } else {
      apiRes.send({
        success: false,
        message: 'Error occurred in  while updating  offers_status'
      })
    }
  })
}

offersService.addOffer = function (apiReq, apiRes) {
  // console.log('version ::::::::::::::', apiReq.body)

  if (apiReq.body.offer_id !== '0') {
    merchantsModel.findOne({
      _id: apiReq.body.merchant_id
    }, function (mongoErr, mongoRes) {
      if (mongoErr) {
        apiRes.send({
          status: false,
          message: 'Error while finding merchant'
        })
      } else {
        let reqObj = {
          merchant_id: apiReq.body.merchant_id,
          offers_status: apiReq.body.status,
          brand_name: apiReq.body.brand_name,
          merchant_logo: mongoRes.logo_url,
          expiry_date_to: apiReq.body.toDate,
          expiry_date_from: apiReq.body.fromDate,
          offer_details: apiReq.body.offer_percentage,
          address: mongoRes.company_address.locality + ',' + mongoRes.company_address.city,
          brand: apiReq.body.brand
        }
        // console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!', reqObj)
        // console.log('in else Address', mongoRes.company_address.locality, mongoRes.company_address.city)
        MobileOffersModel.findByIdAndUpdate({ _id: apiReq.body.offer_id }, { $set: reqObj }, function (updateErr, updateResult) {
          if (updateErr) {
            console.log('Error while fetching allversions data', updateErr)
            apiRes.send({
              status: false,
              error: updateErr
            })
          } else {
            apiRes.send({
              status: true,
              data: updateResult
            })
          }
        })
      }
    })
  } else {
  // console.log('add ::::::::::::::', apiReq.body)
    merchantsModel.findOne({
      _id: apiReq.body.merchant_id
    }, function (mongoErr, mongoRes) {
      if (mongoErr) {
        apiRes.send({
          status: false,
          message: 'Error while finding merchant'
        })
      } else {
        let reqObj = {
          merchant_id: apiReq.body.merchant_id,
          offers_status: apiReq.body.status,
          brand_name: apiReq.body.brand_name,
          merchant_logo: mongoRes.logo_url,
          expiry_date_to: apiReq.body.toDate,
          expiry_date_from: apiReq.body.fromDate,
          offer_details: apiReq.body.offer_percentage,
          address: mongoRes.company_address.locality + ',' + mongoRes.company_address.city,
          brand: apiReq.body.brand
        }
        // console.log(reqObj)
        // console.log('in else Address', mongoRes.company_address.locality, mongoRes.company_address.city)
        var newOffer = new MobileOffersModel(reqObj)
        newOffer.save(function (updateErr, updateResult) {
          if (updateErr) {
            console.log('Error while fetching allversions data', updateErr)
            apiRes.send({
              status: false,
              error: updateErr
            })
          } else {
            apiRes.send({
              status: true,
              data: updateResult
            })
          }
        })
      }
    })
  }
}

offersService.editOffer = function (apiReq, apiRes) {
  // console.log(apiReq.body._id)
  MobileOffersModel.findOne({ _id: apiReq.body._id }, function (errUpdate, alertUpdated) {
    if (errUpdate) {
      apiRes.send({ success: false, message: 'Something went wrong while updating push alert.' })
    } else {
      // console.log('edut iff', alertUpdated)
      apiRes.send({ success: true, data: alertUpdated })
    }
  })
}
