
const UserTransactionsModel = require('../models/userTransactionsModel')
const merchantStatsModel = require('../models/merchantDaywiseDetails')
const daywiseTransactionsModel = require('../models/daywiseTransactions')
const MerchantOverallDetailsModel = require('../models/merchantOverallDetailsModel')

const async = require('async')
var dateFormat = require('dateformat')
module.exports = {
  getMerchantWiseTransactions: () => {
    // console.log('in merchant wise total transactions')
    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)

    UserTransactionsModel.find({ transaction_date: { $lt: (yesterday / 1) - 19800000 }, $or: [{ status: 1 }, { status: 5 }] }, function (error, transactions) {
      if (error) {
        console.log(error)
      } else {
        async.forEach(transactions, (transaction, callbackForEach) => {
          if (transaction.transaction_date !== undefined && transaction.merchant_id !== undefined && transaction.amount !== undefined) {
            var transactionDate = new Date(transaction.transaction_date)
            var date = transactionDate.setHours(23, 59, 59, 999)
            date = (new Date(date).getTime() - 19800000)
            transactionDate = transactionDate.setHours(0, 0, 0, 0)
            transactionDate = (new Date(transactionDate).getTime() - 19800000)
            //  transaction_date = transaction_date/1;
            UserTransactionsModel.distinct('user_id',
              { merchant_id: transaction.merchant_id, transaction_date: { $gte: transactionDate, $lte: date }, $or: [{ status: 1 }, { status: 5 }] }, function (error, usersCount) {
                if (error) {
                  console.log(error)
                } else {
                  var incObj = {}
                  var fAmount = transaction.amount
                  if (transaction.merchant_commission !== undefined) {
                    fAmount = transaction.amount - (transaction.amount * transaction.merchant_commission) / 100
                  }
                  if (transaction.status === 5) {
                    incObj = {
                      payments: 1,
                      paid_amount: transaction.amount,
                      transactions: 1,
                      trans_amount: transaction.amount,
                      amount: fAmount
                    }
                  } else {
                    incObj = {
                      transactions: 1,
                      trans_amount: transaction.amount,
                      amount: fAmount
                    }
                  }
                  merchantStatsModel.findOneAndUpdate({
                    merchant_id: transaction.merchant_id,
                    trans_date: transactionDate
                  }, { $set: { users: usersCount.length },
                    $inc: incObj
                  }, {
                    upsert: true
                  },
                  function (updateError, updateResult) {
                    // console.log(updateResult)
                    if (updateError) {
                      console.log('Error while updating merchant trans stats', updateError, transaction)
                    } else {
                      // console.log("sucess")
                      callbackForEach()
                    }
                  })
                }
              })
          } else {
            callbackForEach()
          }
        }, (success) => {
          console.log('Updated successfully')
        })
      }
    })
  },

  daywiseTransactions: () => {
    console.log('in daywise transactions::::::::::::')
    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)
    var today = dateFormat(Date.now(), 'yyyy-mm-dd')
    today = new Date(today)
    today = new Date(today.getTime() - 19800000)

    UserTransactionsModel.aggregate([
      {
        $match: {
          transaction_date: {
            $gte: (yesterday / 1) - 19800000,
            $lt: today / 1
          },
          $or: [{ status: 1 }, { status: 5 }]
        }
      }, {
        $group: {
          _id: null,
          trans_amount: { $sum: '$amount' },
          transactions: { $sum: 1 }
        }
      }
    ], function (err, transactions) {
      if (err) {
        console.log('Error ', err)
      } else {
        if (transactions.length > 0) {
          // transactions[0]['trans_date'] = (yesterday / 1) - 19800000;
          console.log('daywise::::::::::::::')
          async.forEach(transactions, (transaction, callbackForEach) => {
            let transobj = {
              trans_amount: transaction.trans_amount,
              transactions: transaction.transactions
            }
            daywiseTransactionsModel.findOneAndUpdate({ trans_date: (yesterday / 1) - 19800000 }, { $set: transobj }, { upsert: true }, function (error, saved) {
              if (error) {
                console.log('Error while saving data in daywise Transacions', error)
              } else {
                console.log('saved into daywise transactions')
                callbackForEach()
              }
            })
          }, (success) => {
            console.log('Updated successfully')
          })
        }
      }
    })
  },
  merchantwiseDayTransactions: () => {
    console.log('day wise merchant transactions')

    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)
    var today = dateFormat(Date.now(), 'yyyy-mm-dd')
    today = new Date(today)
    today = new Date(today.getTime() - 19800000)

    UserTransactionsModel.find(
      {
        transaction_date: {
          $gte: (yesterday / 1) - 19800000,
          $lt: today / 1
        },
        $or: [{ status: 1 }, { status: 5 }]
      }, function (error, transactions) {
        if (error) {
          console.log(error)
        } else {
          if (transactions.length > 0) {
            async.forEach(transactions, (transaction, callbackForEach) => {
              // console.log('merchant wise:::::', transaction)
              if (transaction.transaction_date !== undefined && transaction.merchant_id !== undefined && transaction.amount !== undefined) {
                var transactionDate = new Date(transaction.transaction_date)
                var date = transactionDate.setHours(23, 59, 59, 999)
                date = (new Date(date).getTime() - 19800000)
                transactionDate = transactionDate.setHours(0, 0, 0, 0)
                transactionDate = (new Date(transactionDate).getTime() - 19800000)
                //  transaction_date = transaction_date/1;
                UserTransactionsModel.distinct('user_id',
                  { merchant_id: transaction.merchant_id, transaction_date: { $gte: transactionDate, $lte: date }, $or: [{ status: 1 }, { status: 5 }] }, function (error, usersCount) {
                    if (error) {
                      console.log(error)
                    } else {
                      var incObj = {}
                      var fAmount = transaction.amount
                      if (transaction.merchant_commission !== undefined) {
                        fAmount = transaction.amount - (transaction.amount * transaction.merchant_commission) / 100
                      }
                      if (transaction.status === 5) {
                        incObj = {
                          payments: 1,
                          paid_amount: transaction.amount,
                          transactions: 1,
                          trans_amount: transaction.amount,
                          amount: fAmount
                        }
                      } else {
                        incObj = {
                          transactions: 1,
                          trans_amount: transaction.amount,
                          amount: fAmount
                        }
                      }
                      merchantStatsModel.findOneAndUpdate({
                        merchant_id: transaction.merchant_id,
                        trans_date: transactionDate
                      }, { $set: { users: usersCount.length },
                        $inc: incObj
                      }, {
                        upsert: true
                      },
                      function (updateError, updateResult) {
                        // console.log(updateResult)
                        if (updateError) {
                          console.log('Error while updating merchant trans stats', updateError, transaction)
                        } else {
                          // console.log("sucess")
                          callbackForEach()
                        }
                      })
                    }
                  })
              } else {
                callbackForEach()
              }
            }, (success) => {
              console.log('Updated successfully')
            })
          }
        }
      })
  },

  merchantNpa: () => {
    var today = dateFormat(Date.now(), 'yyyy-mm-dd')
    today = new Date(today)
    today = new Date(today.getTime() - 19800000)

    UserTransactionsModel.aggregate([
      {
        $match: {
          transaction_date: {
            $lt: today / 1
          },
          $or: [{ status: 1 }, { status: 5 }]
        }
      }, {
        $group: {
          _id: '$merchant_id',
          trans_amount: { $sum: '$amount' },
          transactions: { $sum: 1 }
        }
      }
    ], function (err, transactionsData) {
      if (err) {
        console.log('Error ', err)
      } else {
        console.log(transactionsData)
        if (transactionsData.length > 0) {
          async.forEach(transactionsData, (transaction, callbackForEach) => {
            UserTransactionsModel.aggregate([
              {
                $match: {
                  merchant_id: transaction._id,
                  transaction_date: {
                    $lt: today / 1
                  },
                  status: 5
                }
              }, {
                $group: {
                  _id: null,
                  paid_amount: { $sum: '$amount' },
                  payments: { $sum: 1 }
                }
              }
            ], function (err, paymentData) {
              if (err) {
                console.log('Error ', err)
              } else {
                if (paymentData.length > 0) {
                  let npaObj = {
                    transactions: transaction.transactions,
                    trans_amount: transaction.trans_amount,
                    payments: paymentData[0].payments,
                    paid_amount: paymentData[0].paid_amount,
                    npa_percentage: Number((((transaction.trans_amount - paymentData[0].paid_amount) / transaction.trans_amount) * 100).toFixed(2))
                  }
                  MerchantOverallDetailsModel.findOneAndUpdate({ merchant_id: transaction._id }, { $set: npaObj }, { upsert: true }, function (updateErr, updateRes) {
                    if (updateErr) {
                      console.log(updateErr)
                      callbackForEach()
                    } else {
                      callbackForEach()
                    }
                  })
                } else {
                  let npaObj = {
                    transactions: transaction.transactions,
                    trans_amount: transaction.trans_amount,
                    payments: 0,
                    paid_amount: 0,
                    npa_percentage: Number((((transaction.trans_amount) / transaction.trans_amount) * 100).toFixed(2))
                  }
                  MerchantOverallDetailsModel.findOneAndUpdate({ merchant_id: transaction._id }, { $set: npaObj }, { upsert: true }, function (updateErr, updateRes) {
                    if (updateErr) {
                      console.log(updateErr)
                      callbackForEach()
                    } else {
                      callbackForEach()
                    }
                  })
                }
              }
            })
          })
        }
      }
    })
  }
}
