/* Push Notifications Service initialization */
var Exports = module.exports;
var PushNotificationModel = require("./../models/PushNotificationsModel");
var PushAlertsModel = require("./../models/PushAlertsModel");
var PushNotificationsConfig = require("./../config/PushNotificationsConfig");
var UserDetailsModel = require("./../models/userDetails");
Exports.storeNotification = function (user_id, template, replace, pn_id, os, version, callback) {
  if ((!user_id && (!os || !pn_id)) || !template || !replace) {
    console.log("bad params");
    return callback(true, "BAD-PARAMS", "No required params found.", null);
  }
  console.log("################", user_id, template, replace, pn_id, os, version);
  console.log("1");
  Exports.getPushNotificationContent(template, replace, false, function (template_error, template_code, template_message, template_content) {
    console.log("2");
    if (template_error) {
      console.log(template_message);
      return callback(true, template_code, template_message, null);
    }
    Exports.userData(user_id, !user_id, (profile_error, profile_code, profile_message, user_data) => {
      console.log("3");
      if (profile_error) {
        console.log("profile_message::", profile_message);
        return callback(true, profile_code, profile_message, null);
      }
      if (!user_data && user_id) {
        return callback(true, "USER-NOT-FOUND", "No user data found with " + user_id, null);
      }
      if (!user_data) user_data = {};
      let PushNotificationData = new PushNotificationModel();
      PushNotificationData.user_id = user_id;
      PushNotificationData.pn_id = pn_id ? pn_id : user_data.pn_id;
      PushNotificationData.os = os ? os : user_data.pn_id_source;
      PushNotificationData.version = version ? version : user_data.version;
      PushNotificationData.content = template_content;
      PushNotificationData.save((push_notification_error, push_notification_data) => {
        if (push_notification_error) {
          console.log(push_notification_error);
          return callback(true, "DB-ERROR", "Internal Server Error", 500);
        }
        console.log("push_notification_data :: ", push_notification_data);
        return callback(null, "Successfully stored push notification.", "SUCCESS", push_notification_data);
      });
    });
  });
};


Exports.getPushNotificationContent = function (template, replace_data, skip_fn, callback) {
  if (skip_fn) {
    return callback(null, null, null, null);
  }
  let alert_filter = {};
  alert_filter.status = "active";
  alert_filter.alert_name = template;
  // return App
  PushAlertsModel.findOne(alert_filter, function (push_notification_error, push_notification_data) {
    if (push_notification_error) {
      console.error("push notification getting error :: ", push_notification_error);
      return callback(true, "DB-ERROR", "Error while getting push notifications data.", null);
    }
    let template_data = push_notification_data;
    if (!push_notification_data) {
      template_data = PushNotificationsConfig.templates[template];
    }
    if (!template_data) {
      return callback(true, "NOT-FOUND", "No push notification data found with " + template, null);
    }
    let content = template_data.alert_content;
    Object.keys(replace_data).forEach(function (obj) {
      content = content.replace(new RegExp('##' + obj + '##', 'g'), replace_data[obj]);
    });

    return callback(null, null, null, content);

  });
};

Exports.userData = function (user_id, skip_fn, callback) {
  if (skip_fn) return callback(null, null, null, null);
  console.log("came to user request.");
  let user_filter = {
    _id: user_id
  };

  UserDetailsModel.findOne(user_filter, function (user_data_error, user_data) {
    if (user_data_error) {
      console.error("user_data getting error :: ", user_data_error);
      return callback(true, "DB-ERROR", "Error while getting user data.", null);
    }
    console.log("user_data:::::::::::", user_data);
    return callback(null, null, null, user_data);
  });
};


