/* User related Models */
var userDetailsModel = require('../models/userDetails')
var userDevicesModel = require('../models/userDevicesModel')
var userPaymentsModel = require('../models/userPaymentsModel')
var userTransactionsModel = require('../models/userTransactionsModel')
var userPaymentPendingTransactionsModel = require('../models/userPaymentPendingTransactions')
var userMobileOTPmodel = require('../models/userMobileOTPmodel')

var mobileOTPmodel = require('../models/mobileOTPmodel')
var refundTransactionsModel = require('../models/refundTransactionsModel')
var authenticateInformationModel = require('../models/AuthenticateInformationModel')
// Merchants related Models
var merchantModel = require('../models/merchantModel')
var merchantStats = require('../models/merchantDaywiseDetails')
var merchantCommissions = require('../models/merchantComissionModel')
var merchantClaims = require('../models/claimRequestsModel')
var merchantOTPmodel = require('../models/merchantOTPs')

var async = require('async')

clearUserService = module.exports
clearUserService.clearUser = function (apiReq, apiRes) {
  queries = []
  userDetailsModel.findOne({ mobile_number: apiReq.body.mobile }, function (user_details_err, user_details) {
    if (user_details_err) {
      res.send({ status: 'fail', message: 'Error In UserDetails' })
    } else {
      if (user_details != null && user_details != undefined) {
        queries.push(function (cb) {
          userDevicesModel.findOneAndRemove({ user_id: user_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          userPaymentsModel.findOneAndRemove({ user_id: user_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          userTransactionsModel.findOneAndRemove({ user_id: user_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          userPaymentPendingTransactionsModel.findOneAndRemove({ user_id: user_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          userMobileOTPmodel.findOneAndRemove({ user_id: user_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          mobileOTPmodel.findOneAndRemove({ mobile_number: user_details.mobile_number
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          refundTransactionsModel.findOneAndRemove({ user_id: user_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          authenticateInformationModel.findOne({ user_id: user_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })

        async.parallel(queries, function (err, results) {
          if (err) {
            apiRes.send({ status: 'fail', message: 'error in async parallel' })
          } else {
            userDetailsModel.findOneAndRemove({ _id: user_details._id }, function (error, user_data) {
              if (error) {
                apiRes.send({ status: 'fail', message: 'error while removing user Details' })
              } else {
                console.log('Successfully Removed the User Details...... ', apiReq.body.mobile)
                apiRes.send({ status: 'success' })
              }
            })
          }
        })
      } else {
        apiRes.send({ status: 'fail', message: 'Merchant Information not found' })
      }
    }
  })
}

clearUserService.clearMerchant = function (apiReq, apiRes) {
  queries = []
  merchantModel.findOne({ mobile: apiReq.body.mobile }, function (merchant_err, merchant_details) {
    if (merchant_err) {
      res.send({ status: 'fail', message: 'Error while fetching merchant Details' })
    } else {
      if (merchant_details != null && merchant_details != undefined) {
        queries.push(function (cb) {
          merchantStats.findOneAndRemove({ merchant_id: merchant_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          merchantCommissions.findOneAndRemove({ merchant_id: merchant_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          merchantClaims.findOneAndRemove({ merchant_id: merchant_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          merchantOTPmodel.findOneAndRemove({ mobile_number: merchant_details.mobile
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })

        queries.push(function (cb) {
          mobileOTPmodel.findOneAndRemove({ mobile_number: merchant_details.mobile
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          refundTransactionsModel.findOneAndRemove({ merchant_id: merchant_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        queries.push(function (cb) {
          authenticateInformationModel.findOneAndRemove({ merchant_id: merchant_details._id
          }).exec(function (err, docs) {
            if (err) {
              throw cb(err)
            }
            cb(null, 'yes')
          })
        })
        async.parallel(queries, function (async_err, results) {
          if (async_err) {
            apiRes.send({ status: 'fail', message: 'error in async parallel' })
          } else {
            merchantModel.findOneAndRemove({ _id: merchant_details._id }, function (error, merchant_data) {
              if (error) {
                apiRes.send({ status: 'fail', message: 'error while removing merchant Details' })
              } else {
                console.log('Successfully Removed the Merchant Details...... ', apiReq.body.mobile)
                apiRes.send({ status: 'success' })
              }
            })
          }
        })
      } else {
        apiRes.send({ status: 'fail', message: 'Merchant Information not found' })
      }
    }
  })
}
