const userPaymentsModel = require('../models/userPaymentsModel')
const dayWisePaymentsModel = require('../models/dayWisePayments')
const userDetailsModel = require('../models/userDetails')
const dayWiseUsersModel = require('../models/daywiseUsers')
const async = require('async')
var dateFormat = require('dateformat')
module.exports = {
  getPayments: () => {
    console.log('In total payments:::::::::::')
    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)
    userPaymentsModel.find({
      paid_date: { $lt: (yesterday / 1) - 19800000 }, paid_status: 1 }, function (error, payments) {
      if (error) {
        console.log(error)
      } else {
        async.forEach(payments, (payment, callbackForEach) => {
          if (payment.paid_date !== undefined && payment.amount_paid !== undefined) {
            var paidd_date = new Date(payment.paid_date)
            paidd_date = paidd_date.setHours(0, 0, 0, 0)
            paidd_date = (new Date(paidd_date).getTime() - 19800000)

            dayWisePaymentsModel.findOneAndUpdate({
              paid_date: paidd_date
            }, {
              $inc: {
                payments: 1,
                amount_paid: payment.amount_paid
              }
            }, {
              upsert: true
            },
            function (updateError, updateResult) {
              // console.log(updateResult)
              if (updateError) {
                console.log('Error while updating daywise payments', updateError)
              } else {
                // console.log("sucess")
                callbackForEach()
              }
            })
          } else {
            callbackForEach()
          }
        }, (success) => {
          console.log('Updated successfully')
        })
      }
    })
  },
  dayWisePayments: () => {
    console.log('in daywise payments:::::::::::')
    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)
    var today = dateFormat(Date.now(), 'yyyy-mm-dd')
    today = new Date(today)
    today = new Date(today.getTime() - 19800000)
    date = Date.now()

    userPaymentsModel.aggregate([
      {
        $match: {
          paid_date: {
            $gte: (yesterday / 1) - 19800000,
            $lt: today / 1
          },
          paid_status: 1
        }
      }, {
        $group: {
          _id: null,
          amount_paid: { $sum: '$amount_paid' },
          payments: { $sum: 1 }
        }
      }
    ], function (err, payments) {
      if (err) {
        console.log('Error ', err)
      } else {
        if (payments.length > 0) {
          // transactions[0]['trans_date'] = (yesterday / 1) - 19800000;
          console.log('daywise::::::::::::::')
          async.forEach(payments, (payment, callbackForEach) => {
            paymentsobj = {
              amount_paid: payment.amount_paid,
              payments: payment.payments
            }
            dayWisePaymentsModel.findOneAndUpdate({ paid_date: (yesterday / 1) - 19800000 }, { $set: paymentsobj }, { upsert: true }, function (error, saved) {
              if (error) {
                console.log('Error while saving data in daywise Payments', error)
              } else {
                console.log('saved into daywise payments')
                callbackForEach()
              }
            })
          }, (success) => {
            console.log('Updated successfully')
          })
        }
      }
    })
  },
  getUsers: () => {
    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)
    userDetailsModel.find({
      register_date: { $lt: (yesterday / 1) - 19800000 } }, function (error, users) {
      if (error) {
        console.log(error)
      } else {
        async.forEach(users, (user, callbackForEach) => {
          if (user.register_date !== undefined) {
            var registeredDate = new Date(user.register_date)
            registeredDate = registeredDate.setHours(0, 0, 0, 0)
            registeredDate = (new Date(registeredDate).getTime() - 19800000)

            dayWiseUsersModel.findOneAndUpdate({
              register_date: registeredDate
            }, {
              $inc: {
                users: 1
              }
            }, {
              upsert: true
            },
            function (updateError, updateResult) {
              if (updateError) {
                console.log('Error while updating daywise payments', updateError)
              } else {
                callbackForEach()
              }
            })
          } else {
            callbackForEach()
          }
        }, (success) => {
          console.log('Updated successfully')
        })
      }
    })
  },
  dayWiseUsers: () => {
    console.log('in daywise payments:::::::::::')
    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)
    var today = dateFormat(Date.now(), 'yyyy-mm-dd')
    today = new Date(today)
    today = new Date(today.getTime() - 19800000)

    userDetailsModel.count({ register_date: {
      $gte: (yesterday / 1) - 19800000,
      $lt: today / 1
    } }, function (err, usersCount) {
      if (err) {
        console.log('Error ', err)
      } else {
        dayWiseUsersModel.findOneAndUpdate({ register_date: (yesterday / 1) - 19800000 }, { $set: { users: usersCount } }, { upsert: true }, function (error, saved) {
          if (error) {
            console.log('Error while saving data in daywise Payments', error)
          } else {
            console.log('saved into daywise payments')
          }
        })
      }
    })
  }

}
