var merchantTransCycle = require('../models/merchantTransCycle')
var userTransactions = require('../models/userTransactionsModel')
var merchant_details = require('../models/merchantModel')
var NotificationUtils = require('../utils/NotificationUtils')
var async = require('async')
var transactionCycleService = module.exports
var mongoose = require('mongoose')
transactionCycleService.getTransactions = function (apiReq, apiRes) {
  console.log('we are in view Transactions', apiReq.body)
  let findObj = {}

  if (apiReq.body.merchant_id !== undefined && apiReq.body.merchant_id != null) {
    findObj = {
      'merchant_id': mongoose.Types.ObjectId(apiReq.body.merchant_id)
    }
  }
  merchantTransCycle.aggregate(
    [{
      $match: findObj
    }, {
      $sort: {
        _id: -1
      }
    }, {
      $skip: apiReq.body.skip
    }, {
      $limit: apiReq.body.limit
    },
    {
      $lookup: {
        from: 'merchants',
        localField: 'merchant_id',
        foreignField: '_id',
        as: 'merchant_details'
      }
    }
    ],
    function (getTransactionsErr, getTransactionsResult) {
      if (getTransactionsErr) {
        console.log('there is an error while getting Transactions details', getTransactionsErr)
        apiRes.send({
          status: false,
          message: 'error while getting Transactions details'
        })
      } else if (getTransactionsResult.length > 0) {
        // console.log("Transactions result", getTransactionsResult);
        let i = 0
        /* removing unwanted urls */
        getTransactionsResult.forEach(element => {
          let new_file_name = ''
          // console.log(element)
          if (element.merchant_details[0] && ('logo_url' in element.merchant_details[0])) {
            // console.log("before:: ", element.merchant_details[0].logo_url)
            new_file_name = element.merchant_details[0].logo_url.substring(element.merchant_details[0].logo_url.lastIndexOf('/') + 1)
            getTransactionsResult[i].merchant_details[0]['logo_url'] = new_file_name
          } else {
            new_file_name = '9000000001_logo_url.png'
          }
          // console.log("after:: ", new_file_name)
          i++
        })
        // console.log(getTransactionsResult)
        apiRes.send({
          status: true,
          message: 'Transactions details found',
          'data': getTransactionsResult
        })
      } else {
        apiRes.send({
          status: false,
          message: 'Transactions details not found'
        })
      }
    })
}

transactionCycleService.getTransactionsCount = function (apiReq, apiRes) {
  console.log('we are in claim requests count', apiReq.body)
  var findObj = apiReq.body
  if (apiReq.body.merchant_id != '') {
    findObj['merchant_id'] = mongoose.Types.ObjectId(apiReq.body.merchant_id)
  } else {
    delete findObj['merchant_id']
  }
  console.log('findobj--->', findObj)
  merchantTransCycle.countDocuments(findObj, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      console.log('count::: ', countResult)
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': countResult
      })
    }
  })
}

transactionCycleService.viewTransactionsSearch = function (apiReq, apiRes) {
  // console.log("searching view Transactions------------------------------------>", apiReq.body)
  // console.log('viewTransactionsSearch------->', apiReq.body)
  let findObj = {}
  // var currDate = Date.now();

  if (apiReq.body.cycle_status) {
    findObj['cycle_status'] = apiReq.body.cycle_status
  }
  // findObj.paid_date=apiReq.body.paid_date;
  if (apiReq.body.type !== undefined && apiReq.body.type != null) {
    if (apiReq.body.fromDate && apiReq.body.toDate) {
      findObj[apiReq.body.type] = {
        $gte: Number(apiReq.body.fromDate),
        $lte: Number(apiReq.body.toDate)
      }
    } else if (apiReq.body.fromDate) {
      findObj[apiReq.body.type] = {
        $gte: Number(apiReq.body.fromDate)
      }
    } else if (apiReq.body.toDate) {
      findObj[apiReq.body.type] = {
        $lte: Number(apiReq.body.toDate)
      }
    }
  }
  transactionCycleService.getMerchantId(apiReq.body, function (m_result) {
    if (m_result) {
      if (m_result !== 1) {
        findObj['merchant_id'] = m_result._id
      }
      merchantTransCycle.aggregate(
        [{
          $match: findObj
        }, {
          $sort: {
            _id: -1
          }
        }, {
          $lookup: {
            from: 'merchants',
            localField: 'merchant_id',
            foreignField: '_id',
            as: 'merchant_details'
          }
        }],
        function (getTransactionsErr, getTransactionsResult) {
          if (getTransactionsErr) {
            console.log('error while getting details from merchantTransCycle')
          } else {
            let final_results = []
            async.eachSeries(getTransactionsResult, function (i, next) {
              if (i.merchant_details[0] !== undefined) {
                var data = {}
                if (i.merchant_details[0].logo_url) {
                  new_file_name = i.merchant_details[0].logo_url.substring(i.merchant_details[0].logo_url.lastIndexOf('/') + 1)
                  data['logo_url'] = new_file_name
                } else {
                  new_file_name = '9000000001_logo_url.png'
                }
                // console.log('reffffffffffff::::::::::::::::::::', i)
                data['_id'] = i._id
                data['merchant_id'] = i.merchant_id
                data['amount'] = i.amount
                data['cycle_date'] = i.cycle_date
                data['cycle_day'] = i.cycle_day
                // if (i.ref_id !== undefined) {
                data['ref_id'] = i.ref_id
                // }
                data['cycle_status'] = i.cycle_status
                data['transactions'] = i.transactions
                data['brand_name'] = i.merchant_details[0].brand_name
                data['name'] = i.merchant_details[0].name
                data['mobile'] = i.merchant_details[0].mobile
                data['email'] = i.merchant_details[0].email
                data['start_date'] = i.start_date
                data['end_date'] = i.end_date
                if (i.commission_rate !== undefined) {
                  data['commission_rate'] = i.commission_rate
                } else {
                  data['commission_rate'] = 0
                }
                final_results.push(data)
              } else {
                console.log('MerchantDetails not found')
              }

              next()
            }, function (error, final_result) {
              if (error) {
                apiRes.send({
                  'status': false,
                  'message': 'details not found'
                })
              } else {
                // console.log('final_result------------->', final_results)
                apiRes.send({
                  status: true,
                  message: 'transaction details found',
                  data: final_results
                })
              }
            })
          }
        })

      // merchantTransCycle.find(findObj, {}, function (claimRequestErr, getTransactionsResult) {
      //     if (claimRequestErr) {
      //         apiRes.send({
      //             "status": false,
      //             "message": claimRequestErr
      //         });
      //     } else if (getTransactionsResult.length > 0) {

      //         // console.log("getTransactionsResult", getTransactionsResult);
      //         var data = {};
      //         let i = 0;
      //         async.waterfall([
      //             (done) => {
      //                 getTransactionsResult.forEach(element => {
      //                     let new_file_name = '';
      //                     // console.log("element",element)
      //                     data = { "merchant_id": element.merchant_id };

      //                     transactionCycleService.getMerchantId(data, function (mer_result) {
      //                         if (mer_result) {
      //                             data = {};
      //                             data = JSON.parse(JSON.stringify(getTransactionsResult[i]));
      //                             // console.log("data:: ", data);
      //                             // data["merchant_details"] = mer_result;
      //                             data["logo_url"] = mer_result.logo_url;
      //                             data["brand_name"] = mer_result.brand_name;
      //                             data["name"] = mer_result.name;
      //                             data["mobile"] = mer_result.mobile;
      //                             data["email"] = mer_result.email;
      //                             getTransactionsResult[i] = data;
      //                             ++i;
      //                             // console.log("data::after :: ", data.merchant_details)
      //                             // console.log("i::::11 ", i, getTransactionsResult.length);
      //                             if (i == getTransactionsResult.length) {
      //                                 done();
      //                             }
      //                         }else{
      //                             console.log("coming to else")
      //                             ++i;
      //                         }
      //                         // if (mer_result) {
      //                         //     if (mer_result.logo_url) {
      //                         //         // console.log("before:: ", element.merchant_details[0].logo_url)
      //                         //         new_file_name = mer_result.logo_url.substring(mer_result.logo_url.lastIndexOf('/') + 1);
      //                         //         getTransactionsResult[i]["logo_url"] = new_file_name;
      //                         //         console.log(i,getTransactionsResult[i]["logo_url"]);

      //                         //     } else {
      //                         //         new_file_name = '9000000001_logo_url.png';

      //                         //     }
      //                         //     // console.log("after:: ", new_file_name)
      //                         //     i++;
      //                         //     done(null);
      //                         // }

      //                     })

      //                 });
      //             },
      //             (done) => {
      //                 console.log("::::::::response sent:::::::::::: ",getTransactionsResult)
      //                 apiRes.send({
      //                     status: true,
      //                     message: "transaction details found",
      //                     data: getTransactionsResult
      //                 });
      //                 done(null);
      //             }])
      //     } else {
      //         // console.log("getTransactionsResult",getTransactionsResult);

      //         apiRes.send({
      //             "status": false,
      //             "message": "details not found",
      //         });
      //     }
      // }).sort({
      //     _id: -1
      // })
    }
  })
}
transactionCycleService.getMerchantId = function (data, callback) {
  var findObj = {}
  // console.log("enter11")
  if (data.brand_name) {
    findObj = {
      'brand_name': data.brand_name
    }

    merchant_details.findOne(findObj, {}, function (findError, findResult) {
      // console.log("result", findResult)
      if (findError) {
        console.log('error', findError)
        callback(1)
      } else {
        callback(findResult)
      }
    })
  } else if (data.merchant_id) {
    findObj = {
      '_id': data.merchant_id
    }
    merchant_details.findOne(findObj, {}, function (findError, findResult) {
      // console.log("result", findResult)
      if (findError) {
        console.log('error', findError)
        callback(1)
      } else {
        callback(findResult)
      }
    })
  } else {
    callback(1)
  }
}
transactionCycleService.updateTransactions = function (apiReq, apiRes) {
  findObj = {
    'merchant_id': mongoose.Types.ObjectId(apiReq.body.merchant_id),
    'amount': apiReq.body.amount,
    'cycle_date': apiReq.body.cycle_date

  }
  console.log('Date:: ', Date.now() / 1)
  var currentDate = Date.now() / 1
  setObj = {
    cycle_status: 'success',
    accept_date: currentDate,
    ref_id: apiReq.body.ref_id
  }
  merchantTransCycle.updateOne(findObj, {
    $set: setObj
  }, function (error, updateResult) {
    if (error) {
      apiRes.send({
        'status': false,
        'message': 'Internal error',
        'error': error
      })
    } else {
      apiRes.send({
        'status': true,
        'message': 'cycle status updated successfully'
      })
    }
  })
}
transactionCycleService.pendingCycleCount = function (apiReq, apiRes) {
  let findObj = {}
  // var currDate = Date.now();

  if (apiReq.body.cycle_status) {
    findObj['cycle_status'] = apiReq.body.cycle_status
  }
  // findObj.paid_date=apiReq.body.paid_date;
  if (apiReq.body.type != undefined && apiReq.body.type != null) {
    if (apiReq.body.fromDate && apiReq.body.toDate) {
      findObj[apiReq.body.type] = {
        $gte: Number(apiReq.body.fromDate),
        $lte: Number(apiReq.body.toDate)
      }
    } else if (apiReq.body.fromDate) {
      findObj[apiReq.body.type] = {
        $gte: Number(apiReq.body.fromDate)
      }
    } else if (apiReq.body.toDate) {
      findObj[apiReq.body.type] = {
        $lte: Number(apiReq.body.toDate)
      }
    }
  }
  transactionCycleService.getMerchantId(apiReq.body, function (m_result) {
    if (m_result) {
      // console.log("Entered:::::::::")
      if (m_result != 1) {
        findObj['merchant_id'] = m_result._id
      }
      merchantTransCycle.countDocuments(findObj, function (error, result) {
        if (error) {
          apiRes.send({
            'status': false,
            'message': 'Error while getting count of pending cycles',
            'error': error
          })
        } else {
          apiRes.send({
            'status': true,
            'message': 'pending cycle count successfully fetched',
            'data': result
          })
        }
      })
    }
  })
}
transactionCycleService.getCycleTransactions = function (apiReq, apiRes) {
  var data = apiReq.body
  // console.log("body:: ", data);

  var matchObj = {
    'merchant_id': mongoose.Types.ObjectId(data.merchant_id)
  }
  matchObj['status'] = { $in: [1, 5] }
  if (data.start_date && data.end_date) {
    matchObj['transaction_date'] = {
      $lte: data.end_date,
      $gte: data.start_date
    }
  }
  // { $skip: data.skip }, { $limit: data.limit },
  userTransactions.aggregate([{
    $match: matchObj
  }, {
    $skip: data.skip
  }, {
    $limit: data.limit
  }, {
    $lookup: {
      from: 'user_details',
      localField: 'user_id',
      foreignField: '_id',
      as: 'user_data'
    }
  }], function (error, result) {
    if (error) {
      console.log('error while fetcheing cycle transactions:: ', error)
      apiRes.send({
        'status': false,
        'message': 'Error while getting transactions of merchant cycle',
        'error': error
      })
    } else {
      apiRes.send({
        'status': true,
        'message': 'merchant cycle transactions successfully fetched',
        'data': result
      })
    }
  })
}
transactionCycleService.getMerchant = function (apiReq, apiRes) {
  // console.log("body:: ", apiReq.body)
  merchant_details.findOne(apiReq.body, {
    brand_name: true,
    logo_url: true
  }, function (error, result) {
    if (error) {
      console.log('error while fetcheing merchant details:: ', error)
      apiRes.send({
        'status': false,
        'message': 'error while fetcheing merchant details',
        'error': error
      })
    } else {
      // console.log("result", result);
      var new_file_name
      if (result) {
        if (result.logo_url) {
          new_file_name = result.logo_url.substring(result.logo_url.lastIndexOf('/') + 1)
        } else {
          new_file_name = '9000000001_logo_url.png'
        }
        result.logo_url = new_file_name
      }
      apiRes.send({
        'status': true,
        'message': 'merchant details  successfully fetched',
        'data': result
      })
    }
  })
}
transactionCycleService.getCycleTransactionsForEmail = function (apiReq, apiRes) {
  var data = apiReq.body
  // console.log("body:: ", data);
  var matchObj = {
    'merchant_id': mongoose.Types.ObjectId(data.merchant_id)
  }
  matchObj['status'] = { $in: [1, 5] }
  if (data.start_date && data.end_date) {
    matchObj['transaction_date'] = {
      $lte: data.end_date,
      $gte: data.start_date
    }
  }
  // { $skip: data.skip }, { $limit: data.limit },
  userTransactions.aggregate([{
    $match: matchObj
  }, {
    $lookup: {
      from: 'user_details',
      localField: 'user_id',
      foreignField: '_id',
      as: 'user_data'
    }
  }], function (error, result) {
    if (error) {
      console.log('error while fetcheing cycle transactions:: ', error)
      apiRes.send({
        'status': false,
        'message': 'Error while getting transactions of merchant cycle',
        'error': error
      })
    } else {
      // console.log('*******************************>>>', result)
      var reportObj = {}
      var configuration = {}
      var template = 'payment_cycle'
      let data = []
      async.forEach(result, function (modelObj, modelClb) {
        // console.log(modelObj)
        let name = modelObj.user_data[0].name
        let transactionDate = new Date(modelObj.transaction_date).getFullYear() + '-' + (new Date(modelObj.transaction_date).getMonth() + 1) + '-' + new Date(modelObj.transaction_date).getDate() + '-' + new Date(modelObj.transaction_date).getHours() + '-' + new Date(modelObj.transaction_date).getMinutes() + '-' + new Date(modelObj.transaction_date).getSeconds()
        // let transactionDate = modelObj.transaction_date
        let amount = modelObj.amount
        if (name && amount && transactionDate) {
          data.push("<tr style='border: 2px solid #FFFFFF;'><td style='border: 1px solid #dddddd; text-align: center;'>" + name + "</td><td style='border: 1px solid #dddddd; text-align: center;'>" + amount + "</td><td style='border: 1px solid #dddddd; text-align: center;'>" + transactionDate + '</td></tr>')
        } else {
          apiRes.send({
            'status': false,
            'message': 'Dont have proper data you have this merchant',
            'error': error
          })
        }
        // console.log(name, transactionDate, amount)
        // console.log(data)
        reportObj['data'] = data.join('')
        configuration.email = apiReq.body.email
      })
      // console.log('DATAAA TABLE EMAIL', reportObj)
      configuration.replace = reportObj
      NotificationUtils.emailsend(template, configuration, function (error, response) {
        if (error) {
          console.log(':::: Email Function ERROR:::', error)
        } else {
          console.log(':::: Email Function Response:::', response)
          apiRes.send({
            'status': true,
            'message': response,
            'data': response
          })
        }
      })
    }
  })
}
