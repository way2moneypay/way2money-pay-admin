var QrCodeGenerator = module.exports

var qr = require('qr-image')
const AESNODE = require('../utils/EncDecNode')
var fs = require('fs')
const AppConfig = require('config')
// var pdf = require('html-pdf');
// var phantom = require('phantom');
// var htmlToPdf = require('html-to-pdf');

/* requiring utils */
// var ResponseUtils = require("./../utils/ResponseUtils");
let this_file_name = 'QrCodeGenerator'

/** Requiring Models */
const MerchantsModel = require('../models/merchantModel')

// const MerchantProfileService = require('../services/MerchantProfileService');

QrCodeGenerator.generateQrCode = function (api_request, api_response) {
  let this_function_name = 'QrCodeGenerator.generateQrCode'
  let Response = new ResponseUtils.response(api_response)
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Logs = new ResponseUtils.Logs(this_file_name, this_function_name)

  try {
    default_error = 'Error while generating qr code'
    default_error_code = 'QRCODEERROR'
    let Qrdata = {}
    MerchantsModel.findOne({
      _id: api_request.user._id
    }, (errFind, MerchatObj) => {
      if (errFind) {
        console.log('Err while finding merchant ', errFind)
        Response.error('Err while finding merchant', 500)
      } else {
        if (MerchatObj.qrcode_path) {
          return Response.success(MerchatObj.qrcode_path)
        } else {
          Qrdata['merchant_name'] = MerchatObj.name
          Qrdata['display_name'] = MerchatObj.brand_name
          Qrdata['id'] = MerchatObj._id
          Qrdata['mobile'] = MerchatObj.mobile

          var encQrdata = AESNODE.encrypt(JSON.stringify(Qrdata), AppConfig.jwtsecret)
          // var decQrdata= AESNODE.decrypt(encQrdata,AppConfig.node_enc_key);
          var qr_svg = qr.image(encQrdata, {
            type: 'png'
          })
          var qr_svg = qr.image(JSON.stringify(Qrdata), {
            type: 'png'
          })
          var finish = function () {
            MerchantsModel.findOneAndUpdate({
              _id: Qrdata.id
            }, {
              $set: {
                qrcode_path: 'https://business.way2money.com/qr_codes/' + Qrdata.id + '.png'
              }
            }, function (err, res) {
              if (err) return Response.error('QRCode path save failed', 500)
              else {
                return Response.success('https://business.way2money.com/qr_codes/' + Qrdata.id + '.png')
              }
            })
          }
          dest = fs.createWriteStream(__dirname + '/../../../uploads/qr_codes/' + Qrdata.id + '.png')
          qr_svg.pipe(dest)
          dest.addListener('finish', finish)
        }
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
QrCodeGenerator.download = function (api_request, api_response) {
  // let this_function_name = "QrCodeGenerator.generateQrCode";
  // let Response = new ResponseUtils.response(api_response);
  // let Print = new ResponseUtils.print(this_file_name, this_function_name);
  // let Logs = new ResponseUtils.Logs(this_file_name, this_function_name);

  try {
    default_error = 'Error while generating qr code'
    default_error_code = 'QRCODEERROR'
    let Qrdata = {}

    MerchantsModel.findOne({
      _id: api_request.params.merchant_id
    }, (errFind, MerchatObj) => {
      if (errFind) {
        console.log('Err while finding merchant ', errFind)
        // Response.error("Err while finding merchant", 500);
        api_response.send({
          success: false,
          message: 'Error while finding merchant'
        })
      } else {
        // if (MerchatObj && MerchatObj.qrcode_path != undefined && MerchatObj.qrcode_path != null) {
        //   // api_response.download(__dirname + '/../../uploads/qr_codes/' + api_request.user._id + '.png')
        //   api_response.download(MerchatObj.qrcode_path);

        // } else {
        // console.log("No path existed");
        Qrdata['merchant_name'] = MerchatObj.name != undefined ? MerchatObj.name : ''
        Qrdata['display_name'] = MerchatObj.brand_name != undefined ? MerchatObj.brand_name : ''
        Qrdata['id'] = MerchatObj._id != undefined ? MerchatObj._id : ''
        Qrdata['mobile'] = MerchatObj.mobile != undefined ? MerchatObj.mobile : ''
        var encQrdata = AESNODE.encrypt(JSON.stringify(Qrdata), AppConfig.jwtsecret_qr)
        // var decQrdata= AESNODE.decrypt(encQrdata,AppConfig.node_enc_key);
        // console.log("Encrypted data : ", encQrdata);
        var qr_svg = qr.image(encQrdata, {
          type: 'svg'
        })
        var finish = function () {
          path = __dirname + AppConfig.Upload_QR_DIR + Qrdata.display_name + '_' + Qrdata.mobile + '.svg'
          api_response.download(path)
        }
        dest = fs.createWriteStream(__dirname + AppConfig.Upload_QR_DIR + Qrdata.display_name + '_' + Qrdata.mobile + '.svg')
        qr_svg.pipe(dest)
        dest.addListener('finish', finish)
        // var finish = function () {
        //   MerchantsModel.findOneAndUpdate({
        //     _id: Qrdata.id
        //   }, {
        //     $set: {
        //       qrcode_path: 'https://business.way2money.com/qr_codes/' + Qrdata.display_name + '_' + Qrdata.mobile + '.png'
        //       // qrcode_path: AppConfig.images_cdn + "qr_codes/" + Qrdata.id + '.png'
        //       // qrcode_path: __dirname + "/../../../way2money-pay-merchant/uploads/qr_codes/" + Qrdata.id + '.png'
        //     }
        //   }, function (err, res) {
        //     if (err) {
        //       // Response.error('QRCode path save failed', 500)
        //       api_response.send({
        //         success: false,
        //         message: "something went wrong while saving qr code path"
        //       });
        //     } else {
        //       // path = __dirname + '/../../uploads/qr_codes/' + Qrdata.id + '.png' BUSINESS_WAY2MONEY_COM  way2money-pay-merchant
        //       path = __dirname + AppConfig.Upload_QR_DIR + Qrdata.display_name + '_' + Qrdata.mobile + '.png';
        //       api_response.download(path);
        //     }
        //   })
        // };
        // dest = fs.createWriteStream(__dirname + '/../../uploads/qr_codes/' + Qrdata.id + '.png') BUSINESS_WAY2MONEY_COM way2money-pay-merchant

        // }
      }
    })
  } catch (exception) {
    Print.exception(exception + '')
    Logs.exception(exception + '', default_error_code, default_error)
    return Response.error(default_error, default_error_code, 500, null)
  }
}
