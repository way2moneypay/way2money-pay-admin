const async = require('async')
const transactionsModel = require('../models/userTransactionsModel')
const npaModel = require('../models/npaDetailsModel')
let npaDetailsService = module.exports
var parallel = require('run-parallel')
const moment = require('moment')
const config = require('config')

let finalArray = []
npaDetailsService.getNpaDetails = function (apiReq, apiRes) {
  parallel([
    function (callback) {
      // console.log('============>>>>>>>>>>>.', apiReq.query.skip)
      let skip = Number(apiReq.query.skip)
      let limit = Number(apiReq.query.limit)
      npaModel.find({}, {}, {
        skip: skip,
        limit: limit
      }).sort({
        from_date: -1
      }).exec(function (error, transData) {
        if (error) {
          callback(error, null)
        } else {
          callback(null, transData)
        }
      })
    },
    function (callback) {
      npaModel.count({}).exec(function (error, CountsTransData) {
        if (error) {
          callback(error, null)
        } else {
          callback(null, CountsTransData)
        }
      })
    }
  ], function (error, results) {
    if (error) {
      apiRes.send({
        status: false,
        message: 'ERROR While Getting NPA Details'
      })
    } else {
      // console.log('==>ewew>>>', results)
      if (results && results.length > 0) {
        let returnData = {}
        returnData.total_records = results[0]
        returnData.smscounts_details = results[1]
        // console.log('Results count', returnData)
        apiRes.send({
          status: true,
          message: 'Get npa count Details successfully',
          data: returnData
        })
      }
    }
  })
}
// let checkDateLessThanToDate = function (fromDate, toDate) {
//   console.log('check from -> ', fromDate + 'to -> ' + toDate)
//   if (fromDate <= toDate) {
//     return true
//   } else {
//     return false
//   }
// }
npaDetailsService.getPendingTransactions = function (apiReq, apiRes) {
  // console.log('Reached to backend getPendingTransactions', apiReq.body)
  var countQuery = []
  let query = []
  // let toDate = new Date(apiReq.body.date.substring(3, 8), apiReq.body.date.substring(0, 2), 1)
  // let fromDate = new Date(apiReq.body.date.substring(3, 8), Number(apiReq.body.date.substring(0, 2)) - 1, 1)
  let fromDate = apiReq.body.from_date
  let toDate = apiReq.body.to_date
  // if (checkDateLessThanToDate(fromDate, toDate)) {
  query.push({
    $match: {
      transaction_date: {
        $gt: fromDate / 1,
        $lte: toDate / 1
      },
      status: 1
    }
  })
  query.push({
    $lookup: {
      from: 'user_details',
      localField: 'user_id',
      foreignField: '_id',
      as: 'userInfo'
    }
  })
  query.push({
    $lookup: {
      from: 'merchants',
      localField: 'merchant_id',
      foreignField: '_id',
      as: 'merchantInfo'
    }
  })
  countQuery.push({
    $match: {
      transaction_date: {
        $gt: fromDate / 1,
        $lte: toDate / 1
      },
      status: 1
    }
  })
  // } else {
  //   // Print.error("Dates out of range 1");
  //   console.log('DATE OUT OF RANGE')
  //   // return Response.error("Dates out of range 1", "BadDate", 400)
  // }
  countQuery.push({
    $group: {
      _id: null,
      count: {
        $sum: 1
      }
    }
  })

  query.push({
    $sort: {
      '_id': -1
    }
  })
  let skip = Number(apiReq.body.skip, 10)
  let limit = Number(apiReq.body.limit, 10)
  query.push({
    $skip: skip
  })
  query.push({
    $limit: limit
  })
  // if (apiReq.body.skip && apiReq.body.limit) {
  //   skip = Number(apiReq.body.skip, 10)
  //   limit = Number(apiReq.body.limit, 10)
  //   query.push({
  //     $skip: skip
  //   })
  //   query.push({
  //     $limit: limit
  //   })
  // }
  parallel([
    function (callback) {
      // console.log('get pending transactions', query)
      // console.log(apiReq.body.date.substring(0, 2), apiReq.body.date.substring(3, 8))
      transactionsModel.aggregate(query,
        function (error, transData) {
          if (error) {
            console.log(error)
            callback(error, null)
          } else {
            callback(null, transData)
            //   console.log(transData)
          }
        })
    },
    function (callback) {
      transactionsModel.aggregate(countQuery, function (errorinTarsCount, resultTransCount) {
        if (errorinTarsCount) {
          // console.log(errorinTarsCount)
          //   Logs.error("Error @get partners parallel", "ERRGETPART", errorinTarsCount.message, new Error());
          callback(errorinTarsCount, null)
        } else {
          if (resultTransCount && resultTransCount[0] && resultTransCount[0].count) {
            // console.log(resultTransCount)

            callback(null, resultTransCount[0].count)
          } else callback(null, 0)
          // callback(null, resultTransCount)
          // console.log('Results  ', resultTransCount);
        }
      })
    }
  ], function (error, results) {
    if (error) {
      console.log(error)
      apiRes.send({
        status: false,
        message: 'ERROR While Getting NPA Details'
      })
    } else {
      // console.log(results)
      if (results && results.length > 0) {
        let returnData = {}
        returnData.total_transactions = results[0]
        returnData.total_counts = results[1]
        // console.log('Results count', returnData)
        apiRes.send({
          status: true,
          message: 'Get Transactions Details successfully',
          data: returnData
        })
      }
    }
  })
}

npaDetailsService.calculateNPA = function (apiReq, apiRes) {
  let days15 = moment().subtract(15, 'days').calendar()
  days15 = new Date(days15)
  let startDate = new Date(config.npaStartDate)
  let months = moment([ days15.getFullYear(), (Number(days15.getMonth()) + 1), days15.getDate() ]).diff(moment([startDate.getFullYear(), (Number(startDate.getMonth()) + 1), startDate.getDate()]), 'months', true)
  months = parseInt(months) + 1
  let fromdate = startDate
  let todate = moment(new Date(fromdate)).add(1, 'month').calendar()
  todate = new Date(todate)
  // console.log('months:::::::::::::::::::::::', months)
  let index = 0
  npa(index, months, fromdate, todate, days15, function (error, npaDetails) {
    if (error) {
      console.log(error)
      apiRes.send({ status: false, message: 'error while updating user events details' })
    } else {
      // console.log(npaDetails)
      async.forEach(npaDetails, (item, cb) => {
        npaModel.findOneAndUpdate({
          from_date: item.from_date
        }, {
          $set: {
            to_date: item.to_date,
            trans_amount: item.trans_amount,
            paid_amount: item.paid_amount
          }
        }, {
          upsert: true
        }, function (updateError, updateData) {
          if (updateError) {
            console.log(updateError)
          } else {
            cb()
          }
        })
      }, (success) => {
        console.log('Updated successfully')
      })
    }
  })
}

let npa = function (index, months, fromDate, toDate, days15, cb) {
  // fromdate = todate
  // todate = moment(new Date(todate)).add(1, 'month').calendar()
  // console.log(fromDate, toDate)
  async.parallel([
    function (callback) {
      transactionsModel.aggregate([{
        $match: {
          status: {
            $in: [1, 5]
          },
          transaction_date: {
            $gte: fromDate / 1,
            $lte: toDate / 1
          }
        }
      },
      {
        $group: {
          _id: null,
          trans_amount: {
            $sum: '$trans_amount'
          }
        }
      }
      ]).sort({
        _id: -1
      }).exec(function (err, result) {
        if (err) {
          console.log('Error while fetching allversions data', err)
        } else {
          callback(null, result)
        }
      })
    },
    function (callback) {
      transactionsModel.aggregate([{
        $match: {
          status: 5,
          transaction_date: {
            $gte: fromDate / 1,
            $lte: toDate / 1
          }
        }
      },
      {
        $group: {
          _id: null,
          paid_amount: {
            $sum: '$trans_amount'
          }
        }
      }
      ]).sort({
        _id: -1
      }).exec(function (err, result) {
        if (err) {
          console.log('Error while fetching allversions data', err)
        } else {
          callback(null, result)
        }
      })
    }
  ],
  function (err, results) {
    if (err) {
      console.log(err)
    }
    // console.log(results)
    let obj = {}
    if (results.length > 0) {
      if (results[0].length > 0 && results[1].length > 0) {
        obj = {
          from_date: fromDate / 1,
          to_date: toDate / 1,
          trans_amount: results[0][0].trans_amount,
          paid_amount: results[1][0].paid_amount
        }
      } else if (results[0].length > 0 && results[1].length === 0) {
        obj = {
          from_date: fromDate / 1,
          to_date: toDate / 1,
          trans_amount: results[0][0].trans_amount,
          paid_amount: 0
        }
      } else {
        obj = {
          from_date: fromDate / 1,
          to_date: toDate / 1,
          trans_amount: 0,
          paid_amount: 0
        }
      }
    }
    finalArray.push(obj)
    let fromdate = toDate
    let todate = moment(new Date(toDate)).add(1, 'month').calendar()
    todate = new Date(todate)
    if (index < (months - 2)) {
      npa(index + 1, months, fromdate, todate, days15, cb)
    } else if (index === (months - 2)) {
      npa(index + 1, months, fromdate, days15, days15, cb)
    } else {
      cb(null, finalArray)
    }
  })
}
