const PaymentSettingsModel = require('../models/SettingsModel')
const userDetails = require('../models/userDetails')
const NotificationUtils = require('../utils/NotificationUtils')
// const PushNotifications = require('../services/PushNotifications');
const PushNotificationService = require('../services/PushNotificationService')
const SmsCountsModel = require('../models/SmsCountsModel')
var JWT = require('jsonwebtoken')
const AppConfig = require('config')
var request = require('request')
var parallel = require('run-parallel')
var moment = require('moment')
var settingsService = module.exports

settingsService.addSettings = function (apiReq, apiRes) {
  console.log('apiReq', apiReq.body)
  // PaymentSettingsModel = new (apiReq.body);
  PaymentSettingsModel.findOneAndUpdate({}, apiReq.body, {
    upsert: true
  }, function (err, setObj) {
    if (err) {
      console.log('error while saving payment_settings object', err)
      apiRes.send({
        status: true,
        message: 'error while saving payment_settings object'
      })
    } else {
      apiRes.send({
        status: true,
        message: 'successfully added payment settings object'
      })
    }
  })
}
settingsService.viewSettings = function (apiReq, apiRes) {
  PaymentSettingsModel.findOne({}, (err, setObj) => {
    if (err) {
      console.log('error while while getting payment_settings object', err)
      apiRes.send({
        status: true,
        message: 'error while getting payment_settings object'
      })
    } else {
      apiRes.send({
        status: true,
        message: 'successfully added payment settings object',
        'data': setObj
      })
    }
  })
}
settingsService.getPaymentsStatus = function (apiReq, apiRes) {
  PaymentSettingsModel.findOne({}, {
      staging_payment_status: true,
      live_payment_status: true
    },
    function (error, result) {
      if (error) {
        console.log('error while while getting payment_settings object', error)
        apiRes.send({
          status: false,
          message: 'error while getting payaments_running_status'
        })
      } else {
        apiRes.send({
          status: true,
          message: 'successfully get payment_running_status',
          'data': result
        })
      }
    })
}
settingsService.updatePaymentStatus = function (apiReq, apiRes) {
  var data = apiReq.body
  console.log('body:: ', data)
  PaymentSettingsModel.findOne({}, function (error, result) {
    if (error) {
      console.log('error while while getting payment_settings object', error)
      apiRes.send({
        status: false,
        message: 'error while updating payaments_running_status'
      })
    } else {
      result[data['key']] = data[data['key']]
      console.log(data)
      result.save(function (saveError, saveResult) {
        if (saveError) {
          console.log('Error while saving settings service updatepaymentStatus method')
          apiRes.send({
            status: false,
            message: 'error while updating payaments_running_status'
          })
        } else {
          if (data['key'] === 'live_payment_status') {
            let configuration = {}
            let reportObj = {}
            if (data[data['key']] === 'active') {
              reportObj['status'] = 'Active'
            } else {
              reportObj['status'] = 'Inactive'
            }
            reportObj['date'] = moment().add(330, 'minutes').format('MMMM Do YYYY, h:mm:ss a')
            // const emails = ['anusha.p@way2online.co.in']
            const emails = ['saikumar.d@way2online.co.in', 'vikrant.d@way2online.co.in', 'anudeep@way2online.net']
            configuration.emails = emails
            configuration.replace = reportObj
            // console.log(configuration.emails);
            NotificationUtils.livePaymentEmail('live-payment-status', configuration, function (err, resp) {
              console.error(err)
              // console.log(resp);
            })
            const mobiles = ['9966000280', '8788552599', '7675983284']
            // const mobiles = ['8500338190']
            configuration.mobiles = mobiles
            configuration.replace = reportObj
            // console.log(configuration.emails);
            NotificationUtils.smssend('live-payment-status', configuration, function (err, resp) {
              console.error(err)
              // console.log(resp);
            })
          }

          apiRes.send({
            status: true,
            message: 'successfully updated payment_running_status',
            'data': saveResult
          })
        }
      })
    }
  })
}

settingsService.sendSmsAlerts = function (apiReq, apiRes) {
  var data = apiReq.query
  // mobile_number = "9494197969"
  // console.log("body:: ", data)
  userDetails.findOne(data, function (error, result) {
    if (error) {
      console.log('error while while getting send sms object', error)
      apiRes.send({
        status: false,
        message: 'error while getting sms sending'
      })
    } else {
      if (result == null) {
        apiRes.send({
          status: false,
          message: 'This Number is Not Register in Way2money'
        })
      } else {
        console.log('sendSmsAlerts Response', result)
        if (result.credit_due > 10) {
          var user = {}
          var configuration = {}
          var replaceObj = {}
          var template = 'due-amount-alert'
          user._id = result._id
          user.credit_due = result.credit_due
          var token = JWT.sign(user, AppConfig.jwtsecret_links)
          //  console.log("sendSmsAlerts Response", token)
          let url = AppConfig.paymentlinkurl + token
          replaceObj.name = result.name
          replaceObj.amount = result.credit_due.toFixed(2)
          replaceObj.due_date = new Date(result.due_date).getDate() + '-' + (new Date(result.due_date).getMonth() + 1) + '-' + new Date(result.due_date).getFullYear()
          configuration.mobile = result.mobile_number
          settingsService.way2shorturl(url, function (short_url) {
            if (short_url) {
              let new_short_url = JSON.parse(short_url)
              replaceObj.url = new_short_url.data.short_url
              configuration.replace = replaceObj
              console.log(configuration, template)
              NotificationUtils.sms(template, configuration, function (error, response) {
                if (error) {
                  console.log('error', error)
                  apiRes.send({
                    status: false,
                    message: error
                  })
                } else {
                  // console.log(":::: SMS Function Response 2 :::", response);
                  apiRes.send({
                    status: true,
                    message: response
                  })
                }
              })
            } else {
              apiRes.send({
                status: false,
                message: 'URL NOT FOUND'
              })
            }
          })
        } else {
          apiRes.send({
            status: false,
            message: 'Due Amount Less Than 10 This Number DueAmount is :' + result.credit_due
          })
        }
      }
    }
  })
}
settingsService.way2shorturl = function (url, callback) {
  request(
    AppConfig.short_url + url,
    function (error, response) {
      if (response && response.body) {
        callback(response.body)
      } else {
        callback(null)
      }
    })
}

settingsService.sendPushAlerts = function (apiReq, apiRes) {
  var data = apiReq.query
  // mobile_number = "9494197969"
  // console.log("body:: ", data)
  userDetails.findOne(data, function (error, result) {
    if (error) {
      console.log('error while while getting send push notification object', error)
      apiRes.send({
        status: false,
        message: 'error while sending push notifications'
      })
    } else {
      if (result == null) {
        apiRes.send({
          status: false,
          message: 'This Number is Not Register in Way2money'
        })
      } else {
        // console.log("sendPushAlerts Response", result)
        var replace = {}
        var userId = result._id
        var template = 'due-amount-alert'
        replace.name = result.name
        replace.amount = result.credit_due.toFixed(2)
        replace.due_date = new Date(result.due_date).getDate() + '-' + (new Date(result.due_date).getMonth() + 1) + '-' + new Date(result.due_date).getFullYear()
        var pnId = result.pn_id
        var os = result.pn_id_source
        var version = result.version
        // console.log(userId, template, replace, pnId, os, version);
        PushNotificationService.storeNotification(userId, template, replace, pnId, os, version, function (error, response) {
          if (error) {
            console.log(error)
            apiRes.send({
              status: false,
              message: error
            })
          } else {
            // console.log(':::: Push in Function:::', response)
            apiRes.send({
              status: true,
              message: response
            })
            // callback()
          }
        })
      }
    }
  })
}
settingsService.SendEmail = function (apiReq, apiRes) {
  var data = apiReq.query
  console.log(data)
  userDetails.findOne(data, function (error, result) {
    if (error) {
      console.log('error while while getting send email object', error)
      apiRes.send({
        status: false,
        message: 'error while getting email sending'
      })
    } else {
      if (result == null) {
        apiRes.send({
          status: false,
          message: 'This email is Not Register in Way2money'
        })
      } else {
        // console.log(result);
        if (result.credit_due > 10) {
          var user = {}
          var configuration = {}
          var replaceObj = {}
          var template = 'due-amount-alert'
          user._id = result._id
          user.credit_due = result.credit_due
          var token = JWT.sign(user, AppConfig.jwtsecret_links)
          //  console.log("sendSmsAlerts Response", token)
          let url = AppConfig.paymentlinkurl + token
          replaceObj.name = result.name
          replaceObj.amount = result.credit_due.toFixed(2)
          replaceObj.due_date = new Date(result.due_date).getDate() + '-' + (new Date(result.due_date).getMonth() + 1) + '-' + new Date(result.due_date).getFullYear()
          configuration.email = result.email
          settingsService.way2shorturl(url, function (short_url) {
            if (short_url) {
              let new_short_url = JSON.parse(short_url)
              replaceObj.url = new_short_url.data.short_url
              configuration.replace = replaceObj
              // console.log(configuration, template);
              NotificationUtils.emailsend(template, configuration, function (error, response) {
                if (error) {
                  console.log('error', error)
                  apiRes.send({
                    status: false,
                    message: error
                  })
                } else {
                  console.log(':::: Email Function Response 2 :::', response)
                  apiRes.send({
                    status: true,
                    message: response
                  })
                }
              })
            } else {
              apiRes.send({
                status: false,
                message: 'URL NOT FOUND'
              })
            }
          })
        } else {
          apiRes.send({
            status: false,
            message: 'Due Amount Less Than 10 This Number DueAmount is :' + result.credit_due
          })
        }
      }
    }
  })
}

checkDateLessThanToDate = function (from_date, to_date) {
  console.log('check from -> ', from_date + 'to -> ' + to_date)
  if (from_date <= to_date) {
    return true
  } else {
    return false
  }
}

settingsService.getSmsCounts = function (api_request, api_response) {
  // let skip = apiReq.query.skip;
  // let limit = apiReq.query.limit;
  try {
    var count_query = []
    let query = []
    if (api_request.query.from_date && api_request.query.to_date) {
      let from_date = new Date(api_request.query.from_date).getTime()
      let to_date = new Date(api_request.query.to_date).getTime() + 86399999
      if (checkDateLessThanToDate(from_date, to_date)) {
        query.push({
          $match: {
            'created_at': {
              $gte: from_date,
              $lte: to_date
            }
          }
        })
        count_query.push({
          $match: {
            'created_at': {
              $gte: from_date,
              $lte: to_date
            }
          }
        })
      } else {
        // Print.error("Dates out of range 1");
        console.log('DATE OUT OF RANGE')
        // return Response.error("Dates out of range 1", "BadDate", 400)
      }
    }
    count_query.push({
      $group: {
        _id: null,
        count: {
          $sum: 1
        }
      }
    })

    query.push({
      $sort: {
        'created_at': -1
      }
    })
    if (api_request.query.skip && api_request.query.limit) {
      let skip = Number(api_request.query.skip, 10)
      let limit = Number(api_request.query.limit, 10)
      query.push({
        $skip: skip
      })
      query.push({
        $limit: limit
      })
    }
    parallel([
        function (callback) {
          SmsCountsModel.aggregate(count_query, function (error_finding_count, group) {
            if (error_finding_count) {
              //   Logs.error("Error @get partners count parallel", "ERRGETPART", error_finding_count2.message, new Error());
              callback(error_finding_count, null)
            } else {
              if (group && group[0] && group[0].count) {
                callback(null, group[0].count)
              } else callback(null, 0)
            }
          })
        },
        function (callback) {
          SmsCountsModel.aggregate(query, function (error_finding_smscounts, result_partners) {
            if (error_finding_smscounts) {
              //   Logs.error("Error @get partners parallel", "ERRGETPART", error_finding_smscounts.message, new Error());
              callback(error_finding_smscounts, null)
            } else {
              callback(null, result_partners)
              // console.log('Results  ', result_partners);
            }
          })
        },
        function (callback) {
          SmsCountsModel.aggregate([{
            $group: {
              _id: null,
              total_counts: {
                $sum: '$sent_count'
              }
            }
          }], function (error_finding_counts, result_total) {
            if (error_finding_counts) {
              //   Logs.error("Error @get partners parallel", "ERRGETPART", error_finding_counts.message, new Error());
              callback(error_finding_counts, null)
            } else {
              callback(null, result_total)
              // console.log('Results  ', result_total);
            }
          })
        }
      ],
      function (error, results) {
        if (error) {
          api_response.send({
            status: false,
            message: 'ERROR While Getting SMS COUNT Details'
          })
        }
        let return_data = {}
        if (results && results.length > 0) {
          return_data.total_records = results[0]
          return_data.smscounts_details = results[1]
          return_data.result_total = results[2]
          console.log('Results count', return_data)
          api_response.send({
            status: true,
            message: 'Get SMS COUNT Details successfully',
            data: return_data
          })
        }
      })
  } catch (exception) {
    Print.exception(exception)
    api_response.send({
      status: false,
      message: 'ERROR While Getting SMS COUNT Details::>'
    })
  }
}