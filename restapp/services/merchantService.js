var merchantModel = require('../models/merchantModel')
var usersModel = require('../models/usersModel')
var merchantStats = require('../models/merchantDaywiseDetails')
const mongoose = require('mongoose')
let MerchantOverallDetailsModel = require('../models/merchantOverallDetailsModel')
let transactionsModel = require('../models/userTransactionsModel')
const async = require('async')
const merchantService = module.exports

merchantService.getMerchantDetails = function (apiReq, apiRes) {
  let findby = {}
  if (apiReq.user.user_type === 'sales_team') {
    findby.created_by = mongoose.Types.ObjectId(apiReq.user._id)
  }
  if (apiReq.user.user_type === 'regional_head') {
    findby.created_by_parent = mongoose.Types.ObjectId(apiReq.user._id)
  }
  if (apiReq.user.user_type === 'super_admin') {
    findby = {}
  }
  console.log('findby888888888888888888888', findby)
  let merchants = []
  merchantModel
    .find(findby, {}, {
      skip: apiReq.body.skip,
      limit: apiReq.body.limit
    },
    function (userError, userResult) {
      if (userError) {
        apiRes.send({
          status: false,
          message: userError
        })
      } else if (userResult.length) {
        async.forEach(userResult, (merchant, callback) => {
          MerchantOverallDetailsModel.findOne({
            merchant_id: merchant._id
          }, {
            _id: false,
            merchant_id: false
          }).sort({
            _id: -1
          }).exec(function (err, result) {
            if (err) {
              apiRes.send({
                status: false,
                message: err
              })
            } else {
              let obj = {}
              if (result) {
                obj = {
                  merchant: merchant,
                  npa: result
                }
              } else {
                obj = {
                  merchant: merchant,
                  npa: {}
                }
              }
              merchants.push(obj)
              callback()
            }
          })
        }, (success) => {
          console.log('Updated successfully')
          apiRes.send({
            status: true,
            message: 'details fetched successfully',
            data: merchants
          })
        })
      } else {
        apiRes.send({
          status: false,
          message: 'details not found'
        })
      }
    }
    ).sort({
      register_date: -1
    })
}
merchantService.merchantSearch = function (apiReq, apiRes) {
  let merchants = []
  var findObj = {}
  if (apiReq.body.merchant_status === 1) {
    findObj = {
      $or: [{
        merchant_status: 1
      },
      {
        merchant_status: 2
      }
      ]
    }
  } else {
    // console.log("apiReq.body==>>>>", apiReq.body)
    if (Object.keys(apiReq.body)[0] === 'brand_name') {
      findObj = {
        'brand_name': {
          $regex: new RegExp('^' + apiReq.body['brand_name'].toLowerCase(), 'i')
        }
      }
    } else findObj = apiReq.body
  }
  if (apiReq.user.user_type == 'sales_team') {
    findObj.created_by = mongoose.Types.ObjectId(apiReq.user._id)
  }
  if (apiReq.user.user_type == 'regional_head') {
    findObj.created_by_parent = mongoose.Types.ObjectId(apiReq.user._id)
  }
  console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@', findObj)
  merchantModel.find(findObj, function (userError, userResult) {
    if (userError) {
      apiRes.send({
        status: false,
        message: userError
      })
    } else if (userResult.length) {
      // console.log(userResult);
      async.forEach(userResult, (merchant, callback) => {
        MerchantOverallDetailsModel.findOne({
          merchant_id: merchant._id
        }, {
          _id: false,
          merchant_id: false
        }).sort({
          _id: -1
        }).exec(function (err, result) {
          if (err) {
            apiRes.send({
              status: false,
              message: err
            })
          } else {
            let obj = {}
            if (result) {
              obj = {
                merchant: merchant,
                npa: result
              }
            } else {
              obj = {
                merchant: merchant,
                npa: {}
              }
            }
            merchants.push(obj)
            callback()
          }
        })
      }, (success) => {
        console.log('Updated successfully')
        apiRes.send({
          status: true,
          message: 'details fetched successfully',
          data: merchants
        })
      })
    } else {
      apiRes.send({
        status: false,
        message: 'details not found'
      })
    }
  })
    .sort({
      register_date: -1
    })
}
merchantService.merchantStats = function (apiReq, apiRes) {
  merchantStats.aggregate(
    [{
      $match: {
        merchant_id: apiReq.body.id
      }
    },
    {
      $group: {
        _id: '$merchant_id',
        total_amount: {
          $sum: '$trans_amount'
        },
        total_transactions: {
          $sum: '$transactions'
        }
      }
    }
    ],
    function (statError, statResult) {
      if (statError) {
        apiRes.send({
          status: false,
          message: statError
        })
      } else if (statResult) {
        apiRes.send({
          status: true,
          message: 'details fetched successfully',
          data: statResult
        })
      }
    }
  )
}
merchantService.getCount = function (apiReq, apiRes) {
  let findby = {}
  if (apiReq.user.user_type == 'sales_team') {
    findby.created_by = mongoose.Types.ObjectId(apiReq.user._id)
  }
  if (apiReq.user.user_type == 'regional_head') {
    findby.created_by_parent = mongoose.Types.ObjectId(apiReq.user._id)
  }
  if (apiReq.user.user_type == 'super_admin') {
    let findby = {}
  }
  merchantModel.countDocuments(findby, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        status: false,
        message: countError
      })
    } else {
      apiRes.send({
        status: true,
        message: 'details fetched successfully',
        data: countResult
      })
    }
  })
}
merchantService.changeMobile = function (apiReq, apiRes) {
  let findObj = {
    _id: apiReq.body.id
  }
  let updateObj = {
    mobile: apiReq.body.new_number
  }
  let addToSetObj = {
    old_mobiles: apiReq.body.old_number
  }
  console.log('find obj', findObj)
  console.log('insert obj', updateObj)
  console.log('addtoset obj', addToSetObj)
  if (apiReq.body.new_number.length == 10) {
    merchantModel.findOne({
      mobile: apiReq.body.new_number
    }, function (
      findErr,
      findResult
    ) {
      if (findErr) {
        apiRes.send({
          status: false,
          message: findErr
        })
      } else if (findResult) {
        apiRes.send({
          status: false,
          message: 'mobile number already existed'
        })
      } else {
        merchantModel.update(
          findObj, {
            '$set': updateObj,
            '$addToSet': addToSetObj
          }, {
            upsert: true
          },
          function (updateErr, updateResult) {
            if (updateErr) {
              console.log('update Error', updateErr)
              apiRes.send({
                status: false,
                message: updateErr
              })
            } else {
              apiRes.send({
                status: true,
                message: 'details updated successfully'
              })
            }
          }
        )
      }
    })
  } else {
    apiRes.send({
      status: false,
      message: 'mobile number length should be 10'
    })
  }
}
merchantService.checkSubmerchantMobile = function (apiReq, apiRes) {
  if (apiReq.body.new_number.length == 10) {
    merchantModel.findOne({
      mobile: apiReq.body.new_number
    }, function (findErr, findResult) {
      if (findErr) {
        console.log('Error while checking sub merchant mobile', findErr)
        apiRes.send({
          status: false,
          message: 'Internal Error'
        })
      } else if (findResult) {
        // console.log(findResult);
        apiRes.send({
          status: true,
          message: 'This mobile number registered with another merchant'
        })
      } else {
        merchantModel.findOne({
          old_mobiles: {
            $in: [apiReq.body.new_number]
          }
        },
        function (checkError, checkResult) {
          if (checkError) {
            console.log('Error while checking sub merchant mobile', findErr)
            apiRes.send({
              status: false,
              message: 'Internal Error'
            })
          } else if (checkResult) {
            apiRes.send({
              status: true,
              message: 'This mobile number registered with another merchant'
            })
          } else {
            console.log('entered')
            merchantModel.findOne({
              sub_merchant_mobile: {
                $in: [apiReq.body.new_number]
              }
            },
            function (err, result) {
              if (err) {
                console.log('Error while checking sub merchant mobile', findErr)
                apiRes.send({
                  status: false,
                  message: 'Internal Error'
                })
              } else if (result) {
                apiRes.send({
                  status: true,
                  message: 'This mobile number registered with another merchant'
                })
              } else {
                apiRes.send({
                  status: false,
                  message: 'No details found'
                })
              }
            })
          }
        })
      }
    })
  } else {
    apiRes.send({
      status: false,
      message: 'mobile number length should be 10'
    })
  }
}
merchantService.addSubmerchant = function (apiReq, apiRes) {
  console.log('Entered')
  let findObj = {
    _id: apiReq.body.id
  }
  let addToSetObj = {
    sub_merchant_mobile: apiReq.body.new_number
  }

  merchantModel.update(
    findObj, {
      '$addToSet': addToSetObj
    }, {
      upsert: true
    },
    function (updateErr, updateResult) {
      if (updateErr) {
        console.log('update Error', updateErr)
        apiRes.send({
          status: false,
          message: updateErr
        })
      } else {
        apiRes.send({
          status: true,
          message: 'details updated successfully'
        })
      }
    })
}
merchantService.changeMerchantStatus = function (apiReq, apiRes) {
  console.log('Entered', apiReq.body.id)
  let findObj = {
    _id: apiReq.body.id
  }
  if (apiReq.body.merchant_status == undefined || apiReq.body.merchant_status == 0) {
    status = 1
  } else {
    status = 0
  }

  merchantModel.update(
    findObj, {
      $set: {
        merchant_status: status
      }
    },
    function (updateErr, updateResult) {
      if (updateErr) {
        console.log('update Error', updateErr)
        apiRes.send({
          status: false,
          message: updateErr
        })
      } else {
        // console.log('merchantstatus updated successfully', updateResult)
        apiRes.send({
          status: true,
          message: 'details updated successfully'
        })
      }
    })
}

merchantService.changeMerchantDisplayStatus = function (apiReq, apiRes) {
  console.log('body :: ', apiReq.body)

  var updateObj = {}
  // findObj['_id'] = apiReq.body.id;
  if (apiReq.body && (apiReq.body.display_status === undefined || apiReq.body.display_status === 'Inactive')) {
    updateObj['display_status'] = 'Active'
  } else {
    updateObj['display_status'] = 'Inactive'
  }

  merchantModel.updateOne({
    _id: apiReq.body.id
  }, {
    $set: updateObj
  }, (errUpdate, DSUpdated) => {
    if (errUpdate) {
      apiRes.send({
        success: false,
        message: 'Merchant Display status failed'
      })
    } else {
      apiRes.send({
        success: true,
        message: 'Merchant Display status successfully updated.'
      })
    }
  })
}

merchantService.getadminusersdata = function (apiReq, apiRes) {
  console.log('!!!!!!!!!!!!!!!!!!!!!!!Entered')
  let findObj = {
    user_type: 'sales_team'
  }
  usersModel.find(findObj, function (updateErr, updateResult) {
    if (updateErr) {
      console.log('find Error', updateErr)
      apiRes.send({
        status: false,
        message: updateErr
      })
    } else {
      apiRes.send({
        status: true,
        message: 'details find successfully',
        data: updateResult
      })
    }
  })
}
merchantService.updateMerchants = function (apiReq, apiRes) {
  console.log('!!!!!!!!!!!!!!!!!!!!!!!Entered', apiReq.query)
  // { user_id: '5bed0230badcf81d21f56e7b',
  //    data_id: '5bee5b444a9e0b1e1633ea1c' }

  let findObj = {
    '_id': mongoose.Types.ObjectId(apiReq.query.user_id)
  }
  usersModel.find(findObj, function (updateErr, updateResult) {
    if (updateErr) {
      console.log('find Error', updateErr)
      apiRes.send({
        status: false,
        message: updateErr
      })
    } else {
      let created_by_parent = updateResult[0].parent_id
      let updateNew = {}
      updateNew.created_by = apiReq.query.user_id
      updateNew.created_by_parent = updateResult[0].parent_id
      updateNew.assigned_status = true
      merchantModel.findOneAndUpdate({
        _id: mongoose.Types.ObjectId(apiReq.query.data_id)
      }, {
        $set: updateNew
      }, {
        new: true
      },
      function (temps_find_error, updateusercap_resp) {
        if (temps_find_error) {
          console.log(temps_find_error)
            apiRes.send({
            status: false,
            message: temps_find_error
          })
        } else {
          console.log(updateusercap_resp)
            apiRes.send({
            status: true,
            message: 'details update  successfully',
            data: updateusercap_resp
          })
        }
      })
    }
  })
}

merchantService.updateIdsOfTransactions = function (transactions, index, callback) {
  console.log('tranasctions :: ', transactions.length, index)
  if (index === transactions.length) {
    return callback(null, true)
  } else {
    var transObj = transactions[index]
    var merchantId = transObj.merchant_id ? transObj.merchant_id : null
    var projectionFields = {
      _id: 1,
      brand_name: 1,
      created_by: 1,
      created_by_parent: 1
    }
    merchantModel.findOne({
      _id: merchantId
    }, projectionFields, (errFind, merchant) => {
      if (errFind) {
        console.error('Error while finding merchant for merchant Id :: ', merchantId)
        merchantService.updateIdsOfTransactions(transactions, index + 1, callback)
      } else if (!merchant) {
        console.log('************************ Merchant not found for given Id :: ', merchantId)
        merchantService.updateIdsOfTransactions(transactions, index + 1, callback)
      } else {
        console.log('merchant details at transaction update ::', merchant)
        var updateData = {
          $set: {
            created_by: merchant.created_by ? merchant.created_by : null,
            created_by_parent: merchant.created_by_parent ? merchant.created_by_parent : null
          }
        }
        transactionsModel.findOneAndUpdate({
          _id: transObj._id
        }, updateData, (errUpdate, updated) => {
          if (errUpdate) {
            console.error('Error while updating transaction with Id ::', transObj._id)
            merchantService.updateIdsOfTransactions(transactions, index + 1, callback)
          } else {
            console.log('Transaction successfully updated ::', transObj._id)
            merchantService.updateIdsOfTransactions(transactions, index + 1, callback)
          }
        })
      }
    })
  }
}

merchantService.updateTransactionIds = function (skip, limit) {
  transactionsModel.find({})
    .skip(skip)
    .limit(limit)
    .exec((errFindTrans, transactions) => {
      if (errFindTrans) {
        console.error('error while finding transactions')
        merchantService.updateTransactionIds(skip, limit)
      } else if (transactions.length === 0) {
        console.log('********** All Transactions completed *************')
        // merchantService.updateTransactionIds()
      } else {
        merchantService.updateIdsOfTransactions(transactions, 0, (error, response) => {
          if (error) {
            console.error('Error while updating transactionss..')
            merchantService.updateTransactionIds(skip + limit, limit)
          } else {
            console.log('Transactions Successfully Updated.')
            merchantService.updateTransactionIds(skip + limit, limit)
          }
        })
      }
    })
}
