const SMSAlertsModel = require('../models/smsAlertsModel')
const EmailAlertsModel = require('../models/EmailAlertsModel')
const PushAlertsModel = require('../models/PushAlertsModel')
var SMSAlertsService = module.exports

SMSAlertsService.getSMSAlerts = function (apiReq, apiRes) {
  SMSAlertsModel.find({}, {}, { skip: apiReq.body.skip, limit: apiReq.body.limit }, (errFind, alertsFound) => {
    if (errFind) {
      // console.log(errFind);

      apiRes.send({ success: false, message: 'Error while fetching sms alerts' })
    } else {
      // console.log(alertsFound)
      apiRes.send({ success: true, message: 'SMS Alerts successfully fetched.', smsAlerts: alertsFound })
    }
  })
}
SMSAlertsService.getCount = function (apiReq, apiRes) {
  console.log('getcount')
  SMSAlertsModel.countDocuments({}, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      console.log('count::: ', countResult)
      apiRes.send({
        'status': true,
        'message': 'count fetched successfully',
        'data': countResult
      })
    }
  })
}
SMSAlertsService.addSMSAlerts = function (apiReq, apiRes) {
  var alertObj = apiReq.body != undefined ? apiReq.body : null
  if (alertObj && Object.keys(alertObj).length) {
    new SMSAlertsModel(alertObj).save((errSave, alertSaved) => {
      if (errSave) {
        apiRes.send({ success: false, message: 'Something went wrong while adding sms alert.' })
      } else {
        apiRes.send({ success: true, message: 'SMS alert inserted successfully' })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Please enter valid data.' })
  }
}

SMSAlertsService.updateSMSAlert = function (apiReq, apiRes) {
  var alertObj = apiReq.body != undefined ? apiReq.body : null
  if (alertObj && Object.values(alertObj).length) {
    SMSAlertsModel.findByIdAndUpdate({ _id: alertObj._id }, { $set: alertObj }, (errUpdate, alertUpdated) => {
      if (errUpdate) {
        apiRes.send({ success: false, message: 'Something went wrong while updating sms alert.' })
      } else {
        apiRes.send({ success: true, message: 'SMS alert updated successfully' })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Please enter valid data.' })
  }
}

SMSAlertsService.editSMSAlerts = function (apiReq, apiRes) {
  var findObj = {}
  var editAlertId = apiReq.params.editAlertId != undefined ? apiReq.params.editAlertId : null
  if (editAlertId) {
    findObj['_id'] = editAlertId
    SMSAlertsModel.find(findObj, (errFind, alertsFound) => {
      if (errFind) {
        apiRes.send({ success: false, message: 'Error while fetching sms alerts' })
      } else {
        apiRes.send({ success: true, message: 'SMS Alerts successfully fetched.', smsAlerts: alertsFound })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Error while fetching sms alerts' })
  }
}

// Email=======================

SMSAlertsService.editemailAlerts = function (apiReq, apiRes) {
  var findObj = {}
  var editAlertId = apiReq.params.editAlertId != undefined ? apiReq.params.editAlertId : null
  if (editAlertId) {
    findObj['_id'] = editAlertId
    EmailAlertsModel.find(findObj, (errFind, alertsFound) => {
      if (errFind) {
        apiRes.send({ success: false, message: 'Error while fetching Email alerts' })
      } else {
        apiRes.send({ success: true, message: 'Email Alerts successfully fetched.', EmailAlerts: alertsFound })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Error while fetching Email alerts' })
  }
}

SMSAlertsService.getemailAlerts = function (apiReq, apiRes) {
  EmailAlertsModel.find({}, {}, { skip: apiReq.body.skip, limit: apiReq.body.limit }, (errFind, alertsFound) => {
    if (errFind) {
      // console.log(errFind);
      apiRes.send({ success: false, message: 'Error while fetching Email alerts' })
    } else {
      // console.log(alertsFound)111
      apiRes.send({ success: true, message: 'Email Alerts successfully fetched.', EmailAlerts: alertsFound })
    }
  })
}

SMSAlertsService.addEmailTemplate = function (apiReq, apiRes) {
  console.log(apiReq.body)
  var alertObj = apiReq.body != undefined ? apiReq.body : null
  if (alertObj && Object.keys(alertObj).length) {
    new EmailAlertsModel(alertObj).save((errSave, alertSaved) => {
      if (errSave) {
        apiRes.send({ success: false, message: 'Something went wrong while adding sms alert.' })
      } else {
        apiRes.send({ success: true, message: 'Email alert inserted successfully' })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Please enter valid data.' })
  }
}

SMSAlertsService.updateemailAlert = function (apiReq, apiRes) {
  var alertObj = apiReq.body != undefined ? apiReq.body : null
  if (alertObj && Object.keys(alertObj).length) {
    console.log('alertObj::::', alertObj)
    EmailAlertsModel.findByIdAndUpdate({ _id: alertObj._id }, { $set: alertObj }, (errUpdate, alertUpdated) => {
      if (errUpdate) {
        apiRes.send({ success: false, message: 'Something went wrong while updating sms alert.' })
      } else {
        apiRes.send({ success: true, message: 'SMS alert updated successfully' })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Please enter valid data.' })
  }
}

SMSAlertsService.getEmailCount = function (apiReq, apiRes) {
  EmailAlertsModel.countDocuments({}, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      console.log('count::: ', countResult)
      apiRes.send({
        'status': true,
        'message': 'count fetched successfully',
        'data': countResult
      })
    }
  })
}

// PUSH ALERTS

SMSAlertsService.editpushAlerts = function (apiReq, apiRes) {
  var findObj = {}
  var editAlertId = apiReq.params.editAlertId != undefined ? apiReq.params.editAlertId : null
  if (editAlertId) {
    findObj['_id'] = editAlertId
    PushAlertsModel.find(findObj, (errFind, alertsFound) => {
      if (errFind) {
        apiRes.send({ success: false, message: 'Error while fetching Email alerts' })
      } else {
        apiRes.send({ success: true, message: 'Push Alerts successfully fetched.', pushAlerts: alertsFound })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Error while fetching Push alerts' })
  }
}

SMSAlertsService.getepushAlerts = function (apiReq, apiRes) {
  PushAlertsModel.find({}, {}, { skip: apiReq.body.skip, limit: apiReq.body.limit }, (errFind, alertsFound) => {
    if (errFind) {
      // console.log(errFind);
      apiRes.send({ success: false, message: 'Error while fetching Push alerts' })
    } else {
      // console.log(alertsFound)111
      apiRes.send({ success: true, message: 'Push Alerts successfully fetched.', pushAlerts: alertsFound })
    }
  })
}

SMSAlertsService.addPushTemplate = function (apiReq, apiRes) {
  console.log(apiReq.body)
  var alertObj = apiReq.body != undefined ? apiReq.body : null
  if (alertObj && Object.keys(alertObj).length) {
    new PushAlertsModel(alertObj).save((errSave, alertSaved) => {
      if (errSave) {
        apiRes.send({ success: false, message: 'Something went wrong while adding sms alert.' })
      } else {
        apiRes.send({ success: true, message: 'Push alert inserted successfully' })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Please enter valid data.' })
  }
}

SMSAlertsService.updatepushAlert = function (apiReq, apiRes) {
  var alertObj = apiReq.body != undefined ? apiReq.body : null
  if (alertObj && Object.keys(alertObj).length) {
    console.log('alertObj::::', alertObj)
    PushAlertsModel.findByIdAndUpdate({ _id: alertObj._id }, { $set: alertObj }, (errUpdate, alertUpdated) => {
      if (errUpdate) {
        apiRes.send({ success: false, message: 'Something went wrong while updating push alert.' })
      } else {
        apiRes.send({ success: true, message: 'push alert updated successfully' })
      }
    })
  } else {
    apiRes.send({ success: false, message: 'Please enter valid data.' })
  }
}

SMSAlertsService.getpushCount = function (apiReq, apiRes) {
  PushAlertsModel.countDocuments({}, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      console.log('count::: ', countResult)
      apiRes.send({
        'status': true,
        'message': 'count fetched successfully',
        'data': countResult
      })
    }
  })
}

SMSAlertsService.gettemplateAuth = function (api_request, api_response) {
  try {
    let response_data = {}
    // response_data.permissions = api_request.user.permissions
    response_data.user_type = api_request.user.user_type
    // console.log("response----------------------->",api_response)respo
    api_response.send({ status: true, message: 'User authentication success.',data:response_data})
  } catch (exception) {
    Print.exception(exception)
    // Logs.exception("Exception while Preparing CONTACT QUERIES.", "ERRPRPQUR", exception + "", new Error());
    api_response.send({ status: true, message: 'User authentication failed.' })
  }
}
