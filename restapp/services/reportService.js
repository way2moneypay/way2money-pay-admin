var userDetails = require('../models/userDetails')
var userTransactions = require('../models/userTransactionsModel')
var notificationUtils = require('../utils/NotificationUtils')
var async = require('async')
var dateFormat = require('dateformat')
const merchantTransCycleModel = require('../models/merchantTransCycle')
const daywiseTransactionsModel = require('../models/daywiseTransactions')
const config = require('config')
const merchantsModel = require('../models/merchantModel')
const merchantComissionModel = require('../models/merchantComissionModel')
var mongoose = require('mongoose')

module.exports = {

  automatedEmailReport: () => {
    var queries = []; var reportObj = {}

    var today = dateFormat(Date.now(), 'yyyy-mm-dd')
    today = new Date(today)
    today = new Date(today.getTime() - 19800000)
    var dateValue5days = new Date()
    dateValue5days.setDate(dateValue5days.getDate() - 5)
    dateValue5days.setHours(0, 0, 0, 0)
    dateValue5days = dateValue5days / 1
    dateValue5days = dateValue5days - 19800000
    var dateValue15days = new Date()
    dateValue15days.setDate(dateValue15days.getDate() - 15)
    dateValue15days.setHours(0, 0, 0, 0)
    dateValue15days = dateValue15days / 1
    dateValue15days = dateValue15days - 19800000
    var dateValue20days = new Date()
    dateValue20days.setDate(dateValue20days.getDate() - 20)
    dateValue20days.setHours(0, 0, 0, 0)
    dateValue20days = dateValue20days / 1
    dateValue20days = dateValue20days - 19800000
    var dateValue30days = new Date()
    dateValue30days.setDate(dateValue30days.getDate() - 30)
    dateValue30days.setHours(0, 0, 0, 0)
    dateValue30days = dateValue30days / 1
    dateValue30days = dateValue30days - 19800000

    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)
    yesterday = (yesterday / 1) - 19800000
    queries.push(function (cb) {
      userDetails.count({
      }).exec(function (err, users) {
        if (err) {
          throw cb(err)
        }
        cb(null, users)
      })
    })
    queries.push(function (cb) {
      userTransactions.count({
        merchant_id: { $ne: mongoose.Types.ObjectId(config.merchant_id) },
        status: { $ne: 0 }
      }).exec(function (err, transactions) {
        if (err) {
          throw cb(err)
        }
        cb(null, transactions)
      })
    })
    queries.push(function (cb) {
      userTransactions.aggregate([
        {
          $match: {
            merchant_id: { $ne: mongoose.Types.ObjectId(config.merchant_id) },
            $or: [{ status: 1 }, { status: 5 }]
          }
        }, {
          $group: {
            _id: null,
            total: { $sum: '$amount' }
          }
        }
      ], function (err, transactions) {
        if (err) {
          throw cb(err)
        }
        cb(null, transactions)
      })
    })
    queries.push(function (cb) {
      userTransactions.aggregate([
        {
          $match: {
            merchant_id: { $ne: mongoose.Types.ObjectId(config.merchant_id) },
            status: 5
          }
        }, {
          $group: {
            _id: null,
            total: { $sum: '$amount' }
          }
        }
      ], function (err, transactions) {
        if (err) {
          throw cb(err)
        }
        cb(null, transactions)
      })
    })
    queries.push(function (cb) {
      userTransactions.find({
        merchant_id: { $ne: mongoose.Types.ObjectId(config.merchant_id) },
        status: 5
      }).distinct('user_id', function (err, payments) {
        if (err) {
          throw cb(err)
        }
        cb(null, payments)
      })
    })

    queries.push(function (cb) {
      userDetails.aggregate([
        {
          $match: {
            due_date: {
              $lt: today / 1,
              $gte: dateValue5days
            }
          }
        }, {
          $group: {
            _id: null,
            total: { $sum: '$credit_due' }
          }
        }

      ], function (err, pendingPayments) {
        if (err) {
          throw cb(err)
        } else {
          cb(null, pendingPayments)
        }
      // userTransactions.aggregate([
      //   {
      //     $match: {
      //       merchant_id: { $ne: mongoose.Types.ObjectId(config.merchant_id) },
      //       status: 1,
      //       transaction_date: {
      //         $lt: dateValue15days,
      //         $gte: dateValue20days
      //       }
      //     }
      //   }, {
      //     $group: {
      //       _id: null,
      //       count: { $sum: 1 },
      //       total: {
      //         $sum: '$amount'
      //       }
      //     }
      //   }], function (error, pendingPayments) {
      //   if (error) {
      //     console.log('error while fetching trans data', error)
      //     throw cb(error)
      //   } else {
      //     cb(null, pendingPayments)
      //   }
      // })
      })
    })
    queries.push(function (cb) {
      userDetails.aggregate([
        {
          $match: {
            due_date: {
              $lt: dateValue5days,
              $gte: dateValue15days

            }
          }
        }, {
          $group: {
            _id: null,
            total: { $sum: '$credit_due' }
          }
        }

      ], function (err, pendingPayments) {
        if (err) {
          throw cb(err)
        }
        cb(null, pendingPayments)
      })
    })
    queries.push(function (cb) {
      userDetails.aggregate([
        {
          $match: {
            due_date: {
              $lt: dateValue15days
            }
          }
        }, {
          $group: {
            _id: null,
            total: { $sum: '$credit_due' }
          }
        }

      ], function (err, pendingPayments) {
        if (err) {
          throw cb(err)
        }
        cb(null, pendingPayments)
      })
    })

    queries.push(function (cb) {
      userDetails.count(
        {
          due_date: {
            $lt: today / 1,
            $gte: dateValue5days
          },
          credit_due: { $exists: true, $ne: 0 }
        }
      ).exec(function (err, pendingPayments) {
        if (err) {
          throw cb(err)
        }
        cb(null, pendingPayments)
      })
    })
    queries.push(function (cb) {
      userDetails.count(
        {
          due_date: {
            $lt: dateValue5days,
            $gte: dateValue15days
          },
          credit_due: { $exists: true, $ne: 0 }
        }
      ).exec(function (err, pendingPayments) {
        if (err) {
          throw cb(err)
        }
        cb(null, pendingPayments)
      })
    })
    queries.push(function (cb) {
      userDetails.count(
        {
          due_date: {
            $lt: dateValue15days
          },
          credit_due: { $exists: true, $ne: 0 }
        }
      ).exec(function (err, pendingPayments) {
        if (err) {
          throw cb(err)
        }
        cb(null, pendingPayments)
      })
    })

    queries.push(function (cb) {
      userDetails.count({
        register_date: {
          $gte: yesterday,
          $lt: today / 1
        }
      }).exec(function (err, yesterdayUsers) {
        if (err) {
          throw cb(err)
        } else {
          cb(null, yesterdayUsers)
        }
      })
    })
    queries.push(function (cb) {
      userTransactions.count({
        merchant_id: { $ne: mongoose.Types.ObjectId(config.merchant_id) },
        transaction_date: {
          $gte: yesterday,
          $lt: today / 1
        },
        status: { $ne: 0 }
      }).exec(function (err, transactions) {
        if (err) {
          throw cb(err)
        }
        cb(null, transactions)
      })
    })
    queries.push(function (cb) {
      userTransactions.aggregate([
        {
          $match: {
            merchant_id: { $ne: mongoose.Types.ObjectId(config.merchant_id) },
            transaction_date: {
              $gte: yesterday,
              $lt: today / 1
            },
            status: { $ne: 0 }
          }
        }, {
          $group: {
            _id: null,
            total: { $sum: '$amount' }
          }
        }
      ], function (err, transactions) {
        if (err) {
          throw cb(err)
        }
        cb(null, transactions)
      })
    })
    // queries.push(function (cb) {
    //     userTransactions.aggregate([
    //         {
    //             $match: {
    //                 transaction_date:{
    //                     $gte:yesterday,
    //                     $lt:today
    //                 },
    //                 status: 5
    //             }
    //         }, {
    //             $group: {
    //                 _id: null,
    //                 total: { $sum: '$amount' }
    //             }
    //         }
    //     ], function (err, transactions) {
    //         if (err) {
    //             throw cb(err)
    //         }
    //         cb(null, transactions)

    //     })
    // })
    // queries.push(function (cb) {
    //     userTransactions.find({
    //         transaction_date:{
    //             $gte:yesterday,
    //             $lt:today
    //         },
    //         status: 5
    //     }).distinct("user_id", function (err, payments) {
    //         if (err) {
    //             throw cb(err)
    //         }
    //         cb(null, payments)

    //     })
    // })
    async.parallel(queries, function (err, docs) {
      // if any query fails
      if (err) {
        throw err
      }
      // console.log(docs)
      reportObj['customers_installed_app'] = docs[0]
      reportObj['no_of_transactions'] = docs[1]
      if (docs[2].length > 0) {
        reportObj['payment_made'] = Number((docs[2][0].total).toFixed(2))
      } else {
        reportObj['payment_made'] = 0
      }
      // reportObj['transaction_amount_per_merchant'] = docs[3];
      if (docs[3].length > 0) {
        reportObj['repaid_amount'] = Number((docs[3][0].total).toFixed(2))
      } else {
        reportObj['repaid_amount'] = 0
      }

      // reportObj['repaid_customers'] = docs[4].length;
      reportObj['customers_installed_app_yesterday'] = docs[11]
      reportObj['yesterday_transactions'] = docs[12]
      // reportObj['yesterday_transactions_amt'] = docs[13][0].total;
      if (docs[13].length > 0) {
        reportObj['yesterday_transactions_amt'] = Number((docs[13][0].total).toFixed(2))
      } else {
        reportObj['yesterday_transactions_amt'] = 0
      }
      // // reportObj['transaction_amount_per_merchant'] = docs[3];
      // if (docs[14].length > 0) {
      //     reportObj['repaid_amount_yesterday'] = docs[14][0].total;
      // } else {
      //     reportObj['repaid_amount_yesterday'] = 0;
      // }

      // reportObj['repaid_customers_yesterday'] = docs[15].length;
      if (docs[5].length > 0) {
        reportObj['amount_not_paid_beyond_15days'] = Number((docs[5][0].total).toFixed(2))
      } else {
        reportObj['amount_not_paid_beyond_15days'] = 0
      }
      if (docs[6].length > 0) {
        reportObj['amount_not_paid_beyond_20days'] = Number((docs[6][0].total).toFixed(2))
      } else {
        reportObj['amount_not_paid_beyond_20days'] = 0
      }
      if (docs[7].length > 0) {
        reportObj['amount_not_paid_beyond_30days'] = Number((docs[7][0].total).toFixed(2))
      } else {
        reportObj['amount_not_paid_beyond_30days'] = 0
      }

      reportObj['customers_not_paid_beyond_15days'] = docs[8]
      reportObj['customers_not_paid_beyond_20days'] = docs[9]
      reportObj['customers_not_paid_beyond_30days'] = docs[10]

      // if ((docs[7].length != 0) && (docs[6].length != 0) && docs[2].length != 0 && docs[3].length != 0) {
      //     reportObj['percentage'] = (docs[7][0].total + docs[6][0].total) / (docs[2][0].total + docs[3][0].total);
      // } else if ((docs[7].length != 0) && docs[2].length != 0 && docs[3].length == 0) {
      //     reportObj['percentage'] = docs[7][0].total / docs[2][0].total;
      // } else if ((docs[7].length != 0) && docs[2].length == 0 && docs[3].length != 0) {
      //     reportObj['percentage'] = docs[7][0].total / docs[3][0].total;
      // } else {
      //     reportObj['percentage'] = 0;
      // }

      if (reportObj.payment_made === 0) {
        reportObj['percentage'] = 0
      } else {
        // console.log(reportObj)
        reportObj['percentage'] = (Number(((reportObj.payment_made - reportObj.repaid_amount) / reportObj.payment_made).toFixed(2))) * 100
      }

      today = new Date()
      yesterday = new Date(today)
      yesterday.setDate(today.getDate() - 1)
      yesterday = dateFormat(yesterday, 'mmmm dS, yyyy')
      reportObj['date'] = yesterday
      // console.log(reportObj)
      let configuration = {}
      /*, "accounts@way2online.net",
            "raju@way2online.net",
            "suresh@way2online.net","saikumar.d@way2online.co.in", "vikrant.d@way2online.co.in", "prabhakar@way2online.net", "goutham.b@way2online.com", "anudeep@way2online.net" */
      const emails = config.emails
      configuration.emails = emails
      configuration.replace = reportObj
      // console.log(configuration.emails);
      notificationUtils.email('email-report', configuration, function (err, resp) {
        console.error(err)
        // console.log(resp);
      })
      // res.send(reportObj)
    })
  },

  merchantTransCycle: () => {
    var getday = new Date().getDay()
    var dateValue4days = new Date()
    dateValue4days.setDate(dateValue4days.getDate() - 4)
    dateValue4days.setHours(0, 0, 0, 0)
    dateValue4days = dateValue4days / 1
    dateValue4days = dateValue4days - 19800000
    var dateValue3days = new Date()
    dateValue3days.setDate(dateValue3days.getDate() - 3)
    dateValue3days.setHours(0, 0, 0, 0)
    dateValue3days = dateValue3days / 1
    dateValue3days = dateValue3days - 19800000

    var today = dateFormat(Date.now(), 'yyyy-mm-dd')
    today = new Date(today)
    today = new Date(today.getTime() - 19800000)
    //     var getday= today.getDay();
    var day = ''; var startDate = 0; var endDate = 0; var matchObj = {}
    if (getday === 2) {
      startDate = dateValue4days
      endDate = today / 1
      day = 'Tuesday'
      matchObj = {
        transaction_date: {
          $gte: dateValue4days,
          $lt: today / 1
        },
        $or: [{ status: 1 }, { status: 5 }]
      }
    } else if (getday === 5) {
      day = 'Friday'
      startDate = dateValue3days
      endDate = today / 1
      matchObj = {
        transaction_date: {
          $gte: dateValue3days,
          $lt: today / 1
        },
        $or: [{ status: 1 }, { status: 5 }]

      }
    }
    if (getday === 2 || getday === 5) {
      userTransactions.aggregate([
        {
          $match: matchObj
        }, {
          $group: {
            _id: '$merchant_id',
            total: { $sum: '$amount' },
            transactions: { $sum: 1 }
          }
        }
      ], function (err, merchantStats) {
        if (err) {
          console.log('Error in merchantStats', err)
        } else {
          if (merchantStats.length > 0) {
            insertIntoMerchantTransCycle(merchantStats, day, startDate, endDate)
          }
        }
      })
    }
  },
  daywiseTransactions: () => {
    var yesterday = new Date()
    yesterday.setDate(yesterday.getDate() - 1)
    yesterday.setHours(0, 0, 0, 0)
    userTransactions.find({ transaction_date: { $lt: (yesterday / 1) - 19800000 }, $or: [{ status: 1 }, { status: 5 }] }, function (error, transactions) {
      if (error) {
        console.log(error)
      } else {
        async.forEach(transactions, (transaction, callbackForEach) => {
          if (transaction.transaction_date !== undefined && transaction.amount !== undefined) {
            var transactionDate = new Date(transaction.transaction_date)
            transactionDate = transactionDate.setHours(0, 0, 0, 0, 0)
            transactionDate = (new Date(transactionDate).getTime() - 19800000)
            //  transaction_date = transaction_date/1;

            daywiseTransactionsModel.findOneAndUpdate({
              trans_date: transactionDate
            }, {
              $inc: {
                transactions: 1,
                trans_amount: transaction.amount
              }
            }, {
              upsert: true
            },
            function (updateError, updateResult) {
              // console.log(updateResult)
              if (updateError) {
                console.log('Error while updating merchant trans stats', updateError)
              } else {
                // console.log("sucess")

                callbackForEach()
              }
            })
          } else {
            callbackForEach()
          }
        }, (success) => {
          console.log('Updated successfully')
        })
      }
    })
  }

}

let insertIntoMerchantTransCycle = function (merchantStats, day, startDate, endDate) {
  var today = dateFormat(Date.now(), 'yyyy-mm-dd')
  today = new Date(today)
  today = new Date(today.getTime() - 19800000)
  // today.setHours(0,0,0,0,0)
  async.eachSeries(merchantStats, (item, callback) => {
    merchantComissionModel.findOne({ merchant_id: item._id }, function (error, result) {
      if (error) {
        console.log('Error while fetching merchant details')
      } else {
        if (Object.getOwnPropertyNames(result).length !== 0) {
          if (result.commission_rate !== undefined) {
            var fAmount = item.total - (item.total * result.commission_rate) / 100
            merchantsModel.findOne({
              _id: item._id
            },
            function (findError, findResult) {
              // console.log(updateResult)
              if (findError) {
                console.log('Error while fetching merchants', findError)
              } else {
                if (findResult != null && findResult !== undefined) {
                  if (Object.getOwnPropertyNames(findResult).length !== 0) {
                    if (findResult.previous_cycle_paid_refund_amount < item.total) {
                      merchantsModel.findOneAndUpdate({
                        _id: item._id
                      }, { $set: { previous_cycle_paid_refund_amount: 0 } },
                      function (findError, findResult) {
                        // console.log(updateResult)
                        if (findError) {
                          console.log('Error while updating merchants', findError)
                        } else {
                          var obj = {
                            amount: item.total,
                            cycle_day: day,
                            cycle_status: 'pending',
                            final_amount: fAmount,
                            commission_rate: result.commission_rate,
                            transactions: item.transactions,
                            start_date: startDate,
                            end_date: endDate
                          }
                          merchantTransCycleModel.findOneAndUpdate({
                            merchant_id: item._id, cycle_date: today / 1
                          }, {
                            $set: obj

                          }, { upsert: true },
                          (error, saved) => {
                            if (error) {
                              console.log('Error while saving in merchant trans cycle', error)
                            } else {
                              callback()
                            }
                          })
                        }
                      })
                    } else {
                      var Amount = item.total - findResult.previous_cycle_paid_refund_amount
                      var obj = {
                        amount: Amount,
                        cycle_day: day,
                        cycle_status: 'pending',
                        final_amount: fAmount,
                        commission_rate: result.commission_rate,
                        transactions: item.transactions,
                        start_date: startDate,
                        end_date: endDate
                      }
                      merchantTransCycleModel.findOneAndUpdate({
                        merchant_id: item._id, cycle_date: today / 1
                      }, {
                        $set: obj

                      }, { upsert: true },
                      (error, saved) => {
                        if (error) {
                          console.log('Error while saving in merchant trans cycle', error)
                        } else {
                          callback()
                        }
                      })
                    }
                  }
                }
              }
            })
          }
        }
      }
    })
  }, (success) => {
    console.log('successfully inserted in merchant trans cycle')
  })
}
