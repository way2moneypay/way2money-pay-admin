const logsModel = require('../models/logsModal')
const shortUrls = require('../models/shortUrls')
var logsService = module.exports
logsService.getCount = function (apiReq, apiRes) {
  logsModel.countDocuments({}, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      console.log('count::: ', countResult)
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': countResult
      })
    }
  })
}
logsService.getLogsDetails = function (apiReq, apiRes) {
  logsModel.find({}, {}, {
    skip: apiReq.body.skip,
    limit: apiReq.body.limit
  },
    function (userError, userResult) {
      if (userError) {
        apiRes.send({
          'status': false,
          'message': userError
        })
      } else if (userResult.length) {
        // console.log("logs:: ",userResult);
        apiRes.send({
          'status': true,
          'message': 'details fetched successfully',
          'data': userResult
        })
      } else {
        apiRes.send({
          'status': false,
          'message': 'details not found'
        })
      }
    }).sort({
      date: -1
    })
}

logsService.getPaymentLinksCount = function (apiReq, apiRes) {
  shortUrls.countDocuments({}, function (countError, countResult) {
    if (countError) {
      console.log('Error', countError)
      apiRes.send({
        'status': false,
        'message': countError
      })
    } else {
      console.log('count::: ', countResult)
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': countResult
      })
    }
  })
}

logsService.getPaymentLinks = function (apiReq, apiRes) {
  console.log('request--------------->', apiReq.body)
  let findFilter = {}
  findFilter['skip'] = apiReq.body.skip
  findFilter['limit'] = apiReq.body.limit
  // findFilter.push({
  //   $match: {}
  // })

  // findFilter.push({
  //   $skip: apiReq.body.skip
  // }, {
  //   $limit: apiReq.body.limit
  // }, {
  //   $lookup: {
  //     from: 'user_details',
  //     localField: 'user_id',
  //     foreignField: '_id',
  //     as: 'UserDetails'
  //   }
  // }, {
  //   $unwind: '$UserDetails'
  // })
  console.log('Find Filter :: ', findFilter)
  shortUrls.find({})
    .sort({
      last_sent: -1
    })
    .skip(apiReq.body.skip)
    .limit(apiReq.body.limit)
    .exec(function (userError, userResult) {
      console.log('Error :: ', userError)
      console.log('Result length :: ', userResult.length)
      if (userError) {
        console.log('error', userError)
        apiRes.send({
          'status': false,
          'message': userError
        })
      } else if (userResult.length) {
        // console.log('logs:: ', userResult)
        apiRes.send({
          'status': true,
          'message': 'details fetched successfully',
          'data': userResult
        })
      } else {
        apiRes.send({
          'status': false,
          'message': 'details not found'
        })
      }
    })
}

logsService.userSearch = function (apiReq, apiRes) {
  let findObj = {}
  if (apiReq.body.mobile_number) {
    findObj['mobile_number'] = apiReq.body.mobile_number
  }
  if (apiReq.body.type != undefined && apiReq.body.type != null) {
    if (apiReq.body.fromDate && apiReq.body.toDate) {
      findObj[apiReq.body.type] = {
        $gte: Number(apiReq.body.fromDate),
        $lte: Number(apiReq.body.toDate)
      }
    } else if (apiReq.body.fromDate) {
      findObj[apiReq.body.type] = {
        $gte: Number(apiReq.body.fromDate)
      }
    } else if (apiReq.body.toDate) {
      findObj[apiReq.body.type] = {
        $lte: Number(apiReq.body.toDate)
      }
    }
  }
  console.log('apireqBody', findObj)
  shortUrls.find(findObj, function (userError, userResult) {
    if (userError) {
      apiRes.send({
        'status': false,
        'message': userError
      })
    } else if (userResult.length) {
      // console.log(userResult);
      apiRes.send({
        'status': true,
        'message': 'details fetched successfully',
        'data': userResult
      })
    } else {
      apiRes.send({
        'status': false,
        'message': 'details not found'
      })
    }
  }).sort({
    _id: -1
  })
}