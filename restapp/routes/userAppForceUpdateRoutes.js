const AuthenticateUtils = require('./../utils/AuthenticateUtils')
const userAppForceUpdateService = require('../services/userAppForceUpdate')
module.exports = function (app) {
  app.get('/api/get-versions', AuthenticateUtils.userAuthenticationWithQuery, userAppForceUpdateService.getAllVersions)
  app.post('/api/add-version', AuthenticateUtils.userAuthenticationWithQuery, userAppForceUpdateService.addVersion)
}
