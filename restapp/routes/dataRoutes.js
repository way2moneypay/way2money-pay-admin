var dataService = require('./../services/dataService')
const AuthenticateUtils = require('./../utils/AuthenticateUtils')

module.exports = function (app) {
  app.post('/api/data/remove-sms-data', AuthenticateUtils.userAuthenticationWithQuery, dataService.removeSmsData)
  app.post('/api/data/repredict-credit-limit', AuthenticateUtils.userAuthenticationWithQuery, dataService.rePredictCreditLimit)
  app.post('/api/data/predict-credit-limit', AuthenticateUtils.userAuthenticationWithQuery, dataService.predictCreditLimit)
}