const merchantTransCycleService = require('../services/merchantTransCycleService')
// const viewClaimsService = require('../services/view_claims_service')

const AuthenticateUtils = require('./../utils/AuthenticateUtils')

module.exports = function (app) {
  app.post('/merchant/transCycles', AuthenticateUtils.userAuthenticationWithQuery, merchantTransCycleService.getTransactions)
  app.post('/merchant/transCycleCounts', AuthenticateUtils.userAuthenticationWithQuery, merchantTransCycleService.getTransactionsCount)
  app.post('/merchant/transCycle/search', AuthenticateUtils.userAuthenticationWithQuery, merchantTransCycleService.viewTransactionsSearch)
  app.post('/merchant/transCycle/update', AuthenticateUtils.userAuthenticationWithQuery, merchantTransCycleService.updateTransactions)
  app.post('/merchant/pendingCycleCount', AuthenticateUtils.userAuthenticationWithQuery, merchantTransCycleService.pendingCycleCount)
  app.post('/merchant/getCycleTransactions', AuthenticateUtils.userAuthenticationWithQuery, merchantTransCycleService.getCycleTransactions)
  app.post('/merchant/getMerchant', AuthenticateUtils.userAuthenticationWithQuery, merchantTransCycleService.getMerchant)
  app.post('/merchant/getCycleTransactionsForEmail', AuthenticateUtils.userAuthenticationWithQuery, merchantTransCycleService.getCycleTransactionsForEmail)
}
