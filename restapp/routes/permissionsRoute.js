const userService = require('../services/userService')
const smsAlertsService = require('../services/sms_alerts_service')
const AuthenticateUtils = require('./../utils/AuthenticateUtils')
module.exports = function (app) {
  app.post('/api/addpermissions', (request, response, next) => {
    request.user_permissions = 'add_permissions'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, userService.addpermissions)
  app.get('/api/getpermissions', AuthenticateUtils.userAuthenticationWithQuery, userService.getpermissions)
  app.post('/api/adduserpermissions', AuthenticateUtils.userAuthenticationWithQuery, userService.adduserpermissions)
  app.get('/api/getPermissionsList', AuthenticateUtils.userAuthenticationWithQuery, userService.getPermissionsList)
  app.post('/api/edituserpermissions', AuthenticateUtils.userAuthenticationWithQuery, userService.edituserpermissions)
  app.get('/api/deleteuserpermissions', AuthenticateUtils.userAuthenticationWithQuery, userService.deleteuserpermissions)
  app.post('/api/addadminuser', AuthenticateUtils.userAuthenticationWithQuery, userService.addadminuser)
  app.get('/api/getAdminUsersList', AuthenticateUtils.userAuthenticationWithQuery, userService.getAdminUsersList)
  app.get('/api/EditAdminUsersList', AuthenticateUtils.userAuthenticationWithQuery, userService.EditAdminUsersList)
  app.post('/api/updateadminuser', AuthenticateUtils.userAuthenticationWithQuery, userService.updateadminuser)
  app.get('/api/user/userPermissions', AuthenticateUtils.userAuthentication, userService.getuserPermissions)
  app.get('/api/gettemplateAuth', (request, response, next) => {
    request.user_permissions = 'permissions'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.gettemplateAuth)
  // app.get('/api/getPermissionsListBHAGAVAN', AuthenticateUtils.userAuthenticationWithQuery, userService.getPermissionsListBHAGAVAN)
}
