const npaDetailsService = require('../services/npaDetailsService')
// const viewClaimsService = require('../services/view_claims_service')

const AuthenticateUtils = require('./../utils/AuthenticateUtils')

module.exports = function (app) {
  app.get('/npa-details', AuthenticateUtils.userAuthenticationWithQuery, npaDetailsService.getNpaDetails)
  app.post('/pendingTransactions', AuthenticateUtils.userAuthenticationWithQuery, npaDetailsService.getPendingTransactions)
}
