const applicationService = require('../services/settingsService');
const AuthenticateUtils = require('./../utils/AuthenticateUtils');
const logsService = require('../services/logsService');
const smsAlertsService = require('../services/sms_alerts_service');

module.exports = function (app) {
    // console.log("********************888888")
    app.post('/add-settings', (request, response, next) => {
        request.user_permissions = "settings";
        next();
    }, AuthenticateUtils.userAuthenticationWithQuery, applicationService.addSettings);
    app.get('/view-settings', (request, response, next) => {
        request.user_permissions = "settings";
        next();
    }, AuthenticateUtils.userAuthenticationWithQuery, applicationService.viewSettings);
    app.get('/view-settingss', (request, response, next) => {
        request.user_permissions = "add_merchant";
        next();
    }, AuthenticateUtils.userAuthenticationWithQuery, applicationService.viewSettings);
    app.get('/logs/authcheck', (request, response, next) => {
        request.user_permissions = "error_logs";
        next();
    }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.gettemplateAuth);
    app.post('/logs/getLogs', AuthenticateUtils.userAuthenticationWithQuery, logsService.getLogsDetails);
    app.get('/logs/getCount', AuthenticateUtils.userAuthenticationWithQuery, logsService.getCount);
    app.get('/paymentStatus', AuthenticateUtils.userAuthenticationWithQuery, applicationService.getPaymentsStatus);
    app.post('/updatePaymentStatus', AuthenticateUtils.userAuthenticationWithQuery, applicationService.updatePaymentStatus);
    app.post('/api/getPaymentLinks', (request, response, next) => {
        request.user_permissions = "settings";
        next();
    }, AuthenticateUtils.userAuthenticationWithQuery, logsService.getPaymentLinks);
    app.get('/api/getPaymentLinksCount', (request, response, next) => {
        request.user_permissions = "settings";
        next();
    }, AuthenticateUtils.userAuthenticationWithQuery, logsService.getPaymentLinksCount);

    app.post('/api/paymentlinksbysms', (request, response, next) => {
        request.user_permissions = "settings";
        next();
    }, AuthenticateUtils.userAuthenticationWithQuery, logsService.userSearch);
}
