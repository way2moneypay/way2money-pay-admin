const AuthenticateUtils = require('./../utils/AuthenticateUtils')
const smsAlertsService = require('../services/sms_alerts_service')
const applicationService = require('../services/settingsService');

module.exports = function (app) {
  console.log('coming to routes file')
  app.post('/api/sms-alerts', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.getSMSAlerts);
  app.post('/api/sms-alerts/register', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.addSMSAlerts);
  app.get('/api/sms-alerts/edit/:editAlertId', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.editSMSAlerts);
  app.post('/api/sms-alerts/update', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.updateSMSAlert);
  app.get('/api/sms-alerts/getCount', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.getCount);
  app.get('api/sms-alerts/gettemplateAuth', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.gettemplateAuth);

  // Email Alerts
  app.post('/api/email-alerts', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.getemailAlerts);
  app.post('/api/email-alerts/register', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.addEmailTemplate);
  app.get('/api/email-alerts/edit/:editAlertId', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.editemailAlerts);
  app.post('/api/email-alerts/update', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.updateemailAlert);
  app.get('/api/email-alerts/getCount', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.getEmailCount);

  // Push Alerts
  app.post('/api/push-alerts', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.getepushAlerts);
  app.post('/api/push-alerts/register', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.addPushTemplate);
  app.get('/api/push-alerts/edit/:editAlertId', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.editpushAlerts);
  app.post('/api/push-alerts/update', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.updatepushAlert);
  app.get('/api/push-alerts/getCount', (request, response, next) => {
    request.user_permissions = 'settings'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.getpushCount);
  app.get('/api/sendSmsAlerts', (request, response, next) => {
    request.user_permissions = "settings";
    next();
  }, AuthenticateUtils.userAuthenticationWithQuery, applicationService.sendSmsAlerts);
  app.get('/api/sendPushAlerts', (request, response, next) => {
    request.user_permissions = "settings";
    next();
  }, AuthenticateUtils.userAuthenticationWithQuery, applicationService.sendPushAlerts);
  app.get('/api/SendEmail', (request, response, next) => {
    request.user_permissions = "settings";
    next();
  }, AuthenticateUtils.userAuthenticationWithQuery, applicationService.SendEmail);
  app.get('/api/getSmsCounts', (request, response, next) => {
    request.user_permissions = "settings";
    next();
  }, AuthenticateUtils.userAuthenticationWithQuery, applicationService.getSmsCounts)
}
