const clearUserService = require('../services/clearUserService')
const AuthenticateUtils = require('./../utils/AuthenticateUtils')

module.exports = function (app) {
  app.post('/clear-user', clearUserService.clearUser)
  app.post('/clear-merchant', clearUserService.clearMerchant)
}
