'use strict'

const MerchantServices = require('./../services/merchant_services')
const QRCodeGenerator = require('./../services/QrCodeGenerator')
const AuthenticateUtils = require('./../utils/AuthenticateUtils')

module.exports = function (app) {
  app.post('/api/merchant/register', AuthenticateUtils.userAuthenticationWithQuery, MerchantServices.registerMerchant)
  app.post('/api/merchant/update', AuthenticateUtils.userAuthenticationWithQuery, MerchantServices.updateMerchant)
  app.get('/api/getMerchant/:merchantId', AuthenticateUtils.userAuthenticationWithQuery, MerchantServices.getMerchantDetails)
  app.post('/api/uploadDoc', MerchantServices.uploadDoc)
  app.get('/merchant/getBrandsList', (request, response, next) => {
    request.user_permissions = 'merchant_payment_cycles'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, MerchantServices.getBrandsList)
  // app.get('/api/banks', MerchantServices.getBanks);
  // app.get('/api/bank_details/:bankId', MerchantServices.getBankDetails);
  // app.get('/api/branches/:bankId', MerchantServices.getBranches);
  app.get('/api/banks', MerchantServices.banks)
  // app.get("/api/banks_branches", MerchantServices.bankBranches);
  app.get('/api/bank_ifsccodes', AuthenticateUtils.userAuthenticationWithQuery, MerchantServices.bankIFSC)
  app.get('/api/merchant-generate-qrcode', QRCodeGenerator.generateQrCode)
  app.get('/api/merchant-download-qrcode/:merchant_id', QRCodeGenerator.download)
}
