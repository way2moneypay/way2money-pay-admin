const dashboard = require('../services/dashboard')
const userService = require('../services/userService')
const AuthenticateUtils = require('./../utils/AuthenticateUtils')
module.exports = function (app) {
  app.post('/login', dashboard.login)
  app.get('/api/user/authentication_info_call', AuthenticateUtils.userAuthenticationWithQuery, userService.getuserPermissions)
  app.get('/users_count', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.users_count)
  app.get('/transactions_count', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.transactions_count)
  app.get('/sales_transactions_count', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.sales_transactions_count)
  // app.get('/sales_payments_count', (request, response, next) => {
  //   request.user_permissions = 'dashboard'
  //   next()
  // }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.sales_payments_count)
  // app.get('/transactions_amount', dashboard.transactions_amount)
  app.get('/month_report/:days', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.month_report)
  app.get('/merchants_count', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.get_merchants)
  // app.get('/merchants_amount', dashboard.merchants_amount)
  // app.get('/amount_to_users', dashboard.amount_to_users)
  // app.get('/payment_by_users', dashboard.payment_by_users)
  // app.get('/earnings', dashboard.earnings)
  app.get('/users_count_merchant/:id', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.users_count_merchant)
  app.get('/merchant_transactions_count/:id', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.merchant_transactions_count)
  app.get('/merchant_transactions_amt/:id', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.merchant_transactions_amt)

  app.get('/merchant_month_report/:days/:id', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.merchant_month_report)
  // user payment counts
  app.get('/user_payment_counts', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.user_payment_counts)
  app.get('/user_payment_daywise', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.user_payment_daywise)
  app.get('/sales_user_payment_daywise', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.user_payment_daywise)
  // app.get('/user_paid_amount', AuthenticateUtils.userAuthenticationWithQuery, dashboard.user_paid_amount)
  // app.get('/user_app_usage', AuthenticateUtils.userAuthenticationWithQuery, dashboard.user_app_usage)
  app.post('/getTotalEvents', AuthenticateUtils.userAuthenticationWithQuery, dashboard.getTotalEvents)
  app.get('/api/getPerfamanceData', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.getPerfamanceData)
  app.get('/api/regionalHeadSalesTeamData', (request, response, next) => {
    request.user_permissions = 'dashboard'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, dashboard.regional_head_sales_payments_count)
}
