const userService = require('../services/userService')
const AuthenticateUtils = require('./../utils/AuthenticateUtils')
const smsAlertsService = require('../services/sms_alerts_service')
const userEventsService = require('../userEventsStats')
const offersService = require('../services/offersService')
module.exports = function (app) {
  app.post('/user/transactions', (request, response, next) => {
    request.user_permissions = 'user_transactions'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, userService.getuserTransactions)
  app.post('/user/payments', AuthenticateUtils.userAuthenticationWithQuery, userService.getUserPayments)
  app.post('/user/getUsers', AuthenticateUtils.userAuthenticationWithQuery, userService.getUserDetails)
  app.post('/user/search', AuthenticateUtils.userAuthenticationWithQuery, userService.userSearch)
  app.post('/user/payments/search', AuthenticateUtils.userAuthenticationWithQuery, userService.paymentSearch)
  app.post('/user/transactions/search', AuthenticateUtils.userAuthenticationWithQuery, userService.transactionSearch)
  app.post('/user/refund', AuthenticateUtils.userAuthenticationWithQuery, userService.refund)
  app.post('/user/checkRefund', AuthenticateUtils.userAuthenticationWithQuery, userService.checkRefund)
  app.get('/user/getCount', AuthenticateUtils.userAuthenticationWithQuery, userService.getCount)
  app.post('/user/getPaymentsCount', AuthenticateUtils.userAuthenticationWithQuery, userService.getPaymentsCount)
  app.get('/user/getTransactionsCount/:merchantId', userService.getTransactionsCount)
  app.post('/user/getMerchantId', userService.getMerchantId)
  app.get('/api/user/details/:merchantId', userService.getDetails)
  app.get('/api/getTransaction/:transactionId', userService.getTransaction)
  app.post('/api/transaction/update', AuthenticateUtils.userAuthenticationWithQuery, userService.updateTransaction)
  app.post('/user/authentication_info', AuthenticateUtils.userAuthenticationWithQuery, userService.getAuthenticateInfo)
  app.post('/user/searchingBrands', userService.searchingBrands)
  app.post('/user/fetchUserPayments', (request, response, next) => {
    request.user_permissions = 'user_payments'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, userService.fetchUserPayments)
  app.get('/user/authcheck', (request, response, next) => {
    request.user_permissions = 'view_user'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, smsAlertsService.gettemplateAuth)
  app.post('/user/paynear-locations', (request, response, next) => {
    request.user_permissions = 'paynear_locations'
    next()
  }, AuthenticateUtils.userAuthenticationWithQuery, userService.getPaynearLocations)
  app.post('/user/check_payment_status', AuthenticateUtils.userAuthenticationWithQuery, userService.checkPaymentStatus)
  app.post('/user/user-credits', AuthenticateUtils.userAuthenticationWithQuery, userService.insertUserCredits)
  app.get('/user/get-user-credits', AuthenticateUtils.userAuthenticationWithQuery, userService.getUserCredits)
  app.post('/user/edit-user-credits', AuthenticateUtils.userAuthenticationWithQuery, userService.EditUserCredits)
  app.post('/user/deleteValue', AuthenticateUtils.userAuthenticationWithQuery, userService.deleteValue)
  app.get('/user/usersCount', AuthenticateUtils.userAuthenticationWithQuery, userService.getUserCounts)
  app.post('/user/edit-user-details', AuthenticateUtils.userAuthenticationWithQuery, userService.EditUserDetails)
  app.post('/user/smscounts', AuthenticateUtils.userAuthenticationWithQuery, userService.getSmsCounts)
  app.post('/user/view-events', AuthenticateUtils.userAuthenticationWithQuery, userEventsService.userEvents)
  app.post('/user/events-search', AuthenticateUtils.userAuthenticationWithQuery, userEventsService.getEvents)
  app.post('/user/PredictedAmountDetails', AuthenticateUtils.userAuthenticationWithQuery, userService.PredictedAmountDetails)
  app.get('/api/LoginUserDetails', AuthenticateUtils.userAuthentication, function (request, response) {
    // console.log("request.user :: " , request.user);
    return response.status(200).send({
      type: 'SUCCESS',
      data: request.user,
      status: 200,
      status_code: 'SUCCESS'
    })
  })
  app.get('/api/get_regional_head', AuthenticateUtils.userAuthenticationWithQuery, userService.getReginalheads)
  app.get('/api/getAllPermission', (request, response, next) => {
    request.user_permissions = ['add_new_user']
    next()
  }, AuthenticateUtils.userAuthentication, userService.getUserPermission)
  app.post('/api/setTarget', AuthenticateUtils.userAuthenticationWithQuery, userService.setTarget)
  app.get('/adminuser/getTargetData', AuthenticateUtils.userAuthenticationWithQuery, userService.getTargetData)
  app.post('/offers/edit-offer', AuthenticateUtils.userAuthenticationWithQuery, offersService.editOffer)
  app.post('/offer/add-offer', AuthenticateUtils.userAuthenticationWithQuery, offersService.addOffer)
  app.post('/api/get-Offers-list', AuthenticateUtils.userAuthenticationWithQuery, offersService.getoffersList)
  app.get('/api/get-Offers-list/getCount', AuthenticateUtils.userAuthenticationWithQuery, offersService.getCount)
  app.get('/api/get-Offers-list/:editOfferId', AuthenticateUtils.userAuthenticationWithQuery, offersService.editOfferList)
  app.get('/api/changeOffersStatus', AuthenticateUtils.userAuthenticationWithQuery, offersService.changeOffersStatus)
  app.get('/api/dueUsers', AuthenticateUtils.userAuthenticationWithQuery, userService.getDueAmountUsers)
  app.post('/api/contactedUser', AuthenticateUtils.userAuthenticationWithQuery, userService.contactedUser)
  app.get('/api/getCommentsData', AuthenticateUtils.userAuthenticationWithQuery, userService.getCommentsData)
  app.post('/api/getCountOfUsers', AuthenticateUtils.userAuthenticationWithQuery, userService.getCountOfUsers)
  app.post('/api/fetchUsers', AuthenticateUtils.userAuthenticationWithQuery, userService.fetchUsers)
}
