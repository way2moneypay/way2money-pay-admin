const claimRequestService = require('../services/claim_requests_service')
const viewClaimsService = require('../services/view_claims_service')
const AuthenticateUtils = require('./../utils/AuthenticateUtils')

module.exports = function (app) {
  app.get('/merchant/getClaimRequestsCount', AuthenticateUtils.userAuthenticationWithQuery, claimRequestService.getClaimsCount)
  app.post('/merchant/claimRequests', AuthenticateUtils.userAuthenticationWithQuery, claimRequestService.getClaimRequests)
  app.post('/merchant/merchantClaims', claimRequestService.merchantClaim)
  app.get('/merchant/getMerchantId/:id', claimRequestService.getMerchantId)
  app.post('/merchant/claimRequests/search', AuthenticateUtils.userAuthenticationWithQuery, claimRequestService.claimRequestSearch)
  app.post('/merchant/viewClaims', viewClaimsService.getClaims)
  app.get('/merchant/viewClaimsCount', viewClaimsService.getClaimsCount)
  app.post('/merchant/viewClaims/search', viewClaimsService.viewClaimsSearch)
}
