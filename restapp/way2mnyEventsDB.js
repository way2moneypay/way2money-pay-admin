'use strict'
const Mongoose = require('mongoose').Mongoose
Mongoose.Promise = require('bluebird')
const config = require('config')
var processExit = false

const options = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 25, // Maintain up to 25 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  promiseLibrary: require('bluebird'),
  useNewUrlParser: true
}

const mongoWay2mnyEvents = new Mongoose()

const db = mongoWay2mnyEvents.connection

db.once('error', function (err) {
  console.error('mongoose connection error' + err)
  mongoWay2mnyEvents.disconnect()
})
db.on('open', function () {
  console.log('successfully connected to mongoWay2mnyEvents DB')
})
db.on('reconnected', function () {
  console.log('MongoDB reconnected!')
})
db.on('disconnected', function () {
  console.log('MongoDB disconnected!!!')
  if (!processExit) {
    mongoWay2mnyEvents.connect(config.mongodb.Eventsuri, options)
      .then(() => console.log('connection successful to Events DB'))
      .catch((err) => console.error(err))
  }
})

// mongoose.connect(config.mongodb.uri, options);

mongoWay2mnyEvents.connect(config.mongodb.Eventsuri, options)
  .then(() => console.log('connection successful to Events DB'))
  .catch((err) => console.error(err))

exports = module.exports = mongoWay2mnyEvents // expose app
