'use strict'
var MongoClient = require('mongodb').MongoClient
// var mongodb = require('mongodb')
var ObjectID = require('mongodb').ObjectID
const async = require('async')
// Mongoose.Promise = require('bluebird')
const config = require('config')
var finalArray = []
var way2mnyEventsService = module.exports
way2mnyEventsService.userEvents = function (apiReq, apiRes) {
  // const db = Mongoose.connection
  // console.log('in user events:::::::::::')
  // console.log('uri of events :: ', config.mongodb.Eventsuri)
  // console.log('in user events req body', apiReq.body)
  MongoClient.connect(config.mongodb.Eventsuri, function (err, client) {
    if (err) {
      console.log('error while connecting mongoose', err)
    } else {
      // console.log('Mongoose: events Connected', client)
      // mongoConn = db
      // console.log('in events:::::::::::::', apiReq.body)
      var db = client.db('way2money_events'.toString())
      var date = new Date().setHours(0, 0, 0, 0)
      date = new Date(date + 19800000)
      // console.log('date::::::::::::::::::::', date.getMonth())
      var month = date.getMonth() + 1
      var year = date.getFullYear()
      var dayOfMonth = date.getDate()
      var userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
      let findObj = {}
      if (apiReq.body.user_id !== undefined && apiReq.body.user_id != null) {
        findObj = {
          user_id: ObjectID(apiReq.body.user_id)
        }
      }
      // let eventsArray = []
      // console.log('collections name :: ', userDaywiseStats)
      db.collection(userDaywiseStats).find(findObj).toArray(function (error, EventResult) {
        if (error) {
          console.log('errr', error)
        } else {
          // console.log('events array normal :: ', EventResult)
          if (EventResult.length > 0) {
            apiRes.send({
              status: true,
              message: 'events details found',
              'data': EventResult
            })
          } else {
            // console.log('no events found today')
            apiRes.send({
              status: false,
              message: 'events details not found'
            })
          }
        }
      })
    }
  })
}

let fetchData = function (findObj, index, differentDays, fromDate, callback) {
  console.log('at fetch events mongo url :: ', config.mongodb.Eventsuri)
  var date
  MongoClient.connect(config.mongodb.Eventsuri, function (err, client) {
    if (err) {
      console.log('error while connecting mongoose', err)
      date = fromDate.getTime() + (24 * 3600 * 1000)
      fetchData(findObj, index + 1, differentDays, date, callback)
    } else {
      // console.log('Mongoose: events Connected')
      var db = client.db('way2money_events'.toString())
      // cb(null, db)
      var userDaywiseStats = ''
      fromDate = new Date(fromDate)
      var month = 0; var dayOfMonth = 0
      month = fromDate.getMonth() + 1
      var year = fromDate.getFullYear()
      dayOfMonth = fromDate.getDate()
      userDaywiseStats = 'user_daywise_stats_' + dayOfMonth + '_' + month + '_' + year
      // console.log('user day wise stats', userDaywiseStats, finalArray)
      db.collection(userDaywiseStats).find(findObj).toArray(function (err, EventResult) {
        if (err) {
          console.log('errr', err)
          date = fromDate.getTime() + (24 * 3600 * 1000)
          fetchData(findObj, index + 1, differentDays, date, callback)
          // callback(null, err)
        } else {
          // console.log('in else block', EventResult)
          date = fromDate.getTime() + (24 * 3600 * 1000)
          if (Object.keys(EventResult).length > 0) {
            async.forEach(EventResult, function (obj, acb) {
              finalArray.push(obj)
            })
          }
          if (index < (differentDays - 1)) {
            fetchData(findObj, index + 1, differentDays, date, callback)
          } else {
            callback(null, finalArray)
          }
        }
      })
    }
  })
}

way2mnyEventsService.getEvents = function (apiReq, apiRes) {
  finalArray = []
  let timeDifference = Math.abs(apiReq.body.fromDate - apiReq.body.toDate)
  let differentDays = Math.ceil(timeDifference / (1000 * 3600 * 24))
  // console.log('diff of days :: ', apiReq.body)
  let findObj = {}
  var fromDate = apiReq.body.fromDate + 19800000
  var index = 0
  if (apiReq.body.user_id !== undefined && apiReq.body.user_id != null) {
    findObj = {
      user_id: ObjectID(apiReq.body.user_id)
    }
  }
  fetchData(findObj, index, differentDays, fromDate, function (error, events) {
    if (error) {
      console.log(error)
      apiRes.send({ status: false, message: 'error while updating user events details' })
    } else {
      if (events.length > 0) {
        // console.log('Events :: ', events)
        apiRes.send({
          status: true,
          message: 'events details found',
          'data': events
        })
      } else {
        // console.log('final array', events)
        apiRes.send({
          status: false,
          message: 'events details not found',
          'data': []
        })
      }
    }
  })
}
