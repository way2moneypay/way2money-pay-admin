var mongoose = require('mongoose')
var Schema = mongoose.Schema

var way2MoneyDataSyncDatabaseLive = require('./../way2MoneyDataSyncDatabaseLive')

var smsCounts = new Schema({

  mid: {
    type: String
  },
  device_registration_sms_count: {
    type: Number
  },
  total_count: {
    type: Number
  },
  unidentified_sms_counts: {
    type: Number
  },
  transaction_unidentified_sms_counts: {
    type: Number
  },
  transaction_un_useful_sms_counts: {
    type: Number
  },
  monthly_processed_count: {
    type: Number
  },
  savings_account_sms_counts: {
    type: Number
  },
  loan_account_sms_counts: {
    type: Number
  },
  credit_card_sms_counts: {
    type: Number
  },
  policy_sms_counts: {
    type: Number
  },
  fixed_deposit_sms_counts: {
    type: Number
  },
  wallet_sms_counts: {
    type: Number
  },
  transaction_sms_counts: {
    type: Number
  },
  fin_profile_count: {
    type: Number,
    default: 0
  }
}, {
  versionKey: false
})

smsCounts.index({
  mid: 1
})

module.exports = way2MoneyDataSyncDatabaseLive.model('sms_counts', smsCounts)