const AppConfig = require('config')
const ResponseUtils = require('./ResponseUtils')
const JWT = require('jsonwebtoken')
var this_file_name = 'restapp/utils/AuthenticateUtils'
var AuthenticateUtils = module.exports
var GeneralUtils = require('./GeneralUtils')

AuthenticateUtils.userAuthentication = function (api_request, api_response, next_service) {
  let this_function_name = 'AuthenticateUtils.userAuthentication'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  let Response = new ResponseUtils.response(api_response)

  // check header or url parameters or post parameters for token
  var authentication_token = api_request.headers['x-access-token']
  // decode token

  if (authentication_token) {
    // verifies secret and checks exp
    JWT.verify(authentication_token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
      console.log(token_verify_error)
      console.log('TOKEN DATA IN AUTHENTICATION UTILS', token_data)
      if (token_verify_error) {
        // app.internalError('Invalid Authencation', 400, res);
        Print.error('Error while verifing user authentication token.')
        return Response.error('User athentication failed.', 'ERR')
      } else {
        // {
        //     // if everything is good, save to request for use in other routes
        //     const now = new Date().getTime();

        //     if(!token_data.a_expiry || token_data.a_expiry < now ){
        //         console.log('---------auth -------------',"verification authontication expired");
        //         const param = "Authenticaiton token expired.Please login again"
        //         // delete api_response.headers['x-access-token'];
        //         return Response.error(param,"AUTHTOKENERR",500);
        //     }else{
        // console.log(".....authontication failed");
        api_request.user = token_data
        next_service()
      }
      // }
    })
  } else {
    Print.error('Authentication token not found in request.11')
    return Response.error('User athentication failed', 'ERR')
  }
}
var checkPermissions = function (user_permissions, required_permissions) {
  if (!required_permissions || user_permissions.indexOf('all') != -1) {
    console.log('came to first conditions', true)
    return true
  }
  if (typeof required_permissions === 'string') {
    // console.log('came to second conditions', user_permissions.indexOf(required_permissions) != -1)
    return user_permissions.indexOf(required_permissions) != -1
  }
  let status = GeneralUtils.isSubArray(user_permissions, required_permissions)
  console.log('came to third conditions', status)
  // console.log();
  return status
}

AuthenticateUtils.userAuthenticationWithQuery = function (api_request, api_response, next_service) {
  let this_function_name = 'AuthenticateUtils.userAuthenticationWithQuery'
  let Print = new ResponseUtils.print(this_file_name, this_function_name)
  // let Response = new ResponseUtils.response(api_response);

  let req_body = api_request.body
  // check header or url parameters or post parameters for token
  // console.log(api_request.headers)
  var authentication_token = api_request.headers['x-access-token']
  if (authentication_token) {
    // verifies secret and checks exp
    JWT.verify(authentication_token, AppConfig.jwtsecret, function (token_verify_error, token_data) {
      if (token_verify_error) {
        // app.internalError('Invalid Authencation', 400, res);
        Print.error('Error while verifing user authentication token.')
        // return Response.error("User authentication failed.", "ERR");
        api_response.send({ staus: false, message: 'User athentication failed.' })
      } else {
        //  console.log('TOKEN DATA AT AUTHENTICATION', token_data);
        // if everything is good, save to request for use in other routes
        api_request.user = token_data
        let user_permissions = api_request.user.permissions
        let required_permissions = api_request.user_permissions
        // console.log('user_permissions :: ', user_permissions)
        // console.log('required_permissions :: ', required_permissions)
        let permission_status = checkPermissions(user_permissions, required_permissions)
        console.log('permission_status :: ', permission_status)
        if (!permission_status) {
          // return Response.error("User authentication failed.", "MISMATCH");
          api_response.send({ staus: false, message: 'User authentication failed' })
        } else {
          next_service()
        }
      }
    })
  } else {
    Print.error('Authentication token not found in request.')
    // return Response.error("User authentication failed.", "AUTHTOKENNOTFOUND", 400);
    api_response.send({ staus: false, message: 'User athentication failed' })
  }
}
