// UTILS AND SERVICES AND CONFIG

const ResponseUtils = require('./ResponseUtils')

const SMSAlertsModel = require('../models/smsAlertsModel')
const EmailAlertsModel = require('../models/EmailAlertsModel')
const EncDec = require('./EncDec')

// NODE MODULES

const config = require('config')
const MYSQL = require('mysql')
const Request = require('request')
const replaceall = require('replaceall')
const async = require('async')
var dateFormat = require('dateformat')
// INTIALIZATIONS
var NotificationUtils = module.exports
var this_file_name = 'restapp/utils/NotificationUtils'
NotificationUtils.notification = function () {

}

NotificationUtils.sms = function (templateName, configuration, callback) {
  let thisFunctionName = 'NotificationUtils.sms'
  let Print = ResponseUtils.print(this_file_name, thisFunctionName)
  // console.log('*******TEmplate Name at NotificationUtils.sms***************', template_name, configuration)
  try {
    console.log('configuration : ', configuration)
    console.log('templateName : ', templateName)
    SMSAlertsModel.findOne({
      alert_type: templateName,
      status: 'active'
    }, function (err, resp) {
      console.log('errr ', err)
      console.log('resp ', resp)
      if (err) {
        console.error('error while getting sms template with name', templateName)
        callback(err)
      } else if (resp && configuration.mobile && configuration.mobile.toString().length === 10) {
        let body = resp.alert_content
        Object.keys(configuration.replace).forEach(function (obj) {
          body = body.replace('##' + obj + '##', configuration.replace[obj])
        })

        let bodyObj = {
          'mobile': configuration.mobile,
          'sender_id': resp.sender_id,
          'alert_content': body
        }

        Request.post({
          url: config.smsURL,
          headers: {
            'Content-Type': 'application/json'
          },
          body: bodyObj,
          json: true
        }, function (error, smsResponse) {
          console.log('sms response', smsResponse.body)

          if (error) {
            console.log('Error from sms api\n', error)
            return callback(error, null)
          } else if (smsResponse) {
            let responseBody = smsResponse.body
            if (responseBody && responseBody.status_code && responseBody.status_code === 200) {
              console.log('***** \n\nSms Send successfully \n\n *****')
              return callback(null, 'success')
            } else {
              console.log('Error in sms response')
              return callback(new Error('Error in response from sms api'), null)
            }
          } else {
            console.log('No response from sms api\n')
            return callback(new Error('No response from sms api'), null)
          }
        })
        // let con = MYSQL.createConnection({
        //   host: config.smsdb.hostname,
        //   user: config.smsdb.username,
        //   password: config.smsdb.password,
        //   database: config.smsdb.database
        // })
        // con.connect(function (err) {
        //   if (err) {
        //     console.error(err)
        //   }
        //   // console.log("Connected!");
        //   let sql = 'insert into user_free_message_reg(custid,mobileno,message,senddate,sender,domain) values(0,?,?,CURRENT_TIMESTAMP,?,?)'
        //   con.query(sql, [configuration.mobile, body, resp.sender_id, resp.sender_id], function (err, result) {
        //     if (err) {
        //       console.error(err)
        //     }
        //     con.end()
        //     callback(null, 'sms sent successfully')
        //   })
        // })
      } else {
        console.log('invalid mobile number length')
        callback(new Error('invalid mobile number'), null)
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}
NotificationUtils.smssend = function (templateName, configuration, callback) {
  let thisFunctionName = 'NotificationUtils.sms'
  let Print = ResponseUtils.print(this_file_name, thisFunctionName)
  // console.log('*******TEmplate Name at NotificationUtils.sms***************', template_name, configuration)
  try {
    console.log(configuration.mobiles.length)
    SMSAlertsModel.findOne({
      alert_type: templateName,
      status: 'active'
    }, function (err, resp) {
      console.log(resp)
      if (err) {
        console.error('error while getting sms template with name', templateName)
        callback(err)
      } else if (resp && configuration.mobiles.length !== 0) {
        let body = resp.alert_content
        Object.keys(configuration.replace).forEach(function (obj) {
          body = body.replace('##' + obj + '##', configuration.replace[obj])
        })
        console.log(configuration.mobiles)
        var mobiles = configuration.mobiles
        async.forEach(mobiles, (mobile, callback) => {
          if (mobile.toString().length === 10) {
            let bodyObj = {
              'mobile': mobile,
              'sender_id': resp.sender_id,
              'alert_content': body
            }
            Request.post({
              url: config.smsURL,
              headers: {
                'Content-Type': 'application/json'
              },
              body: bodyObj,
              json: true
            }, function (error, smsResponse) {
              console.log('sms response', smsResponse.body)
  
              if (error) {
                console.log('Error from sms api\n', error)
                return callback(error, null)
              } else if (smsResponse) {
                let responseBody = smsResponse.body
                if (responseBody && responseBody.status_code && responseBody.status_code === 200) {
                  console.log('***** \n\nSms Send successfully \n\n *****')
                  return callback(null, 'success')
                } else {
                  console.log('Error in sms response')
                  return callback(new Error('Error in response from sms api'), null)
                }
              } else {
                console.log('No response from sms api\n')
                return callback(new Error('No response from sms api'), null)
              }
            })
          } else {
            console.log('invalid mobile number length')
            callback(new Error('invalid mobile number'), null)
          }
        })
        // let con = MYSQL.createConnection({
        //   host: config.smsdb.hostname,
        //   user: config.smsdb.username,
        //   password: config.smsdb.password,
        //   database: config.smsdb.database
        // })
        // con.connect(function (err) {
        //   if (err) {
        //     console.error(err)
        //   }
        //   // console.log("Connected!");
        //   let sql = 'insert into user_free_message_reg(custid,mobileno,message,senddate,sender,domain) values(0,?,?,CURRENT_TIMESTAMP,?,?)'
        //   con.query(sql, [configuration.mobile, body, resp.sender_id, resp.sender_id], function (err, result) {
        //     if (err) {
        //       console.error(err)
        //     }
        //     con.end()
        //     callback(null, 'sms sent successfully')
        //   })
        // })
      } else {
        console.log('invalid mobile number length')
        callback(new Error('invalid mobile number'), null)
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}
NotificationUtils.email = function (template_name, configuration, callback) {
  let this_function_name = 'NotificationUtils.email'
  let Print = ResponseUtils.print(this_file_name, this_function_name)
  // console.log("*******TEmplate Name at NotificationUtils.Email***************", template_name, configuration);
  try {
    EmailAlertsModel.findOne({
      alert_name: template_name,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting email template with name', template_name)
        callback(err)
      } else {
        // console.log("resp, ", resp);
        if (resp && resp.alert_route === 'api') {
          let emailmsg = {}
          let emailBody = resp.alert_content
          Object.keys(configuration.replace).forEach(function (obj) {
            emailBody = replaceall('##' + obj + '##', configuration.replace[obj].toString(), emailBody)
          })
          today = new Date()
          yesterday = new Date(today)
          yesterday.setDate(today.getDate() - 1)
          yesterday = dateFormat(yesterday, 'mmmm dS, yyyy')
          var emails = configuration.emails
          async.forEach(emails, (email, callbackForEach) => {
            emailmsg.em = email
            emailmsg.body = emailBody
            emailmsg.subj = resp.alert_subject + ' ' + yesterday.toString()
            emailmsg.fromname = 'Way2money'
            emailmsg.msgid = Date.now()
            emailmsg.fromdomain = 'way2moneymail.com'
            let encemail = EncDec.encrypt(JSON.stringify(emailmsg), 'truelittle@#$')
            encemail = replaceall('/', '<->', encemail)
            Request(resp.api_url + encemail, function (error, response, body) {
              console.log('error:', error) // Print the error if one occurred
              console.log('statusCode:', response.statusCode) // Print the response status code if a response was received
              // console.log('body:', body); // Print the HTML for the Google homepage.
              console.log('email sent successfully to  ::: ', email)
              callbackForEach()
            })
          }, (success) => {
            callback(null, 'email sent successfully')
          })
        } else {
          console.log('invalid alert route')
          console.log(resp)
          callback('invalid alert route')
        }
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}

NotificationUtils.emailsend = function (template, configuration, callback) {
  let thisFunctionName = 'NotificationUtils.emailsend'
  let Print = ResponseUtils.print(this_file_name, thisFunctionName)
  // console.log('*******TEmplate Name at NotificationUtils.Email***************', template, configuration)
  try {
    EmailAlertsModel.findOne({
      alert_name: template,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting email template with name', template)
        callback(err)
      } else {
        // console.log('Email response,==========> ', resp)
        if (resp && resp.alert_route === 'api') {
          let emailmsg = {}
          emailmsg.em = configuration.email
          let body = resp.alert_content
          Object.keys(configuration.replace).forEach(function (obj) {
            // console.log('Email Body======>', body)
            body = replaceall('##' + obj + '##', configuration.replace[obj].toString(), body)
          })
          emailmsg.body = body
          emailmsg.subj = resp.alert_subject
          emailmsg.fromname = 'Way2Money'
          emailmsg.msgid = Date.now()
          emailmsg.fromdomain = 'way2moneymail.com'
          // console.log("=============>>>", emailmsg)
          let encemail = EncDec.encrypt(JSON.stringify(emailmsg), 'truelittle@#$')

          encemail = replaceall('/', '<->', encemail)
          Request(resp.api_url + encemail, function (error, response, body) {
            console.log('error:', error) // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode) // Print the response status code if a response was received
            console.log('body:', body) // Print the HTML for the Google homepage.
            callback(null, 'email sent successfully')
          })
        } else {
          console.log('invalid alert route')
          console.log(resp)
          let callbackError = 'invalid alert route'
          callback(callbackError)
        }
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}
NotificationUtils.livePaymentEmail = function (template_name, configuration, callback) {
  let this_function_name = 'NotificationUtils.email'
  let Print = ResponseUtils.print(this_file_name, this_function_name)
  // console.log("*******TEmplate Name at NotificationUtils.Email***************", template_name, configuration);
  try {
    EmailAlertsModel.findOne({
      alert_name: template_name,
      status: 'active'
    }, function (err, resp) {
      if (err) {
        console.error('error while getting email template with name', template_name)
        callback(err)
      } else {
        // console.log("resp, ", resp);
        if (resp && resp.alert_route === 'api') {
          let emailmsg = {}
          let emailBody = resp.alert_content
          Object.keys(configuration.replace).forEach(function (obj) {
            emailBody = replaceall('##' + obj + '##', configuration.replace[obj].toString(), emailBody)
          })
          var emails = configuration.emails
          async.forEach(emails, (email, callbackForEach) => {
            emailmsg.em = email
            emailmsg.body = emailBody
            emailmsg.subj = resp.alert_subject
            emailmsg.fromname = 'Way2money'
            emailmsg.msgid = Date.now()
            emailmsg.fromdomain = 'way2moneymail.com'
            let encemail = EncDec.encrypt(JSON.stringify(emailmsg), 'truelittle@#$')
            encemail = replaceall('/', '<->', encemail)
            Request(resp.api_url + encemail, function (error, response, body) {
              console.log('error:', error) // Print the error if one occurred
              console.log('statusCode:', response.statusCode) // Print the response status code if a response was received
              // console.log('body:', body); // Print the HTML for the Google homepage.
              console.log('email sent successfully to  ::: ', email)
              callbackForEach()
            })
          }, (success) => {
            callback(null, 'email sent successfully')
          })
        } else {
          console.log('invalid alert route')
          console.log(resp)
          callback('invalid alert route')
        }
      }
    })
  } catch (error) {
    Print.exception(error)
    callback(error, null)
  }
}
