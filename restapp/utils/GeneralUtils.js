// SERVICES AND CONFIG AND UTILS

const AppConfig = require('config')
const Moment = require('moment')

// INTIALIZATIONS
var GeneralUtils = module.exports

GeneralUtils.objectValues = function (data) {
  let values = []
  let keys = Object.keys(data)
  for (var index = 0; index < keys.length; ++index) {
    // console.log(data[keys[index]])
    values.push(data[keys[index]])
  }
  return values
}

GeneralUtils.getDateFilters = function (from_date, to_date) {
  if (!from_date || !to_date) {
    return false
  };

  from_date = new Date(from_date)
  to_date = new Date(to_date)

  to_date.setDate(to_date.getDate() + 1)
  to_date = to_date.setHours(0, 0, 0, 0)
  to_date = to_date - 1
  to_date = new Date(to_date)

  let filter = {
    $gte: from_date,
    $lte: to_date
  }

  return filter
}

GeneralUtils.getDate = function (date) {
  date = date.setHours(0, 0, 0, 0)
  date = new Date(date)
  return date
}

GeneralUtils.getTimeStampDateFilter = function (from_date, to_date, date_formats, add_day) {
  if (!from_date || !to_date) {
    return null
  }

  if (!Moment(from_date, date_formats).isValid() || !Moment(to_date, date_formats).isValid()) {
    return null
  }

  console.log(from_date, to_date)
  from_date = (Moment(from_date, date_formats) / 1)
  if (add_day != false) to_date = Moment(to_date, date_formats).add(86399, 'seconds') / 1
  if (add_day == false) to_date = Moment(to_date, date_formats) / 1
  console.log('-------------------ADD DAY---------', add_day)
  return {
    $gte: from_date,
    $lte: to_date
  }
}

GeneralUtils.isSubArray = function (super_set, set) {
  if (!super_set || !set) return true
  for (var index = 0; index < set.length; ++index) { if (super_set.indexOf(set[index]) == -1) return false }
  return true
}

// console.log(GeneralUtils.getTimeStampDateFilter("2018-12-03 13:04", "2018-12-03 13:04", ["YYYY-MM-DD HH:mm"], false));

GeneralUtils.convertMillisecondsToStartOfTheDayMilliseconds = function (input_milliseconds) {
  let temporary_date = new Date(input_milliseconds)
  let converted_date = temporary_date.getFullYear() + '-' + (temporary_date.getMonth() + 1) + '-' + temporary_date.getDate()
  return new Date(converted_date).getTime()
}

GeneralUtils.getDifferenceBetweenMillisecondsInSeconds = function (previous_milliseconds, current_milliseconds) {
  return ((current_milliseconds - previous_milliseconds) / 1000)
}
// calculate estimated loan amout to give for leads based on loan type and etc.,
GeneralUtils.estimatedLoanAmount = function (loan_data) {
  console.log('loan data new', loan_data)
  if (loan_data.loan_type == 'PERSONAL') {
    // let M = (loan_data.loan_amount * 11 * (( 12 ** loan_data.loan_tenure) / (12 ** loan_data.loan_tenure) - 1));
    // let M = (loan_data.loan_amount * 11 * (( Math.pow(12, loan_data.loan_tenure)) / (Math.pow(12,loan_data.loan_tenure)) - 1));
    let M = loan_data.loan_amount * 11 * ((12 ^ loan_data.loan_tenure) / ((12 ^ loan_data.loan_tenure) - 1))
    // let M = (loan_data.loan_amount * 11 * (( 12 * loan_data.loan_tenure) / (12*loan_data.loan_tenure) - 1));
    console.log('m value ', M)
    let X = (loan_data.salary * 0.5 - loan_data.current_emi) / (M * 100000 / loan_data.loan_amount)
    console.log('x value ', X)
    return X
  } else if (loan_type == 'HOME') {

  }
}
// convert current date to specific timezone type (in terms of gmt) date and time. ex: 2018-03-02 converted as 2018-03-01T18:30:00.000Z if timezone is gmt
GeneralUtils.dateConversionFromCorrectDateToSpecificTimeZone = function (time_zone_type, input_date) { // converts correct date to utc date
  let new_date = dateFormat(input_date, 'isoUtcDateTime')
  console.log(new_date)
  new_date = new Date(new Date(new_date).getTime() + time_zone_types[time_zone_type]) // converting to indian standards
  return new_date
}

// convert current date to end of the day in specific time zone (in terms of gmt) date and time. ex: 2018-03-02 converted as 2018-03-02T18:29:59.999Z in case time zone type is gmt
GeneralUtils.dateConversionFromCorrectDateToSpecificTimeZoneEndOfTheDay = function (time_zone_type, input_date) { // it converts correct date to last minute of day in utc format
  let new_date = dateFormat(input_date, 'isoUtcDateTime')
  new_date = new Date(new Date(new_date).getTime() + time_zone_types[time_zone_type] + 86399999) // converting to indian standard time and make date to last second of the day
  return new_date
}

GeneralUtils.dateConversionFromCorrectToIst = function (input_date) { // converts correct date to utc date
  let new_date = dateFormat(input_date, 'isoUtcDateTime')
  new_date = new Date(new Date(new_date).getTime() + (11 * 30 * 60 * 1000)) // converting to indian standards
  return new_date
}

GeneralUtils.dateConversionFromCorrectToIstEndOfTheDay = function (input_date) { // it converts correct date to last minute of day in utc format
  let new_date = dateFormat(input_date, 'isoUtcDateTime')
  new_date = new Date(new Date(new_date).getTime() + (11 * 30 * 60 * 1000) + 86399999) // converting to indian standard time and make date to last second of the day
  return new_date
}

// Calculating FOIR percentage
GeneralUtils.calculateFOIR = function (salary, emi) {
  return ((emi / salary) * 100)
}

// Calculating Number years from given date
GeneralUtils.calculateYearsBetweenDate = function (date) {
  let new_date1 = moment(date, 'YYYY-MM-DD')
  let current_date = dateFormat(new Date(), 'isoUtcDateTime')
  let new_date2 = moment(current_date, 'YYYY-MM-DD')

  current_date = null

  return (new_date2.diff(new_date1, 'years'))
}

GeneralUtils.calculateProcessingFee = function (loan_amount, loan_name) {
  let newCalculatedProcessingFeeAmount = 0
  if (loan_name === 'FULLERTON') {
    /* checking loan amount to make processing fess */
    if (loan_amount < 100000) { /* <1L */
      newCalculatedProcessingFeeAmount = (loan_amount * (4 / 100))
    } else if (loan_amount < 200000 && loan_amount >= 100000) { /* 1L-2L */
      newCalculatedProcessingFeeAmount = (loan_amount * (3.5 / 100))
    } else if (loan_amount < 300000 && loan_amount >= 200000) { /* 2L-3L */
      newCalculatedProcessingFeeAmount = (loan_amount * (3.5 / 100))
    } else if (loan_amount < 500000 && loan_amount >= 300000) { /* 3L-5L */
      newCalculatedProcessingFeeAmount = (loan_amount * (3.0 / 100))
    } else if (loan_amount >= 500000) { /* >5L */
      newCalculatedProcessingFeeAmount = (loan_amount * (2.5 / 100))
    }
  }

  return newCalculatedProcessingFeeAmount
}

/* Checking post fior */
GeneralUtils.postFior = function (lead_details, partner_details) {
  /* calculating partner emi */
  let partner_emi = ConsumerLoanController2.calculateEmi(lead_details.loan_amount, lead_details.loan_tenure, partner_details.interest_rate_min)
  let updated_emi = lead_details.current_emi + partner_emi
  let new_monthly_income = lead_details.occupation == 'SALARIED' ? monthly_income : (monthly_income / 12)
  let fior_percentage = (updated_emi / new_monthly_income)
  console.log('partner_emi updated_emi new_monthly_income ', partner_emi, updated_emi, new_monthly_income)
  if (fior_percentage <= partner_details.min_borrow_requirements.fior) {
    return true
  } else {
    return false
  }
}

/* Calculates foir percentage for a lead */
GeneralUtils.calculateFoirPercentage = function (current_emi, monthly_income) {
  current_emi = current_emi || 0
  return ((current_emi * 100) / monthly_income)
}

/* Calculate emi for loan amount */
GeneralUtils.calculateEmi = function (loan_amount, loan_tenure, interest_rate_min) {
  let installments = loan_tenure < 12 ? loan_tenure : 12
  return GeneralUtils.getEmiAfterPreparingData(loan_amount, interest_rate_min, installments, loan_tenure)
}

/* getting emi from ptr formula */
GeneralUtils.getEmiAfterPreparingData = function (P, I, IN, N) {
  let R = I / IN
  R = R / 100
  let R1 = R + 1
  let part_1 = Math.pow(R1, N)
  let part_2 = Math.pow(R1, N) - 1
  let part_3 = part_1 / part_2

  return (P * (R * part_3)).toFixed(2)
}

GeneralUtils.getMonthlyIncome = function (monthly_income, occupation) {
  let new_monthly_income

  /* checking occupation type */
  if (occupation === 'SALARIED') {
    new_monthly_income = monthly_income
  } else {
    new_monthly_income = (monthly_income / 12)
  }

  return new_monthly_income
}
