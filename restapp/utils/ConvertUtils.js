'use strict'
var ConvertUtils = module.exports
var config = require('./../config/GeneralConfig')
var replaceall = require('replaceall')
var childProcess = require('child_process')
var fs = require('fs')

var covertCommandExec = function (conversion_command, directory_path, destination_path, file_name, dest_file_name, input_file_path, output_file_path, same_name, is_delete, extension, callback) {
  var child = childProcess.exec(conversion_command, function (process_error, stdout, stderr) {
    if (process_error) {
      console.error('error while changing aspect ratio')
      console.error(process_error)
      return callback('error while changing aspect ratio', null)
    };
    console.log('conversion_command :: ', conversion_command)
    let return_object = {}
    return_object.original_directory = directory_path
    return_object.converted_directory = destination_path
    return_object.original_file_name = file_name
    return_object.converted_file_name = dest_file_name
    return_object.original_path = input_file_path
    return_object.destination_path = output_file_path
    return_object.converted_extension = extension
    return_object.rename_file = destination_path + '/' + file_name + '.' + extension

    if (!is_delete) {
      if (!same_name) {
        return callback(null, return_object)
      };

      fs.rename(output_file_path, return_object.rename_file, function (renaming_error) {
        console.log('came to rename')
        if (renaming_error) {
          console.error('Error while renaming youtube image after conversion , ', renaming_error)
        };
        return callback(null, return_object)
      })
    } else {
      fs.unlink(input_file_path, (delete_error) => {
        console.log('came to directory unlink')
        if (delete_error) {
          console.error('error while deleting un converted file.')
          console.error(delete_error)
          return callback('error while deleting un converted file.', null)
        };

        if (!same_name) {
          return callback(null, return_object)
        };

        fs.rename(output_file_path, return_object.rename_file, function (renaming_error) {
          if (renaming_error) {
            console.error('Error while renaming youtube image after conversion , ', renaming_error)
          };
          return callback(null, return_object)
        })
      })
    };
  })
}

ConvertUtils.covertFile = function (directory_path, original_file_name, destination_path, dest_file_name, type, extension_check, resize, same_name, is_delete, is_destination_delete, callback) {
  let input_file_path = directory_path + '/' + original_file_name
  destination_path = destination_path || directory_path

  let extension = original_file_name.substring(original_file_name.indexOf('.') + 1, original_file_name.length)

  if (!config['conversion'][type]) {
    let allowed_types = JSON.stringify(Object.keys(config['conversion']))
    return callback('invalid file conversion type , allowed_types ' + allowed_types, null)
  };

  if (config['conversion'][type]['allowed_extensions'].indexOf(extension) == -1 && extension_check) {
    let allowed_types = JSON.stringify(config['conversion'][type]['allowed_extensions'])
    return callback('invalid file extension type , allowed_extensions ' + allowed_types, null)
  };

  if (original_file_name.indexOf('.') == -1) {
    return callback('extension missing in original file name', null)
  };

  let file_name = original_file_name.substring(0, original_file_name.indexOf('.'))
  dest_file_name = dest_file_name ? dest_file_name.substring(0, dest_file_name.indexOf('.')) : ''

  if (!dest_file_name || (dest_file_name == file_name && destination_path == directory_path)) {
    dest_file_name = 'converted_' + file_name
  };

  let output_file_path = destination_path + '/' + dest_file_name + '.' + config['conversion'][type]['final_type']
  let conversion_command = config['conversion'][type]['command']
  conversion_command = conversion_command.replace('##input_path##', input_file_path)
  conversion_command = conversion_command.replace('##output_path##', output_file_path)
  if (resize) {
    conversion_command = conversion_command.replace('##resize##', ' -resize ' + resize + '!')
  } else {
    conversion_command = conversion_command.replace('##resize##', '')
  }

  if (!is_destination_delete) {
    covertCommandExec(conversion_command, directory_path, destination_path, file_name, dest_file_name, input_file_path, output_file_path, same_name, is_delete, config['conversion'][type]['final_type'], function (execution_error, execution_success) {
      return callback(execution_error, execution_success)
    })
  } else {
    if (fs.existsSync(output_file_path)) {
      fs.unlink(output_file_path, (output_delete_error) => {
        console.log('came to destination unlink')
        if (output_delete_error) {
          return callback('error while deleting output_file ', null)
        }
        covertCommandExec(conversion_command, directory_path, destination_path, file_name, dest_file_name, input_file_path, output_file_path, same_name, is_delete, config['conversion'][type]['final_type'], function (execution_error, execution_success) {
          return callback(execution_error, execution_success)
        })
      })
    } else {
      covertCommandExec(conversion_command, directory_path, destination_path, file_name, dest_file_name, input_file_path, output_file_path, same_name, is_delete, config['conversion'][type]['final_type'], function (execution_error, execution_success) {
        return callback(execution_error, execution_success)
      })
    };
  }
}
