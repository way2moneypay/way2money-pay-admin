const mongoose = require('mongoose')

const Schema = mongoose.Schema

var sales_team_target = new Schema({
    targeted_to:{
	  type: Schema.Types.ObjectId
   },
  targeted_by:{
	type: Schema.Types.ObjectId
  },
  target_date_from: {
     type: Number
  },
  target_date_to: {
     type: Number
  },
  target_number: {
    type: Number
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('sales_team_target', sales_team_target)
