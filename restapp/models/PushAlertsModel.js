const mongoose = require('mongoose')

const Schema = mongoose.Schema

var pushAlerts = new Schema({
  status: {
    type: String
  },
  alert_name: {
    type: String
  },
  alert_content: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('push_alerts', pushAlerts)
