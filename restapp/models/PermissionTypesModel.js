const mongoose = require('mongoose')

const Schema = mongoose.Schema

var permission_types = new Schema({
  permission_type: {
    type: String
  },
  permission_title: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('permission_types', permission_types)
