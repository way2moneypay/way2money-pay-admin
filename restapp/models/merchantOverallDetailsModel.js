const mongoose = require('mongoose')

const Schema = mongoose.Schema

var merchantOverallDetails = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  trans_amount: {
    type: Number
  },
  npa_percentage: {
    type: Number
  },
  paid_amount: {
    type: Number
  },
  transactions: {
    type: Number
  },
  payments: {
    type: Number
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('merchant_overall_details', merchantOverallDetails)
