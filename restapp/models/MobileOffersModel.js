var mongoose = require('mongoose')

var Schema = mongoose.Schema
var offersMobile = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  brand: {
    type: String
  },
  brand_name: {
    type: String
  },
  merchant_logo: {
    type: String
  },
  address: {
    type: String
  },
  offer_details: {
    type: String
  },
  expiry_date_from: {
    type: Number
  },
  expiry_date_to: {
    type: Number
  },
  offers_status: {
    type: String,
    enum: ['active', 'inactive'],
    default: 'inactive'
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('offers_mobile', offersMobile)
