const mongoose = require('mongoose')

const Schema = mongoose.Schema

var admin_users = new Schema({
  permission_type: {
    type: String
  },
  permission_title: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('admin_users', admin_users)
