var mongoose = require('mongoose')

var Schema = mongoose.Schema
var npaDetails = new Schema({
  trans_amount: {
    type: Number
  },
  paid_amount: {
    type: Number
  },
  from_date: {
    type: Number
  },
  to_date: {
    type: Number
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('npa_details', npaDetails)
