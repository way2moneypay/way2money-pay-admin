var mongoose = require('mongoose')

var Schema = mongoose.Schema
var banks = new Schema({
  bank: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('banks', banks)
