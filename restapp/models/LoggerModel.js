var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var logger = new Schema({
  info: {
    type: String
  },
  type: {
    type: String
  },
  data: {
    type: Object
  },
  date: {
    type: Number,
    default: Date.now
  }
}, {
  versionKey: false
});


module.exports = mongoose.model('logger', logger);
