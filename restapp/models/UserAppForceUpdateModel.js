var mongoose = require('mongoose')

var Schema = mongoose.Schema
var user_app_force_update_model = new Schema({
  version: {
    type: String
  },
  force_update: {
    type: Boolean,
    default: false
  },

  mobile_type: {
    type: String,
    default: 'android',
    enum: [ 'android', 'ios' ]
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('user_app_force_updates', user_app_force_update_model)
