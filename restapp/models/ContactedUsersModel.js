var mongoose = require('mongoose')

var Schema = mongoose.Schema
var contactedUser = new Schema({
    user_id: {
        type: Schema.Types.ObjectId
    },
    contacted_by_id: {
        type: Schema.Types.ObjectId
    },
    contacted_on: {
        type: Number,
        default: Date.now
    },
    response: {
        type: String
    }
}, {
    versionKey: false
})

module.exports = mongoose.model('contacted_users', contactedUser)