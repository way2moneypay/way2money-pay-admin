var mongoose = require('mongoose')

var Schema = mongoose.Schema
bcrypt = require('bcrypt-nodejs')
var userCreditAmounts = new Schema({
  brand: {
    type: String,
    unique: true
  },
  creditLimit: {
    type: Number
  },
  osName: {
    type: String,
    enum: ['android', 'ios']
  },
  status: {
    type: String,
    enum: ['active', 'inactive']
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('userCreditAmounts', userCreditAmounts)