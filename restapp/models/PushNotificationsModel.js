var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var push_notifications = new Schema({
  user_id: {
    type: Schema.Types.ObjectId
  },
  pn_id: {
    type: String,
    required: true
  },
  os: {
    type: String,
    enum: ["android", "ios"]
  },
  version: {
    type: String
  },
  content: {
    type: String,
    required: true
  }

}, {
  versionKey: false
});


module.exports = mongoose.model('push_notifications', push_notifications, 'push_notifications');
