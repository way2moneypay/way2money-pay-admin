'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt-nodejs')
const validator = require('mongoose-unique-validator')
const Float = require('mongoose-float').loadType(mongoose)
var users = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String
  },
  mobile: {
    type: String
  },
  user_status: {
    type: Number,
    default: 1
  },
  permissions: {
    type: Object
  },
  user_type: {
    type: String
  },
  temp_password: {
    type: String
  },
  parent_id: {
    type: Schema.Types.ObjectId
  },
  created_date: {
    type: Number,
    default: Date.now
  },
}, {
  versionKey: false
})

users.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
}

users.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password)
}
module.exports = mongoose.model('admin_users', users)
