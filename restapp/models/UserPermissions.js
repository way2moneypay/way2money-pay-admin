var mongoose = require('mongoose')

var Schema = mongoose.Schema

var validator = require('mongoose-unique-validator')

var Float = require('mongoose-float').loadType(mongoose)
var userpermissions = new Schema({
  permission_name: {
    type: String
  },
  permission_details_id: {
    type: Schema.Types.ObjectId
  },
  user_types: {
    type: Object
  },
  permission_description: {
    type: String
  },
  hidden_status: {
    type: Number,
    enum: [0, 1],
    default: 0
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('userpermissions', userpermissions)
