'use strict'
const mongoose = require('mongoose');
const mongoosastic = require('mongoosastic')

const config = require('config')

const Schema = mongoose.Schema

var merchant = new Schema({
  mobile: {
    type: String,
    unique: true
  },
  old_mobiles: {
    type: Array
  },
  name: {
    type: String
  },
  email: {
    type: String
  },
  company: {
    type: String
  },
  bank_name: {
    type: String
  },
  branch: {
    type: String
  },
  account_no: {
    type: String
  },
  name_on_bank: {
    type: String
  },
  ifsc_code: {
    type: String
  },
  pan_number: {
    type: String
  },
  pan_name: {
    type: String
  },
  tin: {
    type: String
  },
  tid: {
    type: String
  },
  logo_url: {
    type: String
  },
  check_leaf: {
    type: String
  },
  pan_card: {
    type: String
  },
  token_validity: {
    type: Number,
    default: 15
  },
  merchant_status: {
    type: Number,
    default: 0
  },
  display_status: {
    type: String
  },
  bank_details: {
    type: Number,
    default: 0,
    enum: [0, 1, 2]
  },
  pan_details: {
    type: Number,
    default: 0,
    enum: [0, 1, 2, 3]
  },
  register_date: {
    type: Number
  },
  updated_on: {
    type: Number
  },
  register_by: {
    type: Schema.Types.ObjectId
  },
  pending_amt: {
    type: Number
  },
  approved_amt: {
    type: Number
  },
  claimed_amt: {
    type: Number
  },
  secret_key: {
    type: String
  },
  web_access_token: {
    type: String
  },
  web_secret_key: {
    type: String
  },
  web_domains: {
    type: Array
  },
  brand_name: {
    type: String,
    es_indexed: true
  },
  merchant_category: {
    type: String
  },
  password: {
    type: String
  },
  is_profile_verified: {
    type: Boolean,
    default: false
  },
  is_admin_verified: {
    type: Boolean,
    default: false
  },
  qrcode_path: {
    type: String
  },
  pay_cycle_mode: {
    type: Object
  },
  sub_merchant_mobile: {
    type: Array
  },
  open_time: {
    type: String
  },
  close_time: {
    type: String
  },
  location: {
    type: Object
  },
  company_address: {
    type: Object
  },
  formatted_address: {
    type: String
  },
  previous_cycle_paid_refund_amount: {
    type: Number,
    default: 0
  },
  created_by: {
    type: Schema.Types.ObjectId
  },
  created_by_parent: {
    type: Schema.Types.ObjectId
  },
  assigned_status: {
    type: Boolean,
    default: true
  },
  onboard_date: {
    type: Number
  }
}, {
  versionKey: false
})

merchant.index({
  brand_name: 'text'
})
merchant.plugin(mongoosastic, {
  hosts: [
    config.elastic.url
  ]
})
module.exports = mongoose.model('merchant', merchant, 'merchants')