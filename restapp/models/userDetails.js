var mongoose = require('mongoose')
var Schema = mongoose.Schema
var bcrypt = require('bcrypt-nodejs')
var userDetails = new Schema({
  name: {
    type: String
  },
  mobile_number: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String
  },
  register_date: {
    type: Number,
    required: true,
    default: Date.now
  },
  register_merchant: {
    type: Schema.Types.ObjectId
  },
  password: {
    type: String
  },
  credit_available: {
    type: Number
  },
  credit_due: {
    type: Number,
    default: 0
  },
  credit_limit: {
    type: Number
  },
  user_status: {
    type: Number,
    default: 1
  },
  con_fee: {
    type: Number,
    default: 3.0
  },
  mobile_pin: {
    type: Number
  },
  due_date: {
    type: Number,
    default: 0
  },
  is_mobile_verified: {
    type: Number,
    required: true,
    default: false
  },
  is_email_verified: {
    type: Number,
    required: true,
    default: false
  },
  user_fields_status: {
    enum: ['complete', 'incomplete'],
    type: String
  },
  is_temp_password: {
    type: Boolean,
    default: false
  },
  temp_password: {
    type: String
  },
  email_verification_count: {
    type: Number,
    default: 0
  },
  email_last_hit_date: {
    type: Date
  },
  pn_id: {
    type: String
  },
  mobile_token_id: {
    type: String
  },
  login_status: {
    type: String,
    enum: ['login', 'logout'],
    default: 'login'
  },
  pn_id_source: {
    type: String
  },
  os: {
    type: String
  },
  notification_status: {
    type: String,
    enum: ['active', 'inactive']
  },
  version: {
    type: String
  },
  installed_status: {
    type: String,
    enum: ['installed', 'uninstalled']
  },
  app_version: {
    type: String
  },
  mid: {
    type: String
  },
  mail_details_sent: {
    type: Boolean
  },
  duplicate_account: {
    type: Boolean
  },
  is_predict_value: {
    type: Boolean
  },
  predicted_date: {
    type: Number
  },
  sdk_con_fee: {
    type: Number
  },
  web_con_fee: {
    type: Number
  },
  last_re_predicted_check_date: {
    type: Number
  },
  last_contacted_date: {
    type: Number
  },
  os_version: {
    type: String
  }
}, {
  versionKey: false
})

userDetails.methods.generateHash = function (password) {
  console.log('GENERATING HASH', password)
  let hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8))
  console.log('AFTER GENERATING HASH', password, hashPassword)
  return hashPassword
}

userDetails.methods.validPassword = function (password) {
  console.log('PASSWORD CHECK', password, this.password)
  let validate = bcrypt.compareSync(password, this.password)
  console.log('PASSWORD CHECK AT VALIDATE FUNCTION', validate)
  return validate
}

userDetails.index({
  email: 1,
  mobile_number: 1
}, {
  name: 'user_details_index1',
  background: true
})

module.exports = mongoose.model('user_details', userDetails)