var mongoose = require('mongoose')

var Schema = mongoose.Schema
var bank_details = new Schema({
  bank: {
    type: String
  },
  bank_id: {
    type: Schema.Types.ObjectId
  },
  ifsc: {
    type: String
  },
  branch: {
    type: String
  },
  city: {
    type: String
  },
  district: {
    type: String
  },
  state: {
    type: String
  },
  city_id: {
    type: Schema.Types.ObjectId
  },
  state_id: {
    type: Schema.Types.ObjectId
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('bank_details', bank_details)
