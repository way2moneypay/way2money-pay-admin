'use strict'
const mongoose = require('mongoose')

const Schema = mongoose.Schema
var emailAlerts = new Schema({
  alert_name: {
    type: String,
    required: true
  },
  alert_route: {
    type: String,
    enum: ['api', 'smtp'],
    required: true
  },
  alert_subject: {
    type: String,
    required: true
  },
  alert_content: {
    type: String,
    required: true
  },
  smtp_host: {
    type: String
  },
  smtp_port: {
    type: String
  },
  smtp_username: {
    type: String
  },
  smtp_password: {
    type: String
  },
  api_url: {
    type: String
  },
  status: {
    type: String,
    enum: ['active', 'inactive'],
    default: 'active'
  },
  create_date: {
    type: Number,
    default: Date.now
  }

}, {
  versionKey: false
})

module.exports = mongoose.model('email_alerts', emailAlerts)
