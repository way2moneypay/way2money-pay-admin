const mongoose = require('mongoose')

const Schema = mongoose.Schema

var daywise_transactions = new Schema({

  transactions: {
    type: Number
  },
  trans_amount: {
    type: Number
  },
  trans_date: {
    type: Number,
    unique: true
  },
  amount: {
    type: Number
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('daywise_transactions', daywise_transactions)
