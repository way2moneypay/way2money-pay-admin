const mongoose = require('mongoose')

const Schema = mongoose.Schema

var RefundTransactionsModel = new Schema({
  transaction_id: {
    type: Schema.Types.ObjectId
  },
  amount: {
    type: Number
  },
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  user_id: {
    type: Schema.Types.ObjectId
  },
  trans_amount: {
    type: Number
  },
  refunded_date: {
    type: Number
  },
  pending_amt: {
    type: Number
  },
  credit_available: {
    type: Number
  },
  credit_due: {
    type: Number
  },
  refund_by: {
    type: String,
    default: 'Merchant'
  },
  merchant_commission: {
    type: Number
  },
  con_fee: {
    type: Number
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('refund_transactions', RefundTransactionsModel)
