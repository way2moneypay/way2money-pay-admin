var mongoose = require('mongoose')

var Schema = mongoose.Schema
var smscounts = new Schema({
  sent_count: {
    type: Number,
    default: 0
  },
  created_at: {
    type: Number
  },
}, {
  versionKey: false
})

module.exports = mongoose.model('way2money_sms_counts', smscounts)
