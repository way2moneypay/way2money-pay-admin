'use strict'
const mongoose = require('mongoose')

const Schema = mongoose.Schema

var MerchantClaimsModel = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  amount: {
    type: Number
  },
  request_date: {
    type: Number
  },
  claim_status: {
    type: Number
  },
  ref_id: {
    type: String
  },
  accepted_by: {
    type: String
  },
  accepted_date: {
    type: Number
  },
  brand_name: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('merchant_claims', MerchantClaimsModel)
