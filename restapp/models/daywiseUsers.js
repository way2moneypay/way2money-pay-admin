const mongoose = require('mongoose')

const Schema = mongoose.Schema

var daywiseUsers = new Schema({

  users: {
    type: Number
  },
  register_date: {
    type: Number,
    unique: true
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('daywise_users', daywiseUsers)
