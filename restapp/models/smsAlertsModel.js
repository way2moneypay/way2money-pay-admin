'use strict'
const mongoose = require('mongoose')

const Schema = mongoose.Schema

var smsAlerts = new Schema({
  status: {
    type: String
  },
  alert_type: {
    type: String
  },
  alert_content: {
    type: String
  },
  sender_id: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('sms_alerts', smsAlerts)
