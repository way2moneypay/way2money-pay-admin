var mongoose = require('mongoose')

var Schema = mongoose.Schema
var predictedAmounts = new Schema({
  user_id: {
    type: Schema.Types.ObjectId
  },
  mid: {
    type: String
  },
  old_amount: {
    type: String
  },
  new_amount: {
    type: String
  },
  is_amount_change: {
    type: Boolean
  },
  updated_date: {
    type: Number
  },
  status: {
    type: String,
    enum: ['success', 'error', 'failed']
  },
  message: {
    type: String
  },
  spendings: {
    type: Number
  },
  earnings: {
    type: Number
  },
  main_cluster: {
    type: String
  },
  sub_cluster: {
    type: String
  },
  salary_account: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('predicted_amounts', predictedAmounts)
