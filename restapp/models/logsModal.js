'use strict'
const mongoose = require('mongoose')

const Schema = mongoose.Schema

var logs = new Schema({
  date: {
    type: Number
  },
  message: {
    type: String
  },
  type: {
    type: String
  },
  code: {
    type: String
  },
  error_data: {
    type: String
  },
  file_name: {
    type: String
  },
  function_name: {
    type: String
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('logs', logs)
