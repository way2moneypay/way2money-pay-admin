const mongoose = require('mongoose')

const Schema = mongoose.Schema

var daywise_payments = new Schema({

  payments: {
    type: Number
  },
  amount_paid: {
    type: Number
  },
  paid_date: {
    type: Number,
    unique: true
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('daywise_payments', daywise_payments)
