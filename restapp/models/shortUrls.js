var mongoose = require('mongoose')
const W2fDomainDB = require('../../W2fDomainDB')
const Schema = mongoose.Schema
var shortUrls = new mongoose.Schema({
  short_id: {
    type: String,
    unique: true
  },
  user_id: {
    type: Schema.Types.ObjectId
  },
  long_url: {
    type: String
  },
  short_url: {
    type: String
  },
  sent_count: {
    type: Number,
    default: 1
  },
  clicks: {
    type: Number,
    default: 0
  },
  created_at: {
    type: Number,
    default: Date.now()
  },
  last_sent: {
    type: Number
  },
  name: {
    type: String
  },
  mobile_number: {
    type: String,
    unique: true
  },
  email: {
    type: String
  }
}, {
    versionKey: false
  })

module.exports = W2fDomainDB.model('payment_short_urls', shortUrls)
