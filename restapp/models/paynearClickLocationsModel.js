'use strict'
const mongoose = require('mongoose')

const Schema = mongoose.Schema

var paynearLocations = new Schema({
  user_id: {
    type: Schema.Types.ObjectId
  },
  name: {
    type: String
  },
  email: {
    type: String
  },
  mobile_number: {
    type: String
  },
  coordinates: {
    type: Array
  },
  saved_type: {
    type: String
  },
  clicked_on: {
    type: Number
  },
  formatted_address: {
    type: String
  },
  company_address: {
    type: Object
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('pay_near_click_locations', paynearLocations)
