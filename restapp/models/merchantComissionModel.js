'use strict'
const mongoose = require('mongoose')

const Schema = mongoose.Schema

var merchant_comissions = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  commission_rate: {
    type: Number
  },
  rate_by: {
    type: Schema.Types.ObjectId
  },
  min_amount: {
    type: Number,
    default: 1
  },
  max_amount: {
    type: Number,
    default: 10000000
  },
  status: {
    type: Number
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('merchant_commissions', merchant_comissions)
