const mongoose = require('mongoose')

const Schema = mongoose.Schema

var merchantDaywiseDetails = new Schema({
  merchant_id: {
    type: Schema.Types.ObjectId
  },
  transactions: {
    type: Number
  },
  trans_amount: {
    type: Number
  },
  trans_date: {
    type: Number
  },
  amount: {
    type: Number
  },
  users: {
    type: Number
  },
  payments: {
    type: Number
  },
  paid_amount: {
    type: Number
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('merchant_daywise_details', merchantDaywiseDetails)
