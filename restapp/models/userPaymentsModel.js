var mongoose = require('mongoose')

var Schema = mongoose.Schema
var user_payments = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  mobile_number: {
    type: String
  },
  payment_gateway_id: {
    type: Schema.Types.ObjectId
  },
  payment_ref_id: {
    type: String
  },
  payment_id: {
    type: String
  },
  transaction_ref_id: {
    type: String
  },
  payment_initiated_date: {
    type: Number,
    default: Date.now
  },
  paid_date: {
    type: Number
  },
  amount_paid: {
    type: Number,
    required: true
  },
  paid_status: {
    type: Number,
    default: 0
  },
  payment_mode: {
    type: String
  },
  bank_name: {
    type: String
  },
  payment_remarks: {
    type: Object
  },
  payment_type: {
    type: String
  },
  sub_payment_type: {
    type: String
  },
  trans_details: {
    type: Object,
    default: []
  },
  gateway_response: {
    type: String
  },
  gateway_response_status: {
    type: String,
    default: 'pending',
    enum: ['pending', 'success']
  },
  source: {
    type: String,
    enum: ['ios', 'web', 'android'],
    default: 'ios'
  },
  payment_currency: {
    type: String
  },
  capture_status: {
    type: String
  },
  payment_method: {
    type: String
  },
  payment_card_id: {
    type: String
  },
  payment_card: {
    type: Object
  },
  payment_bank: {
    type: String
  },
  payment_bank: {
    type: String
  },
  payment_wallet: {
    type: String
  },
  payment_bank_name: {
    type: String
  },
  user_email: {
    type: String
  },
  payment_created_at: {
    type: String
  },
  capture_date: {
    type: Number
  },
  payment_captured: {
    type: String
  },
  payment_description: {
    type: String
  },
  payment_entity: {
    type: String
  },
  payment_fee: {
    type: String
  },
  payment_tax: {
    type: String
  },
  payment_error_code: {
    type: String
  },
  payment_error_description: {
    type: String
  },
  gateway_source: {
    type: String,
    default: 'InstaMojo',
    enum: ['InstaMojo', 'RazorPay']
  },
  payout: {
    type: Object
  }
}, {
  versionKey: false
})

module.exports = mongoose.model('user_payments', user_payments)

// MODEL DOCUMENTATION

// paid_status {
//   0 - initiate
//   1 - success
//   2 - fail
//   3 - refund
// }
