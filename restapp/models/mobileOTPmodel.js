var mongoose = require('mongoose')

var Schema = mongoose.Schema
var mobile_otps = new Schema({
  otp_count: {
    type: Number
  },
  otp_verified: {
    type: Boolean
  },
  otp_date: {
    type: Number
  },
  mobile_number: {
    type: String
  },
  otp: {
    type: Number
  }

}, {
  versionKey: false
})

module.exports = mongoose.model('mobile_otps', mobile_otps)
