var mongoose = require('mongoose')

var Schema = mongoose.Schema
var user_devices = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: true
  },
  os_version: {
    type: String
  },
  os_api_level: {
    type: String
  },
  ggl_adv_id: {
    type: String
  },
  internal_storage: {
    type: String
  },
  external_storage: {
    type: String
  },
  time_zone: {
    type: String
  },
  ram: {
    type: String
  },
  model: {
    type: String
  },
  brand: {
    type: String
  },
  screen: {
    type: String
  },
  network_type: {
    type: String
  },
  os_name: {
    type: String
  },
  mid: {
    type: String
  },
  sid: {
    type: String
  },
  emails: {
    type: Object
  },
  latitude: {
    type: String
  },
  longitude: {
    type: String
  }

}, {
  versionKey: false
})

module.exports = mongoose.model('user_devices', user_devices)
