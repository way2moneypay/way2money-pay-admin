'use strict'

const Mongoose = require('mongoose').Mongoose
Mongoose.Promise = require('bluebird')

var config = require('config')

const options = {
  auto_reconnect: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 1000, // Maintain up to 1000 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  keepAlive: 120,
  promiseLibrary: require('bluebird'),
  useNewUrlParser: true
}

const way2MoneyDataSyncDbLive = new Mongoose()

way2MoneyDataSyncDbLive.connect(config.live_sms_process_db.uri, options)
  .then(() => console.log('original Mongo connection successful'))
  .catch((err) => console.error(err))

way2MoneyDataSyncDbLive.set('debug', false)

module.exports = way2MoneyDataSyncDbLive
