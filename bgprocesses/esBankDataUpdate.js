'use strict'
var MongoClient = require('mongodb').MongoClient
var config = require('config')
var sleep = require('system-sleep')
var cron = require('node-cron')
var request = require('request')
var db

MongoClient.connect(config.mongodb.uri, function (err, database) {
  if (err) {
    console.error('error while connecting mongo db')
    console.error(err)
  } else {
    console.log('mongo connection successful')
    db = database
    request.delete('http://172.16.20.43:9200/pay_banks', function (err, httpResponse, body) {
      console.log(body)
      request.put({
        url: 'http://172.16.20.43:9200/pay_banks',
        headers: {
          'content-type': 'application/json'
        },
        json: {
          'settings': {
            'number_of_shards': 1,
            'analysis': {
              'filter': {
                'autocomplete_filter': {
                  'type': 'edge_ngram',
                  'min_gram': 3,
                  'max_gram': 20
                }
              },
              'analyzer': {
                'autocomplete': {
                  'type': 'custom',
                  'tokenizer': 'standard',
                  'filter': [
                    'lowercase',
                    'autocomplete_filter'
                  ]
                }
              }
            }
          },
          'mappings': {
            'bank': {
              'properties': {
                'bank_name': {
                  'type': 'text',
                  'analyzer': 'autocomplete'
                  // ,
                  // "search_analyzer": "standard"
                },
                'bank_id': {
                  'enabled': false
                }
              }
            }
          }
        }
      }, function (err, httpResponse, body) {
        console.log(body)
        updateDate(0)
      })
    })
  }
})

function updateDate (skip) {
  console.log('updating emails data')
  console.log('******************************************')
  console.log('skip', skip)
  console.log('******************************************')

  if (db) {
    db.collection('banks').find({}).skip(skip).limit(10000).toArray(function (err, result) {
      // console.log(result);
      console.log('found users length', result.length)
      if (result && result.length > 0) {
        dataUpdate(result, 0, skip)
      } else {
        db.close()
      }
    })
  } else {
    console.log('No mongo connection available')
    console.log('sleeping for 1 seconds')
    sleep(1000)
    updateDate()
  }
}

function dataUpdate (arr, start, skip) {
  let body = ''
  arr.forEach(function (resp, index) {
    body += '{ "index" : { "_index" : "pay_banks", "_type" : "bank", "_id" : "' + resp._id + '" } }\n'
    body += '{"bank_name":"' + resp.bank.toUpperCase() + '","bank_id":"' + resp._id + '"}\n'
  })
  console.log(body)
  request.post({
    url: 'http://172.16.20.43:9200/_bulk',
    headers: {
      'content-type': 'application/json'
    },
    body: body
  }, function (err, httpResponse, body) {
    console.log(body)
    // start += 1;
    // if (start === arr.length) {
    updateDate(skip + arr.length)
    // }
  })
}
