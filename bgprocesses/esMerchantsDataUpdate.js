'use strict'
var MongoClient = require('mongodb').MongoClient
var config = require('config')
var sleep = require('system-sleep')
var cron = require('node-cron')
var request = require('request')
var db
var client
MongoClient.connect(config.mongodb.uri, function (err, database) {
  if (err) {
    console.error('error while connecting mongo db')
    console.error(err)
  } else {
    console.log('mongo connection successful')
    client = database
    db = database.db('way2pay')
    request.delete(config.elastic.url + 'merchants', function (err, httpResponse, body) {
      console.log(body)
      request.put({
        url: config.elastic.url + 'merchants',
        headers: {
          'content-type': 'application/json'
        },
        json: {
          'settings': {
            'number_of_shards': 1,
            'analysis': {
              'tokenizer': {
                'my_ngrams': {
                  'type': 'ngram',
                  'min_gram': 3,
                  'max_gram': 4
                }
              },
              'filter': {
              },
              'analyzer': {
                'autocomplete': {
                  'type': 'custom',
                  'tokenizer': 'my_ngrams',
                  'filter': [
                    'lowercase'
                  ]
                }
              }
            }
          },
          'mappings': {
            'merchant': {
              'properties': {
                'brand_name': {
                  'type': 'text',
                  'analyzer': 'autocomplete'
                }
              }
            }
          }
        }
      }, function (err, httpResponse, body) {
        console.log(body)
        updateDate(0)
      })
    })
  }
})

function updateDate (skip) {
  console.log('updating merchants data')
  console.log('******************************************')
  console.log('skip', skip)
  console.log('******************************************')
  if (db) {
    db.collection('merchants').find({}).skip(skip).limit(10000).toArray(function (err, result) {
      // console.log(result);
      console.log('found merchants length', result.length)
      if (result && result.length > 0) {
        dataUpdate(result, 0, skip)
      } else {
        client.close()
      }
    })
  } else {
    console.log('No mongo connection available')
    console.log('sleeping for 1 seconds')
    sleep(1000)
    updateDate()
  }
}

function dataUpdate (arr, start, skip) {
  let body = ''
  arr.forEach(function (resp, index) {
    if (resp.brand_name) {
      body += '{ "index" : { "_index" : "merchants", "_type" : "merchant", "_id" : "' + resp._id + '" } }\n'
      body += '{"brand_name":"' + resp.brand_name.toUpperCase() + '"}\n'
    }
  })
  console.log(body)
  request.post({
    url: config.elastic.url + '/_bulk',
    headers: {
      'content-type': 'application/json'
    },
    body: body
  }, function (err, httpResponse, body) {
    console.log(body)
    updateDate(skip + arr.length)
  })
}
