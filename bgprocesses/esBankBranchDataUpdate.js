'use strict'
var MongoClient = require('mongodb').MongoClient
var config = require('config')
var sleep = require('system-sleep')
var cron = require('node-cron')
var request = require('request')
var db

MongoClient.connect(config.mongodb.uri, function (err, database) {
  if (err) {
    console.error('error while connecting mongo db')
    console.error(err)
  } else {
    console.log('mongo connection successful')
    db = database
    request.delete('http://172.16.20.5:9200/pay_bank_branches', function (err, httpResponse, body) {
      console.log(body)
      request.put({
        url: 'http://172.16.20.5:9200/pay_bank_branches',
        headers: {
          'content-type': 'application/json'
        },
        json: {
          'settings': {
            'number_of_shards': 1,
            'analysis': {
              'tokenizer': {
                'my_ngrams': {
                  'type': 'ngram',
                  'min_gram': 3,
                  'max_gram': 4,
                  'token_chars': [
                    'letter',
                    'digit',
                    'whitespace',
                    'punctuation',
                    'symbol'
                  ]
                }
              },
              'analyzer': {
                'autocomplete': {
                  'type': 'custom',
                  'tokenizer': 'my_ngrams',
                  'filter': [
                    'lowercase'
                  ]
                }
              }
            }
          },
          'mappings': {
            'bank_branch': {
              'properties': {
                'branch_name': {
                  'type': 'text',
                  'analyzer': 'autocomplete'
                  // ,
                  // "search_analyzer": "standard"
                },
                'bank_id': {
                  'type': 'text'
                },
                'bank_name': {
                  'enabled': false
                },
                'ifsc_code': {
                  'type': 'text',
                  'analyzer': 'autocomplete'
                  // ,
                  // "search_analyzer": "standard"
                },
                'city_name': {
                  'enabled': false
                },
                'district_name': {
                  'enabled': false
                },
                'state_name': {
                  'enabled': false
                },
                'city_id': {
                  'enabled': false
                },
                'state_id': {
                  'enabled': false
                }
              }
            }
          }
        }
      }, function (err, httpResponse, body) {
        console.log(body)
        updateDate(0)
      })
    })
  }
})

function updateDate (skip) {
  console.log('updating emails data')
  console.log('******************************************')
  console.log('skip', skip)
  console.log('******************************************')

  if (db) {
    db.collection('bank_details').find({}).skip(skip).limit(10000).toArray(function (err, result) {
      // console.log(result);
      console.log('found users length', result.length)
      if (result && result.length > 0) {
        dataUpdate(result, 0, skip)
      } else {
        db.close()
      }
    })
  } else {
    console.log('No mongo connection available')
    console.log('sleeping for 1 seconds')
    sleep(1000)
    updateDate()
  }
}

function dataUpdate (arr, start, skip) {
  let body = ''
  arr.forEach(function (resp, index) {
    let bank = {
      branch_name: resp.branch,
      bank_id: resp.bank_id,
      ifsc_code: resp.ifsc,
      bank_name: resp.bank,
      city_name: resp.city,
      district_name: resp.district,
      state_name: resp.state,
      city_id: resp.city_id,
      state_id: resp.state_id,
      branch_id: resp._id
    }
    body += '{ "index" : { "_index" : "pay_bank_branches", "_type" : "bank_branch", "_id" : "' + resp._id + '" } }\n'
    body += JSON.stringify(bank) + '\n'
  })
  console.log(body)
  request.post({
    url: 'http://172.16.20.5:9200/_bulk',
    headers: {
      'content-type': 'application/json'
    },
    body: body
  }, function (err, httpResponse, body) {
    console.log(body)
    // start += 1;
    // if (start === arr.length) {
    updateDate(skip + arr.length)
    // }
  })
}
